����  �����g���g���g���g���g���g���g���g******************************************************************************
*									     *
*		       Professioneller IFF-Converter			     *
*									     *
* Coding    :  Crazy Copper/DFT						     *
* Desing&GFX:  M.U.D.U./DFT						     *
*									     *
* Begin Work: 13.06.1993						     *
* Final Work: xx.xx.xxxx						     *
*									     *
******************************************************************************

		incdir	include:macros/
;		include	hardware.s
		include	hardware1.s
;		include	vektor.s

		incdir include:
		include	intuition/intuition.i
		include intuition/intuition_lib.i
		include	exec/exec_lib.i
		include libraries/dos_lib.i
		include	graphics/graphics_lib.i

FileRequest	= -84			;Req.Library
textrequest	= -174

begin:

		bsr	clearbss
		bsr	openlibs
		bsr	initreq
		bsr	openscreen
		bsr	initoberflaeche
		bra	waitaction

;----------------------------------------------------------------------------
;--- Clear BSS --------------------------------------------------------------
;----------------------------------------------------------------------------
clearbss
		lea	daten,a5
		move.l	a5,a0
		move.w	#dataslen-1,d0
.clr		clr.b	(a0)+		;l�sche Bss Section
		dbra	d0,.clr
		rts

;----------------------------------------------------------------------------
;--- Open Libs --------------------------------------------------------------
;----------------------------------------------------------------------------
openlibs:
		move.l	$4.w,a6
		lea	reqname(pc),a1
		jsr	_LVOopenlibrary(a6)
		move.l	d0,reqbase(a5)
		beq.w	noreq		;Keine Req.library gefunden
		moveq	#0,d0
		lea	gfxname(pc),a1
		jsr	_LVOopenlibrary(a6)
		move.l	d0,gfxbase(a5)
		lea	dosname(pc),a1
		moveq	#0,d0
		jsr	_LVOopenlibrary(a6)
		move.l	d0,dosbase(a5)
		moveq	#0,d0
		lea	intuiname(pc),a1
		jsr	_LVOopenlibrary(a6)
		move.l	d0,intbase(a5)
		rts				

;---------------------------------------------------------------------------
;--- Init Reqpuffer --------------------------------------------------------
;---------------------------------------------------------------------------
initreq
		lea	dirname,a0
		move.l	a0,dir(a5)
		lea	filename,a0
		move.l	a0,file(a5)		
		lea	filepuffer,a0
		move.l	a0,pathname(a5)
		move.b	#20,flags+3(a5)
		move.w	#3,dirnamecolor(a5)
		move.w	#4,detailcolor(a5)
		move.w	#1,blockcolor(a5)
		move.w	#1,gadgettextcol(a5)
		move.w	#2,textmessagecol(a5)	
		move.w	#3,stringgadgetco(a5)
		move.w	#3,bboxbordercolor(a5)
		move.w	#3,gadgetboxcolor(a5)
		rts

;---------------------------------------------------------------------------
;--- �ffne Screen und Window -----------------------------------------------
;---------------------------------------------------------------------------
openscreen

		move.l	intbase(a5),a6

		lea	NewScreenStructure(pc),a0
		jsr	_LVOopenscreen(a6)
		move.l	d0,screenbase1(a5)
		move.l	d0,scr

		lea	NewWindowStructure1(pc),a0
		jsr	_LVOopenwindow(a6)
		move.l	d0,winbase1(a5)
		move.l	d0,window1(a5)

		lea	NewScreenStructure1(pc),a0
		jsr	_LVOopenscreen(a6)
		move.l	d0,screenbase(a5)
		move.l	d0,scr1
		move.l	d0,scr2

		lea	NewWindowStructure2(pc),a0
		jsr	_LVOopenwindow(a6)
		move.l	d0,winbase(a5)
;-------------------------------------------------------------------
;--- Viewport ermitteln --------------------------------------------
;-------------------------------------------------------------------

		move.l  Winbase1(a5),a0
		move.l  IntBase(a5),a6
		jsr     _LVOViewPortAddress(a6)
		move.l	d0,Uviewport1(a5)

		move.l  Winbase(a5),a0
		move.l  IntBase(a5),a6
		jsr     _LVOViewPortAddress(a6)
		move.l	d0,Uviewport(a5)

;-------------------------------------------------------------------
;--- Farbpalette anpassen ------------------------------------------
;-------------------------------------------------------------------

		move.l	d0,a0
		lea	palette(pc),a1
		move.l	PaletteColorCount,d0
		move.l	gfxbase(a5),a6
		jsr	_LVOloadrgb4(a6)

		move.l	UViewport1(a5),a0
		lea	palette(pc),a1
		move.l	#4,d0
		jsr	_LVOloadrgb4(a6)

;-------------------------------------------------------------------
;--- Bitmaps Initialisieren ----------------------------------------
;-------------------------------------------------------------------

		lea	mymap,a0	;Oberfl�chenbitmap
		moveq	#4,d0
		move.l	#640,d1
		move.l	#80,d2
		move.l	gfxbase(a5),a6
		jsr	_LVOinitbitmap(a6)
		
		lea	mymap,a0
		addq.l	#8,a0
		move.l	#oberflaeche,d0
		moveq	#3,d7
.newplane
		move.l	d0,(a0)		;Planes in Strucktur einbinden
		add.l	#640*80/8,d0
		addq.l	#4,a0
		dbra	d7,.newplane

		lea	fontmap,a0	;Fontbitmap
		moveq	#4,d0
		move.l	#640,d1
		move.l	#16,d2
		jsr	_LVOinitbitmap(a6)
		
		lea	fontmap,a0
		addq.l	#8,a0
		move.l	#font,d0
		moveq	#3,d7
.newplane1
		move.l	d0,(a0)		;Planes in Strucktur einbinden
		add.l	#640*16/8,d0
		addq.l	#4,a0
		dbra	d7,.newplane1
		rts

;---------------------------------------------------------------------------
;--- Copy Oberflaeche in Screen --------------------------------------------
;---------------------------------------------------------------------------
initoberflaeche:
		move.l	winbase(a5),a1		;Fensteradresse

		move.l	50(a1),a1		;Rastport (Ziel)
		move.l	a1,URastport(a5)
		moveq	#0,d2			;X2
		moveq	#0,d3			;Y2
		lea	mymap,a0		;Bitmap (Quelle)
		moveq	#0,d0			;X1
		moveq	#0,d1			;Y1	

		move.l	#640,d4			;Breite
		move.l	#80,d5			;H�he
	
		move.l	#$c0,d6			;Miniterm = nur kopieren

		jsr	_LVObltbitmaprastport(a6)

;-------------------------------------------------------------------
;--- Gadgets refreshen ---------------------------------------------
;-------------------------------------------------------------------

		lea	GadgetList1(pc),a1
		move.l	winbase(a5),a0
		moveq	#0,d0
		move.l	#14,d1
		moveq	#0,d2
		move.l	intbase(a5),a6
		jsr	_LVOAddglist(a6)

		lea	SBorder(pc),a1
		move.l	Urastport(a5),a0
		moveq	#0,d0
		moveq	#0,d1			;Border f�r Statusline
		jsr	_LVOdrawborder(a6)		;ausgeben

		lea	gadgetlist1(pc),a0
		move.l	winbase(a5),a1
		sub.l	a2,a2
		jsr	_LVORefreshgadgets(a6)

;--------------------------------------------------------------------
;--- Write Text -----------------------------------------------------
;--------------------------------------------------------------------

		lea	textlist(pc),a0
		bra	writetext

;---------------------------------------------------------------------------
;--- Warte auf Action ------------------------------------------------------
;---------------------------------------------------------------------------
waitaction:
		bsr.w	wait

werteaus:
		cmp.l	#GADGETUP,d4
		beq.w	gads
		cmp.l	#RAWKEY,d4
		beq.b	tast		;es wurde eine Taste gedr�ckt
		cmp.l	#MOUSEMOVE,d4
;		beq	wertmouseaus
		bra.b	waitaction

;---------------------------------------------------------------------------
;--- Werte tastendruck aus -------------------------------------------------
;---------------------------------------------------------------------------
tast:
		cmp.l	#$4c,d5
		beq.w	down
		cmp.l	#$4d,d5
		beq.w	up
		cmp.l	#$4e,d5
		beq.w	left
		cmp.l	#$4f,d5
		beq.w	right
		cmp.l	#$45,d5
		beq.w	old		;Escape
		cmp.l	#$60,d5		;Shift
		beq.b	shift
		bra.b	waitaction

shift:
		bsr.w	wait
		cmp.l	#RAWKEY,d4
		bne.b	werteaus
		cmp.l	#$4c,d5
		beq.w	top
		cmp.l	#$4d,d5
		beq.w	bottom
		cmp.l	#$4f,d5
		beq.w	gright
		cmp.l	#$4e,d5
		beq.w	gleft
		bra.w	waitaction

;---------------------------------------------------------------------------
;--- Werte Gadgets aus -----------------------------------------------------
;---------------------------------------------------------------------------

gads:
		cmp.l	#gclose,a4
		beq.b	old
		cmp.l	#gload,a4		;Load ?
		beq.w	loadit
		cmp.l	#gdrag,a4
		beq.w	drag
		cmp.l	#gloadroutine,a4
		beq.w	newloadroutine
		bra.w	waitaction

;---------------------------------------------------------------------------
;--- Werte Mousebewegung aus -----------------------------------------------
;---------------------------------------------------------------------------

wertmouseaus:
		tst.w	loadflag(a5)
		beq	waitaction
		move.l	screenbase(a5),a0
		sub.l	a1,a1
		sub.l	a2,a2
		move.w	16(a0),a1	;MouseY
		move.w	18(a0),a2	;MouseX
		cmp.w	#81,a1
;		beq	cut	
		cmp.w	#81,a1
		bhi	weitest
		bra	waitaction
weitest:
		cmp.w	#252,a1
		bhi	up
		cmp.w	#638,a2
		bhi	left
		cmp.w	#3,a2
		blo	right
		cmp.w	#83,a1
		blo	down
		bra	waitaction	


		bra	waitaction
		
;---------------------------------------------------------------------------
;--- Programm Beenden ------------------------------------------------------
;---------------------------------------------------------------------------
old:
		move.l	intbase(a5),a6
		move.l	winbase(a5),a0
		jsr	_LVOclosewindow(a6)
		move.l	screenbase(a5),a0
		jsr	_LVOclosescreen(a6)

;		tst.l	chwinbase(a5)
;		beq	noclose
;		move.l	chwinbase(a5),a0
;		jsr	_LVOClosewindow(a6)
noclose:
		move.l	winbase1(a5),a0
		jsr	_LVOclosewindow(a6)
		move.l	screenbase1(a5),a0
		jsr	_LVOclosescreen(a6)

		move.l	4.w,a6
		tst.w	loadflag(a5)
		beq.b	nofree
		move.l	picmem(a5),a1
		move.l	memsize(a5),d0
		jsr	_LVOfreemem(a6)

nofree:
		move.l	gfxbase(a5),a1
		jsr	_LVOcloselibrary(a6)		
		move.l	dosbase(a5),a1
		jsr	_LVOcloselibrary(a6)
		move.l	reqbase(a5),a1
		jsr	_LVOcloselibrary(a6)
		move.l	intbase(a5),a1
		jsr	_LVOcloselibrary(a6)
		
fini:
		illegal
		moveq	#0,d0
		rts
;---------------------------------------------------------------------------
;--- Screen to Back --------------------------------------------------------
;---------------------------------------------------------------------------
drag:
		move.l	screenbase(a5),a0
		move.l	intbase(a5),a6
		jsr	_LVOScreentoBack(a6)
		move.l	screenbase1(a5),a0
		jsr	_LVOScreentoBack(a6)
		bra.w	waitaction

;---------------------------------------------------------------------------
;--- Choose LoadRoutine ----------------------------------------------------
;---------------------------------------------------------------------------
newloadroutine:
		bsr	openchoosewin

		bsr	initausgadgets

.wa
		move.l	chwinbase(a5),a0
		bsr	wait1

		cmp.l	#gok,a4
		bne	.wa

		move.l	chwinbase(a5),a0
		move.l	intbase(a5),a6
		jsr	_LVOclosewindow(a6)

		bra	waitaction

;---------------------------------------------------------------------------
;--- Load File -------------------------------------------------------------
;---------------------------------------------------------------------------
loadit:
		move.l	screenbase(a5),a0
		move.l	intbase(a5),a6
		jsr	_LVOScreentoBack(a6)

		lea	loadfile(pc),a0
		move.l	a0,windowtitle(a5)
		lea	version(a5),a0
		move.l	reqbase(a5),a6
		jsr	-84(a6)
		tst.l	d0
		bne	load
		move.l	screenbase(a5),a0
		move.l	intbase(a5),a6
		jsr	_LVOScreentoFront(a6)
		bra	waitaction
load:
		move.l	screenbase(a5),a0
		move.l	intbase(a5),a6
		jsr	_LVOScreentoFront(a6)

		move.l	4.w,a6
		tst.w	loadflag(a5)
		beq.b	.nofree
		move.l	picmem(a5),a1
		move.l	memsize(a5),d0
		jsr	_LVOfreemem(a6)
		clr.w	loadflag(a5)
.nofree:
		bsr.w	loadiff

		lea	farbtab,a0		;Hintergrundfarbe
		lea	palette(pc),a1		;dem geladenen Pic
		move.w	(a0),(a1)		;anpassen
		move.l	gfxbase(a5),a6
		move.l	PaletteColorCount,d0
		move.l	Uviewport(a5),a0
		jsr	_LVOloadrgb4(a6)

;---------------------------------------------------------------------------
;--- �ffne neuen Screen f�r geladenes Pic ----------------------------------
;---------------------------------------------------------------------------
		
		move.l	intbase(a5),a6
		move.l	winbase1(a5),a0
		jsr	_LVOclosewindow(a6)
		move.l	screenbase1(a5),a0
		jsr	_LVOclosescreen(a6)

;--- Init neue ScreenStruck ---

		move.w	breite(a5),wigth
		move.w	hoehe(a5),wigth+2
		moveq	#0,d0
		move.b	planes(a5),d0
		move.w	d0,depth
		move.w	screenmode(a5),dismode
		
		lea	NewScreenStructure(pc),a0
		jsr	_LVOopenscreen(a6)
		move.l	d0,screenbase1(a5)
		move.l	d0,scr
		
;--- Neue farben anpassen ---

		move.l	UViewport1(a5),a0
		lea	farbtab,a1
		moveq	#0,d0
		move.w	anzcolor(a5),d0
		move.l	gfxbase(a5),a6
		jsr	_LVOloadrgb4(a6)

		move.l	intbase(a5),a6
		move.l	screenbase(a5),a0
		jsr	_LVOScreentoFront(a6)

		move.w	breite(a5),wwight
		move.w	hoehe(a5),wwight+2

		lea	NewWindowStructure1(pc),a0
		jsr	_LVOopenwindow(a6)
		move.l	d0,winbase1(a5)
		move.l	d0,window1(a5)

		move.l  Winbase1(a5),a0		;neuen Viewport ermitteln
		move.l  IntBase(a5),a6
		jsr     _LVOViewPortAddress(a6)
		move.l	d0,Uviewport1(a5)

;---------------------------------------------------------------------------
;--- Init Bitmap f�r geladenes Pic -----------------------------------------
;---------------------------------------------------------------------------

		lea	picmap,a0	;Picbitmap
		moveq	#0,d0
		move.b	planes(a5),d0
		move.w	breite(a5),d1
		move.w	hoehe(a5),d2
		move.l	gfxbase(a5),a6
		jsr	_LVOinitbitmap(a6)
		
		lea	picmap,a0
		addq.l	#8,a0
		move.l	picmem(a5),d0
		moveq	#0,d7
		move.b	planes(a5),d7
		subq.l	#1,d7
.newplane
		move.l	d0,(a0)		;Planes in Strucktur einbinden
		add.l	oneplanesize(a5),d0
		addq.l	#4,a0
		dbra	d7,.newplane

;---------------------------------------------------------------------------
;--- Kopiere geladenes Pic in Screen ---------------------------------------
;---------------------------------------------------------------------------

		move.l	winbase1(a5),a1		;Fensteradresse

		move.l	50(a1),a1		;Rastport (Ziel)
		move.l	a1,URastport1(a5)
		moveq	#0,d2			;X2
		moveq	#0,d3			;Y2
		lea	picmap,a0		;Bitmap (Quelle)
		moveq	#0,d0			;X1
		moveq	#0,d1			;Y1	

		move.w	breite(a5),d4		;Breite
		move.w	hoehe(a5),d5		;H�he
	
		move.l	#$c0,d6			;Miniterm = nur kopieren

		move.l	gfxbase(a5),a6
		jsr	_LVObltbitmaprastport(a6)


		clr.l	highadd(a5)
		clr.l	wideadd(a5)


		bra.w	waitaction

noreq:		
		bra.w	fini	;Keine Req.library

gfxname:	dc.b	'graphics.library',0
dosname:	dc.b	'dos.library',0
reqname:	dc.b	'req.library',0
intuiname:	dc.b	'intuition.library',0
	even

;---------------------------------------------------------------------------
;--- Schiebe Screen hoch ---------------------------------------------------
;---------------------------------------------------------------------------
up:
		move.l	highadd(a5),d0
		move.w	screenhight(a5),d1
		sub.l	#175,d1
		move.w	screenbyte(a5),d2
		mulu	d2,d1
		cmp.l	d1,d0
		bhs.w	waitaction	;Schon ganz oben
		add.l	d2,d2
		add.l	d2,d2
		add.l	d2,highadd(a5)
		move.w	screenhight(a5),d1
		sub.l	#175,d1
		move.w	screenbyte(a5),d0
		mulu	d1,d0
		move.l	highadd(a5),d1
		cmp.l	d0,d1
		blo.b	nokorr
		move.l	d0,highadd(a5)
nokorr:
		bra.w	makenewclist

;---------------------------------------------------------------------------
;--- Schiebe Screen runter -------------------------------------------------
;---------------------------------------------------------------------------
down:
		move.l	highadd(a5),d0
		tst.l	d0
		beq.w	waitaction	;ganz unten
		move.w	screenbyte(a5),d2
		add.l	d2,d2
		add.l	d2,d2
		sub.l	d2,highadd(a5)
		bpl.b	nonegativ
		clr.l	highadd(a5)
nonegativ:
		bra.w	makenewclist

;---------------------------------------------------------------------------
;--- Schiebe Screen nach links ---------------------------------------------
;---------------------------------------------------------------------------
left:
		move.l	wideadd(a5),d0
		move.w	screenbyte(a5),d1
		tst.b	hires(a5)
		beq.b	.nohi
		sub.l	#80,d1
		bra.b	.wei
.nohi:
		sub.l	#40,d1
.wei
		cmp.l	d1,d0
		bhs.w	waitaction
		add.l	#2,wideadd(a5)
		bra.w	makenewclist

;---------------------------------------------------------------------------
;--- Schiebe Screen nach rechts --------------------------------------------
;---------------------------------------------------------------------------
right:
		move.l	wideadd(a5),d0
		tst.l	d0
		beq.w	waitaction
		sub.l	#2,wideadd(a5)
		bra.w	makenewclist

;---------------------------------------------------------------------------
;--- Schiebe Screen ganz nach oben -----------------------------------------
;---------------------------------------------------------------------------
bottom:
		move.w	screenhight(a5),d1
		sub.l	#175,d1
		move.w	screenbyte(a5),d0
		mulu	d1,d0
		move.l	d0,highadd(a5)
		bra.w	makenewclist

;---------------------------------------------------------------------------
;--- Schiebe Screen ganz nach unten ----------------------------------------
;---------------------------------------------------------------------------
top:
		clr.l	highadd(a5)
		bra.w	makenewclist

;---------------------------------------------------------------------------
;--- Schiebe Screen ganz nach links ----------------------------------------
;---------------------------------------------------------------------------
gleft:
		move.w	screenbyte(a5),d1
		tst.b	hires(a5)
		beq.b	.nohi
		sub.l	#80,d1
		bra.b	.wei
.nohi:
		sub.l	#40,d1
.wei
		move.l	d1,wideadd(a5)
		bra.w	makenewclist

;---------------------------------------------------------------------------
;--- Schiebe Screen ganz nach rechts ---------------------------------------
;---------------------------------------------------------------------------
gright:
		clr.l	wideadd(a5)
		bra.w	makenewclist

;---------------------------------------------------------------------------
;--- �ffne Window f�r Auswahlfenster ---------------------------------------
;---------------------------------------------------------------------------
openchoosewin:
		lea	auswahlWindow(pc),a0
		move.l	intbase(a5),a6
		jsr	_LVOOpenwindow(a6)
		move.l	d0,chwinbase(a5)

		move.l	chwinbase(a5),a1		;Fensteradresse

		move.l	50(a1),a1		;Rastport (Ziel)
		move.l	#0,d2			;X2
		moveq	#0,d3			;Y2
		lea	mymap,a0		;Bitmap (Quelle)
		move.l	#380,d0			;X1
		moveq	#6,d1			;Y1	

		move.l	#170,d4			;Breite
		move.l	#73,d5			;H�he
	
		move.l	#$c0,d6			;Miniterm = nur kopieren

		move.l	gfxbase(a5),a6
		jmp	_LVObltbitmaprastport(a6)

;---------------------------------------------------------------------------
;--- Init Gadgets f�r Auswahlwindow ----------------------------------------
;---------------------------------------------------------------------------
initausgadgets:
		lea	ausGadget1(pc),a1
		move.l	chwinbase(a5),a0
		moveq	#0,d0
		move.l	#14,d1
		moveq	#0,d2
		move.l	intbase(a5),a6
		jsr	_LVOAddglist(a6)

		lea	winborder(pc),a1
		move.l	chwinbase(a5),a0		;Fensteradresse
		move.l	50(a0),a0			;Rastport (Ziel)
		moveq	#0,d0
		moveq	#0,d1
		jsr	_LVODrawBorder(a6)

		lea	ausgadget1(pc),a0
		move.l	chwinbase(a5),a1
		sub.l	a2,a2
		jmp	_LVORefreshgadgets(a6)

;---------------------------------------------------------------------------
;--- Inti CList neu --------------------------------------------------------
;---------------------------------------------------------------------------
makenewclist:
		move.l	Uviewport(a5),a0
		move.l	gfxbase(a5),a6
		jsr	_LVOFreeVPortCopLists(a6)
		move.l	intbase(a5),a6
		move.l	screenbase(a5),a0
		jsr	_LVOmakescreen(a6)	;Screen neu aktivieren
		moveq	#12,d0
		move.l	#$10000,d1
		move.l	4.w,a6
		jsr	_LVOallocmem(a6)
		move.l	d0,ucopperlist(a5)
		bsr.b	initcoplist
		bsr.w	startcoplist
		bra.w	waitaction

;---------------------------------------------------------------------------
;--- Wait auf Message ------------------------------------------------------
;---------------------------------------------------------------------------
wait:
		move.l	winbase(a5),a0
wait1:
		move.l	wd_Userport(a0),a0	;Zeige auf MsgPort
		move.l	a0,a4			;Sichern
		move.b	MP_SIGBIT(a0),d1	;Signal Bit holen
		moveq	#0,d0			;Nummer
		bset	d1,d0			;in Maske wandeln
		move.l	4.w,a6
		jsr	_LVOwaitport(a6)	;warte auf Nachricht	

		move.l	a4,a0			;hole Port-Adresse
		jsr	_LVOgetmsg(a6)		;hole Msg
		move.l	d0,a1			;muss nach a1
		move.l	im_Class(a1),d4		;Msg-Type
		move.w	im_Code(a1),d5		;Untergruppe
		move.l	im_IAddress(a1),a4	;Adr. f�r Gadgets
		jmp	_LVOreplyMsg(a6)	;quittiere Msg in a1

;---------------------------------------------------------------------------
;--- Init Copperliste ------------------------------------------------------
;---------------------------------------------------------------------------
initcoplist:
		move.w	#1,newcopper(a5)
		
		move.l	gfxbase(a5),a6
		move.l	#80,d0		;Vertikale Rasterstrahl-Position
		move.l	#1,d1		;Horizontale Position
		jsr	wait1
		
		tst.b	hires(a5)
		bne.b	nodff

		move.l	#$92,d0		
		move.l	ddf1(a5),d1	;dffstrt
		jsr	move
		move.l	#$94,d0
		move.l	ddf2(a5),d1	;dffstop
		jsr	move

nodff:
		move.l	#$108,d0	;Init Modulo
		move.w	modulo(a5),d1
		jsr	move
		move.l	#$10a,d0
		move.w	modulo(a5),d1
		jsr	move	

		move.l	#$100,d0	;Screenmode
		move.w	screenmode(a5),d1
		jsr	move

		move.l	#$180,d2
		move.w	anzcolor(a5),d7	;Anzahl der Farben
		lea	farbtab,a2
neco:
		moveq	#0,d1
		move.w	(a2)+,d1	;Init ColorTab
		move.l	d2,d0
		jsr	move
		add.w	#2,d2
		dbra	d7,neco

		move.l	#81,d0
		move.l	#1,d1
		jsr	wait1

;---------------------------------------------------------------------------
;--- Init Planeregister ----------------------------------------------------
;---------------------------------------------------------------------------

		move.l	picmem(a5),d6
		add.l	highadd(a5),d6	;Hoch oder runterschieben
		add.l	wideadd(a5),d6	;links oder recht schieben
		move.l	#$e0,d2
		moveq	#0,d7
		move.b	planes(a5),d7
		subq.b	#1,d7
nepl:
		swap	d6
		move.l	d2,d0
		move.w	d6,d1
		jsr	move
		addq.w	#2,d2
		swap	d6
		move.l	d2,d0
		move.w	d6,d1
		jsr	move
		addq.l	#2,d2
		add.l	oneplanesize(a5),d6
		dbra	d7,nepl


		move.l	#10000,d0	;Ende der Copperlist
		move.l	#256,d1
		jmp	wait1

;---------------------------------------------------------------------------
;--- Binde Copperlist ein --------------------------------------------------
;---------------------------------------------------------------------------
startcoplist
		move.l	Uviewport(a5),a0
		move.l	ucopperlist(a5),d0
		move.l	d0,20(a0)	;Ucopins

		move.l	intbase(a5),a6
		jmp	_LVOrethinkdisplay(a6)	;Copperlist einschalten

;---------------------------------------------------------------------------

;wait1:
		move.l	ucopperlist(a5),a1
		jsr	_LVOcwait(a6)
		move.l	ucopperlist(a5),a1
		jsr	_LVOcbump(a6)	;Befehl �bernehemen
		rts
move:
		move.l	ucopperlist(a5),a1
		jsr	_LVOcmove(a6)
		move.l	ucopperlist(a5),a1
		jsr	_LVOcbump(a6)	;Befehl �bernehemen
		rts

;---------------------------------------------------------------------------
;--- Load IFF --------------------------------------------------------------
;---------------------------------------------------------------------------
loadiff:
		move.l	#filepuffer,d1
		move.l	#1005,d2
		move.l	dosbase(a5),a6
		jsr	_LVOopen(a6)		;Open File
		move.l	d0,filehandle(a5)		
;		beq	openerror
				
		lea	bytepuffer,a4
		bsr.w	readbyte
		cmp.l	#'FORM',(a4)		;Test IFF
		bne.w	notiff		
		bsr.w	readbyte
		bsr.w	readbyte
		cmp.l	#'ILBM',(a4)		;Test ob IFF-Pic
		bne.w	notiff
		bsr.w	readbyte
		cmp.l	#'BMHD',(a4)		;Test auf Bitmapheader
		bne.w	notiff
		bsr.w	readbyte		;L�nge des BMHD �berlesen
;		bsr	readbyte
		moveq	#20,d3
		bsr.w	readchunk		;Read Bitmapheader	

		lea	chunkpuffer,a0
		move.w	(a0),d0
		move.w	d0,breite(a5)
		move.w	2(a0),d0
		move.w	d0,hoehe(a5)

		move.b	8(a0),planes(a5)		
		move.b	9(a0),testbyte(a5)
		move.b	10(a0),crunchmode(a5)		

		move.w	12(a0),transcolor(a5)
		move.w	14(a0),verhaeltnis(a5)

		move.w	16(a0),xresolution(a5)	;Bildaufl�sung
		move.w	18(a0),yresolution(a5)
		clr.b	hires(a5)
		cmp.w	#$280,xresolution(a5)
		bne.b	.nohi		;Kein hires
		move.b	#1,hires(a5)
.nohi:
		clr.b	lace(a5)
		cmp.w	#$200,yresolution(a5)
		bne.b	nextread
		move.b	#1,lace(a5)

nextread
		bsr.w	readbyte
		lea	bytepuffer,a0
		cmp.l	#'DPPS',(a0)
		beq.w	dpps
		cmp.l	#'CMAP',(a0)
		beq.w	colortab		
		cmp.l	#'CAMG',(a0)
		beq.w	hamehb
		cmp.l	#'BODY',(a0)
		beq.w	loadpic
		bra.b	nextread

;---------------------------------------------------------------------------
;--- Init Modulo usw. ------------------------------------------------------
;---------------------------------------------------------------------------
initdatas:
		tst.b	lace(a5)
		beq.b	nolacemod

		move.l	bytebreite(a5),d0
		tst.b	hires(a5)
		beq.b	lores1
		cmp.w	#80,d0
		bmi.b	normod
		sub.w	#80,d0
		bra.b	writemod1
lores1:
		cmp.w	#40,d0
		bmi.b	normod
		sub.w	#40,d0
writemod1:
		add.l	bytebreite(a5),d0
		move.w	d0,lmodulo(a5)
		bra.b	weit
normod:
		move.l	bytebreite(a5),d0
		move.w	d0,lmodulo(a5)
		bra.b	weit

;---------------------------------------------------------------------------
;--- Modulo f�r NonLace-Screen ---------------------------------------------
;---------------------------------------------------------------------------
nolacemod:
		lea	modulo+2,a0
		move.l	bytebreite(a5),d0
		tst.b	hires(a5)
		beq.b	lores
		cmp.w	#80,d0
		bmi.b	modnull
		sub.w	#80,d0
		bra.b	writemod
lores:
		cmp.w	#40,d0
		bmi.b	modnull
		sub.w	#40,d0
writemod:
		move.w	d0,modulo(a5)
		bra.b	weit
modnull:
		clr.w	modulo(a5)		;$108
weit:

;---------------------------------------------------------------------------
;--- Init Screenmode -------------------------------------------------------
;---------------------------------------------------------------------------

		move.b	planes(a5),d0
		muls	#$1000,d0
		move.w	viewmode(a5),d1
		btst	#7,d1		;EHB ?
		bne.b	.wei
		and.w	#$fbff,d0	;Ja
.wei:
		and.w	#$800,d1
		tst.w	d1
		beq.b	noham
		or.w	#$800,d0
noham:
		or.w	#$200,d0	;Colorbit		
		tst.b	hires(a5)
		beq.b	nohires
		or.w	#$8000,d0
nohires:					
		tst.b	lace(a5)
		beq.b	nolace
		or.w	#4,d0
nolace:
		move.w	d0,screenmode(a5)	;register $100 initialisieren

;---------------------------------------------------------------------------
;--- DDF Init --------------------------------------------------------------
;---------------------------------------------------------------------------

		move.l	#$38,ddf1(a5)
		move.l	#$d0,ddf2(a5)
		tst.b	hires(a5)
		beq.b	notiff
		move.l	#$3c,ddf1(a5)	;DDF-werte fuer hires
		move.l	#$d4,ddf2(a5)
notiff:
		move.l	filehandle(a5),d1
		move.l	dosbase(a5),a6
		jsr	_LVOclose(a6)
		rts

;------------------------------------------------------------------------
readbyte:
		move.l	filehandle(a5),d1
		lea	bytepuffer,a0
		exg	a0,d2
		moveq	#4,d3
		jmp	_LVOread(a6)
readchunk:
		move.l	filehandle(a5),d1
		lea	chunkpuffer,a0
		exg	a0,d2
		jmp	_LVOread(a6)
		
;---------------------------------------------------------------------------
;--- Read the colortab -----------------------------------------------------
;---------------------------------------------------------------------------
colortab:
		bsr.b	readbyte
		lea	bytepuffer,a0
		move.l	(a0),d3
		move.l	d3,d7
		divs	#3,d7
		bsr.b	readchunk
		subq.w	#1,d7
		move.w	d7,anzcolor(a5)
		lea	chunkpuffer,a0
		lea	farbtab,a1
nextcol:
		moveq	#0,d0
		moveq	#0,d1
		move.b	(a0)+,d1
		lsl.w	#4,d1
		move.b	(a0)+,d1		
		move.b	(a0)+,d0
		lsr.w	#4,d0
		or.w	d0,d1
		move.w	d1,(a1)+
		dbra	d7,nextcol
		bra.w	nextread

;---------------------------------------------------------------------------
;--- werte CAMG Chunk aus --------------------------------------------------
;---------------------------------------------------------------------------
hamehb:
		bsr.b	readbyte
		lea	bytepuffer,a0
		move.l	(a0),d3
		bsr.b	readchunk
		lea	chunkpuffer,a0
		move.l	(a0),d0
		move.w	d0,viewmode(a5)		
		bra.w	nextread		

;---------------------------------------------------------------------------
;--- DPPS �berlesen --------------------------------------------------------
;---------------------------------------------------------------------------
dpps:
		bsr.b	readbyte
		lea	bytepuffer,a0
		move.l	(a0),d3
		bsr.b	readchunk		
		bra.w	nextread

;---------------------------------------------------------------------------
;--- BODY-Chunk einlesen (das ganze bild einlesen) -------------------------
;---------------------------------------------------------------------------
loadpic:
		bsr.w	readbyte
		lea	bytepuffer,a0
		move.l	(a0),d7
		move.l	d7,bodylaenge(a5)
		move.w	breite(a5),d0
		move.w	hoehe(a5),d1
		tst.b	lace(a5)
		bne.b	lacesize
		cmp.w	#256,d1		;�berpr�fe ob pic kleiner als
		bhs.b	testhi		;normal ist
		move.w	#256,d1	
		bra.b	testhi
lacesize:
		cmp.w	#512,d1
		bhs.b	testhi
		move.w	#512,d1
testhi:
		tst.b	hires(a5)
		bne.b	hiressize
		cmp.w	#320,d0
		bhs.b	allmem
		move.w	#320,d0
		bra.b	allmem
hiressize:
		cmp.w	#640,d0
		bhs.b	allmem
		move.w	#640,d0
allmem:				
		move.w	d0,screenbreite(a5)
		move.w	d1,screenhight(a5)
		mulu	d1,d0
		lsr.l	#3,d0
		moveq	#0,d3
		move.l	d0,oneplanesize(a5)
		move.b	planes(a5),d3
		mulu	d3,d0
		move.l	d0,memsize(a5)
		move.l	#$10002,d1
		move.l	4.w,a6
		jsr	_LVOallocmem(a6)
		move.l	d0,picmem(a5)			
		move.w	#1,loadflag(a5)
;		beq	memerror

;---------------------------------------------------------------------------
;--- Load Crunched Picdaten ------------------------------------------------
;---------------------------------------------------------------------------

		move.l	d7,d0
		move.l	#$10000,d1
		jsr	_LVOallocmem(a6)
		move.l	d0,loadbody(a5)
;		beq	memerror

		move.l	filehandle(a5),d1
		move.l	d0,d2
		move.l	d7,d3
		move.l	dosbase(a5),a6
		jsr	_LVOread(a6)
		
		moveq	#0,d7
		move.w	breite(a5),d7
		move.l	d7,d5
		lsr.l	#3,d7		;Breite in Byte
		move.l	d7,d6
		lsl.l	#3,d6
		cmp.w	d6,d5
		beq.b	noadd
		addq.w	#1,d7
noadd:
		move.l	d7,bytebreite(a5)
		moveq	#0,d0
		move.w	screenbreite(a5),d0
		lsr.l	#3,d0
		move.w	d0,screenbyte(a5)
		sub.l	d7,d0	 		;differenz zwischen brush
		move.l	d0,bmodulo(a5)		;und screen
		move.l	picmem(a5),a1
		move.l	loadbody(a5),a0		
		moveq	#0,d5
		move.w	hoehe(a5),d5
		subq.l	#1,d5
		tst.b	crunchmode(a5)
		beq	loadnotcrunch
newline:
		move.l	a1,a2
		moveq	#0,d6
		move.b	planes(a5),d6
		subq.l	#1,d6
nextplane:		
		move.l	a2,a4
		sub.l	a3,a3
loop:
		moveq	#0,d0
		move.b	(a0)+,d0
		bmi.b	packed
copy:
		addq.w	#1,a3
		move.b	(a0)+,(a2)+
		cmp.w	a3,d7
		beq.b	newplane
		dbra	d0,copy
		bra.b	loop	
packed:
		neg.b	d0
		move.b	(a0)+,d1
copy1:
		addq.w	#1,a3
		move.b	d1,(a2)+
		cmp.w	a3,d7
		beq.b	newplane
		dbra	d0,copy1
		bra.b	loop
newplane:
		move.l	a4,a2
		add.l	oneplanesize(a5),a2
		dbra	d6,nextplane		
		add.l	bytebreite(a5),a1
		add.l	bmodulo(a5),a1
		dbra	d5,newline		

endload:
		move.l	loadbody(a5),a1
		move.l	bodylaenge(a5),d0
		move.l	4.w,a6
		jsr	_LVOfreemem(a6)		

		bra.w	initdatas

loadnotcrunch:
		subq.l	#1,d7
		move.l	d7,a4
.newline:
		move.l	a1,a3
		moveq	#0,d6
		move.b	planes(a5),d6
		subq.l	#1,d6
.nextplane:
		move.l	a1,a2
		move.l	a4,d7
.nextbyte:
		move.b	(a0)+,(a1)+
		dbra	d7,.nextbyte

		move.l	a2,a1
		add.l	oneplanesize(a5),a1
		dbra	d6,.nextplane		
		move.l	a3,a1
		add.l	bytebreite(a5),a1
		add.l	bmodulo(a5),a1
		dbra	d5,.newline		

		bra	endload

;-------------------------------------------------------------------------
;--- TEXTWRITERROUTINE ---------------------------------------------------
;-------------------------------------------------------------------------
;a0 = Textstruck

writetext:
		moveq	#0,d0
		moveq	#0,d1
		move.w	(a0)+,d2		;x
		move.w	(a0)+,d3		;y
		move.l	(a0)+,a1		;Text
		move.l	gfxbase(a5),a6		

nextchar:
		moveq	#0,d0
		move.b	(a1)+,d0		;neuer Buchstabe
		beq	endwrite
		sub.b	#33,d0
		cmp.b	#80,d0
		bls	.kleinerP		
		mulu	#8,d0
		move.l	#7,d1
		bra	.wei
.kleinerP:
		mulu	#8,d0
		moveq	#0,d1
.wei
		movem.l	d2/d3/a0-a2,-(a7)

		move.l	URastport(a5),a1	;Rastport (Ziel)
		lea	fontmap,a0		;Bitmap (Quelle)
		lea	fontmask,a2		;Bitmap f�r Maske

		move.l	#8,d4			;Breite
		move.l	#8,d5			;H�he
	
		move.l	#$e0,d6			;Miniterm = nur kopieren

		jsr	_LVObltmaskbitmaprastport(a6)

		movem.l	(a7)+,d2/d3/a0-a2

		addq.l	#8,d2
		bra	nextchar
endwrite:
		move.l	(a0),a0
		cmp.l	#0,a0
		bne	writetext

		rts		




;-------------------------------------------------------------------------
cut:
		move.l	gfxbase(a5),a6
.w		btst	#14,$dff002	;warte auf blitter
		bne.b	.w
		jsr	_LVOownblitter(a6)	;Blitter reservieren

		bsr	lineinit
		bsr	setlines
		move.w	a1,oldy(a5)
		move.w	a2,oldx(a5)	

		move.l	gfxbase(a5),a6
		jsr	_LVOdisownblitter(a6)


		bsr	wait

		cmp.l	#MOUSEMOVE,d4
;		beq	movemouse
			
		bsr.w	checkm
		bsr.w	setspr
		btst	#6,$bfe001
		beq.b	drawgrid
;		bsr.w	setline
		btst	#10,$dff016
		bne.b	cut
		bra.w	old

;--- Drawgrid ---
drawgrid:
		move.w	oldx(a5),d5
		move.w	oldy(a5),d6
;		bsr.w	drawl		;l�sche Fadenkreuz

		move.w	oldx(a5),xbox(a5)	;merke erste Koordinaten
		move.w	oldy(a5),ybox(a5)

		bsr.b	drawbox		;zeichne erste Box

waitmouse:		
		bsr.w	checkm
		bsr.w	setspr
		btst	#6,$bfe001
		bne.w	old
		bsr.b	drawbox
		bra.b	waitmouse


notnew:
		bra.w	old

;--- Zeichne CutGitter ---

drawbox:
		move.w	oldx(a5),d0
		move.w	xmouse(a5),d1
		cmp.w	d0,d1
		bne.b	.dr
		move.w	oldy(a5),d0
		move.w	ymouse(a5),d1
		cmp.w	d0,d1
		bne.b	.dr
		rts
.dr
		move.l	4(a6),d7
		and.l	#$fff00,d7
		cmp.l	#$13000,d7
		bne.b	drawbox

		move.w	xbox(a5),d0
		move.w	ybox(a5),d1
		move.w	oldx(a5),d2
		move.w	oldy(a5),d3
		sub.w	#$80,d0
		sub.w	#$29,d1
		sub.w	#$80,d2
		sub.w	#$29,d3
		move.w	d0,xboxreal(a5)
		move.w	d1,yboxreal(a5)
		move.w	d2,oldxreal(a5)
		move.w	d3,oldyreal(a5)
		move.w	xmouse(a5),d4
		sub.w	#$80,d4
		move.w	d4,xmousereal(a5)
		move.w	ymouse(a5),d5	
		sub.w	#$29,d5
		move.w	d5,ymousereal(a5)

		move.w	oldxreal(a5),d7
		move.w	xboxreal(a5),d6
		sub.w	d6,d7
		bge.b	.wei
		not.w	d7	;Differenz kleiner null
		addq.w	#1,d7
.wei:
		cmp.w	#0,d7
		beq.b	norast
		move.w	d7,boxsize(a5)
		lsr.w	#4,d7	;Wiviel Raster ?
		cmp.w	#0,d7
		beq.b	norast
		move.w	d7,d6
		lsl.w	#4,d7
		move.w	boxsize(a5),d5
		sub.w	d7,d5
		bne.b	sizeok
		subq.w	#1,d6
		cmp.w	#0,d6
		bhi.b	sizeok
		bra.b	norast
sizeok:
		move.w	oldrast(a5),d7	;Wiviel Raster m�ssen gel�scht werden
		cmp.w	#0,d7
		beq.b	noclr	 ;es m�ssen noch keine Raster gel�scht werden
		
		subq.w	#1,d7
		move.w	xboxreal(a5),d0
;.w		move.l	4(a6),d5
;		and.l	#$fff00,d5
;		cmp.l	#$13000,d5
;		bne	.w
nextrast:
		add.w	#16,d0
		move.w	yboxreal(a5),d1
		move.w	d0,d2
		move.w	oldyreal(a5),d3
		bsr.w	drawli		;l�sche alte Raster
		dbra	d7,nextrast
noclr:
;.w		move.l	4(a6),d5
;		and.l	#$fff00,d5
;		cmp.l	#$13000,d5
;		bne	.w
		move.w	d6,newrast(a5)	;Anzahl raster merken
		subq.w	#1,d6
		move.w	xboxreal(a5),d0
nextrast1:
		add.w	#16,d0
		move.w	d0,d2
		move.w	ymousereal(a5),d3
		bsr.w	drawli
		dbra	d6,nextrast1
		move.w	newrast(a5),oldrast(a5)
norast:
		move.w	xmouse(a5),oldx(a5)
		move.w	ymouse(a5),oldy(a5)



		move.w	xboxreal(a5),d0
		move.w	yboxreal(a5),d1
		move.w	oldxreal(a5),d2
		move.w	oldyreal(a5),d3
		move.w	xmousereal(a5),d4
		move.w	ymousereal(a5),d5

		move.w	d1,d3
		bsr.w	drawli		;l�sche alte line
		move.w	d4,d2
		bsr.w	drawli		;zeichen neue line		

		move.w	oldxreal(a5),d0
		move.w	d0,d2
		move.w	oldyreal(a5),d3
		bsr.w	drawli		;l�sche alte y-line
		move.w	d4,d0
		move.w	d4,d2
		move.w	d5,d3		
		bsr.w	drawli		;zeichne neue y-line

		move.w	oldxreal(a5),d0
		move.w	oldyreal(a5),d1
		move.w	xboxreal(a5),d2
		move.w	d1,d3
		bsr.w	drawli
		move.w	d4,d0
		move.w	d5,d1
		move.w	d1,d3
		bsr.w	drawli

		move.w	d2,d0
		move.w	oldyreal(a5),d1
		move.w	yboxreal(a5),d3
		bsr.w	drawli
		move.w	d5,d1
		bsr.w	drawli		

;--- Zeichne Raster ---
		
		rts

;--- SETLINE ---
setlines:
		moveq	#0,d0
		move.l	a1,d1
		sub.w	#81,d1
		move.w	screenbreite(a5),d2
		move.l	d1,d3
		bsr	.draw		;zeichne Wagerechte line

		move.w	a2,d0
		tst.b	hires(a5)
		bne	.wei
		lsr.l	#1,d0
.wei
		moveq	#0,d1
		move.l	d0,d2
		move.w	screenhight(a5),d3
		

.draw:
		move.l	4(a6),d7
		and.l	#$fff00,d7
		cmp.l	#$13000,d7
		bne.b	.draw
drawli:
		movem.l	d0-d7/a0-a6,-(a7)
		move.l	picmem(a5),a0

; ============== Zeichne Linie mit Blitter (AMIGA 11/91) ====
line:
		move.w	screenbyte(a5),d6
		cmp.w	d1,d3
		bgt.b	nohi
		exg	d0,d2
		exg	d1,d3
nohi:		
		move	d0,d4
		move	d1,d5
		mulu	d6,d5	;Breite
		add	d5,a0
		lsr	#4,d4
		add	d4,d4
		lea	(a0,d4.w),a0
		sub.w	d0,d2
		sub.w	d1,d3
		moveq	#15,d5
		and.l	d5,d0
		ror.l	#4,d0
		move.w	#4,d0
		tst.w	d2
		bpl.b	l1
		addq	#1,d0
		neg.w	d2
l1:		cmp.w	d2,d3
		ble.b	l2
		exg	d2,d3
		subq.w	#4,d0
		add.w	d0,d0
l2:
		move.w	d3,d4
		sub.w	d2,d4
		add.w	d4,d4
		add.w	d4,d4
		add.w	d3,d3
		move.w	d3,d6
		sub.w	d2,d6
		bpl.b	l3
		or.w	#16,d0
l3:
		add.w	d3,d3
		add.w	d0,d0
		add.w	d0,d0
		addq.w	#1,d2
		lsl.w	#6,d2
		addq.w	#2,d2
		swap	d3
		move.w	d4,d3
		or.l	#$0b4a0001,d0


wbl2:		btst	#14,2(a6)
		bne.b	wbl2
		move.l	d3,$62(a6)
		move	d6,$52(a6)
		move.l	a0,$48(a6)
		move.l	a0,$54(a6)
		move.l	d0,$40(a6)
		move	d2,$58(a6)
		movem.l	(a7)+,d0-d7/a0-a6
		rts

; =================== Init Blitter ==========================
;line_init vor line_Befehlen
; d0/d1		x1/y2
; d2/d3		x2/y2
; a0		bitplane
; a6		aus line_init belassen
; d4-d6		Arbeitsregister

br = 40 	;breite der bitplane

lineinit:
wbl1:		lea	$dff000,a6
		btst	#14,2(a6)
		bne.b	wbl1
		move	#%1111111111111111,$72(a6)
		move.l	#-1,$44(a6)
		move.w	screenbyte(a5),d0
		move.w	d0,$60(a6)
		move.w	d0,$66(a6)
		move.w	#$8000,$74(a6)
		rts


;--- CHECK MOUSE ---
checkm:
		move.w	$a(a6),d0
		move.w	d0,d1
		lsr	#8,d1
		sub.b	mold(a5),d1
		ext	d1
		add.w	d1,ymouse(a5)		
		sub.b	mold+1(a5),d0
		ext	d0
		add	d0,xmouse(a5)
		move.w	$a(a6),mold(a5)
		rts
		
;--- SET SPRITE d0=x d1=y ---
setspr:
		lea	spr1,a0
		move.w	xmouse(a5),d0
		move.w	ymouse(a5),d1
		cmp.w	#$80,d0
		bhs.b	.weit
		move.w	#$80,d0
		move.w	d0,xmouse(a5)
.weit:
		cmp.w	#$29,d1
		bhs.b	.weit1
		move.w	#$29,d1
		move.w	d1,ymouse(a5)
.weit1:
		cmp.w	#$1bf,d0
		blo.b	.weit2
		move.w	#$1bf,d0
		move.w	d0,xmouse(a5)
.weit2:
		cmp.w	#$128,d1
		blo.b	.weit3
		move.w	#$128,d1
		move.w	d1,ymouse(a5)
.weit3:
		subq.w	#7,d0		;Sprite offset
		subq.w	#7,d1
		moveq	#0,d3
		move.b	d1,(a0)		;Y erstes Byte
		btst	#8,d1		;y8
		beq.b	no
		or.w	#4,d3
no:
		add.l	#15,d1
		move.b	d1,2(a0)
		btst	#8,d1
		beq.b	noz
		or.w	#2,d3
noz:
		lsr	#1,d0
		bcc.b	nox0
		or.w	#1,d3
nox0:
		move.b	d0,1(a0)
		move.b	d3,3(a0)
		rts

;--- TABELLEN UND DATEN -----------------------------------------------------

loadfile:
		dc.b	'Load File',0
savefile:
		dc.b	'Save File',0

NewScreenStructure:
relxy:		dc.w	0,0	;screen XY origin relative to View
wigth:		dc.w	640,256	;screen width and height
depth:		dc.w	2	;screen depth (number of bitplanes)
		dc.b	0,1	;detail and block pens
dismode:	dc.w	V_HIRES	;display modes for this screen
scrtype:	dc.w	CUSTOMSCREEN	;screen type
		dc.l	0	;pointer to default screen font
		dc.l	0	;screen title
		dc.l	0	;first in list of custom screen gadgets
		dc.l	0	;pointer to custom BitMap structure

NewScreenStructure1:
		dc.w	0,176	;screen XY origin relative to View
		dc.w	640,80	;screen width and height
		dc.w	4	;screen depth (number of bitplanes)
		dc.b	0,1	;detail and block pens
		dc.w	V_HIRES	;display modes for this screen
		dc.w	CUSTOMSCREEN	;screen type
		dc.l	0	;pointer to default screen font
		dc.l	0	;screen title
		dc.l	0	;first in list of custom screen gadgets
		dc.l	0	;pointer to custom BitMap structure

Palette:
		dc.w	$0000
		dc.w	$0ABD
		dc.w	$0348
		dc.w	$0458
		dc.w	$0459
		dc.w	$0569
		dc.w	$067A
		dc.w	$078A
		dc.w	$0236
		dc.w	$0EE0
		dc.w	$0DC0
		dc.w	$0CB0
		dc.w	$0C90
		dc.w	$0FF9
		dc.w	$0C80
		dc.w	$0444

PaletteColorCount equ (*-Palette)/2

NewWindowStructure1:
		dc.w	0,0	;window XY origin relative to TopLeft of screen
wwight		dc.w	640,256	;window width and height
		dc.b	0,1	;detail and block pens
idcmp:		dc.l	GADGETUP+RAWKEY+MOUSEMOVE ;IDCMP flags
wflags:		dc.l	BORDERLESS+ACTIVATE+NOCAREREFRESH+RMBTRAP+REPORTMOUSE
		dc.l	0	;first gadget in gadget list
		dc.l	0	;custom CHECKMARK imagery
		dc.l	0	;window title
scr:		dc.l	0	;custom screen pointer
		dc.l	0	;custom bitmap
		dc.w	5,5	;minimum width and height
		dc.w	-1,-1	;maximum width and height
		dc.w	CUSTOMSCREEN	;destination screen type

NewWindowStructure2:
		dc.w	0,0	;window XY origin relative to TopLeft of screen
		dc.w	640,80	;window width and height
		dc.b	0,1	;detail and block pens
		dc.l	GADGETUP+RAWKEY+MOUSEMOVE ;IDCMP flags
		dc.l	BORDERLESS+ACTIVATE+NOCAREREFRESH+RMBTRAP+REPORTMOUSE	
		dc.l	0	;first gadget in gadget list
		dc.l	0	;custom CHECKMARK imagery
		dc.l	0	;window title
scr1:		dc.l	0	;custom screen pointer
		dc.l	0	;custom bitmap
		dc.w	5,5	;minimum width and height
		dc.w	-1,-1	;maximum width and height
		dc.w	CUSTOMSCREEN	;destination screen type

auswahlWindow:
		dc.w	380,6	;window XY origin relative to TopLeft of screen
		dc.w	170,73	;window width and height
		dc.b	0,1	;detail and block pens
		dc.l	GADGETUP ;IDCMP flags
		dc.l	ACTIVATE+NOCAREREFRESH+RMBTRAP+BORDERLESS	
		dc.l	0	;first gadget in gadget list
		dc.l	0	;custom CHECKMARK imagery
		dc.l	0	;window title
scr2:		dc.l	0	;custom screen pointer
		dc.l	0	;custom bitmap
		dc.w	5,5	;minimum width and height
		dc.w	-1,-1	;maximum width and height
		dc.w	CUSTOMSCREEN	;destination screen type

GadgetList1:
Gscreendrag	;Unsichtbares Gadget damit man screen verschieben kann
		dc.l	Gload	;next gadget
		dc.w	0,0
		dc.w	640,3
		dc.w	GADGHNONE	;gadget flags
		dc.w	0		;activation flags
		dc.w	SDRAGGING	;gadget type flags
		dc.l	0	;gadget border or image to be rendered
		dc.l	0	;alternate imagery for selection
		dc.l	0	;first IntuiText structure
		dc.l	0	;gadget mutual-exclude long word
		dc.l	0	;SpecialInfo structure
		dc.w	0	;user-definable data
		dc.l	0	;pointer to user-definable data
gload:
		dc.l	gloadroutine
		dc.w	372,19
		dc.w	52,12
		dc.w	GADGHIMAGE
		dc.w	RELVERIFY
		dc.w	BOOLGADGET
		dc.l	border1
		dc.l	border2
		dc.l	0
		dc.l	0
		dc.l	0
		dc.w	0
		dc.l	0
gloadroutine:
		dc.l	gpalette
		dc.w	432,19
		dc.w	104,12
		dc.w	GADGHIMAGE
		dc.w	RELVERIFY
		dc.w	BOOLGADGET
		dc.l	border3
		dc.l	border4
		dc.l	IText1
		dc.l	0
		dc.l	0
		dc.w	0
		dc.l	0
IText1:
		dc.b	1,0,RP_JAM1,0	
		dc.w	23,2	
		dc.l	0	
		dc.l	ITextText1	
		dc.l	0	
ITextText1:
		dc.b	'Routine',0

gpalette:
		dc.l	gsave
		dc.w	546,19
		dc.w	77,12
		dc.w	GADGHIMAGE
		dc.w	RELVERIFY
		dc.w	BOOLGADGET
		dc.l	border5
		dc.l	border6
		dc.l	0
		dc.l	0
		dc.l	0
		dc.w	0
		dc.l	0
gsave:
		dc.l	gsaveroutine
		dc.w	372,34
		dc.w	52,12
		dc.w	GADGHIMAGE
		dc.w	RELVERIFY
		dc.w	BOOLGADGET
		dc.l	border1
		dc.l	border2
		dc.l	0
		dc.l	0
		dc.l	0
		dc.w	0
		dc.l	0
gsaveroutine:
		dc.l	gprefs
		dc.w	432,34
		dc.w	104,12
		dc.w	GADGHIMAGE
		dc.w	RELVERIFY
		dc.w	BOOLGADGET
		dc.l	border3
		dc.l	border4
		dc.l	IText2
		dc.l	0
		dc.l	0
		dc.w	0
		dc.l	0
IText2:
		dc.b	1,0,RP_JAM1,0	
		dc.w	23,2	
		dc.l	0	
		dc.l	ITextText2	
		dc.l	0	
ITextText2:
		dc.b	'Routine',0
gprefs:
		dc.l	geffect
		dc.w	546,34
		dc.w	77,12
		dc.w	GADGHIMAGE
		dc.w	RELVERIFY
		dc.w	BOOLGADGET
		dc.l	border5
		dc.l	border6
		dc.l	0
		dc.l	0
		dc.l	0
		dc.w	0
		dc.l	0
geffect:
		dc.l	gfree
		dc.w	372,49
		dc.w	164,12
		dc.w	GADGHIMAGE
		dc.w	RELVERIFY
		dc.w	BOOLGADGET
		dc.l	border7
		dc.l	border8
		dc.l	IText3
		dc.l	0
		dc.l	0
		dc.w	0
		dc.l	0
IText3:
		dc.b	1,0,RP_JAM1,0	
		dc.w	55,2	
		dc.l	0	
		dc.l	ITextText3	
		dc.l	0	
ITextText3:
		dc.b	'Routine',0
gfree:
		dc.l	gexecute
		dc.w	546,49
		dc.w	77,12
		dc.w	GADGHIMAGE
		dc.w	RELVERIFY
		dc.w	BOOLGADGET
		dc.l	border5
		dc.l	border6
		dc.l	0
		dc.l	0
		dc.l	0
		dc.w	0
		dc.l	0
gexecute:
		dc.l	gfree1
		dc.w	420,64
		dc.w	77,12
		dc.w	GADGHIMAGE
		dc.w	RELVERIFY
		dc.w	BOOLGADGET
		dc.l	border5
		dc.l	border6
		dc.l	0
		dc.l	0
		dc.l	0
		dc.w	0
		dc.l	0
gfree1:
		dc.l	gclose
		dc.w	546,64
		dc.w	77,12
		dc.w	GADGHIMAGE
		dc.w	RELVERIFY
		dc.w	BOOLGADGET
		dc.l	border5
		dc.l	border6
		dc.l	0
		dc.l	0
		dc.l	0
		dc.w	0
		dc.l	0
gclose:
		dc.l	gzip
		dc.w	6,3
		dc.w	17,10
		dc.w	GADGHIMAGE+GADGIMAGE
		dc.w	RELVERIFY
		dc.w	BOOLGADGET
		dc.l	Image3
		dc.l	Image4
		dc.l	0
		dc.l	0
		dc.l	0
		dc.w	0
		dc.l	0
Image3:
		dc.w	0,0
		dc.w	17,10
		dc.w	4
		dc.l	ImageData3
		dc.b	$000F,$0000
		dc.l	0
Image4:
		dc.w	0,0	
		dc.w	17,10	
		dc.w	4	
		dc.l	ImageData4
		dc.b	$000F,$0000
		dc.l	0
gzip:
		dc.l	gdrag
		dc.w	593,3
		dc.w	19,10
		dc.w	GADGHIMAGE+GADGIMAGE
		dc.w	RELVERIFY
		dc.w	BOOLGADGET
		dc.l	Image5
		dc.l	Image6
		dc.l	0
		dc.l	0
		dc.l	0
		dc.w	0
		dc.l	0
Image5:
		dc.w	0,0
		dc.w	19,10
		dc.w	4
		dc.l	ImageData5
		dc.b	$000F,$0000
		dc.l	0
Image6:
		dc.w	0,0
		dc.w	19,10
		dc.w	4
		dc.l	ImageData6
		dc.b	$000F,$0000
		dc.l	0
gdrag:
		dc.l	0
		dc.w	614,3
		dc.w	19,10
		dc.w	GADGHIMAGE+GADGIMAGE
		dc.w	RELVERIFY
		dc.w	BOOLGADGET
		dc.l	Image7
		dc.l	Image8
		dc.l	0
		dc.l	0
		dc.l	0
		dc.w	0
		dc.l	0

;----------------------------------------------------------------
;--- Gadgets f�r Auswahlwindow ----------------------------------
;----------------------------------------------------------------
ausGadget1:
		dc.l	gok	;next gadget
		dc.w	152,2	;origin XY of hit box relative to window TopLeft
		dc.w	10,68	;hit box width and height
		dc.w	GADGHIMAGE	;gadget flags
		dc.w	RELVERIFY	;activation flags
		dc.w	PROPGADGET	;gadget type flags
		dc.l	Image1	;gadget border or image to be rendered
		dc.l	0	;alternate imagery for selection
		dc.l	0	;first IntuiText structure
		dc.l	0	;gadget mutual-exclude long word
		dc.l	Gadget1SInfo	;SpecialInfo structure
		dc.w	0	;user-definable data
		dc.l	0	;pointer to user-definable data
Gadget1SInfo:
		dc.w	AUTOKNOB+FREEVERT+PROPBORDERLESS	;PropInfo flags
		dc.w	0,0	;horizontal and vertical pot values
		dc.w	13107,13107	;horizontal and vertical body values
		dc.w	0,0,0,0,0,0     ;Intuition initialized and maintained variables
Image1:
		dc.w	0,0	;XY origin relative to container TopLeft
		dc.w	11,15	;Image width and height in pixels
		dc.w	0	;number of bitplanes in Image
		dc.l	0	;pointer to ImageData
		dc.b	$0000,$0000	;PlanePick and PlaneOnOff
		dc.l	0	;next Image structure

gok:
		dc.l	0
		dc.w	40,58
		dc.w	77,12
		dc.w	GADGHIMAGE
		dc.w	RELVERIFY
		dc.w	BOOLGADGET
		dc.l	border5
		dc.l	border6
		dc.l	okIText1
		dc.l	0
		dc.l	0
		dc.w	0
		dc.l	0
okIText1:
		dc.b	1,0,RP_JAM1,0	
		dc.w	30,2	
		dc.l	0	
		dc.l	okITextText1	
		dc.l	0	
okITextText1:
		dc.b	'OK',0



;--- Image Struckturen ------------------------------------------------
	even
Image7:
		dc.w	0,0
		dc.w	19,10
		dc.w	4
		dc.l	ImageData7
		dc.b	$000F,$0000
		dc.l	0
Image8:
		dc.w	0,0
		dc.w	19,10
		dc.w	4
		dc.l	ImageData8
		dc.b	$000F,$0000
		dc.l	0
Border1:
		dc.w	-1,-1	;XY origin relative to container TopLeft
		dc.b	1,0,RP_JAM1	;front pen, back pen and drawmode
		dc.b	3	;number of XY vectors
		dc.l	BorderVectors1	;pointer to XY vectors
		dc.l	Border11	;next border in list
BorderVectors1:
		dc.w	0,12
		dc.w	0,0
		dc.w	51,0
Border11:
		dc.w	-1,-1
		dc.b	1,0,RP_JAM1
		dc.b	2
		dc.l	BorderVectors11
		dc.l	Border12
BorderVectors11:
		dc.w	1,11
		dc.w	1,0
Border12:
		dc.w	-1,-1
		dc.b	8,0,RP_JAM1
		dc.b	3
		dc.l	BorderVectors12
		dc.l	Border13
BorderVectors12:
		dc.w	52,0
		dc.w	52,12
		dc.w	1,12
Border13:
		dc.w	-1,-1
		dc.b	8,0,RP_JAM1
		dc.b	2
		dc.l	BorderVectors13
		dc.l	0
BorderVectors13:
		dc.w	51,1
		dc.w	51,12
	
;--- Border beim Anklicken
Border2:
		dc.w	-1,-1	;XY origin relative to container TopLeft
		dc.b	8,0,RP_JAM1	;front pen, back pen and drawmode
		dc.b	3	;number of XY vectors
		dc.l	BorderVectors2	;pointer to XY vectors
		dc.l	Border21	;next border in list
BorderVectors2:
		dc.w	0,12
		dc.w	0,0
		dc.w	51,0
Border21:
		dc.w	-1,-1
		dc.b	8,0,RP_JAM1
		dc.b	2
		dc.l	BorderVectors21
		dc.l	Border22
BorderVectors21:
		dc.w	1,11
		dc.w	1,0
Border22:
		dc.w	-1,-1
		dc.b	1,0,RP_JAM1
		dc.b	3
		dc.l	BorderVectors22
		dc.l	Border23
BorderVectors22:
		dc.w	52,0
		dc.w	52,12
		dc.w	1,12
Border23:
		dc.w	-1,-1
		dc.b	1,0,RP_JAM1
		dc.b	2
		dc.l	BorderVectors23
		dc.l	0
BorderVectors23:
		dc.w	51,1
		dc.w	51,12

;--- Borderstrucktur f�r die Routinegads ---

Border3:
		dc.w	-1,-1	;XY origin relative to container TopLeft
		dc.b	1,0,RP_JAM1	;front pen, back pen and drawmode
		dc.b	3	;number of XY vectors
		dc.l	BorderVectors3	;pointer to XY vectors
		dc.l	Border31	;next border in list
BorderVectors3:
		dc.w	0,12
		dc.w	0,0
		dc.w	103,0
Border31:
		dc.w	-1,-1
		dc.b	1,0,RP_JAM1
		dc.b	2
		dc.l	BorderVectors31
		dc.l	Border32
BorderVectors31:
		dc.w	1,11
		dc.w	1,0
Border32:
		dc.w	-1,-1
		dc.b	8,0,RP_JAM1
		dc.b	3
		dc.l	BorderVectors32
		dc.l	Border33
BorderVectors32:
		dc.w	104,0
		dc.w	104,12
		dc.w	1,12
Border33:
		dc.w	-1,-1
		dc.b	8,0,RP_JAM1
		dc.b	2
		dc.l	BorderVectors33
		dc.l	0
BorderVectors33:
		dc.w	103,1
		dc.w	103,12

;--- Border beim Anklicken
Border4:
		dc.w	-1,-1	;XY origin relative to container TopLeft
		dc.b	8,0,RP_JAM1	;front pen, back pen and drawmode
		dc.b	3	;number of XY vectors
		dc.l	BorderVectors4	;pointer to XY vectors
		dc.l	Border41	;next border in list
BorderVectors4:
		dc.w	0,12
		dc.w	0,0
		dc.w	103,0
Border41:
		dc.w	-1,-1
		dc.b	8,0,RP_JAM1
		dc.b	2
		dc.l	BorderVectors41
		dc.l	Border42
BorderVectors41:
		dc.w	1,11
		dc.w	1,0
Border42:
		dc.w	-1,-1
		dc.b	1,0,RP_JAM1
		dc.b	3
		dc.l	BorderVectors42
		dc.l	Border43
BorderVectors42:
		dc.w	104,0
		dc.w	104,12
		dc.w	1,12
Border43:
		dc.w	-1,-1
		dc.b	1,0,RP_JAM1
		dc.b	2
		dc.l	BorderVectors43
		dc.l	0
BorderVectors43:
		dc.w	103,1
		dc.w	103,12

;--- Border f�r Palette ect. ---
Border5:
		dc.w	-1,-1	;XY origin relative to container TopLeft
		dc.b	1,0,RP_JAM1	;front pen, back pen and drawmode
		dc.b	3	;number of XY vectors
		dc.l	BorderVectors5	;pointer to XY vectors
		dc.l	Border51	;next border in list
BorderVectors5:
		dc.w	0,12
		dc.w	0,0
		dc.w	76,0
Border51:
		dc.w	-1,-1
		dc.b	1,0,RP_JAM1
		dc.b	2
		dc.l	BorderVectors51
		dc.l	Border52
BorderVectors51:
		dc.w	1,11
		dc.w	1,0
Border52:
		dc.w	-1,-1
		dc.b	8,0,RP_JAM1
		dc.b	3
		dc.l	BorderVectors52
		dc.l	Border53
BorderVectors52:
		dc.w	77,0
		dc.w	77,12
		dc.w	1,12
Border53:
		dc.w	-1,-1
		dc.b	8,0,RP_JAM1
		dc.b	2
		dc.l	BorderVectors53
		dc.l	0
BorderVectors53:
		dc.w	76,1
		dc.w	76,12
	
;--- Border beim Anklicken
Border6:
		dc.w	-1,-1	;XY origin relative to container TopLeft
		dc.b	8,0,RP_JAM1	;front pen, back pen and drawmode
		dc.b	3	;number of XY vectors
		dc.l	BorderVectors6	;pointer to XY vectors
		dc.l	Border61	;next border in list
BorderVectors6:
		dc.w	0,12
		dc.w	0,0
		dc.w	76,0
Border61:
		dc.w	-1,-1
		dc.b	8,0,RP_JAM1
		dc.b	2
		dc.l	BorderVectors61
		dc.l	Border62
BorderVectors61:
		dc.w	1,11
		dc.w	1,0
Border62:
		dc.w	-1,-1
		dc.b	1,0,RP_JAM1
		dc.b	3
		dc.l	BorderVectors62
		dc.l	Border63
BorderVectors62:
		dc.w	77,0
		dc.w	77,12
		dc.w	1,12
Border63:
		dc.w	-1,-1
		dc.b	1,0,RP_JAM1
		dc.b	2
		dc.l	BorderVectors63
		dc.l	0
BorderVectors63:
		dc.w	76,1
		dc.w	76,12

;--- Border f�r Effect ---
Border7:
		dc.w	-1,-1	;XY origin relative to container TopLeft
		dc.b	1,0,RP_JAM1	;front pen, back pen and drawmode
		dc.b	3	;number of XY vectors
		dc.l	BorderVectors7	;pointer to XY vectors
		dc.l	Border71	;next border in list
BorderVectors7:
		dc.w	0,12
		dc.w	0,0
		dc.w	163,0
Border71:
		dc.w	-1,-1
		dc.b	1,0,RP_JAM1
		dc.b	2
		dc.l	BorderVectors71
		dc.l	Border72
BorderVectors71:
		dc.w	1,11
		dc.w	1,0
Border72:
		dc.w	-1,-1
		dc.b	8,0,RP_JAM1
		dc.b	3
		dc.l	BorderVectors72
		dc.l	Border73
BorderVectors72:
		dc.w	164,0
		dc.w	164,12
		dc.w	1,12
Border73:
		dc.w	-1,-1
		dc.b	8,0,RP_JAM1
		dc.b	2
		dc.l	BorderVectors73
		dc.l	0
BorderVectors73:
		dc.w	163,1
		dc.w	163,12
	
;--- Border beim Anklicken
Border8:
		dc.w	-1,-1	;XY origin relative to container TopLeft
		dc.b	8,0,RP_JAM1	;front pen, back pen and drawmode
		dc.b	3	;number of XY vectors
		dc.l	BorderVectors8	;pointer to XY vectors
		dc.l	Border81	;next border in list
BorderVectors8:
		dc.w	0,12
		dc.w	0,0
		dc.w	163,0
Border81:
		dc.w	-1,-1
		dc.b	8,0,RP_JAM1
		dc.b	2
		dc.l	BorderVectors81
		dc.l	Border82
BorderVectors81:
		dc.w	1,11
		dc.w	1,0
Border82:
		dc.w	-1,-1
		dc.b	1,0,RP_JAM1
		dc.b	3
		dc.l	BorderVectors82
		dc.l	Border83
BorderVectors82:
		dc.w	164,0
		dc.w	164,12
		dc.w	1,12
Border83:
		dc.w	-1,-1
		dc.b	1,0,RP_JAM1
		dc.b	2
		dc.l	BorderVectors83
		dc.l	0
BorderVectors83:
		dc.w	163,1
		dc.w	163,12


;--- Borderstrucktur f�r linkes Statuswindow ---

SBorder:
		dc.w	29,4	;XY origin relative to container TopLeft
		dc.b	8,0,RP_JAM1	;front pen, back pen and drawmode
		dc.b	3	;number of XY vectors
		dc.l	SBorderVectors	;pointer to XY vectors
		dc.l	SBorder1;next border in list
SBorderVectors:
		dc.w	0,70
		dc.w	0,0
		dc.w	334,0
SBorder1:
		dc.w	29,4	
		dc.b	8,0,RP_JAM1	
		dc.b	2	
		dc.l	SBorderVectors1	
		dc.l	SBorder2	
SBorderVectors1:
		dc.w	1,69
		dc.w	1,0
SBorder2:
		dc.w	29,4	
		dc.b	1,0,RP_JAM1	
		dc.b	3	
		dc.l	SBorderVectors2	
		dc.l	SBorder3	
SBorderVectors2:
		dc.w	335,0
		dc.w	335,70
		dc.w	1,70
SBorder3:
		dc.w	29,4	
		dc.b	1,0,RP_JAM1	
		dc.b	2	
		dc.l	SBorderVectors3	
		dc.l	artpro	
SBorderVectors3:
		dc.w	334,1
		dc.w	334,70

;--- Border f�r Titelleiste ---

artpro:
		dc.w	372,3	
		dc.b	1,0,RP_JAM1	
		dc.b	3	
		dc.l	artVectors	
		dc.l	artpro1	
artvectors:
		dc.w	0,11
		dc.w	0,0
		dc.w	212,0
artpro1:		
		dc.w	372,3	
		dc.b	1,0,RP_JAM1	
		dc.b	2	
		dc.l	artVectors1	
		dc.l	artpro2	
artvectors1:
		dc.w	1,10
		dc.w	1,0
artpro2:
		dc.w	372,3	
		dc.b	8,0,RP_JAM1	
		dc.b	3	
		dc.l	artVectors2
		dc.l	artpro3	
artVectors2:
		dc.w	213,0
		dc.w	213,11
		dc.w	1,11
artpro3:
		dc.w	372,3	
		dc.b	8,0,RP_JAM1	
		dc.b	2	
		dc.l	artVectors3
		dc.l	0	
artVectors3:
		dc.w	212,1
		dc.w	212,10


;---------------------------------------------------------------------------
;--- Borders f�r auswahlwindow ---------------------------------------------
;---------------------------------------------------------------------------
winborder:
		dc.w	0,0	;XY origin relative to container TopLeft
		dc.b	1,0,RP_JAM1	;front pen, back pen and drawmode
		dc.b	3	;number of XY vectors
		dc.l	winVectors1	;pointer to XY vectors
		dc.l	winBorder1	;next border in list
winVectors1:
		dc.w	0,73
		dc.w	0,0
		dc.w	169,0
winBorder1:
		dc.w	0,0
		dc.b	1,0,RP_JAM1
		dc.b	2
		dc.l	winVectors12
		dc.l	winBorder12
winVectors12:
		dc.w	1,72
		dc.w	1,0
winBorder12:
		dc.w	-1,-1
		dc.b	8,0,RP_JAM1
		dc.b	3
		dc.l	winVectors13
		dc.l	winBorder13
winVectors13:
		dc.w	170,0
		dc.w	170,73
		dc.w	1,73
winBorder13:
		dc.w	-1,-1
		dc.b	8,0,RP_JAM1
		dc.b	2
		dc.l	winVectors14
		dc.l	rborder
winVectors14:
		dc.w	169,1
		dc.w	169,70
 		
;--- Border f�r Rahmen in auswahlwindow ---

rBorder:
		dc.w	7,3	;XY origin relative to container TopLeft
		dc.b	8,0,RP_JAM1	;front pen, back pen and drawmode
		dc.b	3	;number of XY vectors
		dc.l	rBorderVectors1	;pointer to XY vectors
		dc.l	rBorder1	;next border in list
rBorderVectors1:
		dc.w	0,50
		dc.w	0,0
		dc.w	136,0
rBorder1:
		dc.w	7,3
		dc.b	8,0,RP_JAM1
		dc.b	2
		dc.l	rBorderVectors11
		dc.l	rBorder11
rBorderVectors11:
		dc.w	1,49
		dc.w	1,0
rBorder11:
		dc.w	7,3
		dc.b	1,0,RP_JAM1
		dc.b	3
		dc.l	rBorderVectors12
		dc.l	rBorder12
rBorderVectors12:
		dc.w	137,0
		dc.w	137,50
		dc.w	1,50
rBorder12:
		dc.w	7,3
		dc.b	1,0,RP_JAM1
		dc.b	2
		dc.l	rBorderVectors13
		dc.l	0
rBorderVectors13:
		dc.w	136,1
		dc.w	136,50




;---------------------------------------------------------------------------
;--- Textstruckturen  f�r Gadgets ------------------------------------------
;---------------------------------------------------------------------------
textlist:

arttext:	dc.w	377,5
		dc.l	textart
		dc.l	loadtext
textart:
		dc.b	'ART-Professional  V0.001b',0
	even
loadtext:
		dc.w	382,21	;x,y
		dc.l	textload
		dc.l	savetext
textload:
		dc.b	'Load',0
	even
savetext:
		dc.w	382,36	
		dc.l	textsave
		dc.l	palettetext
textsave:
		dc.b	'Save',0
	even
palettetext:
		dc.w	556,21	
		dc.l	textpalette
		dc.l	prefstext
textpalette:
		dc.b	'Palette',0
	even
prefstext:
		dc.w	565,36	
		dc.l	textprefs
		dc.l	freitext
textprefs:
		dc.b	'Prefs',0
	even
freitext:
		dc.w	569,51	
		dc.l	textfrei
		dc.l	frei1text
textfrei:
		dc.b	'Frei',0
	even
frei1text:
		dc.w	569,66	
		dc.l	textfrei1
		dc.l	exetext
textfrei1:
		dc.b	'Frei',0
	even
exetext:
		dc.w	431,66	
		dc.l	textexe
		dc.l	0
textexe:
		dc.b	'Execute',0

		section	daten,BSS

daten:

;--- System ---
gfxbase:	rs.l	1
dosbase:	rs.l	1
reqbase:	rs.l	1
intbase:	rs.l	1
filehandle:	rs.l	1
oldcop:		rs.l	1
picmem:		rs.l	1
memsize:	rs.l	1
loadbody:	rs.l	1
bodylaenge:	rs.l	1
screenbase:	rs.l	1
screenbase1:	rs.l	1
winbase:	rs.l	1
winbase1:	rs.l	1
chwinbase:	rs.l	1
loadflag:	rs.w	1
newcopper:	rs.w	1
ucopperlist:	rs.l	1
highadd:	rs.l	1
wideadd:	rs.l	1
Uviewport:	rs.l	1
Uviewport1:	rs.l	1
URastport:	rs.l	1
URastport1:	rs.l	1

;--- Intern ---
ymouse:		rs.w	1
xmouse:		rs.w	1
mold:		rs.w	1
xmousereal:	rs.w	1
ymousereal:	rs.w	1
oldx:		rs.w	1
oldy:		rs.w	1
oldxreal:	rs.w	1
oldyreal:	rs.w	1
xbox:		rs.w	1
ybox:		rs.w	1
xboxreal:	rs.w	1
yboxreal:	rs.w	1
boxsize:	rs.w	1
oldrast:	rs.w	1
newrast:	rs.w	1

;--- Picdaten ---
breite:		rs.w	1
hoehe:		rs.w	1
planes:		rs.b	1
testbyte:	rs.b	1
crunchmode:	rs.b	1
leer:		rs.b	1
transcolor:	rs.w	1
verhaeltnis:	rs.w	1
xresolution:	rs.w	1
yresolution:	rs.w	1
viewmode:	rs.w	1
bytebreite:	rs.l	1
oneplanesize:	rs.l	1
hires:		rs.b	1
lace:		rs.b	1
screenbreite:	rs.w	1
screenhight:	rs.w	1
screenbyte:	rs.w	1
bmodulo:	rs.l	1
modulo:		rs.w	1
lmodulo:	rs.w	1
screenmode:	rs.w	1
ddf1:		rs.l	1
ddf2:		rs.l	1
anzcolor:	rs.w	1

ReqPuffer:

Version		rs.w	1			;Version
windowtitle	rs.l	1			;Zeiger auf Titletext
Dir		rs.l	1			;Zeiger auf Dir
File		rs.l	1			;Zeiger auf File
Pathname	rs.l	1			;Zeiger auf Pathname
rp:
window1		rs.l	1			;Zeiger Window
maxExtendedSe	rs.w	1			;MaxExtendetSelect
NumLines	rs.w	1			;Anzahl der Linien in Filerequester
NumColumss	rs.w	1			;Zeichenl�nge ? in Filerequester
DevColumns	rs.w	1			;Zeichenl�nge ? in Devicewindow
Flags		rs.l	1			;Flags

Dirnamecolor	rs.w	1			;Dirnamencolor
Filenamecolor	rs.w	1			;Filenamencolor
Devicenamecolr	rs.w	1			;Devicenamencolor
Fontnamecolor	rs.w	1			;Fontnamencolor
fontsizecolor	rs.w	1			;color 32 or some other colors

detailcolor	rs.w	1			;Detailcolor
blockcolor	rs.w	1			;Blockcolor

gadgettextcol	rs.w	1			;Color f�r 5 boolean gadgets
textmessagecol	rs.w	1			;Color f�r Top-Text
stringnamecol	rs.w	1			;Color f�r Wort-Schreiber
stringgadgetco	rs.w	1			;BorderGadgetsColor	Hidewindow
bboxbordercolor	rs.w	1			;BoxBorderColor-	Filewindow
gadgetboxcolor	rs.w	1			;Gadgetboxcolor-	OK-Gadget

Struct1		rs.b	36			;frq_rfu_stuf
Struct2:	rs.b	12			;frq_dirStateStamp

WindowleftEdge	rs.w	1			;linke Ecke des Requesters
WindowTopEdge	rs.w	1			;WindowTopEdge

FontYSize	rs.w	1			;Returnfeld FontYSize (H�he)
Fontstyle	rs.w	1			;Schriftparameter (Kursiv,Fett)

ExtentedSelect	rs.l	1			;ExtendetSelect

Struct3:	rs.b	32			;frq_Hide,Wildlenght+2
Struct4:	rs.b	32			;frq_Show,Wildlenght+2

FilePufferPos	rs.w	1			;FilepufferPosition
FileDispPos	rs.w	1			;FileDispPosition
DirBufferPos	rs.w	1			;DirPufferposition
DirDispPos	rs.w	1			;DirDispPosition
HideBufferPos	rs.w	1			;HidePufferposition
HideDispPos	rs.w	1			;HideDispPos
ShowBufferPos	rs.w	1			;ShowBufferPos
ShowDispPos	rs.w	1			;ShowDispPos

Memory		rs.l	1			;Memoryadr f�r Directories
Memory2		rs.l	1			;Memory2adr f�r Hidden-Files
.Lock:		rs.l	1			;frq_lock

Struct5:	rs.b	132			;PrivaterDirPuffer,dsize+2

FileInfoBlock	rs.l	1			;FileInfoblockadr
NumEntries	rs.w	1			;NumEntries
NumHiddenEnt	rs.w	1			;NumHiddenEntries
FileStartnum	rs.w	1			;FilesStartnumber
Devicestartnum	rs.w	1			;DeviceStartnumber
	
dataslen:	rs.w	0
		ds.b	dataslen

Filepuffer:	ds.b	162
Dirname:	ds.b	30	
Filename:	ds.b	20

bytepuffer:	ds.b	4
chunkpuffer:	ds.b	150

farbtab:	ds.w	256

mymap:
		ds.b	8+4*4
fontmap:
		ds.b	8+4*4
picmap:
		ds.b	8+4*10

		section	copper,data_c

;--- Copperlist for Loresscreens ---

;--- Mousepointer ---

spr1:		dc.w	$0000,$0000
		dc.w	$0100,$0000,$0100,$0100
		dc.w	$0100,$0000,$0100,$0100
		dc.w	$0100,$0000,$0000,$0000
		dc.w	$0000,$0000,$F83E,$5114
		dc.w	$0000,$0000,$0000,$0000
		dc.w	$0100,$0000,$0100,$0100
		dc.w	$0100,$0000,$0100,$0100
		dc.w	$0100,$0000
		dc.w	$0000,$0000


oberflaeche:
		incdir	sources:converter/old/
		incbin	oberflaeche.640*80.raw
font:
		incbin	font.640*16.raw
fontmask:
		incbin	font.640*16.mask
		
ImageData3:
	dc.w	$FFFF,$8000,$FE80,$0000,$EFFE,$0000,$F012,$0000
	dc.w	$E072,$0000,$FDFE,$0000,$F3FE,$0000,$FE00,$0000
	dc.w	$DC1E,$0000,$0000,$0000,$0000,$0000,$387E,$0000
	dc.w	$0000,$0000,$0C0C,$0000,$1D8C,$0000,$0000,$0000
	dc.w	$0000,$0000,$01FE,$0000,$23FE,$0000,$0000,$0000
	dc.w	$0000,$0000,$07FE,$0000,$3FFE,$0000,$3C1E,$0000
	dc.w	$3D9E,$0000,$3D9E,$0000,$3C1E,$0000,$3FFE,$0000
	dc.w	$3FFE,$0000,$0000,$0000,$0000,$0000,$0001,$8000
	dc.w	$0001,$8000,$03E1,$8000,$0201,$8000,$0201,$8000
	dc.w	$0001,$8000,$0001,$8000,$0001,$8000,$FFFF,$8000
ImageData4:
	dc.w	$0000,$0000,$3E81,$8000,$2FFF,$8000,$33F3,$8000
	dc.w	$2213,$8000,$3F9F,$8000,$301F,$8000,$3E01,$8000
	dc.w	$1C1F,$8000,$FFFF,$8000,$0000,$0000,$387E,$0000
	dc.w	$0000,$0000,$0C0C,$0000,$1D8C,$0000,$0000,$0000
	dc.w	$0000,$0000,$01FE,$0000,$23FE,$0000,$0000,$0000
	dc.w	$0000,$0000,$07FE,$0000,$3FFE,$0000,$3C1E,$0000
	dc.w	$3D9E,$0000,$3D9E,$0000,$3C1E,$0000,$3FFE,$0000
	dc.w	$3FFE,$0000,$0000,$0000,$FFFF,$8000,$C000,$0000
	dc.w	$C000,$0000,$C000,$0000,$C060,$0000,$C060,$0000
	dc.w	$C3E0,$0000,$C000,$0000,$C000,$0000,$0000,$0000
ImageData5:
	dc.w	$FFFF,$C000,$ED4E,$8000,$FFFE,$8000,$FCE1,$0000
	dc.w	$EFF9,$8000,$FC71,$8000,$FC39,$8000,$F001,$8000
	dc.w	$FF9B,$8000,$8000,$0000,$0000,$0000,$03BF,$0000
	dc.w	$0001,$0000,$0338,$0000,$0020,$0000,$0000,$0000
	dc.w	$0180,$0000,$2000,$0000,$3004,$0000,$0000,$0000
	dc.w	$0000,$0000,$3FFF,$8000,$3001,$8000,$3339,$8000
	dc.w	$3039,$8000,$33F9,$8000,$3279,$8000,$1001,$8000
	dc.w	$0FFF,$8000,$0000,$0000,$0000,$2000,$0000,$6000
	dc.w	$0000,$6000,$0006,$6000,$0006,$6000,$0006,$6000
	dc.w	$0006,$6000,$0FFE,$6000,$0000,$6000,$7FFF,$E000
ImageData6:
	dc.w	$0000,$2000,$2D4E,$E000,$3FFE,$E000,$3CE1,$6000
	dc.w	$2FF9,$E000,$3C71,$E000,$3C39,$E000,$3001,$E000
	dc.w	$3F9B,$E000,$7FFF,$E000,$0000,$0000,$03BF,$0000
	dc.w	$0001,$0000,$0338,$0000,$0020,$0000,$0000,$0000
	dc.w	$0180,$0000,$2000,$0000,$3004,$0000,$0000,$0000
	dc.w	$0000,$0000,$3FFF,$8000,$3001,$8000,$3339,$8000
	dc.w	$3039,$8000,$33F9,$8000,$3279,$8000,$1001,$8000
	dc.w	$0FFF,$8000,$0000,$0000,$FFFF,$C000,$C000,$0000
	dc.w	$C000,$0000,$C006,$0000,$C006,$0000,$C006,$0000
	dc.w	$C006,$0000,$CFFE,$0000,$C000,$0000,$8000,$0000
ImageData7:
	dc.w	$FFFF,$C000,$E120,$0000,$CFFB,$0000,$DCAA,$0000
	dc.w	$DDFE,$0000,$C18D,$0000,$D190,$0000,$FC01,$0000
	dc.w	$C32F,$8000,$8000,$0000,$0000,$0000,$1FDF,$8000
	dc.w	$3007,$8000,$2307,$8000,$2201,$8000,$3069,$8000
	dc.w	$3E61,$8000,$3E00,$8000,$3CC0,$0000,$0000,$0000
	dc.w	$0000,$0000,$3E3F,$8000,$300F,$8000,$33EF,$8000
	dc.w	$3201,$8000,$3075,$8000,$3E7D,$8000,$3E01,$8000
	dc.w	$3FFF,$8000,$0000,$0000,$0000,$2000,$0000,$6000
	dc.w	$0000,$6000,$0010,$6000,$0000,$6000,$0E02,$6000
	dc.w	$0002,$6000,$01FE,$6000,$0000,$6000,$7FFF,$E000
ImageData8:
	dc.w	$0000,$2000,$2120,$6000,$0FFB,$6000,$1CAA,$6000
	dc.w	$1C6E,$6000,$000D,$6000,$1190,$6000,$3C01,$6000
	dc.w	$032F,$E000,$7FFF,$E000,$0000,$0000,$1FDF,$8000
	dc.w	$3007,$8000,$2307,$8000,$22A1,$8000,$3009,$8000
	dc.w	$3E61,$8000,$3E00,$8000,$3CC0,$0000,$0000,$0000
	dc.w	$0000,$0000,$3E3F,$8000,$300F,$8000,$33EF,$8000
	dc.w	$33E1,$8000,$3005,$8000,$3E7D,$8000,$3E01,$8000
	dc.w	$3FFF,$8000,$0000,$0000,$FFFF,$C000,$C000,$0000
	dc.w	$C000,$0000,$C010,$0000,$C010,$0000,$CFF2,$0000
	dc.w	$C002,$0000,$C1FE,$0000,$C000,$0000,$8000,$0000
imageend:
