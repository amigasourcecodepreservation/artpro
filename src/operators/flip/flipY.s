����  ��&b�&b�&b�&b�&b�&b�&b�&b;=1�;===========================================================================
;
;		Flipt einen Ausschnitt oder Bild um x axe
;		(c) 1997 ,Frank (Copper) Pagels  /DFT
;
;		19.10.97

		incdir 	include:

		include	exec/exec_lib.i
		include	exec/lists.i
		include	exec/memory.i
		include intuition/intuition_lib.i
		include	libraries/wb_lib.i
		include	libraries/reqtools_lib.i
		include	graphics/graphics_lib.i
		Include utility/tagitem.i
		include	dos/dos.i		
		include	dos/dos_lib.i		

		include misc/artpro.i
		include render/render_lib.i
		include render/render.i
		

		OPERATORHEADER FL_NODE

		dc.b	'$VER: Flip Y Operator Modul 1.0 (05.01.98)',0
	even
;---------------------------------------------------------------------------
FL_NODE:
		dc.l	0,0
		dc.b	0,0
		dc.l	FL_name
		dc.l	FL_Party
		dc.b	'FLYP'
		dc.b	'EXTR'	;for extern loader
		dc.l	0
		dc.l	FL_Tags
FL_name:
		dc.b	'Flip Y',0
	even
;---------------------------------------------------------------------------

FL_Tags:
		dc.l	APT_Creator,FL_Creator
		dc.l	APT_Version,5
		dc.l	APT_Info,FL_Info
		dc.l	TAG_DONE

FL_Creator:
		dc.b	'(c) 1998 Frank Pagels /DEFECT Softworks',0
	even
FL_Info:
		dc.b	'Flip Y',10,10
		dc.b	'Flip a selected area or the whole',10
		dc.b	'image around the Y axis',0 
	even

FL_chunkymem	=	APG_Free2

;---------------------------------------------------------------------------
FL_Party:

		tst.w	APG_Cutflag(a5)
		bne.b	.brushok
		move.w	#0,APG_xbrush1(a5)	;Es soll das ganze 
		move.w	#0,APG_ybrush1(a5)	;Pic abgesaved werden
		move.w	APG_ImageWidth(a5),APG_xbrush2(a5)
		move.w	APG_ImageHeight(a5),APG_ybrush2(a5)	
.brushok:

;---------------------------------------------------------------------------------
FL_normal:
		move.b	APG_HAM_FLag(a5),d0
		add.b	APG_HAM8_FLag(a5),d0
		tst.b	d0
		bne.w	FL_makeham

		tst.l	APG_rndr_rendermem(a5)
		bne.w	fl_true
		
		move.l	APG_ByteWidth(a5),d0
		lsl.l	#3,d0
		move.l	d0,d7
		mulu	APG_ImageHeight(a5),d0
		move.l	d0,APG_Free1(a5)

		move.l	#MEMF_ANY+MEMF_CLEAR,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)
		move.l	d0,FL_chunkymem(a5)
		bne.b	.memok
		moveq	#APOE_NOMEM,d3
		rts

.memok:
		
;m�ch chunkys

		lea	FL_bitmaptab,a1	;puffer f�r BitmapPointerTab
		move.l	a1,a0
		move.l	APG_Picmem(a5),d0
		moveq	#0,d1
		move.b	APG_Planes(a5),d1		
		subq.w	#1,d1
.ncp:		move.l	d0,(a1)+	;erzeuge bitmappointer Tab
		add.l	APG_Oneplanesize(a5),d0
		dbra	d1,.ncp

		move.l	FL_chunkymem(a5),a1	;Chunky-Buffer
		move.l	APG_bytewidth(a5),d0
		move.w	APG_ImageHeight(a5),d1
		moveq	#0,d2
		move.b	APG_Planes(a5),d2
		move.l	d0,d3
		move.l	APG_Renderbase(a5),a6

		sub.l	a2,a2

		jsr	_LVOPlanar2Chunky(a6)

;flippe bereich um die y achse

		move.w	APG_ybrush2(a5),d4
		sub.w	APG_ybrush1(a5),d4
		subq.w	#1,d4
		lsr.w	#1,d4
		move.w	APG_xbrush2(a5),d7
		sub.w	APG_xbrush1(a5),d7
		subq.w	#1,d7
		move.w	d7,d5
		move.l	APG_bytewidth(a5),d6
		lsl.l	#3,d6
		move.l	FL_chunkymem(a5),a0
		move.l	a0,a1
		moveq	#0,d2
		move.w	APG_ybrush1(a5),d2
		mulu	d6,d2
		add.l	d2,a0		;erste zeile einstellen
		move.w	APG_ybrush2(a5),d3
		subq.w	#1,d3
		mulu	d6,d3
		add.l	d3,a1		;letzte zeile einstellen
.nl:
		move.w	APG_xbrush1(a5),d0
.nx
		move.b	(a0,d0.l),d3
		move.b	(a1,d0.l),(a0,d0.l)
		move.b	d3,(a1,d0.l)
		addq.w	#1,d0
		dbra	d7,.nx
		add.l	d6,a0
		sub.l	d6,a1
		move.w	d5,d7
		dbra	d4,.nl		

;mappe wieder zu planar

		jsr	APR_InitPicMap(a5)

		move.l	FL_chunkymem(a5),a0
		moveq	#0,d0
		moveq	#0,d1
		move.l	APG_ByteWidth(a5),d2
		lsl.l	#3,d2
		move.w	APG_ImageHeight(a5),d3
		moveq	#0,d4
		moveq	#0,d5
		sub.l	a2,a2
		lea	APG_Picmap(a5),a1
		move.l	APG_renderbase(a5),a6
		jsr	_LVOChunky2BitmapA(a6)
				
		move.l	fl_chunkymem(a5),a1
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)
		clr.l	fl_chunkymem(a5)
		
		jsr	APR_MakePreview(a5)

		moveq	#0,d3
		rts

;-------------------------------------------------------------------------
;flippe mit truecolor daten
fl_true:

;flippe bereich um die x achse

		move.w	APG_ybrush2(a5),d4
		sub.w	APG_ybrush1(a5),d4
		subq.w	#1,d4
		lsr.w	#1,d4
		move.w	APG_xbrush2(a5),d7
		sub.w	APG_xbrush1(a5),d7
		subq.w	#1,d7
		move.w	d7,d5
		move.l	APG_bytewidth(a5),d6
		lsl.l	#3+2,d6
		move.l	APG_rndr_rgb(a5),a0
		move.l	a0,a1
		moveq	#0,d2
		move.w	APG_ybrush1(a5),d2
		mulu	d6,d2
		add.l	d2,a0		;erste zeile einstellen
		move.w	APG_ybrush2(a5),d3
		subq.w	#1,d3
		mulu	d6,d3
		add.l	d3,a1		;letzte zeile einstellen
.nl:
		moveq	#0,d0
		move.w	APG_xbrush1(a5),d0
.nx
		move.l	(a0,d0.l*4),d3
		move.l	(a1,d0.l*4),(a0,d0.l*4)
		move.l	d3,(a1,d0.l*4)
		addq.w	#1,d0
		dbra	d7,.nx
		add.l	d6,a0
		sub.l	d6,a1
		move.w	d5,d7
		dbra	d4,.nl		

		move.b	APG_HAM_FLag(a5),d0
		add.b	APG_HAM8_FLag(a5),d0
		tst.b	d0
		beq.b	.noham

		jsr	APR_Render(a5)

.noham:
		jsr	APR_MakePreview(a5)

		moveq	#0,d3
		rts

;----------------------------------------------------------------------------
FL_makeham:
		tst.l	APG_rndr_rendermem(a5)
		bne.b	fl_true			:wir haben schon rgb daten

		jsr	APR_MakeRGBData(a5)
		
		bra.b	fl_true
		
;----------------------------------------------------------------------------


		section	laber,bss
FL_bitmaptab:	ds.l	8
