����  G�&b�&b�&b�&b�&b�&b�&b�&b;=9�;===========================================================================
;
;		Z�hlt Anzahl der Farben in einem Bild
;		(c) 1997 ,Frank (Copper) Pagels  /DFT
;
;		15.07.97

		incdir 	include:

		include	exec/exec_lib.i
		include	exec/lists.i
		include	exec/memory.i
		include intuition/intuition_lib.i
		include	libraries/wb_lib.i
		include	libraries/reqtools_lib.i
		include	graphics/graphics_lib.i
		Include utility/tagitem.i
		include	dos/dos.i		
		include	dos/dos_lib.i		

		include misc/artpro.i
		include render/render_lib.i
		include render/render.i
		

		OPERATORHEADER CC_NODE

		dc.b	'$VER: Count Colors Operator Modul 0.9 (22.07.97)',0
	even
;---------------------------------------------------------------------------
CC_NODE:
		dc.l	0,0
		dc.b	0,0
		dc.l	CC_name
		dc.l	CC_Party
		dc.b	'CCOP'
		dc.b	'EXTR'	;for extern loader
		dc.l	0
		dc.l	CC_Tags
CC_name:
		dc.b	'Count Colors',0
	even
;---------------------------------------------------------------------------

CC_Tags:
		dc.l	APT_Creator,CC_Creator
		dc.l	APT_Version,5
		dc.l	TAG_DONE

CC_Creator:
		dc.b	'(c) 1997 Frank Pagels /DEFECT Softworks',0
	even

CC_Histo:	dc.l	0
cc_colors:	dc.l	0

cc_chunkymem	=	APG_Free2
cc_counttab	=	APG_Free3
cc_rgb		=	APG_Free4

;---------------------------------------------------------------------------
CC_Party:

;create Histogramm

		move.l	#TAG_DONE,-(a7)
		move.l	#HSTYPE_24Bit,-(a7)
		move.l	#RND_HSTYPE,-(a7)
		move.l	a7,a1
		move.l	APG_Renderbase(a5),a6
		jsr	_LVOCreateHistogramA(a6)
		lea	12(a7),a7
		tst.l	d0
		bne	.ok
		lea	nohistomsg(pc),a4
		move.l	APG_Status(a5),a6
		jsr	(a6)
		move.l	#APLE_ERROR,d3
		rts
.ok
		move.l	d0,CC_Histo

		jsr	APR_OpenProcess(a5)

		lea	cc_countmsg(pc),a1
		moveq	#0,d0
		move.w	APG_ImageHeight(a5),d0
		jsr	APR_Initprocess(a5)

		cmp.b	#8,APG_Planes(a5)
		bhi	CC_True

;---------------------------------------------------------------------------------
cc_normal:
		move.b	APG_HAM_Flag(a5),d0
		add.b	APG_HAM8_Flag(a5),d0
		tst.b	d0
		bne	cc_makeham

		move.l	APG_ByteWidth(a5),d0
		lsl.l	#3,d0
		move.l	d0,d7
		mulu	APG_ImageHeight(a5),d0
		move.l	d0,APG_Free1(a5)
		add.l	#256*4,d0

		move.l	#MEMF_ANY+MEMF_CLEAR,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)
		move.l	d0,cc_chunkymem(a5)
		bne.b	.memok
		jsr	APR_Clearprocess(a5)
		moveq	#APOE_NOMEM,d3
		rts

.memok:
		add.l	APG_Free1(a5),d0
		move.l	d0,cc_counttab(a5)	;countertab
		
;m�ch chunkys
		lea	cc_bitmaptab,a1	;puffer f�r BitmapPointerTab
		move.l	a1,a0
		move.l	APG_Picmem(a5),d0
		moveq	#0,d1
		move.b	APG_Planes(a5),d1		
		subq.w	#1,d1
.ncp:		move.l	d0,(a1)+	;erzeuge bitmappointer Tab
		add.l	APG_Oneplanesize(a5),d0
		dbra	d1,.ncp

		move.l	cc_chunkymem(a5),a1	;Chunky-Buffer
		move.l	APG_bytewidth(a5),d0
		move.w	APG_ImageHeight(a5),d1
		moveq	#0,d2
		move.b	APG_Planes(a5),d2
		move.l	d0,d3
		move.l	APG_Renderbase(a5),a6

		sub.l	a2,a2

		jsr	_LVOPlanar2Chunky(a6)

;Z�hle Colors
		move.l	cc_counttab(a5),a0
		move.l	cc_chunkymem(a5),a1
		move.w	APG_ImageHeight(a5),d7
		move.l	APG_ByteWidth(a5),d5
		lsl.l	#3,d5
		sub.w	APG_ImageWidth(a5),d5		;Modulo
		subq.w	#1,d7
.nl:		move.w	APG_ImageWidth(a5),d6
		subq.w	#1,d6
.nch:		moveq	#0,d0
		move.b	(a1)+,d0
		add.l	#1,(a0,d0.l*4)
		dbra	d6,.nch
		add.l	d5,a1		;modulo f�r eine Zeile

		jsr	APR_DoProcess(a5)
		tst.l	d0
		bne.b	.ok

		move.l	cc_chunkymem(a5),a1
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)
		clr.l	cc_chunkymem(a5)
		jsr	APR_Clearprocess(a5)
		moveq	#APOE_ABORT,d3
		rts
.ok:
		dbra	d7,.nl

;werte aus

		move.l	cc_counttab(a5),a0
		move.w	#256-1,d7
		moveq	#0,d5
.nc		move.l	(a0)+,d0
		beq	.nixcount
		addq.w	#1,d5
.nixcount:	dbra	d7,.nc
		
		move.l	cc_chunkymem(a5),a1
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)
		clr.l	cc_chunkymem(a5)

		jsr	APR_Clearprocess(a5)

		move.l	d5,d0
		bra	cc_end

;----------------------------------------------------------------------------
cc_palitag:
		dc.l	RND_PaletteFormat,PALFMT_RGB32
		dc.l	RND_NewPalette,1
		dc.l	TAG_DONE
cc_makeham:

		move.l	APG_rndr_Palette(a5),a0
		lea	APG_Farbtab8(a5),a1
		move.l	(a1)+,d0
		swap	d0
		lea	cc_palitag(pc),a2
		move.l	APG_RenderBase(a5),a6
		jsr	_LVOImportPaletteA(a6)		

		move.l	APG_ByteWidth(a5),d0
		lsl.l	#3,d0
		move.l	d0,d5
		move.l	d0,d1
		add.l	d1,d1
		add.l	d1,d1
		add.l	d1,d0

		move.l	#MEMF_ANY+MEMF_CLEAR,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)
		move.l	d0,cc_chunkymem(a5)
		add.l	d5,d0
		move.l	d0,cc_rgb(a5)

		moveq	#0,d7
		move.w	APG_ImageHeight(a5),d7
		subq.w	#1,d7
cc_newhamline:
		lea	cc_bitmaptab,a1		;puffer f�r BitmapPointerTab
		move.l	a1,a0
		move.l	APG_Picmem(a5),d0
		move.l	APG_ByteWidth(a5),d2
		mulu	d7,d2
		add.l	d2,d0
		moveq	#0,d1
		move.b	APG_Planes(a5),d1		
		subq.w	#1,d1
.ncp:		move.l	d0,(a1)+		;erzeuge bitmappointer Tab
		add.l	APG_Oneplanesize(a5),d0
		dbra	d1,.ncp

		lea	cc_bitmaptab,a0		;puffer f�r BitmapPointerTab
		move.l	cc_chunkymem(a5),a1	;Chunky-Buffer
		move.l	APG_bytewidth(a5),d0
;		move.w	APG_ImageHeight(a5),d1
		moveq	#1,d1
		moveq	#0,d2
		move.b	APG_Planes(a5),d2
		move.l	d0,d3
		move.l	APG_Renderbase(a5),a6

		sub.l	a2,a2

		jsr	_LVOPlanar2Chunky(a6)

		move.l	cc_chunkymem(a5),a0	;Chunkypuffer
		move.l	cc_rgb(a5),a1		;RGB Buffer
		move.l	APG_rndr_Palette(a5),a2

		moveq	#0,d0
		move.l	APG_ByteWidth(a5),d0
		lsl.l	#3,d0			;gesamt Chunkybreite
;		move.w	APG_ImageHeight(a5),d1
		moveq	#1,d1
		
		moveq	#COLORMODE_HAM8,d5

		tst.b	APG_HAM_FLAG(a5)
		beq.b	.w
		moveq	#COLORMODE_HAM6,d5
.w

		move.l	#TAG_DONE,-(a7)
		clr.l	-(a7)
		move.l	#RND_LeftEdge,-(a7)
		move.l	d5,-(a7)
		move.l	#RND_ColorMode,-(a7)
		move.l	a7,a3

		jsr	_LVOChunky2RGBA(a6)
		lea	20(a7),a7

		move.l	cc_histo(pc),a0
		move.l	cc_RGB(a5),a1		;RGB Pic
		move.l	APG_ByteWidth(a5),d0
		lsl.l	#3,d0
;		move.w	APG_ImageHeight(a5),d1
		moveq	#1,d1
		sub.l	a2,a2
		move.l	APG_Renderbase(a5),a6
		jsr	_LVOAddRGBImageA(a6)

		jsr	APR_DoProcess(a5)

		dbra	d7,cc_newhamline

		move.l	cc_chunkymem(a5),a1
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)
		clr.l	cc_chunkymem(a5)

		bra	cc_gibaus

;----------------------------------------------------------------------------
CC_True:
		tst.l	APG_rndr_rgb(a5)

		move.l	cc_histo(pc),a0
		move.l	APG_rndr_rgb(a5),a1		;RGB Pic
		move.l	APG_bytewidth(a5),d0
		lsl.l	#3,d0
		move.w	APG_ImageHeight(a5),d1
	move.l	#TAG_DONE,-(a7)
	move.l	APG_DoProcessHook(a5),-(a7)
	move.l	#RND_ProgressHook,-(a7)
	move.l	a7,a2
	;	sub.l	a2,a2
		move.l	APG_Renderbase(a5),a6
		jsr	_LVOAddRGBImageA(a6)
	lea	12(a7),a7

;		cmp.l	#ADDH_SUCCESS,d0	;!!!!!!!!!!!!!
		cmp.l	#ADDH_CALLBACK_ABORTED,d0
			


cc_gibaus:
		jsr	APR_ClearProcess(a5)

		move.l	cc_histo(pc),a0
		move.l	#RND_NumColors,d0
		move.l	APG_Renderbase(a5),a6
		jsr	_LVOQueryHistogram(a6)
		move.l	d0,cc_colors


cc_end:
		lea	array(pc),a4
		move.l	d0,(a4)

		lea	foundmsg(pc),a1
		lea	foundokmsg(pc),a2
		sub.l	a3,a3
		move.l	APG_RTRequestsTags(a5),a0
		move.l	APG_Reqtoolsbase(a5),a6
		jsr	_LVOrtEZRequestA(a6)


		move.l	CC_Histo(pc),a0
		move.l	APG_Renderbase(a5),a6
		jsr	_LVODeleteHistogram(a6)


		rts


nohistomsg:	dc.b	'Can',39,'t create histogram!',0
cc_countmsg:	dc.b	'Counting Colors ...',0

foundmsg:	dc.b	'Found %ld unique colors!',0
foundokmsg:	dc.b	'Interesting',0

	even
array:		dc.l	0,0



		section	laber,bss
cc_bitmaptab:	ds.l	8
