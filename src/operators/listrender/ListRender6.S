����  �  7��&b�&b�&b�&b�&b�&b�&b;=;===========================================================================
;
;		Test f�r einen Operator
;		(c) 1997 ,Frank (Copper) Pagels  /DFT
;
;		13.04.97

		incdir 	include:

		include	exec/exec_lib.i
		include	exec/lists.i
		include	exec/memory.i
		include intuition/intuition_lib.i
		include	libraries/wb_lib.i
		include libraries/gadtools.i
		include	graphics/graphics_lib.i
		Include utility/tagitem.i
		include	dos/dos.i		
		include	dos/dos_lib.i		
		include	libraries/reqtools.i
		include	libraries/reqtools_lib.i
		include utility/tagitem.i
		include utility/utility_lib.i
		include libraries/gadtools.i
		include libraries/gadtools_lib.i

		include misc/artpro.i
		include render/render_lib.i
		include	render/render.i
		

		OPERATORHEADER LR_NODE

		dc.b	'$VER: ListRender Operator Modul 1.00 (09.07.98)',0
	even
;---------------------------------------------------------------------------
LR_NODE:
		dc.l	0,0
		dc.b	0,0
		dc.l	LR_name
		dc.l	LR_Party
		dc.b	'LRDR'
		dc.b	'EXTR'	;for extern loader
		dc.l	0
		dc.l	LR_Tags
LR_name:
		dc.b	'List Render',0
	even
;---------------------------------------------------------------------------

LR_Tags:
		dc.l	APT_Creator,LR_Creator
		dc.l	APT_Version,5
		dc.l	APT_Info,LR_Info
		dc.l	APT_Prefs,LR_Prefsrout		;routine for Prefs
		dc.l	APT_Prefsbuffer,LR_Format	;buffer for Prefs
		dc.l	APT_Prefsbuffersize,24		;size of buffer
		dc.l	APT_PrefsVersion,3		;Prefs Version
		dc.l	0

LR_Creator:
		dc.b	'(c) 1997 Frank Pagels /DEFECT Softworks',0
LR_Info:	dc.b	'Render and/or convert a List of Images ',0
	even

LR_Format:	dc.b	1
LR_Hist:	dc.b	6
LR_AddCheck:	dc.b	1
LR_suffbody:	dc.b	'.ren'
		ds.b	11
LR_Offset:	dc.b	0
		dc.b	0

LR_windowp	dc.l	0		;Enth�lt pointer vom eingenen Window
LR_GList	dc.l	0		
LR_gadarray	dcb.l	6,0

LR_myrequest:	dc.l	0
LR_Filelist:	dc.l	0
LR_Filename:	dc.l	0
LR_Savedirname:	dc.l	0
LR_AktList:	dc.l	0

LR_PalMode:	dc.l	0
LR_Colorlevel:	dc.l	0
LR_myPalette:	dc.l	0
LR_aktsuff:	dc.l	LR_suffbody

;---------------------------------------------------------------------------
LR_Party:

;--- Checke ob der eingestellte Saver �berhaupt geht

		moveq	#nc_selectsaver,d0
		move.l	#APT_OperatorUse,d1
		jsr	APR_GetTag(a5)
		cmp.l	#'EROR',d0
		beq	.nixsave
		tst.l	d0
		bne	LR_Go

.nixsave:
		lea	LR_wrongsaver(pc),a1
		lea	LR_ok(pc),a2
		sub.l	a3,a3
		sub.l	a4,a4
		sub.l	a0,a0
		move.l	APG_Reqtoolsbase(a5),a6
		jsr	_LVOrtEZRequestA(a6)

		moveq	#APOE_ERROR,d3
		rts
LR_Go:
;ersma render einstellungen sichern

		lea	LR_PalMode(pc),a0
		move.l	APG_ren_ColorMode(a5),(a0)
		move.l	APG_ren_ColorLevel(a5),4(a0)

		move.l	#RT_FileReq,d0
		sub.l	a0,a0
		move.l	APG_ReqToolsbase(a5),a6
		jsr	_LVOrtAllocrequestA(a6)
		move.l	d0,LR_myrequest

		move.l	d0,a1
		move.l	#FREQF_MULTISELECT!FREQF_PATGAD,rtfi_flags(a1)

		lea	LR_FirstFilename,a2
		lea	LR_Firstframe(pc),a3
		lea	LR_FileTags(pc),a0
		move.l	APG_Scr(a5),4(a0)
		jsr	_LVOrtFileRequestA(a6)

		tst.l	d0
		bne	RL_changedir
LR_nixchoose:
		move.l	LR_myrequest(pc),a1
		move.l	APG_ReqToolsbase(a5),a6
		jsr	_LVOrtFreeRequest(a6)

		lea	LR_nofilemsg(pc),a4
		move.l	APG_Status(a5),a6
		jsr	(a6)

		move.l	#APOE_ERROR,d3
		clr.b	APG_RenderFlag(a5)
		rts

RL_changedir:
		move.l	d0,LR_Filelist		;Filelistmerken

		move.l	LR_myrequest(pc),a0
		move.l	rtfi_dir(a0),a1
		lea	LR_LoadName,a0

		tst.b	(a1)
		beq.b	.no
.newchar:
		move.b	(a1)+,(a0)+
		bne.b	.newchar
		subq.l	#2,a0
		cmp.b	#':',(a0)+
		beq.b	.no
		move.b	#'/',(a0)+
.no:		move.l	a0,LR_Filename

;--------------------------------
LR_ChooseDir:
		move.l	LR_myrequest(pc),a1
		move.l	#FREQF_SELECTDIRS!FREQF_NOFILES,rtfi_flags(a1)

		lea	LR_SaveName,a2
		lea	LR_savedir(pc),a3
		lea	LR_FileTags(pc),a0
		jsr	_LVOrtFileRequestA(a6)

		tst.l	d0
		beq	LR_nixchoose

		move.l	LR_myrequest(pc),a0
		move.l	rtfi_dir(a0),a1
		lea	LR_SaveName,a0

		tst.b	(a1)
		beq.b	.no
.nr:
		move.b	(a1)+,(a0)+
		bne.b	.nr
		subq.l	#2,a0
		cmp.b	#':',(a0)+
		beq.b	.no
		move.b	#'/',(a0)+
.no:		move.l	a0,LR_Savedirname


		move.l	LR_myrequest(pc),a1
		move.l	APG_ReqToolsbase(a5),a6
		jsr	_LVOrtFreeRequest(a6)

;-------------------------------------------------------------------------------

		bsr	LR_FreeImage

		st	APG_rndr_lock(a5)	;locke die render einstellungen

		lea	LR_AddTag+4(pc),a1
		move.l	APG_DoProcessHook(a5),(a1);DoProcess routine in Taglist
						  ;eintragen

;�ffne erstes File
		move.l	LR_FileList(pc),a1
		move.l	a1,LR_AktList
LR_LoadFile:
		move.l	LR_Filename(pc),a0
		move.l	LR_AktList(pc),a1

		move.l	rtfl_name(a1),a1
.w		move.b	(a1)+,(a0)+
		bne	.w
		clr.b	(a0)
		lea	LR_LoadName,a0
		move.l	a0,d1
		move.l	#MODE_OLDFILE,d2
		move.l	APG_Dosbase(a5),a6
		jsr	_LVOOpen(a6)
		tst.l	d0
		bne	RL_LoadOk
		lea	LR_Filenot(pc),a4
		move.l	APG_Status(a5),a6
		jsr	(a6)
		jsr	APR_ClearProcess(a5)
		moveq	#APOE_ERROR,d3
		clr.b	APG_RenderFlag(a5)
		rts

RL_LoadOk:
		move.l	d0,APG_Filehandle(a5)
		move.l	d0,d1

		lea	LR_LoadName,a1
		move.l	APG_Checkfilesize(a5),a6
		jsr	(a6)
		
		clr.b	APG_Intern1(a5)		;usedatatype
		st	APG_Intern2(a5)		;newloaded
		
		jsr	APR_LoadUniversal(a5)

			;hier noch abfrage ob file geladen einbauen

		move.l	APG_Dosbase(a5),a6
		move.l	APG_Filehandle(a5),d1
		jsr	_LVOClose(a6)
		
	cmp.l	#APLE_ERROR,d3
	beq	lr_abort
	cmp.l	#APLE_ABORT,d3
	beq	lr_abort

;Kucke ob schon 24 Bit geladen , sonst m�ch RGB daten

		cmp.b	#2,LR_Format
		beq	LR_Save		; es soll nix gerendert werden

		tst.l	APG_rndr_rendermem(a5)
		bne	LR_rgbOk		;wir haben schon rgb daten
		jsr	APR_MakeRGBData(a5)
		tst.l	d0
		beq	LR_rgbOK
LR_rgbOk:

;adde RGB to Histogramm

		move.l	APG_RenderBase(a5),a6

		lea	LR_buildhistomsg(pc),a1
		moveq	#1,d0
		jsr	APR_InitProcess(a5)

		move.b	APG_HAM_Flag(a5),d0
		add.b	APG_HAM_Flag(a5),d0
		tst.b	d0
		bne.b	.ham

		cmp.b	#24,APG_planes(a5)
		beq.b	.ham

		move.l	APG_rndr_histogram(a5),a0
		move.l	APG_rndr_chunky(a5),a1	;Chunkypuffer
		move.l	APG_rndr_palette(a5),a2
		
		move.l	APG_ByteWidth(a5),d0
		lsl.l	#3,d0
		move.w	APG_ImageHeight(a5),d1
		lea	LR_addtag(pc),a3
		
		jsr	_LVOAddChunkyImageA(a6)
;		cmp.l	#ADDH_SUCCESS,d0	;!!!!!!!!!!!!!
		cmp.l	#ADDH_CALLBACK_ABORTED,d0
		beq.w	LR_rndr_fail
		bra.b	.addok
.ham:
		move.l	APG_rndr_histogram(a5),a0
		move.l	APG_rndr_rgb(a5),a1		;RGB Pic
		move.l	APG_ByteWidth(a5),d0
		lsl.l	#3,d0
		move.w	APG_ImageHeight(a5),d1
		lea	LR_addtag(pc),a2
		jsr	_LVOAddRGBImageA(a6)
;		cmp.l	#ADDH_SUCCESS,d0	;!!!!!!!!!!!!!
		cmp.l	#ADDH_CALLBACK_ABORTED,d0
		beq.w	LR_rndr_fail
		
.addok
		tst.b	LR_Format
		beq	.nixmore		;nur eine locale Palette

		move.l	APG_rndr_rendermem(a5),d0
		beq	.w
		move.l	d0,a1
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)
		clr.l	APG_rndr_rendermem(a5)
		clr.l	APG_rndr_rgb(a5)
.w
		move.l	LR_AktList(pc),a0
		moveq	#0,d1
		move.b	LR_Offset(pc),d1
.nn		move.l	rtfl_Next(a0),d0
		beq	LR_EndLoad
		move.l	d0,a0
		dbra	d1,.nn

		move.l	d0,LR_AktList

.nixmore:
		move.l	4.w,a6
		move.l	APG_Picmem(a5),d0
		beq	.nixpic
		move.l	d0,a1
		jsr	_LVOFreeVec(a6)
		clr.l	APG_PicMem(a5)
.nixpic
		tst.b	LR_Format
		bne	LR_LoadFile		;nur eine locale Palette

;--------------------------------------------------------------------------
LR_EndLoad:
		move.l	APG_rndr_Palette(a5),d0
		move.l	d0,a0
		move.l	APG_Renderbase(a5),a6
		jsr	_LVOFlushPalette(a6)

	;Choose Palette

		lea	LR_choosingmsg(pc),a1
		moveq	#1,d0
		jsr	APR_InitProcess(a5)

;Lege neue Palette an
		lea	LR_paltag(pc),a1
;		move.l	APG_rndr_memhandler(a5),4(a1)
		move.l	#HSTYPE_15Bit,12(a1)
		tst.b	LR_Format
		beq	.nixglobal
		move.l	#HSTYPE_18Bit,12(a1)
.nixglobal:
		move.l	APG_Renderbase(a5),a6
		jsr	_LVOCreatePaletteA(a6)
		
		move.l	d0,LR_myPalette

;extract palette

		lea	LR_exttag(pc),a2
		move.l	LR_Palmode(pc),d0
		lea	LR_coltab(pc),a0
		move.l	(a0,d0.l*4),4(a2)	;Colormode eintragen
		move.l	APG_DoProcessHook(a5),12(a2)

		move.l	APG_rndr_Histogram(a5),a0
		move.l	LR_myPalette(pc),a1
		move.l	LR_Colorlevel(pc),d0
		tst.l	APG_ren_Custom(a5)
		beq	.nixcust
		move.l	APG_ren_UsedColors(a5),d0
.nixcust:
		jsr	_LVOExtractPaletteA(a6)

		cmp.l	#EXTP_CALLBACK_ABORTED,d0
		beq.w	lR_abort
				
;Sortiere Palette

		tst.l	APG_ren_palsortmode(a5)
		beq.b	LR_render		;nicht sortieren
		move.l	APG_rndr_palette(a5),a0

		move.l	APG_ren_palsortmode(a5),d0
		or.l	APG_ren_palordermode(a5),d0

		lea	LR_sorttag(pc),a1
		move.l	APG_rndr_histogram(a5),4(a1)
		jsr	_LVOSortPaletteA(a6)		

;--------------------------------------------------------------------------
LR_Render:
		tst.b	LR_Format
		beq	LR_Rendergo\.rgbok

		move.l	LR_Filelist(pc),a1
		move.l	a1,LR_AktList
LR_Rendergo:
		move.l	LR_Filename(pc),a0
		move.l	rtfl_name(a1),a1
.nc
		move.b	(a1)+,(a0)+		;Name kopieren
		bne	.nc
		clr.b	(a0)

		move.l	APG_rndr_rendermem(a5),d0
		beq	.w
		move.l	d0,a1
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)
		clr.l	APG_rndr_rendermem(a5)
		clr.l	APG_rndr_rgb(a5)
.w
		lea	LR_LoadName,a0
		move.l	a0,d1
		move.l	#MODE_OLDFILE,d2
		move.l	APG_DosBase(a5),a6
		jsr	_LVOOpen(a6)
		tst.l	d0
		bne	.fileok

		lea	LR_Filenot(pc),a4
		move.l	APG_Status(a5),a6
		jsr	(A6)
		jsr	APR_ClearProcess(a5)
		move	#-1,D3
		clr.b	APG_RenderFlag(a5)
		rts
.fileok:
		move.l	d0,APG_FileHandle(a5)
		move.l	d0,d1

		lea	LR_LoadName,a1
		move.l	APG_Checkfilesize(a5),a6
		jsr	(a6)

		clr.b	APG_Intern1(a5)
		st	APG_Intern2(a5)
		jsr	APR_LoadUniversal(a5)

		move.l	APG_DosBase(a5),a6
		move.l	APG_FileHandle(a5),d1
		jsr	_LVOClose(a6)

	cmp.l	#APLE_ABORT,d3
	beq	lr_abort
	
		tst.l	APG_rndr_Rendermem(a5)
		bne	.rgbok
		jsr	APR_MakeRGBData(a5)
		tst.l	d0
		bne	LR_rndr_fail
.rgbok
		lea	LR_rendermsg(pc),a1
		moveq	#1,d0
		jsr	APR_InitProcess(a5)

		move.l	LR_PalMode(pc),d0
		lea	LR_coltab(pc),a0
		lea	LR_rendertag(pc),a3
		move.l	(a0,d0.l*4),4(a3)
		clr.l	20(a3)
		tst.l	APG_ren_Custom(a5)
		beq	.nixcust
		move.l	APG_ren_FirstColor(a5),20(a3)
.nixcust:
		move.l	APG_ren_DitherMode(a5),12(a3)
		move.l	APG_ren_DitherAmount(a5),28(a3)
		move.l	APG_DoProcessHook(a5),36(a3)
		move.l	APG_rndr_rgb(a5),a0
		move.l	APG_rndr_chunky(a5),a1
		move.l	LR_mypalette(pc),a2
		move.l	APG_ByteWidth(a5),d0
		lsl.l	#3,d0
		move.w	APG_ImageHeight(a5),d1
		move.l	APG_Renderbase(a5),a6
		jsr	_LVORenderA(a6)

		cmp.l	#REND_SUCCESS,d0
		bne.w	LR_rndr_fail

;nu Bitplanes M�chen

		lea	LR_makebitmapmsg(pc),a1
		moveq	#2,d0
		jsr	APR_InitProcess(a5)
		move.l	4.w,a6
		move.l	APG_Picmem(a5),d0
		beq	.nixpic
		move.l	d0,a1
		jsr	_LVOFreevec(a6)
		clr.l	APG_picmem(a5)
.nixpic:
		sf	APG_HAM_Flag(a5)		
		sf	APG_HAM8_Flag(a5)		
		sf	APG_EHB_Flag(a5)		

		move.l	LR_PalMode(pc),d1
		cmp.w	#3,d1
		beq	LR_renderEHB
		cmp.w	#2,d1
		beq	LR_renderHAM6
		cmp.w	#1,d1
		beq	LR_renderHAM8
		bra	LR_renderCLUT

LR_renderEHB:
		move.w	#32,APG_ColorCount(a5)
		moveq	#6,d0
		move.b	d0,APG_Planes(a5)
		st	APG_EHB_Flag(a5)
		bra	LR_makeplane

LR_renderHAM6:
		move.w	#16,APG_ColorCount(a5)
		moveq	#6,d0
		move.b	d0,APG_Planes(a5)
		st	APG_HAM_Flag(a5)
		bra	LR_makeplane

LR_renderHAM8:
		move.w	#64,APG_ColorCount(a5)
		moveq	#8,d0
		move.b	d0,APG_Planes(a5)
		st	APG_HAM8_Flag(a5)
		bra	LR_makeplane

LR_renderCLUT:
		moveq	#0,d0
		move.l	LR_Colorlevel(pc),d1
		move.w	d1,APG_ColorCount(a5)

.np:		lsr.l	#1,d1
		bcs	.ep
		addq.l	#1,d0
		bra	.np
.ep:
		move.b	d0,APG_Planes(a5)

LR_makeplane:
		move.l	APG_Oneplanesize(a5),d1
		mulu.l	d1,d0			;neue Gr��e des Pic

		move.l	4.w,a6
		move.l	#MEMF_ANY+MEMF_CLEAR,d1		
		jsr	_LVOAllocVec(a6)
		tst.l	d0
		beq.w	LR_rndr_fail
		move.l	d0,APG_picmem(a5)		

		jsr	APR_DoProcess(a5)
		tst.l	d0
		beq.w	LR_abort

		lea	LR_picmap,a0	
		moveq	#0,d0
		moveq	#0,d1
		moveq	#0,d2
		move.b	APG_Planes(a5),d0
		move.l	APG_Bytewidth(a5),d1
		lsl.l	#3,d1
		move.w	APG_ImageHeight(a5),d2
		move.l	APG_Gfxbase(a5),a6
		jsr	_LVOinitbitmap(a6)

		lea	LR_Picmap+8,a0
		move.l	APG_Picmem(a5),d0
		moveq	#0,d7		
		move.b	APG_Planes(a5),d7	
		subq	#1,d7
.newplane
		move.l	d0,(a0)+	;Planes in Strucktur einbinden
		add.l	APG_Oneplanesize(a5),d0
		dbra	d7,.newplane

		move.l	APG_Renderbase(a5),a6

		move.l	APG_rndr_chunky(a5),a0	;chunky
		lea	LR_Picmap,a1
		moveq	#0,d0
		moveq	#0,d1
		move.l	APG_Bytewidth(a5),d2
		lsl.l	#3,d2
		move.w	APG_ImageHeight(a5),d3
		moveq	#0,d4
		moveq	#0,d5
		sub.l	a2,a2			;TagList
		jsr	_LVOChunky2BitmapA(a6)

		jsr	APR_DoProcess(a5)
		tst.l	d0
		beq.w	LR_rndr_fail

		move.l	LR_myPalette(pc),a0
		lea	LR_Pal,a1
		move.w	APG_Colorcount(a5),d7
		tst.b	APG_HAM_Flag(a5)
		beq	.nixham
		moveq	#16,d7
.nixham:
		tst.b	APG_HAM8_Flag(a5)
		beq	.nixham8
		moveq	#64,d7
.nixham8:
		move.l	d7,d6
		tst.l	APG_ren_Custom(a5)
		beq	.nixcust

		move.l	d7,d5
		subq.l	#1,d5
		move.l	a1,a3
.nc:		clr.l	(a3)+
		dbra	d5,.nc
		move.l	APG_ren_FirstColor(a5),d0
		lsl.l	#2,d0
		add.l	d0,a1
.nixcust:
		lea	LR_palitag(pc),a2
		jsr	_LVOExportPaletteA(a6)

		lea	LR_Pal,a0
		lea	APG_Farbtab8(a5),a1
		move.l	d6,d7
		move.w	d7,(a1)+
		clr.w	(a1)+
		sub.w	#1,d7
.lc:		move.l	(a0)+,d0
		
		moveq	#0,d1
		bfins	d0,d1{0:8}
		lsr.l	#8,d0		
		moveq	#0,d2
		bfins	d0,d2{0:8}
		lsr.l	#8,d0		
		moveq	#0,d3
		bfins	d0,d3{0:8}

		tst.b	APG_HAM_Flag(a5)
		beq.b	.w

		move.l	d3,d0
		lsr.l	#4,d0
		or.l	d0,d3
		move.l	d2,d0
		lsr.l	#4,d0
		or.l	d0,d2
		move.l	d1,d0
		lsr.l	#4,d0
		or.l	d0,d1

.w		move.l	d3,(a1)+
		move.l	d2,(a1)+
		move.l	d1,(a1)+
		dbra	d7,.lc
		clr.l	(a1)

;------------------------------------------------------------------------------
;Speicher Image
LR_Save:
		move.l	LR_Savedirname(pc),a0
		move.l	LR_AktList(pc),a1
		move.l	rtfl_name(a1),a1
.nc		move.b	(a1)+,(a0)+		;Name kopieren
		bne	.nc

		subq.l	#1,a0

		tst.b	LR_AddCheck
		beq	.nixadd

		move.l	LR_aktsuff(pc),a1
.cs		move.b	(a1)+,(a0)+
		bne	.cs
.nixadd:
		clr.l	(a0)
		
		move.l	#LR_SaveName,d1
		move.l	#1006,d2		;newfile
		move.l	APG_Dosbase(a5),a6
		jsr	_LVOOpen(a6)
		move.l	d0,APG_Filehandle(a5)
		beq.w	LR_rndr_Fail

		move.l	APG_Savernode(a5),a0
		tst.l	NC_PREFSROUT(a0)
		beq.b	.noprefs
		move.l	NC_PREFSORIBUF(a0),a1
		move.l	NC_PREFSLEN(a0),d7
		subq.w	#1,d7
		lea	NC_PREFS(a0),a0
.np		move.b	(a0)+,(a1)+
		dbra	d7,.np
.noprefs:
		move.l	APG_Saver(a5),a0
		jsr	(a0)		;Springe Saveroutine an
		
		move.l	APG_Filehandle(a5),d1
		move.l	APG_DosBase(a5),a6
		jsr	_LVOClose(a6)

		cmp.l	#APSE_ABORT,d3		;;;
		beq	lr_abort

		move.l	LR_AktList(pc),a1
		move.l	rtfl_Next(a1),d0
		beq	LR_Ende
		move.l	d0,a1
		move.l	a1,LR_AktList

		cmp.b	#2,LR_Format
		beq	LR_Loadfile

		tst.b	LR_Format
		bne	LR_Rendergo

		bsr	LR_FreeImage
		bra	LR_LoadFile


lr_abort:
		move.l	#'ABOT',d7

;--------------------------------------------------------------------------
LR_Ende:
		move.l	LR_FileList(pc),a0
		move.l	APG_Reqtoolsbase(a5),a6
		jsr	_LVOrtFreeFileList(a6)
		
		move.l	APG_rndr_rendermem(a5),d0
		beq.b	.wp

		move.l	d0,a1
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)
		clr.l	APG_rndr_rendermem(a5)
		clr.l	APG_rndr_rgb(a5)
		clr.l	APG_rndr_chunky(a5)
.wp
		move.l	APG_RenderBase(a5),a6

		move.l	APG_rndr_histogram(a5),d0
		beq	.w
		move.l	d0,a0
		jsr	_LVODeleteHistogram(a6)
		clr.l	APG_rndr_histogram(a5)
.w
		move.l	LR_myPalette(pc),d0
		beq	.np
		move.l	d0,a0
		jsr	_LVODeletePalette(a6)
		clr.l	LR_myPalette
.np:
		jsr	APR_ClearProcess(a5)

		moveq	#APOE_OK,d3

	cmp.l	#'ABOT',d7
	bne	.na
	move.l	#APOE_ABORT,d3
.na		clr.b	APG_RenderFlag(a5)
		rts	

LR_rndr_fail:
		bra	LR_Ende
		rts

;------------------------------------------------------------------------------
;altes Image rausschmei�en

LR_FreeImage:
		move.l	4.w,a6
		move.l	APG_Picmem(a5),d0
		beq	.nixpic
		move.l	d0,a1
		jsr	_LVOFreeVec(a6)
		clr.l	APG_PicMem(a5)
.nixpic:	move.l	APG_rndr_rendermem(a5),d0
		beq	.nixrender
		move.l	d0,a1
		jsr	_LVOFreeVec(a6)
		clr.l	APG_rndr_rendermem(a5)
.nixrender

;L�sche altes Histogramm

		move.l	APG_Renderbase(a5),a6
		move.l	APG_rndr_histogram(a5),d0
		beq.b	.w2
		move.l	d0,a0
		jsr	_LVODeleteHistogram(a6)
		clr.l	APG_rndr_histogram(a5)
.w2
		clr.w	APG_Loadflag(a5)	;signalisieren das wir nix geladen
						;haben
;lege neues Histogramm an

		lea	LR_histotag(pc),a1
		move.l	APG_rndr_memhandler(a5),4(a1)
		moveq	#0,d0
		move.b	LR_Hist(pc),d0
		lea	LR_HSTab(pc),a0
		move.l	(a0,d0.l*4),d0
		move.l	d0,12(a1)
		jsr	_LVOCreateHistogramA(a6)
		tst.l	d0
		bne	.histook
		lea	LR_histofail(pc),a4
		move.l	APG_Status(a5),a6
		jsr	(a6)
		moveq	#APOE_ERROR,d3
		clr.b	APG_RenderFlag(a5)
		rts
.histook
		move.l	d0,APG_rndr_histogram(a5)
		rts


LR_HSTab:
		dc.l	HSTYPE_12BIT	
		dc.l	HSTYPE_15BIT	
		dc.l	HSTYPE_18BIT	
		dc.l	HSTYPE_21BIT	
		dc.l	HSTYPE_24BIT	
		dc.l	HSTYPE_12BIT_TURBO
		dc.l	HSTYPE_15BIT_TURBO
		dc.l	HSTYPE_18BIT_TURBO

LR_coltab:
		dc.l	COLORMODE_CLUT
		dc.l	COLORMODE_HAM8
		dc.l	COLORMODE_HAM6
		dc.l	COLORMODE_CLUT

LR_palitag:	dc.l	RND_PaletteFormat,PALFMT_RGB8
		dc.l	TAG_DONE

LR_rendertag:
		dc.l	RND_ColorMode,0
		dc.l	RND_DitherMode,0
		dc.l	RND_OffsetColorZero,0
		dc.l	RND_DitherAmount,0
		dc.l	RND_ProgressHook,0
		dc.l	TAG_DONE

LR_sorttag:
		dc.l	RND_Histogram,0
		dc.l	TAG_DONE

LR_exttag:
		dc.l	RND_ColorMode,0
		dc.l	RND_ProgressHook,0
		dc.l	RND_NewPalette,1
		dc.l	TAG_DONE

LR_paltag:	dc.l	RND_RMHandler,0
		dc.l	RND_HSType,HSTYPE_15BIT
		dc.l	TAG_DONE

LR_addtag:
		dc.l	RND_ProgressHook,0
		dc.l	TAG_DONE

LR_histotag:
		dc.l	RND_RMHandler,0
		dc.l	RND_HSType,0
		dc.l	TAG_DONE	

LR_FileTags:	dc.l	_RT_Screen,0
		dc.l	RTFI_FLAGS,FREQF_MULTISELECT
		dc.l	TAG_DONE


LR_Firstframe:	dc.b	'Select frame',$27,'s!',0
LR_savedir:	dc.b	'Select save dir!',0

LR_nofilemsg:	dc.b	'No file selected',0
LR_histofail:	dc.b	'Create Histogram failed',0
LR_Filenot:	dc.b	'File not found!',0

LR_buildhistomsg:  dc.b 'Creating histogram ...',0
LR_choosingmsg:	   dc.b	'Choosing colors ...',0
LR_rendermsg:	   dc.b 'Rendering image ...',0
LR_makebitmapmsg:  dc.b	'Making bitplanes ...',0

LR_wrongsaver:	dc.b	'This Operator can',$27,'t use the',10
		dc.b	'current saver, try an other one!',0
LR_Ok:		dc.b	'I see!',0
	even
;---------------------------------------------------------------------------
LR_Prefsrout:
;init Prefs
		moveq	#0,d0
		move.b	LR_Format(pc),d0
		lea	LR_CY(pc),a0
		move.l	d0,(a0)
		lea	LR_offghost(pc),a0
		moveq	#0,d1
		tst.b	d0
		bne	.ng
		moveq	#1,d1
.ng:		move.l	d1,(a0)

		move.b	LR_Hist(pc),d0
		lea	LR_HS(pc),a0
		move.l	d0,(a0)
		move.b	LR_AddCheck(pc),d0
		lea	LR_check(pc),a0
		move.l	d0,(a0)
		moveq	#0,d0
		tst.b	LR_AddCheck
		bne	.w
		moveq	#1,d0
.w		move.l	d0,LR_Suff
		move.b	LR_Offset(pc),d0
		lea	LR_off(pc),a0
		move.l	d0,(a0)
		
;---------------------------------------------------------------------------

		move.l	#WindowTags,APG_WindowTagList(a5) ;Tag list for the Window
		move.l	#WinWG+4,APG_WGadgets(a5)	;addr. of WA_Gadgets Tag + 4
		move.l	#winSC+4,APG_WScreen(a5)	;addr. of WA_CustomScreen Tag + 4
		move.l	#LR_windowp,APG_WinWnd(a5)	;a point for the Window
		move.l	#LR_Glist,APG_Glist(a5)		;a point for the Glist
		move.l	#NTypes,APG_NGads(a5)		;the NGads list
		move.l	#Gtypes,APG_GTypes(a5)		;the GTypes list
		move.l	#Gtags,APG_Gtags(a5)		;the Gtags list
		move.w	#7,APG_CNT(a5)			;the number of Gadgets
		move.l	#LR_gadarray,APG_Gadgets(a5)	;a pointer for the Gadgetsarry, size=4*number of Gadgets
		move.l	APG_Scr(a5),APG_ScrBase(a5)	;the Screenpointer , stored in APG_Scr
	
		move.l	APG_CheckMainWinPos(a5),a6	;returns d0,d1 Pos
		jsr	(a6)

		add.w	#315,d0
		add.w	#100,d1
		
		move.w	d0,APG_WinLeft(a5)		;the left pos. of the Win
		move.w	d1,APG_WinTop(a5)		;the top pos. of the Win
		move.w	#225,APG_WinWidth(a5)		;the width of the Win
		move.w	#90,APG_WinHeight(a5)		;the height of the Win

		move.l	#WinL,APG_WinL(a5)		;addr. of WA_Left Tag 
		move.l	#WinT,APG_WinT(a5)		;addr. of WA_Top Tag
		move.l	#WinW,APG_WinW(a5)		;addr. of WA_Width Tag
		move.l	#WinH,APG_WinH(a5)		;addr. of WA_Height Tag

		move.l	APG_OpenWindow(a5),a6
		jsr	(a6)

		lea	LR_Prefsback,a0
		lea	LR_Format(pc),a1
		moveq	#24-1,d7
.np		move.b	(a1)+,(a0)+
		dbra	d7,.np
		

LR_wait:	move.l	LR_windowp(pc),a0
		move.l	APG_Wait(a5),a6
		jsr	(a6)				;Handle input

		cmp.l	#IDCMP_CLOSEWINDOW,d4
		beq	LR_Cancel		
		cmp.l	#GADGETUP,d4
		beq.b	LR_gads
		cmp.l	#GADGETDOWN,d4
		beq.b	LR_gads
		bra.b	LR_wait

LR_gads:
		moveq	#0,d0
		move.w	38(a4),d0

		cmp.w	#GD_Offset,d0
		beq	handleoffset
		cmp.w	#GD_Suff,d0
		beq	handlesuff
		cmp.w	#GD_AddSuff,d0
		beq	checksuff
		cmp.w	#GD_format,d0
		beq.b	handleformat
		cmp.w	#GD_Ok,d0
		beq.b	LR_Cancel\.wech
		cmp.w	#GD_Histo,d0
		beq	LR_Histo
		cmp.w	#GD_Cancel,d0
		bne.b	LR_wait
LR_Cancel:
		lea	LR_Prefsback,a0
		lea	LR_Format(pc),a1
		moveq	#24-1,d7
.np		move.b	(a0)+,(a1)+
		dbra	d7,.np

		moveq	#0,d0
		move.b	LR_Format(pc),d0
		move.l	d0,LR_CY
		move.b	LR_Hist(pc),d0
		move.l	d0,LR_HS
		move.b	LR_AddCheck(pc),d0
		move.l	d0,LR_Check
.wech:
		move.l	LR_windowp(pc),a0
		move.l	APG_Intuitionbase(a5),a6
		jmp	_LVOCloseWindow(a6)

handleformat:
		move.b	d5,LR_Format
		ext.w	d5
		ext.l	d5
		move.l	d5,LR_CY
		moveq	#1,d0
		tst.b	d5
		beq	.ghost
		moveq	#0,d0
.ghost:
		lea	LR_gadarray(pc),a0
		move.l	GD_Offset*4(a0),a0
		move.l	LR_windowp(pc),a1
		sub.l	a2,a2
		lea	LR_ghost(pc),a3

		move.l	d0,4(a3)

		move.l	APG_Gadtoolsbase(a5),a6
		jsr	_LVOGT_SetGadgetAttrsA(a6)

		bra.w	LR_wait

LR_Histo:
		move.b	d5,LR_Hist
		ext.w	d5
		ext.l	d5
		move.l	d5,LR_HS
		bra.w	LR_wait

checksuff:
		lea	LR_gadarray(pc),a0
		move.l	GD_AddSuff*4(a0),a0

		moveq	#0,d1
		moveq	#1,d2
		move.w	gg_Flags(a0),d0
		and.w	#SELECTED,d0
		beq.w	.w	;nicht selected
		moveq	#1,d1
		moveq	#0,d2
.w		lea	LR_check(pc),a0
		move.l	d1,(a0)
		lea	LR_AddCheck(pc),a0
		move.b	d1,(a0)

		lea	LR_gadarray(pc),a0
		move.l	GD_Suff*4(a0),a0
		move.l	LR_windowp(pc),a1
		sub.l	a2,a2
		lea	LR_ghost(pc),a3

		move.l	d2,4(a3)

		move.l	APG_Gadtoolsbase(a5),a6
		jsr	_LVOGT_SetGadgetAttrsA(a6)

		bra.w	LR_wait

handlesuff:
		lea	LR_gadarray(pc),a0
		move.l	GD_Suff*4(a0),a0
		move.l	LR_windowp(pc),a1
		sub.l	a2,a2
		lea	LR_Stringtag(pc),a3

		move.l	APG_Gadtoolsbase(a5),a6
		jsr	_LVOGT_GetGadgetAttrsA(a6)
		
		move.l	LR_String(pc),a0
		lea	LR_suffbody(pc),a1
.w		move.b	(a0)+,(a1)+
		bne	.w	

		bra	LR_Wait
handleoffset:
		lea	LR_gadarray(pc),a0
		move.l	GD_Offset*4(a0),a0
		move.l	LR_windowp(pc),a1
		sub.l	a2,a2
		lea	LR_Integer(pc),a3

		move.l	APG_Gadtoolsbase(a5),a6
		jsr	_LVOGT_GetGadgetAttrsA(a6)
		
		move.l	LR_String(pc),d0
		move.b	d0,LR_Offset
		bra	LR_Wait

LR_Stringtag:
		dc.l	GTST_String,LR_String
		dc.l	TAG_DONE

LR_Integer:	dc.l	GTIN_Number,LR_String
		dc.l	TAG_DONE

LR_String	dc.l	0

LR_ghost:	dc.l	GA_Disabled,0,0

WindowTags:
winL:	dc.l	WA_Left,212
winT:	dc.l	WA_Top,78
winW:	dc.l	WA_Width,350
winH:	dc.l	WA_Height,85
		dc.l	WA_IDCMP,GADGETUP!VANILLAKEY!BUTTONIDCMP!IDCMP_REFRESHWINDOW!IDCMP_CLOSEWINDOW
		dc.l	WA_Flags,WFLG_CLOSEGADGET!WFLG_DEPTHGADGET!WFLG_ACTIVATE!WFLG_DRAGBAR!WFLG_SMART_REFRESH!WFLG_RMBTRAP
winWG:	dc.l	WA_Gadgets,0
		dc.l	WA_Title,winWTitle
winSC:	dc.l	WA_CustomScreen,0
		dc.l	TAG_DONE

WinWTitle:	dc.b	'ListRender',0
	even

GD_format	=	0
GD_Ok		=	1
GD_Cancel	=	2
GD_Histo	=	3
GD_AddSuff	=	4
GD_Suff		=	5
GD_Offset	=	6

Gtypes:
		dc.w	CYCLE_KIND
		dc.w	BUTTON_KIND
		dc.w	BUTTON_KIND
		dc.w	CYCLE_KIND
		dc.w	CHECKBOX_KIND
		dc.w	STRING_KIND
		dc.w	INTEGER_KIND

Gtags:
		dc.l	GTCY_Labels,Gadget00Labels
		dc.l	GTCY_Active
LR_CY:		dc.l	1
		dc.l	TAG_DONE
		dc.l	TAG_DONE
		dc.l	TAG_DONE
		dc.l	GTCY_Labels,hslabels
		dc.l	GTCY_Active
LR_HS:		dc.l	6
		dc.l	TAG_DONE
		dc.l	GTCB_Checked
LR_check:	dc.l	1
		dc.l	GTCB_Scaled,1
		dc.l	TAG_DONE
		dc.l	GTST_String,LR_suffbody
		dc.l	GTST_MaxChars,10
		dc.l	GA_Disabled
LR_Suff:	dc.l	1
		dc.l	TAG_DONE
		dc.l	GTIN_Number
LR_off:		dc.l	0
		dc.l	GTIN_MaxChars,2
		dc.l	GA_Disabled
LR_offghost:	dc.l	0
		dc.l	TAG_DONE

Gadget00Labels:
		dc.l	Gadget00Lab0
		dc.l	Gadget00Lab1
		dc.l	Gadget00Lab2
		dc.l	0

Gadget00Lab0:    DC.B    'Local',0
Gadget00Lab1:    DC.B    'Global',0
Gadget00Lab2:    DC.B    'No Render',0


	even

hslabels:
		dc.l	hslab0
		dc.l	hslab1
		dc.l	hslab2
		dc.l	hslab3
		dc.l	hslab4
		dc.l	hslab5
		dc.l	hslab6
		dc.l	hslab7
		dc.l	0

hslab0:		dc.b	'12BIT',0
hslab1:		dc.b	'15BIT',0
hslab2:		dc.b	'18BIT',0
hslab3:		dc.b	'21BIT',0
hslab4:		dc.b	'24BIT',0
hslab5:		dc.b	'12BIT_TURBO',0
hslab6:		dc.b	'15BIT_TURBO',0
hslab7:		dc.b	'18BIT_TURBO',0

	even
NTypes:
		dc.w	104,10,115,13
		dc.l	Project0IText0,0
		dc.w	GD_format
		dc.l	PLACETEXT_LEFT,0,0
		dc.w	20,74,80,13
		dc.l	oktext,0
		dc.w	GD_Ok
		dc.l	PLACETEXT_IN,0,0
		dc.w	126,74,80,13
		dc.l	canceltext,0
		dc.w	GD_Cancel
		dc.l	PLACETEXT_IN,0,0
		dc.w	104,26,115,13
		dc.l	hstext,0
		dc.w	GD_Histo
		dc.l	PLACETEXT_LEFT,0,0
		dc.w	104,43,26,11
		dc.l	addsufftext,0
		dc.w	GD_AddSuff
		dc.l	PLACETEXT_LEFT,0,0
		dc.w	135,42,84,13
		dc.l	0,0
		dc.w	GD_Suff
		dc.l	0,0,0
		dc.w	104,58,35,13
		dc.l	offsettext,0
		dc.w	GD_Offset
		dc.l	PLACETEXT_LEFT,0,0


oktext:		dc.b	'Ok',0
canceltext:	dc.b	'Cancel',0

Project0IText0:	dc.b    'Palette Type',0

hstext:		dc.b	'Histo Type',0
addsufftext:	dc.b	'Add Suffix',0
offsettext:	dc.b	'Frame Offset',0

		section laber,bss

LR_FirstFilename:	ds.b	100
LR_LoadName:		ds.b	200
LR_SaveName:		ds.b	200
LR_picmap:		ds.l	10
LR_Pal:			ds.l	256
LR_Prefsback:		ds.b	26
