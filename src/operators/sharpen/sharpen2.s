����  )�&b�&b�&b�&b�&b�&b�&b�&b;=F�;===========================================================================
;
;		Sch�rfen eines Ausschnitt oder Bild
;		(c) 1998 ,Frank (Copper) Pagels  /DFT
;
;		25.06.98

		incdir 	include:

		include	exec/exec_lib.i
		include	exec/lists.i
		include	exec/memory.i
		include intuition/intuition_lib.i
		include libraries/gadtools.i
		include	libraries/wb_lib.i
		include	libraries/reqtools_lib.i
		include	graphics/graphics_lib.i
		Include utility/tagitem.i
		include	dos/dos.i		
		include	dos/dos_lib.i		

		include misc/artpro.i
		include render/render_lib.i
		include render/render.i
		

		OPERATORHEADER sh_NODE

		dc.b	'$VER: Sharpen Operator Modul 0.10 (25.06.98)',0
	even
;---------------------------------------------------------------------------
sh_NODE:
		dc.l	0,0
		dc.b	0,0
		dc.l	sh_name
		dc.l	sh_Party
		dc.b	'SHAP'
		dc.b	'EXTR'	;for extern loader
		dc.l	0
		dc.l	sh_Tags
sh_name:
		dc.b	'Sharpen',0
	even
;---------------------------------------------------------------------------

sh_Tags:
		dc.l	APT_Creator,sh_Creator
		dc.l	APT_Version,5
		dc.l	APT_Info,sh_Info
		dc.l	APT_Prefs,sh_Prefsrout		;routine for Prefs
		dc.l	APT_Prefsbuffer,sh_mirror
		dc.l	APT_Prefsbuffersize,1
		dc.l	APT_PrefsVersion,1
		dc.l	TAG_DONE

sh_Creator:
		dc.b	'(c) 1998 Frank Pagels /DEFECT Softworks',0
	even
sh_Info:
		dc.b	'Sharpen',10,10
		dc.b	'Sharped a image or a selected brush',0
	even

sh_mirror:	dc.w	0

sh_chunkymem	=	APG_Free2

sh_window	dc.l	0
sh_GList	dc.l	0
sh_gadarray	dcb.l	3,0

z1		= APG_Free1
z2		= APG_Free2
z3		= APG_Free3
tmp		= APG_Free8
modulo		= APG_Free4
line		= APG_Free5

;---------------------------------------------------------------------------
sh_Party:

		tst.w	APG_Cutflag(a5)
		bne.b	.brushok
		move.w	#0,APG_xbrush1(a5)	;Es soll das ganze 
		move.w	#0,APG_ybrush1(a5)	;Pic abgesaved werden
		move.w	APG_ImageWidth(a5),APG_xbrush2(a5)
		move.w	APG_ImageHeight(a5),APG_ybrush2(a5)	
.brushok:

;---------------------------------------------------------------------------------
sh_normal:
		tst.l	APG_rndr_rendermem(a5)
		bne.w	sh_true
		jsr	APR_MakeRGBData(a5)
		tst.l	d0
		beq	sh_true
sh_nomem:
		moveq	#APOE_NOMEM,d3
		rts

sh_true:

		moveq	#0,d0
		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
		addq.w	#2,d0			;links rechts zusatzbyte
		lsl.l	#2,d0
		move.l	d0,d5
		mulu.w	#4,d0			;3 zeilen
		move.l	#MEMF_CLEAR!MEMF_ANY,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)
		move.l	d0,APG_Free1(a5)
		beq	sh_nomem
		move.l	d0,tmp(a5)
		add.l	d5,d0
		move.l	d0,APG_Free2(a5)
		add.l	d5,d0
		move.l	d0,APG_Free3(a5)
		
;erste Zeile einlesen

		move.l	APG_rndr_rgb(a5),a0
		move.l	APG_Bytewidth(a5),d0
		lsl.l	#3+2,d0
		moveq	#0,d1
		move.w	APG_xbrush2(a5),d1
		sub.w	APG_xbrush1(a5),d1
		lsl.l	#2,d1
		move.l	d0,d2
		sub.w	d1,d2
		move.l	d2,modulo(a5)

		mulu	APG_ybrush1(a5),d0
		add.l	d0,a0
		moveq	#0,d0
		move.w	APG_xbrush1(a5),d0
		lsl.l	#2,d0
		add.l	d0,a0		;Anfangsadresse
		move.l	a0,a4
		
		move.w	APG_xbrush2(a5),d7
		sub.w	APG_xbrush1(a5),d7
		subq.w	#2,d7
		move.l	z2(a5),a1
		move.l	(a0)+,(a1)
		move.l	(a1)+,d0
		move.l	d0,(a1)+
.nl:
		move.l	(a0)+,(a1)+
		dbra	d7,.nl
		move.l	-1(a1),(a1)

	move.l	a4,a0
	;	add.l	modulo(a5),a0

		move.w	APG_ybrush2(a5),d0
		sub.w	APG_ybrush1(a5),d0
		move.w	d0,line(a5)

		move.w	APG_xbrush2(a5),d7
		sub.w	APG_xbrush1(a5),d7
		subq.w	#2,d7
		move.l	z3(a5),a1
		move.l	(a0)+,(a1)
		move.l	(a1)+,d0
		move.l	d0,(a1)+
.nl2:
		move.l	(a0)+,(a1)+
		dbra	d7,.nl2
		move.l	-1(a1),(a1)
		

		jsr	APR_OpenProcess(a5)
		lea	sh_workmsg(pc),a1
		moveq	#0,d0
		move.w	line(a5),d0
		jsr	APR_InitProcess(a5)
sh_newline:
		jsr	APR_DoProcess(a5)

		move.l	z1(a5),-(a7)
		move.l	z2(a5),z1(a5)	;zeilen vertauschen
		move.l	z3(a5),z2(a5)
		move.l	(a7)+,z3(a5)

		add.l	modulo(a5),a0

		move.w	APG_xbrush2(a5),d7
		sub.w	APG_xbrush1(a5),d7
		subq.w	#2,d7
		move.l	z3(a5),a1
		move.l	(a0)+,(a1)
		move.l	(a1)+,d0
		move.l	d0,(a1)+
.nl3:
		move.l	(a0)+,(a1)+
		dbra	d7,.nl3
		move.l	-1(a1),(a1)

		move.l	z1(a5),a1
		move.l	z2(a5),a2
		move.l	z3(a5),a3

		moveq	#0,d0
		move.w	APG_xbrush2(a5),d2
		sub.w	APG_xbrush1(a5),d2
		sub.w	#1,d2

sh_newrow:

;erste Spalte
		moveq	#0,d4
		moveq	#0,d3
		move.b	3(a1,d0*4),d4	;Blau
		move.b	3(a2,d0*4),d3
		add.l	d3,d4
		move.b	3(a3,d0*4),d3
		add.l	d3,d4

		moveq	#0,d5
		moveq	#0,d3
		move.b	2(a1,d0*4),d5	;Gr�n
		move.b	2(a2,d0*4),d3
		add.l	d3,d5
		move.b	2(a3,d0*4),d3
		add.l	d3,d5

		moveq	#0,d6
		moveq	#0,d3
		move.b	1(a1,d0*4),d6	;Rot
		move.b	1(a2,d0*4),d3
		add.l	d3,d6
		move.b	1(a3,d0*4),d3
		add.l	d3,d6

;zeite spalte

		add.w	#1,d0

		moveq	#0,d3
		move.b	3(a1,d0*4),d3	;Blau
		add.l	d3,d4
		move.b	3(a2,d0*4),d3
		mulu.l	#-8,d3		;optimieren ?
		add.l	d3,d4
		moveq	#0,d3
		move.b	3(a3,d0*4),d3
		add.l	d3,d4

		moveq	#0,d3
		move.b	2(a1,d0*4),d3	;Gr�n
		add.l	d3,d5
		move.b	2(a2,d0*4),d3
		mulu.l	#-8,d3
		add.l	d3,d5
		moveq	#0,d3
		move.b	2(a3,d0*4),d3
		add.l	d3,d5

		moveq	#0,d3
		move.b	1(a1,d0*4),d3	;Rot
		add.l	d3,d6
		move.b	1(a2,d0*4),d3
		mulu.l	#-8,d3
		add.l	d3,d6
		moveq	#0,d3
		move.b	1(a3,d0*4),d3
		add.l	d3,d6
		
;dritte spalte

		add.w	#1,d0

		moveq	#0,d3
		move.b	3(a1,d0*4),d3	;Blau
		add.l	d3,d4
		move.b	3(a2,d0*4),d3
		add.l	d3,d4
		move.b	3(a3,d0*4),d3
		add.l	d3,d4

		moveq	#0,d3
		move.b	2(a1,d0*4),d3	;Gr�n
		add.l	d3,d5
		move.b	2(a2,d0*4),d3
		add.l	d3,d5
		move.b	2(a3,d0*4),d3
		add.l	d3,d5

		moveq	#0,d3
		move.b	1(a1,d0*4),d3	;Rot
		add.l	d3,d6
		move.b	1(a2,d0*4),d3
		add.l	d3,d6
		move.b	1(a3,d0*4),d3
		add.l	d3,d6

;werte normieren

		cmp.l	#256,d6
		blt	.klr
		move.l	#255,d6
.klr		tst.l	d6
		bpl	.okr
		moveq	#0,d6
.okr:
		cmp.l	#256,d5
		blt	.klg
		move.l	#255,d5
.klg		tst.l	d5
		bpl	.okg
		moveq	#0,d5
.okg:
		cmp.l	#256,d4
		blt	.klb
		move.l	#255,d4
.klb		tst.l	d4
		bpl	.okb
		moveq	#0,d4
.okb:

		jsr	apr_dummy(a5)

		movem.l	d0/d2,-(a7)

		lsr.l	#1,d4
		lsr.l	#1,d5
		lsr.l	#1,d6
		move.l	(a4),d0
		moveq	#0,d1
		move.b	d0,d1	;b
		lsr.l	#8,d0
		moveq	#0,d2
		move.b	d0,d2	;g
		lsr.l	#8,d0	;r

		sub.w	d4,d1
		bpl	.w
		moveq	#0,d1
.w		sub.w	d5,d2
		bpl	.w1
		moveq	#0,d2
.w1		sub.w	d6,d0
		bpl	.w2
		moveq	#0,d0
.w2
		swap	d0
		move.b	d2,d0
		lsl.w	#8,d0
		move.b	d1,d0

		move.l	d0,(a4)+	;wieder zur�ckschreiben

		movem.l	(a7)+,d0/d2

		subq.w	#1,d0
		dbra	d2,sh_newrow

		add.l	modulo(a5),a4

		move.w	line(a5),d5
		subq.w	#1,d5
		beq	sh_end
		move.w	d5,line(a5)
		bra	sh_newline
sh_end:

		jsr	apr_dummy(a5)

		move.l	tmp(a5),d0
		beq	.nomem
		move.l	d0,a1
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)
		clr.l   tmp(a5)
.nomem:
		jsr	APR_ClearProcess(a5)
		moveq	#0,d3
		rts


		
;----------------------------------------------------------------------------
sh_Prefsrout
		moveq	#0,d0
		move.b	sh_mirror(pc),d0
		lea	sh_MXmirror(pc),a0
		move.l	d0,(a0)
;----------------------------------------------------------------------------

		move.l	#WindowTags,APG_WindowTagList(a5)	;Tag list for the Window
		move.l	#WinWG+4,APG_WGadgets(a5)	;addr. of WA_Gadgets Tag + 4
		move.l	#winSC+4,APG_WScreen(a5)	;addr. of WA_CustomScreen Tag + 4
		move.l	#sh_window,APG_WinWnd(a5)	;a point for the Window
		move.l	#sh_Glist,APG_Glist(a5)		;a point for the Glist
		move.l	#NTypes,APG_NGads(a5)		;the NGads list
		move.l	#Gtypes,APG_GTypes(a5)		;the GTypes list
		move.l	#Gtags,APG_Gtags(a5)		;the Gtags list
		move.w	#3,APG_CNT(a5)			;the number of Gadgets
		move.l	#sh_gadarray,APG_Gadgets(a5)	;a pointer for the Gadgetsarry, size=4*number of Gadgets
		move.l	APG_Scr(a5),APG_ScrBase(a5)	;the Screenpointer , stored in APG_Scr
	
		move.l	APG_CheckMainWinPos(a5),a6
		jsr	(a6)

		add.w	#315+30,d0
		add.w	#100,d1
		
		move.w	d0,APG_WinLeft(a5)		;the left pos. of the Win
		move.w	d1,APG_WinTop(a5)		;the top pos. of the Win
		move.w	#160,APG_WinWidth(a5)		;the width of the Win
		move.w	#70,APG_WinHeight(a5)		;the height of the Win

		move.l	#WinL,APG_WinL(a5)		;addr. of WA_Left Tag 
		move.l	#WinT,APG_WinT(a5)		;addr. of WA_Top Tag
		move.l	#WinW,APG_WinW(a5)		;addr. of WA_Width Tag
		move.l	#WinH,APG_WinH(a5)		;addr. of WA_Height Tag

		move.l	APG_OpenWindow(a5),a6
		jsr	(a6)

		lea	Text0(pc),a2
		move.w	#Project0_TNUM,APG_TNUM(a5)
		move.l	APG_Writetext(a5),a6
		jsr	(a6)

		move.b	sh_mirror(pc),APG_Mark1(a5)	;rette alte Prefs

sh_wait:	move.l	sh_window(pc),a0
		move.l	APG_Wait(a5),a6
		jsr	(a6)

		cmp.l	#IDCMP_CLOSEWINDOW,d4
		beq.b	sh_Cancel		
		cmp.l	#GADGETUP,d4
		beq.b	sh_gads
		cmp.l	#GADGETDOWN,d4
		beq.b	sh_gads
		bra.b	sh_wait

sh_gads:
		moveq	#0,d0
		move.w	38(a4),d0

		add.l	d0,d0
		lea	sh_jumptab(pc),a0
		move.w	(a0,d0.l),d0
		jmp	(a0,d0.w)

sh_cancel:
		move.b	APG_Mark1(a5),d0
		ext.w	d0
		ext.l	d0
		move.b	d0,sh_mirror
		move.l	d0,sh_MXmirror

sh_pok:
		move.l	sh_window(pc),a0
		move.l	APG_Intuitionbase(a5),a6
		jmp	_LVOCloseWindow(a6)

sh_jumptab:
		dc.w	handlemirror-sh_jumptab
		dc.w	sh_pok-sh_jumptab,sh_cancel-sh_jumptab

handlemirror:
		move.b	d5,sh_mirror
		ext.w	d5
		ext.l	d5
		move.l	d5,sh_MXmirror
		bra.b	sh_wait

;------------------------------------------------------------------------------
WindowTags:
winL:	dc.l	WA_Left,212
winT:	dc.l	WA_Top,78
winW:	dc.l	WA_Width,220
winH:	dc.l	WA_Height,85
		dc.l	WA_IDCMP,IDCMP_CLOSEWINDOW!MXIDCMP!GADGETUP!VANILLAKEY!BUTTONIDCMP!IDCMP_REFRESHWINDOW
		dc.l	WA_Flags,WFLG_CLOSEGADGET!WFLG_DEPTHGADGET!WFLG_ACTIVATE!WFLG_DRAGBAR!WFLG_SMART_REFRESH!WFLG_RMBTRAP
winWG:	dc.l	WA_Gadgets,0
		dc.l	WA_Title,winWTitle
winSC:	dc.l	WA_CustomScreen,0
		dc.l	TAG_DONE

WinWTitle:	dc.b	'Flip X Options',0
	even

GD_Mirror	=	0
GD_Ok		=	1
GD_Cancel	=	2

Gtypes:
		dc.w	MX_KIND
		dc.w	BUTTON_KIND
		dc.w	BUTTON_KIND
Gtags:
		dc.l	GTMX_Labels,Gadget00Labels
		dc.l	GTMX_Active
sh_mxmirror:	dc.l	0
		dc.l	TAG_DONE
		dc.l	GT_Underscore,'_'
		dc.l	TAG_DONE
		dc.l	GT_Underscore,'_'
		dc.l	TAG_DONE
Gadget00Labels:
		dc.l	Gadget00Lab0
		dc.l	Gadget00Lab1
		dc.l	0

Gadget00Lab0:	dc.b	'Flip',0
Gadget00Lab1:	dc.b	'Mirror',0

NTypes:
		DC.W    70,20,17,9
		DC.L    0,0
		DC.W    GD_mirror
		DC.L    0,0,0
		DC.W    10,50,60,13
		DC.L    oktext,0
		DC.W    GD_Ok
		DC.L    PLACETEXT_IN,0,0
		DC.W    85,50,60,13
		DC.L    canceltext,0
		DC.W    GD_Cancel
		DC.L    PLACETEXT_IN,0,0

oktext:		dc.b	'_Ok',0
canceltext:	dc.b	'_Cancel',0

Text0:
		DC.B    2,0
		DC.B    RP_JAM1
		DC.B    0
		DC.W    80,10
		DC.L    0
		DC.L    Project0IText0
		DC.L    0

Project0_TNUM EQU 1

Project0IText0:
		DC.B    'Choose',0

sh_workmsg:	dc.b	'Sharping image!',0

;----------------------------------------------------------------------------
		section	laber,bss
sh_bitmaptab:	ds.l	8
