����  �  B��&b�&b�&b�&b�&b�&b�&b;=a�;===========================================================================
;
;		Sch�rfen eines Ausschnitt oder Bild
;		(c) 1998 ,Frank (Copper) Pagels  /DFT
;
;		25.06.98

		incdir 	include:

		include	exec/exec_lib.i
		include	exec/lists.i
		include	exec/memory.i
		include intuition/intuition_lib.i
		include libraries/gadtools.i
		include libraries/gadtools_lib.i
		include	libraries/wb_lib.i
		include	libraries/reqtools_lib.i
		include	graphics/graphics_lib.i
		Include utility/tagitem.i
		include	dos/dos.i		
		include	dos/dos_lib.i		

		include misc/artpro.i
		include render/render_lib.i
		include render/render.i
		

		OPERATORHEADER sh_NODE

		dc.b	'$VER: Sharpen Operator Modul 1.10 (09.07.98)',0
	even
;---------------------------------------------------------------------------
sh_NODE:
		dc.l	0,0
		dc.b	0,0
		dc.l	sh_name
		dc.l	sh_Party
		dc.b	'SHAP'
		dc.b	'EXTR'	;for extern loader
		dc.l	0
		dc.l	sh_Tags
sh_name:
		dc.b	'Sharpen',0
	even
;---------------------------------------------------------------------------

sh_Tags:
		dc.l	APT_Creator,sh_Creator
		dc.l	APT_Version,5
		dc.l	APT_Info,sh_Info
		dc.l	APT_Prefs,sh_Prefsrout		;routine for Prefs
		dc.l	APT_Prefsbuffer,sh_prefs
		dc.l	APT_Prefsbuffersize,6
		dc.l	APT_PrefsVersion,1
		dc.l	TAG_DONE

sh_Creator:
		dc.b	'(c) 1998 Frank Pagels /Defect Softworks',0
	even
sh_Info:
		dc.b	'Sharpen',10,10
		dc.b	'Sharped a image or a selected brush',0
	even

sh_window	dc.l	0
sh_GList	dc.l	0
sh_gadarray	ds.l	6

z1		= APG_Free1
z2		= APG_Free2
z3		= APG_Free3
tmp		= APG_Free8
modulo		= APG_Free4
line		= APG_Free5

rtmp		= APG_Free6
gtmp		= APG_Free7
btmp		= APG_Free9

sh_Prefs
sh_Quali:	dc.l	25
sh_askinput:	dc.b	0
sh_mode:	dc.b	0
	even
sh_pbak:	dc.l	0,0
	even

;---------------------------------------------------------------------------
sh_Party:

		tst.b	sh_askinput
		bne	.w
		bsr	sh_askfor
.w		tst.w	APG_Cutflag(a5)
		bne.b	.brushok
		move.w	#0,APG_xbrush1(a5)	;Es soll das ganze 
		move.w	#0,APG_ybrush1(a5)	;Pic abgesaved werden
		move.w	APG_ImageWidth(a5),APG_xbrush2(a5)
		move.w	APG_ImageHeight(a5),APG_ybrush2(a5)	
.brushok:

;---------------------------------------------------------------------------------
sh_normal:
		tst.l	APG_rndr_rendermem(a5)
		bne.w	sh_true
		jsr	APR_MakeRGBData(a5)
		tst.l	d0
		beq	sh_true
sh_nomem:
		moveq	#APOE_NOMEM,d3
		rts

sh_true:

		moveq	#0,d0
		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
		addq.w	#2,d0			;links rechts zusatzbyte
		lsl.l	#2,d0
		move.l	d0,d5
		mulu.w	#4,d0			;3 zeilen
		move.l	#MEMF_CLEAR!MEMF_ANY,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)
		move.l	d0,APG_Free1(a5)
		beq	sh_nomem
		move.l	d0,tmp(a5)
		add.l	d5,d0
		move.l	d0,APG_Free2(a5)
		add.l	d5,d0
		move.l	d0,APG_Free3(a5)
		
;erste Zeile einlesen

		move.l	APG_rndr_rgb(a5),a0
		move.l	APG_Bytewidth(a5),d0
		lsl.l	#3+2,d0
		moveq	#0,d1
		move.w	APG_xbrush2(a5),d1
		sub.w	APG_xbrush1(a5),d1
		lsl.l	#2,d1
		move.l	d0,d2
		sub.w	d1,d2
		move.l	d2,modulo(a5)

		mulu	APG_ybrush1(a5),d0
		add.l	d0,a0
		moveq	#0,d0
		move.w	APG_xbrush1(a5),d0
		lsl.l	#2,d0
		add.l	d0,a0		;Anfangsadresse
		move.l	a0,a4
		
		move.w	APG_xbrush2(a5),d7
		sub.w	APG_xbrush1(a5),d7
		subq.w	#2,d7
		move.l	z2(a5),a1
		move.l	(a0)+,(a1)
		move.l	(a1)+,d0
		move.l	d0,(a1)+
.nl:
		move.l	(a0)+,(a1)+
		dbra	d7,.nl
		move.l	-1(a1),(a1)

		move.l	a4,a0
	;	add.l	modulo(a5),a0

		move.w	APG_ybrush2(a5),d0
		sub.w	APG_ybrush1(a5),d0
		move.w	d0,line(a5)

		move.w	APG_xbrush2(a5),d7
		sub.w	APG_xbrush1(a5),d7
		subq.w	#2,d7
		move.l	z3(a5),a1
		move.l	(a0)+,(a1)
		move.l	(a1)+,d0
		move.l	d0,(a1)+
.nl2:
		move.l	(a0)+,(a1)+
		dbra	d7,.nl2
		move.l	-1(a1),(a1)
		

		jsr	APR_OpenProcess(a5)
		lea	sh_workmsg(pc),a1
		moveq	#0,d0
		move.w	line(a5),d0
		jsr	APR_InitProcess(a5)

		tst.b	sh_mode
		bne	sh_hq		;mache ein hq sch�rfen

sh_newline:
		jsr	APR_DoProcess(a5)

		move.l	z1(a5),-(a7)
		move.l	z2(a5),z1(a5)	;zeilen vertauschen
		move.l	z3(a5),z2(a5)
		move.l	(a7)+,z3(a5)

		add.l	modulo(a5),a0

		move.w	APG_xbrush2(a5),d7
		sub.w	APG_xbrush1(a5),d7
		subq.w	#2,d7
		move.l	z3(a5),a1
		move.l	(a0)+,(a1)
		move.l	(a1)+,d0
		move.l	d0,(a1)+
.nl3:
		move.l	(a0)+,(a1)+
		dbra	d7,.nl3
		move.l	-1(a1),(a1)

		move.l	z1(a5),a1
		move.l	z2(a5),a2
		move.l	z3(a5),a3

		moveq	#0,d0
		move.w	APG_xbrush2(a5),d2
		sub.w	APG_xbrush1(a5),d2
		sub.w	#1,d2

sh_newrow:

;erste Spalte
		moveq	#0,d4
		moveq	#0,d3
		move.b	3(a1,d0*4),d4	;Blau
		move.b	3(a2,d0*4),d3
		add.l	d3,d4
		move.b	3(a3,d0*4),d3
		add.l	d3,d4

		moveq	#0,d5
		moveq	#0,d3
		move.b	2(a1,d0*4),d5	;Gr�n
		move.b	2(a2,d0*4),d3
		add.l	d3,d5
		move.b	2(a3,d0*4),d3
		add.l	d3,d5

		moveq	#0,d6
		moveq	#0,d3
		move.b	1(a1,d0*4),d6	;Rot
		move.b	1(a2,d0*4),d3
		add.l	d3,d6
		move.b	1(a3,d0*4),d3
		add.l	d3,d6

;zeite spalte

		add.w	#1,d0

		moveq	#0,d3
		move.b	3(a1,d0*4),d3	;Blau
		add.l	d3,d4
		move.b	3(a2,d0*4),d3
	;	mulu.l	#-8,d3		;optimieren ?
		lsl.l	#3,d3
		neg.l	d3
		add.l	d3,d4
		moveq	#0,d3
		move.b	3(a3,d0*4),d3
		add.l	d3,d4

		moveq	#0,d3
		move.b	2(a1,d0*4),d3	;Gr�n
		add.l	d3,d5
		move.b	2(a2,d0*4),d3
	;	mulu.l	#-8,d3
		lsl.l	#3,d3
		neg.l	d3
		add.l	d3,d5
		moveq	#0,d3
		move.b	2(a3,d0*4),d3
		add.l	d3,d5

		moveq	#0,d3
		move.b	1(a1,d0*4),d3	;Rot
		add.l	d3,d6
		move.b	1(a2,d0*4),d3
	;	mulu.l	#-8,d3
	lsl.l	#3,d3
	neg.l	d3
		add.l	d3,d6
		moveq	#0,d3
		move.b	1(a3,d0*4),d3
		add.l	d3,d6
		
;dritte spalte

		add.w	#1,d0

		moveq	#0,d3
		move.b	3(a1,d0*4),d3	;Blau
		add.l	d3,d4
		move.b	3(a2,d0*4),d3
		add.l	d3,d4
		move.b	3(a3,d0*4),d3
		add.l	d3,d4

		moveq	#0,d3
		move.b	2(a1,d0*4),d3	;Gr�n
		add.l	d3,d5
		move.b	2(a2,d0*4),d3
		add.l	d3,d5
		move.b	2(a3,d0*4),d3
		add.l	d3,d5

		moveq	#0,d3
		move.b	1(a1,d0*4),d3	;Rot
		add.l	d3,d6
		move.b	1(a2,d0*4),d3
		add.l	d3,d6
		move.b	1(a3,d0*4),d3
		add.l	d3,d6

;Laplace vom Original subtraieren

		movem.l	d0/d2,-(a7)

		move.l	sh_Quali(pc),d0
		muls.l	d0,d4
		muls.l	d0,d5
		muls.l	d0,d6
		divs.l	#100,d4
		divs.l	#100,d5
		divs.l	#100,d6

		move.l	(a4),d0
		moveq	#0,d1
		move.b	d0,d1	;b
		lsr.l	#8,d0
		moveq	#0,d2
		move.b	d0,d2	;g
		lsr.l	#8,d0	;r

		sub.l	d4,d1
		cmp.l	#256,d1
		blt	.klb
		move.l	#255,d1
.klb		tst.l	d1
		bpl	.okb
		moveq	#0,d1
.okb
		sub.l	d5,d2
		cmp.l	#256,d2
		blt	.klg
		move.l	#255,d2
.klg		tst.l	d2
		bpl	.okg
		moveq	#0,d2
.okg
		sub.l	d6,d0
		sub.l	d5,d0
		cmp.l	#256,d0
		blt	.klr
		move.l	#255,d0
.klr		tst.l	d0
		bpl	.okr
		moveq	#0,d0
.okr

		swap	d0
		move.b	d2,d0
		lsl.w	#8,d0
		move.b	d1,d0

		move.l	d0,(a4)+	;wieder zur�ckschreiben

		movem.l	(a7)+,d0/d2

		subq.w	#1,d0
		dbra	d2,sh_newrow

		add.l	modulo(a5),a4

		move.w	line(a5),d5
		subq.w	#1,d5
		beq	sh_end
		move.w	d5,line(a5)
		bra	sh_newline
sh_end:
		cmp.b	#16,APG_Planes(a5)
		bhs	.norender
		jsr	APR_Render(a5)
		bra	.nopreview
.norender:
		jsr	APR_Makepreview(a5)
.nopreview
		move.l	tmp(a5),d0
		beq	.nomem
		move.l	d0,a1
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)
		clr.l   tmp(a5)
.nomem:
		jsr	APR_ClearProcess(a5)
		moveq	#0,d3
		rts

;----------------------------------------------------------------------
; mache ein hq sch�rfen mit 4 sobel operatoren
sh_hq

sh_hqnewline:
		jsr	APR_DoProcess(a5)

		move.l	z1(a5),-(a7)
		move.l	z2(a5),z1(a5)	;zeilen vertauschen
		move.l	z3(a5),z2(a5)
		move.l	(a7)+,z3(a5)

		add.l	modulo(a5),a0

		move.w	APG_xbrush2(a5),d7
		sub.w	APG_xbrush1(a5),d7
		subq.w	#2,d7
		move.l	z3(a5),a1
		move.l	(a0)+,(a1)
		move.l	(a1)+,d0
		move.l	d0,(a1)+
.nl3:
		move.l	(a0)+,(a1)+
		dbra	d7,.nl3
		move.l	-1(a1),(a1)

		move.l	z1(a5),a1
		move.l	z2(a5),a2
		move.l	z3(a5),a3

		moveq	#0,d0
		move.w	APG_xbrush2(a5),d2
		sub.w	APG_xbrush1(a5),d2
		sub.w	#1,d2


sh_hqnewrow:

		lea	matrixtab(pc),a6

;erste Spalte
		moveq	#7,d7
		clr.l	rtmp(a5)
		clr.l	gtmp(a5)
		clr.l	btmp(a5)

sh_hqnewpixel:
		swap	d7
		move.w	#2,d7
		move.l	d0,-(a7)
		moveq	#0,d4
		moveq	#0,d3
		moveq	#0,d5
		moveq	#0,d6

.nr:
		move.l	(a6)+,d1	;Marix wert

		moveq	#0,d3
		move.b	3(a1,d0*4),d3	;Blau
		muls.l	d1,d3
		add.l	d3,d4
		moveq	#0,d3
		move.b	2(a1,d0*4),d3	;Gr�n
		muls.l	d1,d3
		add.l	d3,d5
		moveq	#0,d3
		move.b	1(a1,d0*4),d3	;Rot
		muls.l	d1,d3
		add.l	d3,d6

		move.l	(a6)+,d1	;Marix wert

		moveq	#0,d3
		move.b	3(a2,d0*4),d3	;Blau
		muls.l	d1,d3
		add.l	d3,d4
		moveq	#0,d3
		move.b	2(a2,d0*4),d3	;Gr�n
		muls.l	d1,d3
		add.l	d3,d5
		moveq	#0,d3
		move.b	1(a2,d0*4),d3	;Rot
		muls.l	d1,d3
		add.l	d3,d6

		move.l	(a6)+,d1	;Marix wert

		moveq	#0,d3
		move.b	3(a3,d0*4),d3	;Blau
		muls.l	d1,d3
		add.l	d3,d4
		moveq	#0,d3
		move.b	2(a3,d0*4),d3	;Gr�n
		muls.l	d1,d3
		add.l	d3,d5
		moveq	#0,d3
		move.b	1(a3,d0*4),d3	;Rot
		muls.l	d1,d3
		add.l	d3,d6

		add.w	#1,d0

		dbra	d7,.nr

		move.l	rtmp(a5),d1
		tst.l	d1
		bpl	.rok1
		neg	d1
.rok1:		move.l	d4,d3
		tst.l	d3
		bpl	.rok
		neg	d3
.rok:		cmp.w	d3,d1
		bhi	.rw
		move.l	d4,rtmp(a5)
.rw:
		move.l	gtmp(a5),d1
		tst.l	d1
		bpl	.gok1
		neg	d1
.gok1:		move.l	d5,d3
		tst.l	d3
		bpl	.gok
		neg	d3
.gok:		cmp.w	d3,d1
		bhi	.gw
		move.l	d5,gtmp(a5)
.gw:
		move.l	btmp(a5),d1
		tst.l	d1
		bpl	.bok1
		neg	d1
.bok1:		move.l	d6,d3
		tst.l	d3
		bpl	.bok
		neg	d3
.bok:		cmp.w	d3,d1
		bhi	.bw
		move.l	d6,btmp(a5)
.bw:

		swap	d7
		move.l	(a7)+,d0
		dbra	d7,sh_hqnewpixel		

;Laplace vom Original subtraieren

	jsr	apr_dummy(a5)

		movem.l	d0/d2,-(a7)

		move.l	rtmp(a5),d4
		move.l	gtmp(a5),d5
		move.l	btmp(a5),d6

		move.l	sh_Quali(pc),d0
		muls.l	d0,d4
		muls.l	d0,d5
		muls.l	d0,d6
		divs.l	#100,d4
		divs.l	#100,d5
		divs.l	#100,d6

		move.l	(a4),d0
		moveq	#0,d1
		move.b	d0,d1	;b
		lsr.l	#8,d0
		moveq	#0,d2
		move.b	d0,d2	;g
		lsr.l	#8,d0	;r

		sub.l	d4,d1
		cmp.l	#256,d1
		blt	.klb
		move.l	#255,d1
.klb		tst.l	d1
		bpl	.okb
		moveq	#0,d1
.okb
		sub.l	d5,d2
		cmp.l	#256,d2
		blt	.klg
		move.l	#255,d2
.klg		tst.l	d2
		bpl	.okg
		moveq	#0,d2
.okg
		sub.l	d6,d0
		sub.l	d5,d0
		cmp.l	#256,d0
		blt	.klr
		move.l	#255,d0
.klr		tst.l	d0
		bpl	.okr
		moveq	#0,d0
.okr

		swap	d0
		move.b	d2,d0
		lsl.w	#8,d0
		move.b	d1,d0

		move.l	d0,(a4)+	;wieder zur�ckschreiben

		movem.l	(a7)+,d0/d2

		addq.w	#1,d0
		dbra	d2,sh_hqnewrow

		add.l	modulo(a5),a4

		move.w	line(a5),d5
		subq.w	#1,d5
		beq	sh_end
		move.w	d5,line(a5)
		bra	sh_hqnewline


;----------------------------------------------------------------------------
sh_askfor:
		moveq	#1,d0
		lea	sh_popdis(pc),a0
		move.l	d0,(a0)
		bra	sh_pweiter
sh_Prefsrout:
;init Prefs
		lea	sh_popdis(pc),a0
		clr.l	(a0)
sh_pweiter:
		move.l	sh_quali(pc),d0
		lea	sh_Level(pc),a0
		move.l	d0,(a0)
		lea	sh_numlevel(pc),a0
		move.l	d0,(a0)
		moveq	#0,d0
		move.b	sh_askinput(pc),d0
		lea	sh_dontpop(pc),a0
		move.l	d0,(a0)
		move.b	sh_mode(pc),d0
		lea	sh_modet(pc),a0
		move.l	d0,(a0)

;---------------------------------------------------------------------------

		move.l	#WindowTags,APG_WindowTagList(a5) ;Tag list for the Window
		move.l	#WinWG+4,APG_WGadgets(a5)	;addr. of WA_Gadgets Tag + 4
		move.l	#winSC+4,APG_WScreen(a5)	;addr. of WA_CustomScreen Tag + 4
		move.l	#sh_window,APG_WinWnd(a5)	;a point for the Window
		move.l	#sh_Glist,APG_Glist(a5)		;a point for the Glist
		move.l	#NTypes,APG_NGads(a5)		;the NGads list
		move.l	#Gtypes,APG_GTypes(a5)		;the GTypes list
		move.l	#Gtags,APG_Gtags(a5)		;the Gtags list
		move.w	#6,APG_CNT(a5)			;the number of Gadgets
		move.l	#sh_gadarray,APG_Gadgets(a5)	;a pointer for the Gadgetsarry, size=4*number of Gadgets
		move.l	APG_Scr(a5),APG_ScrBase(a5)	;the Screenpointer , stored in APG_Scr
	
		move.l	APG_CheckMainWinPos(a5),a6	;returns d0,d1 Pos
		jsr	(a6)

		add.w	#320,d0
		add.w	#100,d1
		
		move.w	d0,APG_WinLeft(a5)		;the left pos. of the Win
		move.w	d1,APG_WinTop(a5)		;the top pos. of the Win
		move.w	#210,APG_WinWidth(a5)		;the width of the Win
		move.w	#55+30,APG_WinHeight(a5)		;the height of the Win

		move.l	#WinL,APG_WinL(a5)		;addr. of WA_Left Tag 
		move.l	#WinT,APG_WinT(a5)		;addr. of WA_Top Tag
		move.l	#WinW,APG_WinW(a5)		;addr. of WA_Width Tag
		move.l	#WinH,APG_WinH(a5)		;addr. of WA_Height Tag

		move.l	APG_OpenWindow(a5),a6
		jsr	(a6)

		lea	sh_pbak(pc),a0
		move.l	sh_Quali(pc),(a0)+	;Prefs backuppen
		move.b	sh_askinput(pc),(a0)+
		move.b	sh_mode(pc),(a0)

sh_wait:
		move.l	sh_window(pc),a0
		move.l	APG_Wait(a5),a6
		jsr	(a6)				;Handle input

		cmp.l	#GADGETDOWN,d4
		beq.w	sh_slider
		cmp.l	#CLOSEWINDOW,d4
		beq.w	sh_Cancel
		cmp.l	#GADGETUP,d4
		beq.b	sh_gads
		bra.b	sh_wait

sh_gads:
		moveq	#0,d0
		move.w	38(a4),d0
		add.l	d0,d0
		move.l	d0,d1
		lea	sh_pjumptab(pc),a0
		move.w	(a0,d0.l),d0
		jmp	(a0,d0.w)

sh_pjumptab:
		dc.w	sh_handlequali-sh_pjumptab
		dc.w	sh_ok-sh_pjumptab
		dc.w	sh_cancel-sh_pjumptab
		dc.w	sh_handelnum-sh_pjumptab
		dc.w	sh_handlepop-sh_pjumptab
		dc.w	sh_handlemode-sh_pjumptab

sh_handlemode:
		move.b	d5,sh_mode
		ext.l	d5
		move.l	d5,sh_modet
		bra	sh_wait

sh_handlepop:
		sf	sh_askinput
		lea	sh_gadarray(pc),a0
		move.l	GD_dontpop*4(a0),a1
		move.w	gg_Flags(a1),d0
		and.w	#SELECTED,d0
		beq	sh_wait
		st	sh_askinput
		bra	sh_wait

sh_handlequali:
		ext.l	d5
		move.l	d5,sh_Quali

		lea	sh_numtag(pc),a3
		move.l	d5,4(a3)
		move.l	#GD_qinter,d0
		lea	sh_gadarray(pc),a0
		move.l	(a0,d0.l*4),a0
		move.l	sh_window(pc),a1
		sub.l	a2,a2
		move.l	APG_Gadtoolsbase(a5),a6
		jsr	_LVOGT_SetGadgetAttrsA(a6)

sh_sliderwait:
		move.l	sh_window(pc),a0
		move.l	APG_Wait(a5),a6
		jsr	(a6)				;Handle input

		cmp.l	#GADGETUP,d4
		beq.w	sh_wait			;Slidergadet wurde losgelassen

		cmp.l	#IDCMP_MOUSEMOVE,d4
		beq.b	sh_slider
		bra.b	sh_sliderwait

sh_slider:
		move.w	38(a4),d0
		cmp.w	#GD_quali,d0
		beq.b	sh_handlequali

		bra.w	sh_wait
sh_handelnum:
		lea	sh_numtag1(pc),a3
		move.l	#GD_qinter,d0
		lea	sh_gadarray(pc),a0
		move.l	(a0,d0.l*4),a0
		move.l	sh_window(pc),a1
		sub.l	a2,a2
		move.l	APG_Gadtoolsbase(a5),a6
		jsr	_LVOGT_GetGadgetAttrsA(a6)

		move.l	sh_number(pc),d5
		cmp.l	#100,d5
		bls.b	.w
		move.l	#100,d5			;eingetragener wert war zu hoch

		lea	sh_numtag(pc),a3
		move.l	d5,4(a3)
		move.l	#GD_qinter,d0
		lea	sh_gadarray(pc),a0
		move.l	(a0,d0.l*4),a0
		move.l	sh_window(pc),a1
		sub.l	a2,a2
		jsr	_LVOGT_SetGadgetAttrsA(a6)
.w
		move.l	d5,sh_Quali

		lea	sh_leveltag(pc),a3
		move.l	d5,4(a3)
		move.l	#GD_quali,d0
		lea	sh_gadarray(pc),a0
		move.l	(a0,d0.l*4),a0
		move.l	sh_window(pc),a1
		sub.l	a2,a2
		jsr	_LVOGT_SetGadgetAttrsA(a6)

		bra.w	sh_wait


sh_cancel:
		lea	sh_pbak(pc),a0
		lea	sh_Quali(pc),a1
		move.l	(a0)+,(a1)+
		move.b	(a0)+,(a1)+
		move.b	(a0)+,(a1)+

sh_ok:
		move.l	sh_window(pc),a0
		move.l	APG_Intuitionbase(a5),a6
		jmp	_LVOCloseWindow(a6)

;------------------------------------------------------------------------------
	even

WindowTags:
winL:	dc.l	WA_Left,212
winT:	dc.l	WA_Top,78
winW:	dc.l	WA_Width,220
winH:	dc.l	WA_Height,85
		dc.l	WA_IDCMP,SLIDERIDCMP!IDCMP_CLOSEWINDOW!GADGETUP!GADGETDOWN!VANILLAKEY!BUTTONIDCMP!IDCMP_REFRESHWINDOW!INTEGERIDCMP!IDCMP_MOUSEBUTTONS
		dc.l	WA_Flags,WFLG_CLOSEGADGET!WFLG_DEPTHGADGET!WFLG_ACTIVATE!WFLG_DRAGBAR!WFLG_SMART_REFRESH!WFLG_RMBTRAP
winWG:	dc.l	WA_Gadgets,0
		dc.l	WA_Title,winWTitle
winSC:	dc.l	WA_CustomScreen,0
		dc.l	TAG_DONE

WinWTitle:	dc.b	'JPEG Options',0

	even

sh_numtag:	dc.l	GTIN_Number,0,TAG_DONE

sh_numtag1:
		dc.l	GTIN_Number,sh_number
		dc.l	TAG_DONE

sh_number:	dc.l	0

sh_leveltag:
		dc.l	GTSL_Level,0
		dc.l	TAG_DONE


GD_quali	=	0
GD_Ok		=	1
GD_Cancel	=	2
GD_qinter	=	3
GD_dontpop	=	4
GD_mode		=	5

Gtypes:
		dc.w	SLIDER_KIND
		dc.w	BUTTON_KIND
		dc.w	BUTTON_KIND
		dc.w	INTEGER_KIND
		dc.w	CHECKBOX_KIND
		dc.w	CYCLE_KIND

Gtags:
		dc.l	GTSL_Level
sh_Level:	dc.l	30
		dc.l	GTSL_Max,100
		dc.l	GTSL_MaxLevelLen,3
		dc.l	PGA_Freedom,LORIENT_HORIZ
		dc.l	GA_RelVerify,1
		dc.l	GA_Immediate,1
		dc.l	TAG_DONE
		dc.l	TAG_DONE
		dc.l	TAG_DONE
		dc.l	GTIN_Number
sh_numlevel:	dc.l	30
		dc.l	GTIN_MaxChars,3
		dc.l	TAG_DONE
		dc.l	GTCB_Scaled,1
		dc.l	GTCB_Checked
sh_dontpop:	dc.l	0
		dc.l	GA_Disabled
sh_popdis:	dc.l	0
		dc.l	TAG_DONE
		dc.l	GTCY_Labels,modeLabels
		dc.l	GTCY_ACTIVE
sh_modet:	dc.l	0
		dc.l	GT_Underscore,'_'
		dc.l	TAG_DONE

	even
NTypes:
 		dc.w	10,15,150,14
		dc.l	qualiText,0
		dc.w	GD_quali
		dc.l	PLACETEXT_ABOVE!NG_HIGHLABEL,0,0
		DC.W    10,37+15+15,70,14
		DC.L    oktext,0
		DC.W    GD_Ok
		DC.L    PLACETEXT_IN,0,0
		DC.W    130,37+15+15,70,14
		DC.L    canceltext,0
		DC.W    GD_Cancel
		DC.L    PLACETEXT_IN,0,0
		dc.w	162,15,38,14
		dc.l	0,0
		dc.w	GD_qinter
		dc.l	0,0,0
		dc.w	10,32+17,26,11
		dc.l	poptext,0
		dc.w	GD_dontpop
		dc.l	PLACETEXT_RIGHT,0,0
		dc.w	10,32,80,14
		dc.l	modetext,0
		dc.w	GD_mode
		dc.l	PLACETEXT_RIGHT!NG_HIGHLABEL,0,0


oktext:		dc.b	'Ok',0
canceltext:	dc.b	'Cancel',0
qualiText:	dc.b	'Sharpen Amount',0
poptext:	dc.b	"don't popup",0
modetext:	dc.b	'Mode',0

sh_workmsg:	dc.b	'Sharpen image!',0

	even
modeLabels:
		dc.l	label1
		dc.l	label2
		dc.l	0

label1:		dc.b	'Fast',0
label2:		dc.b	'HQ',0


;matrix tab

matrixtab:

		dc.l	-1,-1,-1
		dc.l	1,-2,1
		dc.l	1,1,1

		dc.l	1,-1,-1
		dc.l	1,-2,-1
		dc.l	1,1,1

		dc.l	1,1,-1
		dc.l	1,-2,-1
		dc.l	1,1,-1

		dc.l	1,1,1
		dc.l	1,-2,-1
		dc.l	1,-1,-1

		dc.l	1,1,1
		dc.l	1,-2,1
		dc.l	-1,-1,-1

		dc.l	1,1,1
		dc.l	-1,-2,1
		dc.l	-1,-1,1

		dc.l	-1,1,1
		dc.l	-1,-2,1
		dc.l	-1,1,1

		dc.l	-1,-1,1
		dc.l	-1,-2,1
		dc.l	1,1,1

;		dc.l	1,0,-1
;		dc.l	2,0,-2
;		dc.l	1,0,-1
;
;		dc.l	1,2,1
;		dc.l	0,0,0
;		dc.l	-1,-2,-1
;
;		dc.l	2,1,0
;		dc.l	1,0,-1
;		dc.l	0,-1,-2
;
;		dc.l	0,-1,-2
;		dc.l	1,0,-1
;		dc.l	2,1,0


;----------------------------------------------------------------------------
		section	laber,bss
sh_bitmaptab:	ds.l	8
