����  ��&b�&b�&b�&b�&b�&b�&b�&b;=,;===========================================================================
;
;		Negiert die Farben in einem Ausschnitt oder Bild 
;		(c) 1998 ,Frank (Copper) Pagels  /DFT
;
;		26.08.98

		incdir 	include:

		include	exec/exec_lib.i
		include	exec/lists.i
		include	exec/memory.i
		include intuition/intuition_lib.i
		include libraries/gadtools.i
		include	libraries/wb_lib.i
		include	libraries/reqtools_lib.i
		include	graphics/graphics_lib.i
		Include utility/tagitem.i
		include	dos/dos.i		
		include	dos/dos_lib.i		

		include misc/artpro.i
		include render/render_lib.i
		include render/render.i
		

		OPERATORHEADER ne_NODE

		dc.b	'$VER: Negate Operator Modul 1.00 (26.08.98)',0
	even
;---------------------------------------------------------------------------
ne_NODE:
		dc.l	0,0
		dc.b	0,0
		dc.l	ne_name
		dc.l	ne_Party
		dc.b	'NEOP'
		dc.b	'EXTR'	;for extern loader
		dc.l	0
		dc.l	ne_Tags
ne_name:
		dc.b	'Negative',0
	even
;---------------------------------------------------------------------------

ne_Tags:
		dc.l	APT_Creator,ne_Creator
		dc.l	APT_Version,5
		dc.l	APT_Info,ne_Info
		dc.l	TAG_DONE

ne_Creator:
		dc.b	'(c) 1998 Frank Pagels /DEFECT Softworks',0
	even
ne_Info:
		dc.b	'Negative',10,10
		dc.b	'Negate the color values of a Picture',0
	even

ne_mirror:	dc.w	0

ne_chunkymem	=	APG_Free2

ne_window	dc.l	0
ne_GList	dc.l	0
ne_gadarray	dcb.l	3,0

c24to3Byte	macro
		move.l	\1,\2
		and.l	#$0000ff00,\2
		lsr.l	#8,\2
		move.l	\1,\3
		and.l	#$000000ff,\3
		and.l	#$00ff0000,\1
		swap	\1
		endm

;---------------------------------------------------------------------------
ne_Party:

		tst.w	APG_Cutflag(a5)
		bne.b	.brushok
		move.w	#0,APG_xbrush1(a5)	;Es soll das ganze 
		move.w	#0,APG_ybrush1(a5)	;Pic abgesaved werden
		move.w	APG_ImageWidth(a5),APG_xbrush2(a5)
		move.w	APG_ImageHeight(a5),APG_ybrush2(a5)	
.brushok:

;---------------------------------------------------------------------------------
ne_normal:
		move.b	APG_HAM_FLag(a5),d0
		add.b	APG_HAM8_FLag(a5),d0
		tst.b	d0
		bne.w	ne_makeham

		cmp.b	#8,APG_Planes(a5)
		beq	.norgb
		bcc	ne_true

.norgb:		
		tst.l	APG_rndr_rendermem(a5)
		bne	ne_true

		jsr	APR_MakeRGBData(a5)
		tst.l	d0
		beq	ne_true				
		moveq	#APOE_NOMEM,d3
		rts

;-------------------------------------------------------------------------
;flippe mit truecolor daten
ne_true:

;flippe bereich um die x achse

		move.w	APG_ybrush2(a5),d4
		sub.w	APG_ybrush1(a5),d4
		subq.w	#1,d4
		move.w	APG_xbrush2(a5),d7
		sub.w	APG_xbrush1(a5),d7
		subq.w	#1,d7
		move.w	d7,d5
		move.l	APG_bytewidth(a5),d6
		lsl.l	#3+2,d6
		move.l	APG_rndr_rgb(a5),a0
		moveq	#0,d2
		move.w	APG_ybrush1(a5),d2
		mulu	d6,d2
		add.l	d2,a0		;erste zeile einstellen
		move.l	a0,a1
		move.b	ne_mirror(pc),d2
.nl:
		moveq	#0,d0
		moveq	#0,d1
		move.w	APG_xbrush1(a5),d0
		move.w	APG_xbrush2(a5),d1
		subq.w	#1,d1


	movem.l	d4-d6,-(a7)
.nx
		move.l	(a0,d0.l*4),d3

	moveq	#0,d4
	moveq	#0,d5

		c24to3Byte d3,d4,d5

		not.b	d3
		not.b	d4
		not.b	d5

		swap	d3
		or.b	d4,d3
		lsl.w	#8,d3
		or.b	d5,d3

		move.l	d3,(a0,d0.l*4)
		addq.w	#1,d0
		subq.w	#1,d1			
		dbra	d7,.nx
	movem.l	(a7)+,d4-d6

		add.l	d6,a1
		move.l	a1,a0
		move.w	d5,d7
		dbra	d4,.nl		

		cmp.b	#8,APG_Planes(a5)
		beq	.render
		bcc	.norender
.render:
		jsr	APR_Render(a5)
		bra	.w
.norender:
		jsr	APR_Save2TMP(a5)
		jsr	APR_MakePreview(a5)
.w
		moveq	#0,d3
		rts

;----------------------------------------------------------------------------
ne_makeham:
		tst.l	APG_rndr_rendermem(a5)
		bne.w	ne_true			:wir haben schon rgb daten

		jsr	APR_MakeRGBData(a5)
		
		bra.w	ne_true
		
;----------------------------------------------------------------------------
		section	laber,bss
ne_bitmaptab:	ds.l	8
