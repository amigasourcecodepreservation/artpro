����  $0�&b�&b�&b�&b�&b�&b�&b�&b;=��;===========================================================================
;
;		Scale Operator
;		(c) 1998 ,Frank (Copper) Pagels  /DFT
;
;		15.04.98

		incdir 	include:

		include	exec/exec_lib.i
		include	exec/lists.i
		include	exec/memory.i
		include intuition/intuition_lib.i
		include libraries/gadtools.i
		include	libraries/wb_lib.i
		include	libraries/reqtools_lib.i
		include libraries/gadtools.i
		include libraries/gadtools_lib.i
		include	graphics/graphics_lib.i
		Include utility/tagitem.i
		include	dos/dos.i		
		include	dos/dos_lib.i		

		include misc/artpro.i
		include render/render_lib.i
		include render/render.i
		

		OPERATORHEADER sc_NODE

		dc.b	'$VER: Scale Operator Modul 1.00 (17.08.98)',0
	even
;---------------------------------------------------------------------------
sc_NODE:
		dc.l	0,0
		dc.b	0,0
		dc.l	sc_name
		dc.l	sc_Party
		dc.b	'SCAL'
		dc.b	'EXTR'	;for extern loader
		dc.l	0
		dc.l	sc_Tags
sc_name:
		dc.b	'Scale',0
	even
;---------------------------------------------------------------------------

sc_Tags:
		dc.l	APT_Creator,sc_Creator
		dc.l	APT_Version,5
		dc.l	APT_Info,sc_Info
		dc.l	APT_Prefs,sc_Prefsrout		;routine for Prefs
		dc.l	APT_Prefsbuffer,sc_prefs
		dc.l	APT_Prefsbuffersize,20
		dc.l	APT_PrefsVersion,1
		dc.l	TAG_DONE

sc_Creator:
		dc.b	'(c) 1998 Frank Pagels /Defect Softworks',0
	even
sc_Info:
		dc.b	'Scale',10,10
		dc.b	'Scale a image to the given size',0
	even

sc_chunkymem	=	APG_Free2
sc_chunkymem1	=	APG_Free3
sc_scaleengine	=	APG_Free4
sc_chunky8size	=	APG_Free5
sc_rgbsize	=	APG_Free6
sc_rendermem	=	APG_Free7
sc_chunky	=	APG_Free8
sc_rgb		=	APG_Free9

sc_window	dc.l	0
sc_GList	dc.l	0
sc_gadarray	dcb.l	16,0

sc_prefs:
sc_pxsize:	dc.l	2
sc_pysize:	dc.l	2
sc_pxsizepro:	dc.l	100
sc_pysizepro:	dc.l	100
sc_pkeepascpect dc.b	0
sc_pdontpop:	dc.b	0
sc_pmode:	dc.b	0
		dc.b	0

sc_xori:	dc.l	0
sc_yori:	dc.l	0

itsprefs:	dc.w	0

;---------------------------------------------------------------------------
sc_Party:

		tst.w	APG_Cutflag(a5)
		bne.b	.brushok
		move.w	#0,APG_xbrush1(a5)	;Es soll das ganze 
		move.w	#0,APG_ybrush1(a5)	;Pic abgesaved werden
		move.w	APG_ImageWidth(a5),APG_xbrush2(a5)
		move.w	APG_ImageHeight(a5),APG_ybrush2(a5)	
.brushok:

;---------------------------------------------------------------------------------

		tst.w	APG_LoadFlag(a5)
		bne.b	.loadok
		moveq	#APOE_NOIMAGE,d3
		rts

.loadok:
		tst.b	sc_pdontpop
		beq.b	.pop
		tst.b	sc_pmode
		beq	.allok
		move.l	sc_pxsizepro(pc),d0
		moveq	#0,d1
		move.w	APG_ImageWidth(a5),d1
		mulu	d1,d0
		divu.l	#100,d0
		move.l	d0,sc_pxsize
		move.l	sc_pysizepro(pc),d0
		moveq	#0,d1
		move.w	APG_ImageHeight(a5),d1
		mulu	d1,d0
		divu.l	#100,d0
		move.l	d0,sc_pysize
		bra	.allok
.pop:
		bsr.w	sc_askforinput
		tst.l	d7
		beq.b	.allok
		moveq	#APOE_ABORT,d3
		rts

.allok:
		move.b	APG_HAM_FLag(a5),d0
		add.b	APG_HAM8_FLag(a5),d0
		tst.b	d0
		bne.w	sc_makeham

		cmp.b	#8,APG_Planes(a5)
		bls.b	sc_normal

		tst.l	APG_rndr_rendermem(a5)
		bne.w	sc_true
		
;-------------------------------------------------------------------
;-- Normal bitmap scaling

sc_normal:
		move.l	APG_rndr_rendermem(a5),d0
		beq.b	.nr
		move.l	d0,a1
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)
		clr.l	APG_rndr_rendermem(a5)
		clr.l	APG_rndr_rgb(a5)
		clr.l	APG_rndr_chunky(a5)

.nr:
		jsr	APR_OpenProcess(a5)
		lea	scalemsg(pc),a1
		moveq	#5,d0
		jsr	APR_InitProcess(a5)

		moveq	#0,d0
		move.w	APG_ImageWidth(a5),d0
		move.l	d0,d7
		mulu	APG_ImageHeight(a5),d0
		move.l	d0,APG_Free1(a5)

		move.l	#MEMF_ANY+MEMF_CLEAR,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)
		move.l	d0,sc_chunkymem(a5)
		bne.b	.memok
		moveq	#APOE_NOMEM,d3
		rts
.memok:
;speicher f�r scaled chunkys

		move.l	sc_pxsize(pc),d0
		move.l	sc_pysize(pc),d1
		mulu	d1,d0
		move.l	#MEMF_ANY+MEMF_CLEAR,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)
		move.l	d0,sc_chunkymem1(a5)
		bne.b	.smemok
		move.l	sc_chunkymem(a5),a1
		jsr	_LVOFreeVec(a6)
		moveq	#APOE_NOMEM,d3
		rts
.smemok		
		jsr	APR_Doprocess(a5)

;m�ch chunkys
		lea	sc_bitmaptab,a1	;puffer f�r BitmapPointerTab
		move.l	a1,a0
		move.l	APG_Picmem(a5),d0
		moveq	#0,d1
		move.b	APG_Planes(a5),d1		
		subq.w	#1,d1
.ncp:		move.l	d0,(a1)+	;erzeuge bitmappointer Tab
		add.l	APG_Oneplanesize(a5),d0
		dbra	d1,.ncp

		move.l	sc_chunkymem(a5),a1	;Chunky-Buffer
		move.l	APG_bytewidth(a5),d0
		move.w	APG_ImageHeight(a5),d1
		moveq	#0,d2
		move.b	APG_Planes(a5),d2
		move.l	d0,d3
		move.l	APG_Renderbase(a5),a6

		move.l	#TAG_DONE,-(a7)
		moveq	#0,d7
		move.w	APG_ImageWidth(a5),d7
		move.l	d7,-(a7)
		move.l	#RND_DestWidth,-(a7)
		move.l	a7,a2

		jsr	_LVOPlanar2Chunky(a6)
		lea	3*4(a7),a7
		jsr	APR_Doprocess(a5)

;create scale engine

		move.w	APG_ImageWidth(a5),d0
		move.w	APG_ImageHeight(a5),d1
		move.l	sc_pxsize(pc),d2
		move.l	sc_pysize(pc),d3
		sub.l	a1,a1
		jsr	_LVOCreateScaleEngineA(A6)
		move.l	d0,sc_scaleengine(a5)

		move.l	d0,a0
		move.l	sc_chunkymem(a5),a1
		move.l	sc_chunkymem1(a5),a2
;		move.l	#TAG_DONE,-(a7)
;		move.l	APG_DoProcessHook(a5),-(a7)
;		move.l	#RND_LineHook,-(a7)
;		move.l	a7,a3
		sub.l	a3,a3
		jsr	_LVOScaleA(a6)
;		lea	3*4(a7),a7
		
		jsr	APR_Doprocess(a5)

		move.l	sc_scaleengine(a5),a0
		jsr	_LVODeleteScaleengine(a6)

		move.l	APG_Picmem(a5),d0
		beq.b	.nm
		move.l	d0,a1
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)
.nm
		move.l	sc_pxsize(pc),d0
		move.w	d0,APG_ImageWidth(a5)
		move.l	sc_pysize(pc),d0
		move.w	d0,APG_ImageHeight(a5)

		moveq	#0,d0
		move.w	APG_ImageWidth(a5),d0
		add.w	#15,d0
		lsr.w	#4,d0
		lsl.w	#1,d0
		move.l	d0,APG_ByteWidth(a5)
		move.w	APG_ImageHeight(a5),d1
		mulu	d1,d0
		move.l	d0,APG_Oneplanesize(a5)

;neuen Picmem allocieren

		move.l	APG_ByteWidth(a5),d0
		lsl.l	#3,d0
		moveq	#0,d1
		move.w	APG_ImageHeight(a5),d1
		mulu	d1,d0
		move.l	#MEMF_ANY!MEMF_CLEAR,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)
		tst.l	d0
		bne.b	.mo
		move.l	4.w,a6
		move.l	sc_chunkymem(a5),a1
		jsr	_LVOFreeVec(a6)
		move.l	sc_chunkymem1(a5),a1
		jsr	_LVOFreeVec(a6)
		clr.w	APG_LoadFlag(a5)
		moveq	#APOE_NOMEM,d3
		rts

.mo:		move.l	d0,APG_Picmem(a5)
		
		jsr	APR_Doprocess(a5)
		
;mappe wieder zu planar

		jsr	APR_InitPicMap(a5)

		move.l	sc_chunkymem1(a5),a0
		moveq	#0,d0
		moveq	#0,d1
		moveq	#0,d2
		move.w	APG_ImageWidth(a5),d2
		move.w	APG_ImageHeight(a5),d3
		moveq	#0,d4
		moveq	#0,d5
		sub.l	a2,a2
		lea	APG_Picmap(a5),a1
		move.l	APG_renderbase(a5),a6
		jsr	_LVOChunky2BitmapA(a6)
				
		move.l	4.w,a6
		move.l	sc_chunkymem(a5),a1
		jsr	_LVOFreeVec(a6)
		clr.l	sc_chunkymem(a5)
		move.l	sc_chunkymem1(a5),a1
		jsr	_LVOFreeVec(a6)
		clr.l	sc_chunkymem1(a5)
		
		jsr	APR_Doprocess(a5)

		move.l	APG_FindMonitor(a5),a6
		jsr	(a6)

		jsr	APR_SetPicSizeGads(a5)
		jsr	APR_Save2TMP(a5)
		jsr	APR_MakePreview(a5)


		moveq	#0,d3
		rts

;-------------------------------------------------------------------------
;Scale ein Truecolor/HAM Image 
sc_true:
		jsr	APR_OpenProcess(a5)
		lea	scalemsg(pc),a1
		moveq	#4,d0
		jsr	APR_InitProcess(a5)

;speicher f�r scaled chunkys

		move.l	sc_pxsize(pc),d0
		add.w	#15,d0
		lsr.w	#4,d0
		lsl.w	#1,d0
		move.l	sc_pysize(pc),d1
		lsl.l	#3,d0
		mulu.w	d1,d0
		move.l	d0,d7			;Chunky8 gr��e
		move.l	d0,sc_chunky8size(a5)
		mulu.l	#4,d0
		move.l	d0,sc_rgbsize(a5)
		add.l	d0,d7
		move.l	d7,d0
		move.l	4.w,a6
		move.l	#MEMF_ANY+MEMF_CLEAR,d1		
		jsr	_LVOAllocVec(a6)
		tst.l	d0
		bne.b	.memok
		moveq	#APOE_NOMEM,d3
		rts
.memok:
		move.l	d0,sc_rendermem(a5)
		move.l	d0,sc_chunky(a5)	;merken
		add.l	sc_chunky8size(a5),d0	;RGB puffer
		move.l	d0,sc_rgb(a5)


		jsr	APR_Doprocess(a5)

;create scale engine

		move.l	APG_Renderbase(a5),a6
		move.w	APG_ImageWidth(a5),d0
		move.w	APG_ImageHeight(a5),d1
		move.l	sc_pxsize(pc),d2
		move.l	sc_pysize(pc),d3
		move.l	#TAG_DONE,-(a7)
		move.l	#PIXFMT_0RGB_32,-(a7)
		move.l	#RND_PixelFormat,-(a7)
		move.l	a7,a1
		jsr	_LVOCreateScaleEngineA(A6)
		move.l	d0,sc_scaleengine(a5)
		lea	3*4(a7),a7

		move.l	d0,a0
		move.l	APG_rndr_rgb(a5),a1
		move.l	sc_rgb(a5),a2
		move.l	#TAG_DONE,-(a7)
		moveq	#0,d0
		move.w	APG_ImageWidth(a5),d0
		add.w	#15,d0
		lsr.w	#4,d0
		lsl.w	#4,d0
		move.l	d0,-(a7)
		move.l	#RND_SourceWidth,-(a7)
		move.l	sc_pxsize(pc),d0
		add.w	#15,d0
		lsr.w	#4,d0
		lsl.w	#4,d0
		move.l	d0,-(a7)
		move.l	#RND_DestWidth,-(a7)
		move.l	a7,a3
		jsr	_LVOScaleA(a6)
		lea	5*4(a7),a7
		
		jsr	APR_Doprocess(a5)

		move.l	sc_scaleengine(a5),a0
		jsr	_LVODeleteScaleengine(a6)

		move.l	APG_Picmem(a5),d0
		beq.b	.nm
		move.l	d0,a1
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)
		clr.l	APG_PicMem(a5)
.nm
		move.l	sc_pxsize(pc),d0
		move.w	d0,APG_ImageWidth(a5)
		move.l	sc_pysize(pc),d0
		move.w	d0,APG_ImageHeight(a5)

		moveq	#0,d0
		move.w	APG_ImageWidth(a5),d0
		add.w	#15,d0
		lsr.w	#4,d0
		lsl.w	#1,d0
		move.l	d0,APG_ByteWidth(a5)
		move.w	APG_ImageHeight(a5),d1
		mulu	d1,d0
		move.l	d0,APG_Oneplanesize(a5)

.npicmem:
		jsr	APR_Doprocess(a5)

		move.l	4.w,a6
		move.l	APG_rndr_rendermem(a5),a1
		jsr	_LVOFreeVec(a6)
		move.l	sc_rendermem(a5),APG_rndr_rendermem(a5)
		move.l	sc_chunky(a5),APG_rndr_chunky(a5)
		move.l	sc_chunky8size(a5),APG_rndr_chunky8size(a5)
		move.l	sc_rgb(a5),APG_rndr_rgb(a5)

		jsr	APR_Doprocess(a5)

		move.l	APG_FindMonitor(a5),a6
		jsr	(a6)

		move.b	APG_HAM_FLag(a5),d0
		add.b	APG_HAM8_FLag(a5),d0
		tst.b	d0
		beq.b	.noham

		jsr	APR_Render(a5)
		bra.b	.w

.noham:
		jsr	APR_Doprocess(a5)
		jsr	APR_MakePreview(a5)

.w
		jsr	APR_SetPicSizeGads(a5)
		jsr	APR_Save2TMP(a5)
		moveq	#0,d3
		rts

;----------------------------------------------------------------------------
sc_makeham:
		tst.l	APG_rndr_rendermem(a5)
		bne.w	sc_true			;wir haben schon rgb daten

		jsr	APR_MakeRGBData(a5)
		
		bra.w	sc_true
		
;----------------------------------------------------------------------------
sc_askforinput:
		sf	itsprefs
		moveq	#0,d0
		move.w	APG_ImageWidth(a5),d0
		lea	sc_xori(pc),a0
		move.l	d0,(a0)
		lea	sc_xsnumber(pc),a0
		move.l	d0,(a0)
		lea	sc_pxsize(pc),a0
		move.l	d0,(a0)
		move.w	APG_ImageHeight(a5),d0
		lea	sc_yori(pc),a0
		move.l	d0,(a0)
		lea	sc_ysnumber(pc),a0
		move.l	d0,(a0)
		lea	sc_pysize(pc),a0
		move.l	d0,(a0)

		moveq	#1,d0
		moveq	#0,d1
		lea	sc_popdis(pc),a0
		move.l	d0,(a0)
		lea	sc_modis(pc),a0
		move.l	d0,(a0)
		lea	sc_xdivdis(pc),a0
		move.l	d1,(a0)
		lea	sc_xmuldis(pc),a0
		move.l	d1,(a0)
		lea	sc_ydivdis(pc),a0
		move.l	d1,(a0)
		lea	sc_ymuldis(pc),a0
		move.l	d1,(a0)
		lea	sc_resetdis(pc),a0
		move.l	d1,(a0)
		lea	sc_xsizedis(pc),a0
		move.l	d1,(a0)
		lea	sc_ysizedis(pc),a0
		move.l	d1,(a0)
		lea	sc_xprodis(pc),a0
		move.l	d1,(a0)
		lea	sc_yprodis(pc),a0
		move.l	d1,(a0)
		lea	sc_xleveldis(pc),a0
		move.l	d1,(a0)
		lea	sc_yleveldis(pc),a0
		move.l	d1,(a0)

		moveq	#100,d0
		lea	sc_pxsizepro(pc),a0
		move.l	d0,(a0)
		lea	sc_xlevel(pc),a0
		move.l	d0,(a0)
		lea	sc_pysizepro(pc),a0
		move.l	d0,(a0)
		lea	sc_ylevel(pc),a0
		move.l	d0,(a0)

		bra.w	sc_goprefs


;----------------------------------------------------------------------------
sc_Prefsrout
		st	itsprefs

;prefs backuppen
		lea	sc_bprefs,a0
		lea	sc_prefs,a1
		moveq	#18,d7
.nb		move.b	(a1)+,(a0)+
		dbra	d7,.nb

		moveq	#1,d0
		tst.b	sc_pdontpop
		beq.b	.w
		moveq	#0,d0
.w
		lea	sc_modis(pc),a0
		move.l	d0,(a0)

		lea	sc_popdis(pc),a0
		move.l	#0,(a0)

		tst.b	sc_pdontpop
		beq.w	sc_disall
		moveq	#0,d0
		moveq	#1,d1
		tst.b	sc_pmode
		beq.b	.abso
		moveq	#1,d0
		moveq	#0,d1
.abso:
dodis:
		move.l	d0,sc_xsizedis
		move.l	d0,sc_ysizedis
		move.l	d1,sc_xprodis
		move.l	d1,sc_yprodis
		move.l	d1,sc_xleveldis
		move.l	d1,sc_yleveldis
		move.l	d1,sc_xdivdis
		move.l	d1,sc_xmuldis
		move.l	d1,sc_ydivdis
		move.l	d1,sc_ymuldis
		move.l	#1,sc_resetdis
		move.l	#0,sc_modis

		move.l	sc_pxsize(pc),sc_xsnumber
		move.l	sc_pysize(pc),sc_ysnumber
		move.l	sc_pxsizepro(pc),sc_xpronum
		move.l	sc_pysizepro(pc),sc_ypronum

		bra.b	sc_goprefs

;alles dissablen da dont popup nicht an ist
sc_disall:
		moveq	#1,d0
		move.l	d0,sc_xdivdis
		move.l	d0,sc_xmuldis
		move.l	d0,sc_ydivdis
		move.l	d0,sc_ymuldis
		move.l	d0,sc_resetdis
		move.l	d0,sc_xsizedis
		move.l	d0,sc_ysizedis
		move.l	d0,sc_xprodis
		move.l	d0,sc_yprodis
		move.l	d0,sc_xleveldis
		move.l	d0,sc_yleveldis
		move.l	d0,sc_modis

sc_goprefs:
		moveq	#0,d0
		tst.b	sc_pkeepascpect
		beq.b	.ns
		moveq	#1,d0
.ns:		lea	sc_selectas(pc),a0
		move.l	d0,(a0)

		moveq	#0,d0
		tst.b	sc_pdontpop
		beq.b	.nsp
		moveq	#1,d0
.nsp:		lea	sc_selectpop(pc),a0
		move.l	d0,(a0)

		move.l	sc_pxsizepro(pc),sc_xlevel
		move.l	sc_pysizepro(pc),sc_ylevel

		move.l	#WindowTags,APG_WindowTagList(a5) ;Tag list for the Window
		move.l	#WinWG+4,APG_WGadgets(a5)	;addr. of WA_Gadgets Tag + 4
		move.l	#winSC+4,APG_WScreen(a5)	;addr. of WA_CustomScreen Tag + 4
		move.l	#sc_window,APG_WinWnd(a5)	;a point for the Window
		move.l	#sc_Glist,APG_Glist(a5)		;a point for the Glist
		move.l	#NGads,APG_NGads(a5)		;the NGads list
		move.l	#Gtypes,APG_GTypes(a5)		;the GTypes list
		move.l	#Gtags,APG_Gtags(a5)		;the Gtags list
		move.w	#scale_CNT,APG_CNT(a5)			;the number of Gadgets
		move.l	#sc_gadarray,APG_Gadgets(a5)	;a pointer for the Gadgetsarry, size=4*number of Gadgets
		move.l	APG_Scr(a5),APG_ScrBase(a5)	;the Screenpointer , stored in APG_Scr
	
		move.l	APG_CheckMainWinPos(a5),a6
		jsr	(a6)

		add.w	#150,d0
		add.w	#60,d1
		
		move.w	d0,APG_WinLeft(a5)		;the left pos. of the Win
		move.w	d1,APG_WinTop(a5)		;the top pos. of the Win
		move.w	#330,APG_WinWidth(a5)		;the width of the Win
		move.w	#150,APG_WinHeight(a5)		;the height of the Win

		move.l	#WinL,APG_WinL(a5)		;addr. of WA_Left Tag 
		move.l	#WinT,APG_WinT(a5)		;addr. of WA_Top Tag
		move.l	#WinW,APG_WinW(a5)		;addr. of WA_Width Tag
		move.l	#WinH,APG_WinH(a5)		;addr. of WA_Height Tag

		move.l	APG_OpenWindow(a5),a6
		jsr	(a6)


sc_wait:	move.l	sc_window(pc),a0
		move.l	APG_Wait(a5),a6
		jsr	(a6)

		cmp.l	#IDCMP_CLOSEWINDOW,d4
		beq.w	sc_Cancel		
		cmp.l	#GADGETUP,d4
		beq.w	sc_gads
		cmp.l	#GADGETDOWN,d4
		beq.w	sc_slider
		cmp.l	#IDCMP_VANILLAKEY,d4
		beq.w	sc_tast		;es wurde eine Taste gedr�ckt
		bra.b	sc_wait

sc_tast:
		cmp.w	#'w',d5
		bne	.nw
		moveq	#GD_xsize,d0
		bra	sc_aktint
.nw:
		cmp.w	#'h',d5
		bne	.nh
		moveq	#GD_ysize,d0
		bra	sc_aktint
.nh:
		cmp.w	#'d',d5
		bne	.wp
		moveq	#GD_widthpro,d0
		bra	sc_aktint
.wp		
		cmp.w	#'g',d5
		bne	.hp
		moveq	#GD_heightpro,d0
		bra	sc_aktint
.hp
		cmp.w	#'o',d5
		beq	sc_ok
		cmp.w	#'c',d5
		beq	sc_cancel
		cmp.w	#'r',d5
		beq	sc_reset

		bra	sc_wait



sc_aktint:
		lea	sc_gadarray(pc),a0
		move.l	(a0,d0.l*4),a0
		move.w	gg_FLAGS(a0),d1
		and.w	#GFLG_DISABLED,d1
		bne.w	sc_wait
		move.l	sc_window(pc),a1
		sub.l	a2,a2
		move.l	APG_Intuitionbase(a5),a6
		jsr	_LVOActivateGadget(a6)		
		bra	sc_wait


sc_gads:
		moveq	#0,d0
		move.w	38(a4),d0

		add.l	d0,d0
		lea	sc_jumptab(pc),a0
		move.w	(a0,d0.l),d0
		jmp	(a0,d0.w)

sc_jumptab:
		dc.w	sc_xsize-sc_jumptab
		dc.w	sc_ysize-sc_jumptab
		dc.w	sc_cancel-sc_jumptab	;xslider
		dc.w	sc_xsizepro-sc_jumptab
		dc.w	sc_ysizepro-sc_jumptab
		dc.w	sc_cancel-sc_jumptab	;yslider
		dc.w	sc_div2x-sc_jumptab
		dc.w	sc_mul2x-sc_jumptab
		dc.w	sc_div2y-sc_jumptab
		dc.w	sc_mul2y-sc_jumptab
		dc.w	sc_aspect-sc_jumptab
		dc.w	sc_dontpop-sc_jumptab
		dc.w	sc_reset-sc_jumptab
		dc.w	sc_ok-sc_jumptab
		dc.w	sc_cancel-sc_jumptab
		dc.w	sc_mode-sc_jumptab

sc_cancel:
		tst.b	itsprefs
		beq.b	.w
		lea	sc_bprefs,a0
		lea	sc_prefs,a1
		moveq	#18,d7
.nb		move.b	(a0)+,(a1)+
		dbra	d7,.nb
.w:		moveq	#-1,d7
		bra.b	sc_ok1

sc_ok:		moveq	#0,d7
sc_ok1:		move.l	sc_window(pc),a0
		move.l	APG_Intuitionbase(a5),a6
		jmp	_LVOCloseWindow(a6)

sc_slider:
		move.w	38(a4),d0
		cmp.w	#GD_xslider,d0
		beq.b	sc_slidex
		cmp.w	#GD_yslider,d0
		beq.b	sc_slidey

		bra.w	sc_wait

sc_sliderwait:
		move.l	sc_window(pc),a0
		move.l	APG_Wait(a5),a6
		jsr	(a6)				;Handle input

		cmp.l	#GADGETUP,d4
		beq.w	sc_wait			;Slidergadet wurde losgelassen

		cmp.l	#IDCMP_MOUSEMOVE,d4
		beq.b	sc_slider
		bra.b	sc_sliderwait

;-----------------------------------------------------------------------------
sc_slidex:
		ext.l	d5
		lea	sc_pxsizepro(pc),a0
		move.l	d5,(a0)

		move.l	#GD_widthpro,d0
		bsr.w	sc_setwert
		move.l	#'RTS',d7
		bsr.w	sc_xsizeprogo

		bra.b	sc_sliderwait
;-----------------------------------------------------------------------------
sc_slidey:
		ext.l	d5
		lea	sc_pysizepro(pc),a0
		move.l	d5,(a0)

		move.l	#GD_heightpro,d0
		bsr.w	sc_setwert
		move.l	#'RTS',d7
		bsr.w	sc_ysizeprogo

		bra.b	sc_sliderwait


;---------------------------------------------------------------------------
sc_reset:
		moveq	#100,d0
		lea	sc_pxsizepro(pc),a0
		move.l	d0,(a0)
		lea	sc_pysizepro(pc),a0
		move.l	d0,(a0)
		lea	sc_pxsize(pc),a0
		move.l	sc_xori(pc),(a0)
		lea	sc_pysize(pc),a0
		move.l	sc_yori(pc),(a0)
		moveq	#100,d5
		move.l	#GD_xslider,d0
		bsr.w	sc_setslider
		move.l	#GD_widthpro,d0
		bsr.w	sc_setwert
		move.l	#GD_yslider,d0
		bsr.w	sc_setslider
		move.l	#GD_heightpro,d0
		bsr.w	sc_setwert
		move.l	sc_pxsize(pc),d5
		move.l	#GD_xsize,d0
		bsr.w	sc_setwert
		move.l	sc_pysize(pc),d5
		move.l	#GD_ysize,d0
		bsr.w	sc_setwert

		bra.w	sc_wait		

;---------------------------------------------------------------------------
sc_xsize:
		move.l	#GD_xsize,d0
		bsr.w	sc_holewert
		move.l	sc_number(pc),sc_pxsize
		tst.b	sc_pdontpop
		bne.w	sc_wait
		tst.b	sc_pkeepascpect
		beq.b	.noaspect
;berechne y wert
		move.l	sc_yori(pc),d0
		move.l	sc_pxsize(pc),d1
		mulu.l	d1,d0
		move.l	sc_xori(pc),d1
		divu.l	d1,d0
		move.l	d0,sc_pysize
		move.l	d0,d5
		move.l	#GD_ysize,d0
		bsr.w	sc_setwert
.noaspect:
		moveq	#100,d0
		move.l	sc_pxsize(pc),d1
		mulu.l	d1,d0
		move.l	sc_xori(pc),d1
		divu.l	d1,d0		;Prozentwert
		move.l	d0,d5
		move.l	#GD_xslider,d0
		bsr.w	sc_setslider
		move.l	#GD_widthpro,d0
		bsr.w	sc_setwert
		tst.b	sc_pkeepascpect
		beq.w	sc_wait
		move.l	#GD_yslider,d0
		bsr.w	sc_setslider
		move.l	#GD_heightpro,d0
		bsr.w	sc_setwert
		bra.w	sc_wait

;---------------------------------------------------------------------------
sc_ysize:
		move.l	#GD_ysize,d0
		bsr.w	sc_holewert
		move.l	sc_number(pc),sc_pysize
		tst.b	sc_pdontpop
		bne.w	sc_wait
		tst.b	sc_pkeepascpect
		beq.b	.noaspect

;berechne x wert wenn keep aspect an

		move.l	sc_xori(pc),d0
		move.l	sc_pysize(pc),d1
		mulu.l	d1,d0
		move.l	sc_yori(pc),d1
		divu.l	d1,d0
		move.l	d0,sc_pxsize
		move.l	d0,d5
		move.l	#GD_xsize,d0
		bsr.w	sc_setwert
.noaspect:
		moveq	#100,d0
		move.l	sc_pysize(pc),d1
		mulu.l	d1,d0
		move.l	sc_yori(pc),d1
		divu.l	d1,d0		;Prozentwert
		move.l	d0,d5
		move.l	#GD_yslider,d0
		bsr.w	sc_setslider
		move.l	#GD_heightpro,d0
		bsr.w	sc_setwert
		tst.b	sc_pkeepascpect
		beq.w	sc_wait
		move.l	#GD_xslider,d0
		bsr.w	sc_setslider
		move.l	#GD_widthpro,d0
		bsr.w	sc_setwert
		bra.w	sc_wait

;---------------------------------------------------------------------------
sc_xsizepro:
		move.l	#GD_widthpro,d0
		bsr.w	sc_holewert
		move.l	sc_number(pc),sc_pxsizepro
sc_xsizeprogo:
		tst.b	sc_pdontpop
		bne.b	sc_xend
		tst.b	sc_pkeepascpect
		beq.b	.noaspect
;y wert = x wert
		move.l	sc_pxsizepro(pc),d5
		move.l	d5,sc_pysizepro
		move.l	#GD_heightpro,d0
		bsr.w	sc_setwert
		move.l	#GD_yslider,d0
		bsr.w	sc_setslider
		move.l	sc_yori(pc),d0
		mulu.l	d5,d0
		divu.l	#100,d0
		move.l	d0,sc_pysize
		move.l	d0,d5
		move.l	#GD_ysize,d0
		bsr.w	sc_setwert
.noaspect:
		move.l	sc_pxsizepro(pc),d5
		move.l	#GD_xslider,d0
		bsr.w	sc_setslider
		move.l	sc_xori(pc),d0
		mulu.l	d5,d0
		divu.l	#100,d0
		move.l	d0,sc_pxsize
		move.l	d0,d5
		move.l	#GD_xsize,d0
		bsr.w	sc_setwert
sc_xend:
		cmp.l	#'RTS',d7
		bne.w	sc_wait
		rts

;-----------------------------------------------------------------------------
sc_ysizepro:
		move.l	#GD_heightpro,d0
		bsr.w	sc_holewert
		move.l	sc_number(pc),sc_pysizepro
sc_ysizeprogo:
		tst.b	sc_pdontpop
		bne.b	sc_yend
		tst.b	sc_pkeepascpect
		beq.b	.noaspect
;y wert = x wert
		move.l	sc_pysizepro(pc),d5
		move.l	d5,sc_pxsizepro
		move.l	#GD_widthpro,d0
		bsr.w	sc_setwert
		move.l	#GD_xslider,d0
		bsr.w	sc_setslider
		move.l	sc_xori(pc),d0
		mulu.l	d5,d0
		divu.l	#100,d0
		move.l	d0,sc_pxsize
		move.l	d0,d5
		move.l	#GD_xsize,d0
		bsr.w	sc_setwert
.noaspect:
		move.l	sc_pysizepro(pc),d5
		move.l	#GD_yslider,d0
		bsr.w	sc_setslider
		move.l	sc_yori(pc),d0
		mulu.l	d5,d0
		divu.l	#100,d0
		move.l	d0,sc_pysize
		move.l	d0,d5
		move.l	#GD_ysize,d0
		bsr.w	sc_setwert
sc_yend:
		cmp.l	#'RTS',d7
		bne.w	sc_wait
		rts

;---------------------------------------------------------------------------
sc_div2x:
		move.l	sc_pxsizepro(pc),d5
		lsr.l	#1,d5
		cmp.w	#2,d5
		bhs.b	.w
		moveq	#2,d5
.w
		move.l	#GD_widthpro,d0
		bsr.w	sc_setwert
	moveq	#0,d7

		bra.w	sc_xsizepro
;---------------------------------------------------------------------------
sc_mul2x:

		jsr	apr_dummy(a5)

		move.l	sc_pxsizepro(pc),d5
		lsl.l	#1,d5
		cmp.w	#800,d5
		bls.b	.w
		move.l	#800,d5
.w
		move.l	#GD_widthpro,d0
		bsr.w	sc_setwert
	moveq	#0,d7
		bra.w	sc_xsizepro

;---------------------------------------------------------------------------
sc_div2y:
		move.l	sc_pysizepro(pc),d5
		lsr.l	#1,d5
		cmp.w	#2,d5
		bhs.b	.w
		moveq	#2,d5
.w
		move.l	#GD_heightpro,d0
		bsr.w	sc_setwert
	moveq	#0,d7
		bra.w	sc_ysizepro
;---------------------------------------------------------------------------
sc_mul2y:
		move.l	sc_pysizepro(pc),d5
		lsl.l	#1,d5
		cmp.w	#800,d5
		bls.b	.w
		move.l	#800,d5
.w
		move.l	#GD_heightpro,d0
		bsr.w	sc_setwert
	moveq	#0,d7
		bra.w	sc_ysizepro

;----------------------------------------------------------------------------
sc_dontpop:
		move.l	#GD_dontpop,d0
		bsr.w	sc_checkselect
		tst.l	d0
		bne.b	sc_popcheck	;dontpop ist aktiv

;dontpop ist nicht aktiv --> Gadgets disablen

		moveq	#GD_xsize,d0
		moveq	#1,d1
		bsr.w	sc_ghost
		moveq	#GD_ysize,d0
		moveq	#1,d1
		bsr.w	sc_ghost
		moveq	#GD_xslider,d0
		moveq	#1,d1
		bsr.w	sc_ghost
		moveq	#GD_widthpro,d0
		moveq	#1,d1
		bsr.w	sc_ghost
		moveq	#GD_heightpro,d0
		moveq	#1,d1
		bsr.w	sc_ghost
		moveq	#GD_yslider,d0
		moveq	#1,d1
		bsr.w	sc_ghost
		moveq	#GD_div2,d0
		moveq	#1,d1
		bsr.w	sc_ghost
		moveq	#GD_mul2width,d0
		moveq	#1,d1
		bsr.w	sc_ghost
		moveq	#GD_div2height,d0
		moveq	#1,d1
		bsr.w	sc_ghost
		moveq	#GD_mul2height,d0
		moveq	#1,d1
		bsr.w	sc_ghost
		moveq	#GD_reset,d0
		moveq	#1,d1
		bsr.w	sc_ghost
		moveq	#GD_mode,d0
		moveq	#1,d1
		bsr.w	sc_ghost

		sf	sc_pdontpop

		bra.w	sc_wait

;dontpop selectiert
sc_popcheck:
		moveq	#0,d4
		moveq	#1,d5
		tst.b	sc_pmode
		beq.b	.abso
		moveq	#1,d4
		moveq	#0,d5
.abso:
		moveq	#GD_xsize,d0
		move.l	d4,d1
		bsr.w	sc_ghost
		moveq	#GD_ysize,d0
		move.l	d4,d1
		bsr.w	sc_ghost
		moveq	#GD_xslider,d0
		move.l	d5,d1
		bsr.w	sc_ghost
		moveq	#GD_widthpro,d0
		move.l	d5,d1
		bsr.w	sc_ghost
		moveq	#GD_heightpro,d0
		move.l	d5,d1
		bsr.w	sc_ghost
		moveq	#GD_yslider,d0
		move.l	d5,d1
		bsr.w	sc_ghost
		moveq	#GD_div2,d0
		move.l	d5,d1
		bsr.w	sc_ghost
		moveq	#GD_mul2width,d0
		move.l	d5,d1
		bsr.w	sc_ghost
		moveq	#GD_div2height,d0
		move.l	d5,d1
		bsr.w	sc_ghost
		moveq	#GD_mul2height,d0
		move.l	d5,d1
		bsr.w	sc_ghost
		moveq	#GD_reset,d0
		moveq	#0,d1
		bsr.w	sc_ghost
		moveq	#GD_mode,d0
		moveq	#0,d1
		bsr.w	sc_ghost

		st	sc_pdontpop

		bra.w	sc_wait

sc_mode:
		move.b	d5,sc_pmode
		bra.w	sc_popcheck
		
sc_aspect:
		sf	sc_pkeepascpect
		move.l	#GD_keepascpect,d0
		bsr.w	sc_checkselect
		tst.l	d0
		beq.w	sc_wait
		st	sc_pkeepascpect
		bra.w	sc_wait

;----------------------------------------------------------------------------------
;hole wert aus integer gadget
sc_holewert:
		lea	sc_numtag1(pc),a3
		lea	sc_gadarray(pc),a0
		move.l	(a0,d0.l*4),a0
		move.l	sc_window(pc),a1
		sub.l	a2,a2
		move.l	APG_Gadtoolsbase(a5),a6
		jmp	_LVOGT_GetGadgetAttrsA(a6)

;setzt wert in integer gadget
sc_setwert:
		lea	sc_numtag(pc),a3
		move.l	d5,4(a3)
		lea	sc_gadarray(pc),a0
		move.l	(a0,d0.l*4),a0
		move.l	sc_window(pc),a1
		sub.l	a2,a2
		move.l	APG_Gadtoolsbase(a5),a6
		jmp	_LVOGT_SetGadgetAttrsA(a6)

sc_setslider:
		lea	sc_leveltag(pc),a3
		move.l	d5,4(a3)
		lea	sc_gadarray(pc),a0
		move.l	(a0,d0.l*4),a0
		move.l	sc_window(pc),a1
		sub.l	a2,a2
		move.l	APG_Gadtoolsbase(a5),a6
		jmp	_LVOGT_SetGadgetAttrsA(a6)

sc_ghost:
;d0 = Gadget d1 = Ghost 

		lea	sc_gadarray(pc),a0
		move.l	(a0,d0.l*4),a0
		move.l	sc_window(pc),a1
		sub.l	a2,a2
		lea	sc_ghosttag(pc),a3
		move.l	d1,4(a3)
		move.l	APG_Gadtoolsbase(a5),a6
		jmp	_LVOGT_SetGadgetAttrsA(a6)

sc_ghosttag:	dc.l	GA_Disabled,0,0

sc_checkselect:
		lea	sc_gadarray(pc),a0
		move.l	(a0,d0.l*4),a1
		move.w	gg_Flags(a1),d0
		and.w	#SELECTED,d0
		rts

;------------------------------------------------------------------------------
WindowTags:
winL:	dc.l	WA_Left,212
winT:	dc.l	WA_Top,78
winW:	dc.l	WA_Width,220
winH:	dc.l	WA_Height,85
		dc.l	WA_IDCMP,IDCMP_VANILLAKEY!INTEGERIDCMP!SLIDERIDCMP!BUTTONIDCMP!CHECKBOXIDCMP!IDCMP_CLOSEWINDOW!IDCMP_REFRESHWINDOW
		dc.l	WA_Flags,WFLG_CLOSEGADGET!WFLG_DEPTHGADGET!WFLG_ACTIVATE!WFLG_DRAGBAR!WFLG_SMART_REFRESH!WFLG_RMBTRAP
winWG:	dc.l	WA_Gadgets,0
		dc.l	WA_Title,winWTitle
winSC:	dc.l	WA_CustomScreen,0
		dc.l	TAG_DONE

WinWTitle:	dc.b	'Scale Options',0
	even

sc_numtag:	dc.l	GTIN_Number,0,TAG_DONE

sc_numtag1:
		dc.l	GTIN_Number,sc_number
		dc.l	TAG_DONE

sc_number:	dc.l	0

sc_leveltag:
		dc.l	GTSL_Level,0
		dc.l	TAG_DONE

GD_xsize	=	0
GD_ysize	=	1
GD_xslider	=	2
GD_widthpro	=	3
GD_heightpro	=	4
GD_yslider	=	5
GD_div2		=	6
GD_mul2width	=	7
GD_div2height	=	8
GD_mul2height	=	9
GD_keepascpect	=	10
GD_dontpop	=	11
GD_reset	=	12
GD_ok		=	13
GD_cancel	=	14
GD_mode		=	15

scale_CNT    EQU    16



GTypes:
		dc.w	INTEGER_KIND
		dc.w	INTEGER_KIND
		dc.w	SLIDER_KIND
		dc.w	INTEGER_KIND
		dc.w	INTEGER_KIND
		dc.w	SLIDER_KIND
		dc.w	BUTTON_KIND
		dc.w	BUTTON_KIND
		dc.w	BUTTON_KIND
		dc.w	BUTTON_KIND
		dc.w	CHECKBOX_KIND
		dc.w	CHECKBOX_KIND
		dc.w	BUTTON_KIND
		dc.w	BUTTON_KIND
		dc.w	BUTTON_KIND
		dc.w	CYCLE_KIND

NGads:
		dc.w	70,10,50,12
		dc.l	xsizeText,0
		dc.w	GD_xsize
		dc.l	PLACETEXT_LEFT,0,0
		dc.w	262,10,50,12
		dc.l	ysizeText,0
		dc.w	GD_ysize
		dc.l	PLACETEXT_LEFT,0,0
		dc.w	70,40,200,12
		dc.l	xsliderText,0
		dc.w	GD_xslider
		dc.l	PLACETEXT_LEFT,0,0
		dc.w	70,25,50,12
		dc.l	widthproText,0
		dc.w	GD_widthpro
		dc.l	PLACETEXT_LEFT,0,0
		dc.w	262,25,50,12
		dc.l	heightproText,0
		dc.w	GD_heightpro
		dc.l	PLACETEXT_LEFT,0,0
		dc.w	70,55,200,12
		dc.l	ysliderText,0
		dc.w	GD_yslider
		dc.l	PLACETEXT_LEFT,0,0
		dc.w	271,40,20,12
		dc.l	div2Text,0
		dc.w	GD_div2
		dc.l	PLACETEXT_IN,0,0
		dc.w	292,40,20,12
		dc.l	mul2widthText,0
		dc.w	GD_mul2width
		dc.l	PLACETEXT_IN,0,0
		dc.w	271,55,20,12
		dc.l	div2heightText,0
		dc.w	GD_div2height
		dc.l	PLACETEXT_IN,0,0
		dc.w	292,55,20,12
		dc.l	mul2heightText,0
		dc.w	GD_mul2height
		dc.l	PLACETEXT_IN,0,0
		dc.w	70,87,26,11
		dc.l	keepascpectText,0
		dc.w	GD_keepascpect
		dc.l	PLACETEXT_RIGHT,0,0
		dc.w	195,87,26,11
		dc.l	dontpopText,0
		dc.w	GD_dontpop
		dc.l	PLACETEXT_RIGHT,0,0
		dc.w	70,70,242,14
		dc.l	resetText,0
		dc.w	GD_reset
		dc.l	PLACETEXT_IN,0,0
		dc.w	20,125,120,14
		dc.l	okText,0
		dc.w	GD_ok
		dc.l	PLACETEXT_IN,0,0
		dc.w	192,125,120,14
		dc.l	cancelText,0
		dc.w	GD_cancel
		dc.l	PLACETEXT_IN,0,0
		dc.w	70,105,152,12
		dc.l	modeText,0
		dc.w	GD_mode
		dc.l	PLACETEXT_RIGHT,0,0

GTags:
		dc.l	GTIN_Number
sc_xsnumber:	dc.l	2
		dc.l	GTIN_MaxChars,5
		dc.l	STRINGA_Justification,GACT_STRINGRIGHT
		dc.l	GA_Disabled
sc_xsizedis:	dc.l	0
		dc.l	GT_Underscore,'_'
		dc.l	TAG_DONE
		dc.l	GTIN_Number
sc_ysnumber:	dc.l	2
		dc.l	GTIN_MaxChars,5
		dc.l	STRINGA_Justification,GACT_STRINGRIGHT
		dc.l	GA_Disabled
sc_ysizedis:	dc.l	0
		dc.l	GT_Underscore,'_'
		dc.l	TAG_DONE
		dc.l	PGA_Freedom,LORIENT_HORIZ
		dc.l	GA_Immediate,1
		dc.l	GA_RelVerify,1
		dc.l	GTSL_min,25
		dc.l	GTSL_Max,400
		dc.l	GTSL_Level
sc_xlevel:	dc.l	100
		dc.l	GA_Disabled
sc_xleveldis:	dc.l	0
		dc.l	TAG_DONE
		dc.l	GTIN_Number
sc_xpronum:	dc.l	100
		dc.l	GTIN_MaxChars,5
		dc.l	STRINGA_Justification,GACT_STRINGRIGHT
		dc.l	GA_Disabled
sc_xprodis:	dc.l	0
		dc.l	GT_Underscore,'_'
		dc.l	TAG_DONE
		dc.l	GTIN_Number
sc_ypronum:	dc.l	100
		dc.l	GTIN_MaxChars,5
		dc.l	STRINGA_Justification,GACT_STRINGRIGHT
		dc.l	GA_Disabled
sc_yprodis:	dc.l	0
		dc.l	GT_Underscore,'_'
		dc.l	TAG_DONE
		dc.l	PGA_Freedom,LORIENT_HORIZ
		dc.l	GA_Immediate,1
		dc.l	GA_RelVerify,1
		dc.l	GTSL_min,25
		dc.l	GTSL_Max,400
		dc.l	GTSL_Level
sc_ylevel:	dc.l	100
		dc.l	GA_Disabled
sc_yleveldis:	dc.l	0
		dc.l	TAG_DONE
		dc.l	GA_Disabled
sc_xdivdis:	dc.l	0
		dc.l	TAG_DONE
		dc.l	GA_Disabled
sc_xmuldis:	dc.l	0
		dc.l	TAG_DONE
		dc.l	GA_Disabled
sc_ydivdis:	dc.l	0
		dc.l	TAG_DONE
		dc.l	GA_Disabled
sc_ymuldis:	dc.l	0
		dc.l	TAG_DONE
		dc.l	GTCB_Scaled,1
		dc.l	GTCB_Checked
sc_selectas:	dc.l	0
		dc.l	TAG_DONE
		dc.l	GTCB_Scaled,1
		dc.l	GTCB_Checked
sc_selectpop:	dc.l	0
		dc.l	GA_Disabled
sc_popdis:	dc.l	0
		dc.l	TAG_DONE
		dc.l	GA_Disabled
sc_resetdis:	dc.l	0
		dc.l	GT_Underscore,'_'
		dc.l	TAG_DONE
		dc.l	GT_Underscore,'_'
		dc.l	TAG_DONE
		dc.l	GT_Underscore,'_'
		dc.l	TAG_DONE
		dc.l	GTCY_Labels,sc_modelabels
		dc.l	GT_Underscore,'_'
		dc.l	GTCY_ACTIVE
sc_modetag:	dc.l	0
		dc.l	GA_Disabled
sc_modis:	dc.l	1
		dc.l	TAG_DONE

	even

xsizeText:	dc.b	'_Width',0
ysizeText:	dc.b	'_Height',0
xsliderText:	dc.b	'Width',0
widthproText:	dc.b	'Wi_dth %',0
heightproText:	dc.b	'Hei_ght %',0
ysliderText:	dc.b	'Height',0
div2Text:	dc.b	'�2',0
mul2widthText:	dc.b	'�2',0
div2heightText:	dc.b	'�2',0
mul2heightText:	dc.b	'�2',0
keepascpectText:dc.b	'keep aspect',0
dontpopText:	dc.b	"don't popup",0
resetText:	dc.b	'_Reset to default',0
okText:		dc.b	'_Ok',0
cancelText:	dc.b	'_Cancel',0
modeText:	dc.b	'Scale Mode',0

	even
sc_modelabels:
		dc.l	modelab0
		dc.l	modelab1
		dc.l	0

modelab0:	dc.b	'absolute',0
modelab1:	dc.b	'percental',0

scalemsg:	dc.b	'Scale image',0

;----------------------------------------------------------------------------
		section	laber,bss

sc_bprefs:
sc_bpxsize:	ds.l	1
sc_bpysize:	ds.l	1
sc_bpxsizepro:	ds.l	1
sc_bpysizepro:	ds.l	1
sc_bpkeepascpect ds.b	1
sc_bpdontpop:	ds.b	1
sc_bpmode:	ds.b	1
		ds.b	1


sc_bitmaptab:	ds.l	8
