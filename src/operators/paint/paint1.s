����  ]�&b�&b�&b�&b�&b�&b�&b�&b;=A�;===========================================================================
;
;		Fake ein malen nach zahlen bild
;		(c) 1998 ,Frank (Copper) Pagels  /DFT
;
;		25.01.98

		incdir 	include:

		include	exec/exec_lib.i
		include	exec/lists.i
		include	exec/memory.i
		include intuition/intuition_lib.i
		include libraries/gadtools.i
		include	libraries/wb_lib.i
		include	libraries/reqtools_lib.i
		include	graphics/graphics_lib.i
		Include utility/tagitem.i
		include	dos/dos.i		
		include	dos/dos_lib.i		

		include misc/artpro.i
		include render/render_lib.i
		include render/render.i
		

		OPERATORHEADER PT_NODE

		dc.b	'$VER: Fake Paint Operator Modul 0.02 (01.02.98)',0
	even
;---------------------------------------------------------------------------
PT_NODE:
		dc.l	0,0
		dc.b	0,0
		dc.l	PT_name
		dc.l	PT_Party
		dc.b	'PAIN'
		dc.b	'EXTR'	;for extern operator
		dc.l	0
		dc.l	PT_Tags
PT_name:
		dc.b	'Paint',0
	even
;---------------------------------------------------------------------------

PT_Tags:
		dc.l	APT_Creator,PT_Creator
		dc.l	APT_Version,5
		dc.l	APT_Info,PT_Info
		dc.l	APT_Prefs,PT_Prefsrout		;routine for Prefs
		dc.l	APT_Prefsbuffer,PT_mirror
		dc.l	APT_Prefsbuffersize,1
		dc.l	APT_PrefsVersion,1
		dc.l	TAG_DONE

PT_Creator:
		dc.b	'(c) 1998 Frank Pagels /DEFECT Softworks',0
	even
PT_Info:
		dc.b	'Paint',10,10
		dc.b	'Flip or mirror a selected area or the whole',10
		dc.b	'image around the X axis',0 
	even

PT_mirror:	dc.w	0

pt_offset:	dc.l	$300

rline		=	APG_Free1
gline		=	APG_Free2
bline		=	APG_Free3
modulo		=	APG_Free4
xend		=	APG_Free5

PT_window	dc.l	0
PT_GList	dc.l	0
PT_gadarray	dcb.l	3,0

;---------------------------------------------------------------------------
PT_Party:

		tst.w	APG_Cutflag(a5)
		bne.b	.brushok
		move.w	#0,APG_xbrush1(a5)	;Es soll das ganze 
		move.w	#0,APG_ybrush1(a5)	;Pic abgesaved werden
		move.w	APG_ImageWidth(a5),APG_xbrush2(a5)
		move.w	APG_ImageHeight(a5),APG_ybrush2(a5)	
.brushok:

		tst.l	APG_rndr_rendermem(a5)
		bne.w	PT_true

		jsr	APR_MakeRGBData(a5)
PT_true:

;erste Zeile durch checken

		clr.l	rline(a5)
		clr.l	gline(a5)
		clr.l	bline(a5)
		move.l	APG_rndr_rgb(a5),a0		
		moveq	#0,d7
		move.l	APG_Bytewidth(a5),d7
		lsl.l	#3+2,d7			;zeilen modulo
		move.l	d7,modulo(a5)
		sub.l	a4,a4
		move.w	APG_ybrush1(a5),a4
		mulu.w	APG_ybrush1(a5),d7
		add.l	d7,a0			;erste zeile einstellen
		move.w	APG_xbrush1(a5),d0
		move.w	APG_xbrush2(a5),d1
		sub.w	d0,d1
		subq.w	#1,d1
		move.w	d1,xend(a5)
		lea	(a0,d0.w*4),a0		;ersten x wert einstellen
		move.l	pt_offset(pc),d5
		moveq	#0,d6
		moveq	#0,d4			;anzahl der gleichen farbwerte
.nchunk
		cmp.w	xend(a5),d6
		beq	.lineend
		
		move.l	(a0,d6.w*4),d0

		moveq	#0,d1		;addiere die einzelnen farbwerte in puffer
		move.b	d0,d1
		add.l	d1,bline(a5)
		ror.l	#8,d0
		move.b	d0,d1
		add.l	d1,gline(a5)
		ror.l	#8,d0
		move.b	d0,d1
		add.l	d1,rline(a5)
		swap	d0

		addq.w	#1,d4
		addq.w	#1,d6
		move.l	(a0,d6.w*4),d1
		jsr	APR_RGBDiversity24(a5)
		cmp.l	d5,d0			;thereshold
		bls	.nchunk		;wert passt

;wert passt nicht mehr

;mache neuen wert - mittelwert aller werte
.lineend:
		move.l	rline(a5),d0
		divu.l	d4,d0
		and.l	#$000000ff,d0
		move.l	gline(a5),d1
		divu.l	d4,d1
		lsl.l	#8,d0
		move.b	d1,d0
		move.l	bline(a5),d1
		divu.l	d4,d1
		lsl.l	#8,d0
		move.b	d1,d0		;fertiger rgb wert
		
		move.l	d6,d3
		sub.l	d4,d3
		subq	#1,d4

		move.l	d0,(a0,d3.w*4)
		addq.w	#1,d3
		dbra	d4,*-6		;trage gleiche werte ein		

		clr.l	rline(a5)
		clr.l	gline(a5)
		clr.l	bline(a5)
		moveq	#0,d4			;anzahl der gleichen farbwerte
		cmp.w	xend(a5),d6
		blo	.nchunk

;jetzt die restlichen zeilen
pt_rest:
		addq.w	#1,a4
		cmp.w	APG_ybrush2(a5),a4
		beq	pt_end

		move.l	a0,a1
		add.l	modulo(a5),a0

;erster durchlauf vergleicht nur obere und unteren wert der 2 zeilen

		moveq	#0,d6
.nchunk
		cmp.w	xend(a5),d6
		beq	.lineend
		
		move.l	(a0,d6.w*4),d0
		move.l	(a1,d6.w*4),d1
		jsr	APR_RGBDiversity24(a5)
		cmp.l	d5,d0
		bhi	.bigger
		move.l	(a1,d6.w*4),(a0,d6.w*4)
.bigger:
		addq.w	#1,d6
		bra	.nchunk
.lineend

;zweiter durchlauf geht nur noch die zeile lang um gleiche werte zu finden

		jsr	APR_dummy(a5)

pt_second:
		moveq	#0,d6
		moveq	#0,d4			;anzahl der gleichen farbwerte
		clr.l	rline(a5)
		clr.l	gline(a5)
		clr.l	bline(a5)
.nchunk
		cmp.w	xend(a5),d6
		beq	.lineend
		
		move.l	(a0,d6.w*4),d0

		moveq	#0,d1		;addiere die einzelnen farbwerte in puffer
		move.b	d0,d1
		add.l	d1,bline(a5)
		ror.l	#8,d0
		move.b	d0,d1
		add.l	d1,gline(a5)
		ror.l	#8,d0
		move.b	d0,d1
		add.l	d1,rline(a5)
		swap	d0

		addq.w	#1,d4
		addq.w	#1,d6
		move.l	(a0,d6.w*4),d1
		jsr	APR_RGBDiversity24(a5)
		cmp.l	d5,d0			;thereshold
		bls	.nchunk		;wert passt

;wert passt nicht mehr

;mache neuen wert - mittelwert aller werte
.lineend:
		move.l	rline(a5),d0
		divu.l	d4,d0
		and.l	#$000000ff,d0
		move.l	gline(a5),d1
		divu.l	d4,d1
		lsl.l	#8,d0
		move.b	d1,d0
		move.l	bline(a5),d1
		divu.l	d4,d1
		lsl.l	#8,d0
		move.b	d1,d0		;fertiger rgb wert
		
		move.l	d6,d3
		sub.l	d4,d3
		subq	#1,d4

		move.l	d0,(a0,d3.w*4)
		addq.w	#1,d3
		dbra	d4,*-6		;trage gleiche werte ein		

		clr.l	rline(a5)
		clr.l	gline(a5)
		clr.l	bline(a5)
		moveq	#0,d4			;anzahl der gleichen farbwerte
		cmp.w	xend(a5),d6
		blo	.nchunk

		bra	pt_rest

pt_end:
		moveq	#0,d3
		rts

		
;----------------------------------------------------------------------------
PT_Prefsrout
		moveq	#0,d0
		move.b	PT_mirror(pc),d0
		lea	PT_MXmirror(pc),a0
		move.l	d0,(a0)
;----------------------------------------------------------------------------

		move.l	#WindowTags,APG_WindowTagList(a5)	;Tag list for the Window
		move.l	#WinWG+4,APG_WGadgets(a5)	;addr. of WA_Gadgets Tag + 4
		move.l	#winSC+4,APG_WScreen(a5)	;addr. of WA_CustomScreen Tag + 4
		move.l	#PT_window,APG_WinWnd(a5)	;a point for the Window
		move.l	#PT_Glist,APG_Glist(a5)		;a point for the Glist
		move.l	#NTypes,APG_NGads(a5)		;the NGads list
		move.l	#Gtypes,APG_GTypes(a5)		;the GTypes list
		move.l	#Gtags,APG_Gtags(a5)		;the Gtags list
		move.w	#3,APG_CNT(a5)			;the number of Gadgets
		move.l	#PT_gadarray,APG_Gadgets(a5)	;a pointer for the Gadgetsarry, size=4*number of Gadgets
		move.l	APG_Scr(a5),APG_ScrBase(a5)	;the Screenpointer , stored in APG_Scr
	
		move.l	APG_CheckMainWinPos(a5),a6
		jsr	(a6)

		add.w	#315+30,d0
		add.w	#100,d1
		
		move.w	d0,APG_WinLeft(a5)		;the left pos. of the Win
		move.w	d1,APG_WinTop(a5)		;the top pos. of the Win
		move.w	#160,APG_WinWidth(a5)		;the width of the Win
		move.w	#70,APG_WinHeight(a5)		;the height of the Win

		move.l	#WinL,APG_WinL(a5)		;addr. of WA_Left Tag 
		move.l	#WinT,APG_WinT(a5)		;addr. of WA_Top Tag
		move.l	#WinW,APG_WinW(a5)		;addr. of WA_Width Tag
		move.l	#WinH,APG_WinH(a5)		;addr. of WA_Height Tag

		move.l	APG_OpenWindow(a5),a6
		jsr	(a6)

		lea	Text0(pc),a2
		move.w	#Project0_TNUM,APG_TNUM(a5)
		move.l	APG_Writetext(a5),a6
		jsr	(a6)

		move.b	PT_mirror(pc),APG_Mark1(a5)	;rette alte Prefs

PT_wait:	move.l	PT_window(pc),a0
		move.l	APG_Wait(a5),a6
		jsr	(a6)

		cmp.l	#IDCMP_CLOSEWINDOW,d4
		beq.b	PT_Cancel		
		cmp.l	#GADGETUP,d4
		beq.b	PT_gads
		cmp.l	#GADGETDOWN,d4
		beq.b	PT_gads
		bra.b	PT_wait

PT_gads:
		moveq	#0,d0
		move.w	38(a4),d0

		add.l	d0,d0
		lea	PT_jumptab(pc),a0
		move.w	(a0,d0.l),d0
		jmp	(a0,d0.w)

PT_cancel:
		move.b	APG_Mark1(a5),d0
		ext.w	d0
		ext.l	d0
		move.b	d0,PT_mirror
		move.l	d0,PT_MXmirror

PT_pok:
		move.l	PT_window(pc),a0
		move.l	APG_Intuitionbase(a5),a6
		jmp	_LVOCloseWindow(a6)

PT_jumptab:
		dc.w	handlemirror-PT_jumptab
		dc.w	PT_pok-PT_jumptab,PT_cancel-PT_jumptab

handlemirror:
		move.b	d5,PT_mirror
		ext.w	d5
		ext.l	d5
		move.l	d5,PT_MXmirror
		bra.b	PT_wait

;------------------------------------------------------------------------------
WindowTags:
winL:	dc.l	WA_Left,212
winT:	dc.l	WA_Top,78
winW:	dc.l	WA_Width,220
winH:	dc.l	WA_Height,85
		dc.l	WA_IDCMP,IDCMP_CLOSEWINDOW!MXIDCMP!GADGETUP!VANILLAKEY!BUTTONIDCMP!IDCMP_REFRESHWINDOW
		dc.l	WA_Flags,WFLG_CLOSEGADGET!WFLG_DEPTHGADGET!WFLG_ACTIVATE!WFLG_DRAGBAR!WFLG_SMART_REFRESH!WFLG_RMBTRAP
winWG:	dc.l	WA_Gadgets,0
		dc.l	WA_Title,winWTitle
winSC:	dc.l	WA_CustomScreen,0
		dc.l	TAG_DONE

WinWTitle:	dc.b	'Flip X Options',0
	even

GD_Mirror	=	0
GD_Ok		=	1
GD_Cancel	=	2

Gtypes:
		dc.w	MX_KIND
		dc.w	BUTTON_KIND
		dc.w	BUTTON_KIND
Gtags:
		dc.l	GTMX_Labels,Gadget00Labels
		dc.l	GTMX_Active
PT_mxmirror:	dc.l	0
		dc.l	TAG_DONE
		dc.l	GT_Underscore,'_'
		dc.l	TAG_DONE
		dc.l	GT_Underscore,'_'
		dc.l	TAG_DONE
Gadget00Labels:
		dc.l	Gadget00Lab0
		dc.l	Gadget00Lab1
		dc.l	0

Gadget00Lab0:	dc.b	'Flip',0
Gadget00Lab1:	dc.b	'Mirror',0

NTypes:
		DC.W    70,20,17,9
		DC.L    0,0
		DC.W    GD_mirror
		DC.L    0,0,0
		DC.W    10,50,60,13
		DC.L    oktext,0
		DC.W    GD_Ok
		DC.L    PLACETEXT_IN,0,0
		DC.W    85,50,60,13
		DC.L    canceltext,0
		DC.W    GD_Cancel
		DC.L    PLACETEXT_IN,0,0

oktext:		dc.b	'_Ok',0
canceltext:	dc.b	'_Cancel',0

Text0:
		DC.B    2,0
		DC.B    RP_JAM1
		DC.B    0
		DC.W    80,10
		DC.L    0
		DC.L    Project0IText0
		DC.L    0

Project0_TNUM EQU 1

Project0IText0:
		DC.B    'Choose',0

;----------------------------------------------------------------------------
		section	laber,bss
PT_bitmaptab:	ds.l	8

pt_colorindex:	ds.l	256
