����  ��&b�&b�&b�&b�&b�&b�&b�&b;=Z�;===========================================================================
;
;		Macht einen 3D-Rahmen um ein Bild
;		(c) 1997 ,Frank (Copper) Pagels  /DFT
;
;		05.11.97

		incdir 	include:

		include	exec/exec_lib.i
		include	exec/lists.i
		include	exec/memory.i
		include intuition/intuition_lib.i
		include libraries/gadtools.i
		include libraries/gadtools_lib.i
		include	libraries/wb_lib.i
		include	libraries/reqtools_lib.i
		include	graphics/graphics_lib.i
		Include utility/tagitem.i
		include	dos/dos.i		
		include	dos/dos_lib.i		

		include misc/artpro.i
		include render/render_lib.i
		include render/render.i
		include	guigfx/guigfx.i
		include	guigfx/guigfx_lib.i
		

		OPERATORHEADER MF_NODE

		dc.b	'$VER: 3D Button Operator Modul 1.0 (10.11.97)',0
	even
;---------------------------------------------------------------------------
MF_NODE:
		dc.l	0,0
		dc.b	0,0
		dc.l	MF_name
		dc.l	MF_Party
		dc.b	'MFOP'
		dc.b	'EXTR'	;for extern loader
		dc.l	0
		dc.l	MF_Tags
MF_name:
		dc.b	'Make Button',0
	even
;---------------------------------------------------------------------------
MF_Tags:
		dc.l	APT_Creator,MF_Creator
		dc.l	APT_Version,5
		dc.l	APT_Prefs,MF_Prefsrout		;routine for Prefs
		dc.l	APT_Prefsbuffer,MF_framewide	;buffer for Prefs
		dc.l	APT_Prefsbuffersize,16		;size of buffer
		dc.l	APT_PrefsVersion,1		;Prefs Version
		dc.l	APT_Info,MF_Info
		dc.l	TAG_DONE

MF_Creator:
		dc.b	'(c) 1997 Frank Pagels /DEFECT Softworks',0
	even

MF_Info:
		dc.b	'Make Button',10,10
		dc.b	'Makes a button like 3d  frame around ',10
		dc.b	'a selected area.',0

MF_alphasize	=	APG_Free1
MF_alpha	=	APG_Free2
MF_picture	=	APG_Free3
mf_width	=	APG_Free4
mf_height	=	APG_Free4+2

mf_framewide	dc.l	3
mf_framebright	dc.l	50
mf_invers:	dc.l	0
mf_gradien:	dc.l	1

MF_window:	dc.l	0
MF_Glist:	dc.l	0
MF_gadarray:	ds.l	8

mf_prefsback:	ds.l	4

;---------------------------------------------------------------------------

mf_werror:	lea	sizetomsg(pc),a4
		move.l	APG_Status(a5),a6
		jsr	(a6)
		move.l	#APOE_Error,d3
		rts
;---------------------------------------------------------------------------
MF_Party:
		tst.w	APG_Cutflag(a5)
		bne.b	.brushok
		move.w	#0,APG_xbrush1(a5)	;Es soll das ganze 
		move.w	#0,APG_ybrush1(a5)	;Pic abgesaved werden
		move.w	APG_ImageWidth(a5),APG_xbrush2(a5)
		move.w	APG_ImageHeight(a5),APG_ybrush2(a5)	
.brushok:
		moveq	#0,d0
		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
		move.l	mf_framewide(pc),d1
		add.w	d1,d1
		cmp.w	d1,d0
		bls.b	mf_werror
		move.w	APG_ybrush2(a5),d0
		sub.w	APG_ybrush1(a5),d0
		cmp.w	d1,d0	
		bls.b	mf_werror

		moveq	#0,d0
		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
		move.l	d0,d1
		add.l	#15,d1
		lsr.l	#4,d1		;breite in worte
		move.w	d1,APG_Brushword(a5)

		jsr	APR_Openprocess(a5)
		moveq	#4,d0
		moveq	#0,d1
		lea	Makebuttonmsg(pc),a1
		jsr	APR_Initprocess(a5)

		tst.l	APG_rndr_rgb(a5)
		bne.b	.rgbok			;haben wir rgb daten ?
		
		jsr	APR_MakeRGBData(a5)

.rgbok:
		jsr	APR_DoProcess(a5)

;create alphachannel

;erstmal speicher holen

		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
		move.w	APG_ybrush2(a5),d1
		sub.w	APG_ybrush1(a5),d1
		mulu	d1,d0
		move.l	d0,MF_alphasize(a5)	;gr��e merken
		move.l	#MEMF_ANY!MEMF_CLEAR,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)
		tst.l	d0
		bne.b	.memok
		move.l	#APOE_NOMEM,d3
		rts

.memok:
		move.l	d0,MF_alpha(a5)

;jetzt den channel machen
		
;obere wagerechte Line

		sub.l	a1,a1
		lea	mf_gradien(pc),a0
		tst.l	(a0)
		beq.b	.nograd
		move.l	mf_framewide(pc),d1
		mulu	#-2,d1
		add.w	#28,d1
		ext.l	d1
		move.l	mf_framebright(pc),d0
		mulu	d1,d0
		divu	#100,d0
		move.l	d0,a1
.nograd:	moveq	#0,d7
		move.w	APG_xbrush2(a5),d7
		sub.w	APG_xbrush1(a5),d7
		move.w	d7,mf_width(a5)
		move.l	d7,d6
		move.l	MF_framewide(pc),d5
		subq.w	#1,d5
		moveq	#0,d3
.nl:		move.l	d6,d7
		sub.l	d5,d7
		subq.w	#2,d7
		move.l	MF_Alpha(a5),a0
		move.l	d5,d4
		mulu	d6,d4
		add.l	d4,a0		;auf die richtige zeile bringen
		move.l	mf_framebright(pc),d2
		sub.b	d3,d2
		bcc.b	.na
		moveq	#0,d2

.na		move.b	d2,(a0)+
		dbra	d7,.na
		add.w	a1,d3
		subq.w	#1,d5
		bpl.b	.nl

;linke sektrechte seite

		moveq	#0,d7
		move.w	APG_xbrush2(a5),d7
		sub.w	APG_xbrush1(a5),d7
		move.w	APG_ybrush2(a5),d6
		sub.w	APG_ybrush1(a5),d6
		move.w	d6,mf_height(a5)
		move.l	MF_framewide(pc),d5
		subq.w	#1,d5
		moveq	#0,d3
		move.l	mf_framewide(pc),d1
		subq.l	#1,d1
		mulu	d7,d1
.nl1:		move.l	d6,d4
		sub.l	d5,d4
		subq.w	#1,d4
		move.l	MF_Alpha(a5),a0
		add.l	d5,a0		;auf die richtige spalte bringen
		add.l	d1,a0
		sub.l	mf_framewide(pc),d4
		move.l	mf_framebright(pc),d2
		sub.b	d3,d2
		bcc.b	.na1
		moveq	#0,d2
.na1		move.b	d2,(a0)
		add.l	d7,a0
		dbra	d4,.na1
		add.w	a1,d3
		sub.l	d7,d1
		addq.w	#1,d6
		subq.w	#1,d5
		bpl.b	.nl1

		jsr	APR_DoProcess(a5)

;mit alphachannel verkn�pfen

		jsr	APR_Dummy(a5)

		lea	mf_invers(pc),a0
		tst.l	(a0)
		bne.b	.ni
		bsr.b	mf_makelight
		bra.b	mf_rotatealpha
.ni		bsr.w	mf_makedark

;Alphachannel drehen
mf_rotatealpha
		move.l	mf_alpha(a5),a0
		move.l	a0,a1
		move.l	mf_alphasize(a5),d7
		add.l	d7,a1
		subq.l	#1,a1
		lsr.w	#1,d7
		beq.b	.w
		subq.w	#1,d7
.w		move.b	(a0),d0
		move.b	(a1),d1
		move.b	d0,(a1)
		subq.l	#1,a1
		move.b	d1,(a0)+
		dbra	d7,.w

		jsr	APR_DoProcess(a5)

mf_black
		lea	mf_invers(pc),a0
		tst.l	(a0)
		bne.b	.ni
		bsr.w	mf_makedark
		bra.b	mf_endmix
.ni		bsr.b	mf_makelight

mf_endmix
		cmp.b	#24,APG_Planes(a5)
		bhs.b	.nixrender

		jsr	APR_Render(a5)
.nixrender:

;free alles

mf_ende.
		move.l	MF_Alpha(a5),a1
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)

		jsr	APR_MakePreview(a5)

		moveq	#APOE_OK,d3	
		rts

mf_makelight:
		move.l	mf_alpha(a5),a1
		moveq	#0,d0
		moveq	#0,d1
		move.w	APG_xbrush2(a5),a2
		sub.w	APG_xbrush1(a5),a2
		subq.w	#1,a2
		move.w	APG_ybrush1(a5),d7
		move.l	APG_ByteWidth(a5),d5
		lsl.l	#5,d5
.nel		move.l	a2,d6
		move.l	APG_rndr_rgb(a5),a0
		move.l	d7,d0
		mulu	d5,d0
		add.l	d0,a0
		moveq	#0,d0
		move.w	APG_xbrush1(a5),d0
		lsl.l	#2,d0
		add.l	d0,a0		
.nc		move.l	(a0),d0		;rgb wert
		move.b	(a1)+,d4	;alpha wert
		moveq	#0,d1
		move.b	d0,d1
		add.b	d4,d1
		bcc.b	.ns
		move.b	#$ff,d1
.ns		move.b	d1,d0
		bfextu	d0{16:8},d1
		add.b	d4,d1
		bcc.b	.ns1
		move.b	#$ff,d1
.ns1:		bfins	d1,d0{16:8}
		bfextu	d0{8:8},d1
		add.b	d4,d1
		bcc.b	.ns2
		move.b	#$ff,d1
.ns2:		bfins	d1,d0{8:8}
		move.l	d0,(a0)+
		dbra	d6,.nc
		add.w	#1,d7
		cmp.w	APG_ybrush2(a5),d7
		bne.b	.nel		
		rts

mf_makedark:
		move.l	mf_alpha(a5),a1
		moveq	#0,d0
		moveq	#0,d1
		move.w	APG_xbrush2(a5),a2
		sub.w	APG_xbrush1(a5),a2
		subq.w	#1,a2
		move.w	APG_ybrush1(a5),d7
		move.l	APG_ByteWidth(a5),d5
		lsl.l	#5,d5
.nel		move.l	a2,d6
		move.l	APG_rndr_rgb(a5),a0
		move.l	d7,d0
		mulu	d5,d0
		add.l	d0,a0
		moveq	#0,d0
		move.w	APG_xbrush1(a5),d0
		lsl.l	#2,d0
		add.l	d0,a0		
.nc		move.l	(a0),d0
		move.b	(a1)+,d4
		moveq	#0,d1
		move.b	d0,d1
		sub.b	d4,d1
		bcc.b	.ns
		move.b	#$00,d1
.ns		move.b	d1,d0
		bfextu	d0{16:8},d1
		sub.b	d4,d1
		bcc.b	.ns1
		move.b	#$00,d1
.ns1:		bfins	d1,d0{16:8}
		bfextu	d0{8:8},d1
		sub.b	d4,d1
		bcc.b	.ns2
		move.b	#$00,d1
.ns2:		bfins	d1,d0{8:8}
		move.l	d0,(a0)+
		dbra	d6,.nc
		add.w	#1,d7
		cmp.w	APG_ybrush2(a5),d7
		bne.b	.nel		
		rts
		
mf_cantmsg:	dc.b	"Can't make operation, no memory?",0
Makebuttonmsg:	dc.b	"Make Button ...",0

	even

;----------------------------------------------------------------------------
MF_Prefsrout:
;init Prefs

		lea	mf_framewide(pc),a0
		lea	MF_wide(pc),a1
		move.l	(a0),(a1)
		lea	MF_numwide(pc),a1
		move.l	(a0)+,(a1)
		lea	MF_bright(pc),a1
		move.l	(a0),(a1)
		lea	MF_numbright(pc),a1
		move.l	(a0)+,(a1)
		lea	MF_inver(pc),a1
		move.l	(a0)+,(a1)		
		lea	mf_grad(pc),a1
		move.l	(a0)+,(a1)

;---------------------------------------------------------------------------

		move.l	#WindowTags,APG_WindowTagList(a5) ;Tag list for the Window
		move.l	#WinWG+4,APG_WGadgets(a5)	;addr. of WA_Gadgets Tag + 4
		move.l	#winSC+4,APG_WScreen(a5)	;addr. of WA_CustomScreen Tag + 4
		move.l	#MF_window,APG_WinWnd(a5)	;a point for the Window
		move.l	#MF_Glist,APG_Glist(a5)		;a point for the Glist
		move.l	#NTypes,APG_NGads(a5)		;the NGads list
		move.l	#Gtypes,APG_GTypes(a5)		;the GTypes list
		move.l	#Gtags,APG_Gtags(a5)		;the Gtags list
		move.w	#8,APG_CNT(a5)			;the number of Gadgets
		move.l	#MF_gadarray,APG_Gadgets(a5)	;a pointer for the Gadgetsarry, size=4*number of Gadgets
		move.l	APG_Scr(a5),APG_ScrBase(a5)	;the Screenpointer , stored in APG_Scr
	
		move.l	APG_CheckMainWinPos(a5),a6	;returns d0,d1 Pos
		jsr	(a6)

		add.w	#315,d0
		add.w	#100,d1

		move.w	d0,APG_WinLeft(a5)		;the left pos. of the Win
		move.w	d1,APG_WinTop(a5)		;the top pos. of the Win
		move.w	#210,APG_WinWidth(a5)		;the width of the Win
		move.w	#100,APG_WinHeight(a5)		;the height of the Win

		move.l	#WinL,APG_WinL(a5)		;addr. of WA_Left Tag 
		move.l	#WinT,APG_WinT(a5)		;addr. of WA_Top Tag
		move.l	#WinW,APG_WinW(a5)		;addr. of WA_Width Tag
		move.l	#WinH,APG_WinH(a5)		;addr. of WA_Height Tag

		move.l	APG_OpenWindow(a5),a6
		jsr	(a6)

		tst.l	d0
		beq.b	.wok
		rts

.wok		lea	mf_prefsback(pc),a0
		lea	mf_framewide(pc),a1
		move.l	(a1)+,(a0)+
		move.l	(a1)+,(a0)+
		move.l	(a1)+,(a0)+
		move.l	(a1),(a0)

MF_wait:
		move.l	MF_window(pc),a0
		move.l	APG_Wait(a5),a6
		jsr	(a6)				;Handle input

		cmp.l	#GADGETDOWN,d4
		beq.w	MF_slider
		cmp.l	#CLOSEWINDOW,d4
		beq.w	MF_Cancel
		cmp.l	#GADGETUP,d4
		beq.b	MF_gads
		bra.b	MF_wait

MF_gads:
		moveq	#0,d0
		move.w	38(a4),d0
		add.l	d0,d0
		move.l	d0,d1
		lea	MF_pjumptab(pc),a0
		move.w	(a0,d0.l),d0
		jmp	(a0,d0.w)

MF_pjumptab:
		dc.w	MF_ok-MF_pjumptab
		dc.w	MF_cancel-MF_pjumptab
		dc.w	MF_handlewide-MF_pjumptab	;wide
		dc.w	MF_handlebright-MF_pjumptab	;bright
		dc.w	MF_hwinter-MF_pjumptab	;winter
		dc.w	MF_hbinter-MF_pjumptab	;binter
		dc.w	MF_hinver-MF_pjumptab	;inver
		dc.w	MF_hgrad-MF_pjumptab	;reserve

MF_handlewide:
		ext.l	d5
		lea	MF_wide(pc),a0
		move.l	d5,(a0)
		lea	mf_framewide(pc),a0
		move.l	d5,(a0)

		lea	MF_numtag(pc),a3
		move.l	d5,4(a3)
		move.l	#GD_winter,d0
		lea	MF_gadarray(pc),a0
		move.l	(a0,d0.l*4),a0
		move.l	MF_window(pc),a1
		sub.l	a2,a2
		move.l	APG_Gadtoolsbase(a5),a6
		jsr	_LVOGT_SetGadgetAttrsA(a6)
		bra.b	mf_sliderwait

MF_handlebright:
		ext.l	d5
		lea	MF_bright(pc),a0
		move.l	d5,(a0)
		lea	mf_framebright(pc),a0
		move.l	d5,(a0)

		lea	MF_numtag(pc),a3
		move.l	d5,4(a3)
		move.l	#GD_binter,d0
		lea	MF_gadarray(pc),a0
		move.l	(a0,d0.l*4),a0
		move.l	MF_window(pc),a1
		sub.l	a2,a2
		move.l	APG_Gadtoolsbase(a5),a6
		jsr	_LVOGT_SetGadgetAttrsA(a6)

MF_sliderwait:
		move.l	MF_window(pc),a0
		move.l	APG_Wait(a5),a6
		jsr	(a6)				;Handle input

		cmp.l	#GADGETUP,d4
		beq.w	MF_wait			;Slidergadet wurde losgelassen

		cmp.l	#IDCMP_MOUSEMOVE,d4
		beq.b	MF_slider
		bra.b	MF_sliderwait

MF_slider:
		move.w	38(a4),d0
		cmp.w	#GD_wide,d0
		beq.w	MF_handlewide
		cmp.w	#GD_bright,d0
		beq.b	MF_handlebright
		bra.w	MF_wait
MF_hwinter:
		lea	MF_numtag1(pc),a3
		move.l	#GD_winter,d0
		lea	MF_gadarray(pc),a0
		move.l	(a0,d0.l*4),a0
		move.l	MF_window(pc),a1
		sub.l	a2,a2
		move.l	APG_Gadtoolsbase(a5),a6
		jsr	_LVOGT_GetGadgetAttrsA(a6)

		move.l	MF_number(pc),d5
		tst.l	d5
		bne.b	.n
		moveq	#1,d5
		bra.b	.nn
.n		cmp.l	#10,d5
		bls.b	.w
		move.l	#10,d5			;eingetragener wert war zu hoch
.nn
		lea	MF_numtag(pc),a3
		move.l	d5,4(a3)
		move.l	#GD_winter,d0
		lea	MF_gadarray(pc),a0
		move.l	(a0,d0.l*4),a0
		move.l	MF_window(pc),a1
		sub.l	a2,a2
		jsr	_LVOGT_SetGadgetAttrsA(a6)
.w
		lea	mf_wide(pc),a0
		move.l	d5,(a0)
		lea	mf_framewide(pc),a0
		move.l	d5,(a0)

		lea	MF_leveltag(pc),a3
		move.l	d5,4(a3)
		move.l	#GD_wide,d0
		lea	MF_gadarray(pc),a0
		move.l	(a0,d0.l*4),a0
		move.l	MF_window(pc),a1
		sub.l	a2,a2
		jsr	_LVOGT_SetGadgetAttrsA(a6)

		bra.w	MF_wait

MF_hbinter:
		lea	MF_numtag1(pc),a3
		move.l	#GD_binter,d0
		lea	MF_gadarray(pc),a0
		move.l	(a0,d0.l*4),a0
		move.l	MF_window(pc),a1
		sub.l	a2,a2
		move.l	APG_Gadtoolsbase(a5),a6
		jsr	_LVOGT_GetGadgetAttrsA(a6)

		move.l	MF_number(pc),d5
		cmp.l	#255,d5
		bls.b	.w1
		move.l	#255,d5			;eingetragener wert war zu hoch

		lea	MF_numtag(pc),a3
		move.l	d5,4(a3)
		move.l	#GD_binter,d0
		lea	MF_gadarray(pc),a0
		move.l	(a0,d0.l*4),a0
		move.l	MF_window(pc),a1
		sub.l	a2,a2
		jsr	_LVOGT_SetGadgetAttrsA(a6)
.w1
		lea	mf_bright(pc),a0
		move.l	d5,(a0)
		lea	mf_framebright(pc),a0
		move.l	d5,(a0)

		lea	MF_leveltag(pc),a3
		move.l	d5,4(a3)
		move.l	#GD_bright,d0
		lea	MF_gadarray(pc),a0
		move.l	(a0,d0.l*4),a0
		move.l	MF_window(pc),a1
		sub.l	a2,a2
		jsr	_LVOGT_SetGadgetAttrsA(a6)

		bra.w	MF_wait

MF_hinver:
		lea	mf_invers(pc),a1
		lea	MF_inver(pc),a2
		move.l	#GD_inver*4,d0
mf_cbutton:
		clr.l	(a1)
		clr.l	(a2)
		lea	MF_gadarray(pc),a0
		move.l	(a0,d0),a0
		move.w	gg_Flags(a0),d0
		and.w	#SELECTED,d0
		beq.w	mf_wait		;nicht selected
		move.l	#1,(a1)
		move.l	#1,(a2)
		bra.w	mf_wait

MF_hgrad:
		lea	mf_gradien(pc),a1
		lea	MF_grad(pc),a2
		move.l	#GD_grad*4,d0
		bra.b	mf_cbutton

MF_cancel:
		lea	mf_prefsback(pc),a0
		lea	mf_framewide(pc),a1
		move.l	(a0)+,(a1)+
		move.l	(a0)+,(a1)+
		move.l	(a0)+,(a1)+
		move.l	(a0),(a1)+

MF_ok:
		move.l	MF_window(pc),a0
		move.l	APG_Intuitionbase(a5),a6
		jmp	_LVOCloseWindow(a6)


;--------------------------------------------------------------------------------
	even

WindowTags:
winL:	dc.l	WA_Left,212
winT:	dc.l	WA_Top,78
winW:	dc.l	WA_Width,220
winH:	dc.l	WA_Height,85
		dc.l	WA_IDCMP,SLIDERIDCMP!IDCMP_CLOSEWINDOW!GADGETUP!GADGETDOWN!VANILLAKEY!BUTTONIDCMP!IDCMP_REFRESHWINDOW!INTEGERIDCMP!IDCMP_MOUSEBUTTONS
		dc.l	WA_Flags,WFLG_CLOSEGADGET!WFLG_DEPTHGADGET!WFLG_ACTIVATE!WFLG_DRAGBAR!WFLG_SMART_REFRESH!WFLG_RMBTRAP
winWG:	dc.l	WA_Gadgets,0
		dc.l	WA_Title,winWTitle
winSC:	dc.l	WA_CustomScreen,0
		dc.l	TAG_DONE

WinWTitle:	dc.b	'Make Button Options',0

	even

MF_numtag:	dc.l	GTIN_Number,0,TAG_DONE

MF_numtag1:
		dc.l	GTIN_Number,MF_number
		dc.l	TAG_DONE

MF_number:	dc.l	0

MF_leveltag:
		dc.l	GTSL_Level,0
		dc.l	TAG_DONE


GD_Ok		=	0
GD_Cancel	=	1
GD_wide		=	2
GD_bright	=	3
GD_winter	=	4
GD_binter	=	5
GD_inver	=	6
GD_grad		=	7

Gtypes:
		dc.w	BUTTON_KIND
		dc.w	BUTTON_KIND
		dc.w	SLIDER_KIND
		dc.w	SLIDER_KIND
		dc.w	INTEGER_KIND
		dc.w	INTEGER_KIND
		dc.w	CHECKBOX_KIND
		dc.w	CHECKBOX_KIND

Gtags:
		dc.l	TAG_DONE
		dc.l	TAG_DONE
		dc.l	GTSL_Level
MF_wide:	dc.l	3
		dc.l	GTSL_Max,10
		dc.l	GTSL_Min,1
		dc.l	GTSL_MaxLevelLen,3
		dc.l	PGA_Freedom,LORIENT_HORIZ
		dc.l	GA_RelVerify,1
		dc.l	GA_Immediate,1
		dc.l	TAG_DONE
		dc.l	GTSL_Level
MF_bright:	dc.l	50
		dc.l	GTSL_Max,255
		dc.l	GTSL_MaxLevelLen,3
		dc.l	PGA_Freedom,LORIENT_HORIZ
		dc.l	GA_RelVerify,1
		dc.l	GA_Immediate,1
		dc.l	TAG_DONE
		dc.l	GTIN_Number
MF_numwide:	dc.l	3
		dc.l	GTIN_MaxChars,3
		dc.l	TAG_DONE
		dc.l	GTIN_Number
MF_numbright:	dc.l	50
		dc.l	GTIN_MaxChars,3
		dc.l	TAG_DONE
		dc.l	GTCB_Checked
MF_inver:	dc.l	0
		dc.l	GTCB_Scaled,1
		dc.l	TAG_DONE
		dc.l	GTCB_Checked
MF_grad:	dc.l	1
		dc.l	GTCB_Scaled,1
		dc.l	TAG_DONE

NTypes:
		DC.W    10,80,70,14
		DC.L    oktext,0
		DC.W    GD_Ok
		DC.L    PLACETEXT_IN,0,0
		DC.W    130,80,70,14
		DC.L    canceltext,0
		DC.W    GD_Cancel
		DC.L    PLACETEXT_IN,0,0
		dc.w	10,17,150,12
		dc.l	wideText,0
		dc.w	GD_wide
		dc.l	PLACETEXT_ABOVE!NG_HIGHLABEL,0,0
 		dc.w	10,45,150,12
		dc.l	brightText,0
		dc.w	GD_bright
		dc.l	PLACETEXT_ABOVE!NG_HIGHLABEL,0,0
		dc.w	162,17,38,12
		dc.l	0,0
		dc.w	GD_winter
		dc.l	0,0,0
		dc.w	162,45,38,12
		dc.l	0,0
		dc.w	GD_binter
		dc.l	0,0,0
		dc.w	10,62,26,11
		dc.l	invertext,0
		dc.w	GD_inver
		DC.L    PLACETEXT_RiGHT,0,0
		dc.w	100,62,26,11
		dc.l	gradtext,0
		dc.w	GD_grad
		DC.L    PLACETEXT_RiGHT,0,0


oktext:		dc.b	'Ok',0
canceltext:	dc.b	'Cancel',0
wideText:	dc.b	'Frame Wide',0
brighttext:	dc.b	'Bright',0
invertext:	dc.b	'Invers',0
gradtext:	dc.b	'Gradiant',0

sizetomsg:	dc.b	'Selected brush to small!',0
 
