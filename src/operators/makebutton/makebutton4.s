����  6�  +��&b�&b�&b�&b�&b�&b�&b;=��;===========================================================================
;
;		Macht einen 3D-Rahmen um ein Bild
;		(c) 1997 ,Frank (Copper) Pagels  /DFT
;
;		05.11.97

		incdir 	include:

		include	exec/exec_lib.i
		include	exec/lists.i
		include	exec/memory.i
		include intuition/intuition_lib.i
		include intuition/intuition.i
		include libraries/gadtools.i
		include libraries/gadtools_lib.i
		include	libraries/wb_lib.i
		include	libraries/reqtools_lib.i
		include	graphics/graphics_lib.i
		Include utility/tagitem.i
		include	dos/dos.i		
		include	dos/dos_lib.i		

		include misc/artpro.i
		include render/render_lib.i
		include render/render.i
		include	guigfx/guigfx.i
		include	guigfx/guigfx_lib.i
		

		OPERATORHEADER MF_NODE

		dc.b	'$VER: 3D Button Operator Modul 2.03 (17.08.98)',0
	even
;---------------------------------------------------------------------------
MF_NODE:
		dc.l	0,0
		dc.b	0,0
		dc.l	MF_name
		dc.l	MF_Party
		dc.b	'MFOP'
		dc.b	'EXTR'	;for extern loader
		dc.l	0
		dc.l	MF_Tags
MF_name:
		dc.b	'Make Button',0
	even
;---------------------------------------------------------------------------
MF_Tags:
		dc.l	APT_Creator,MF_Creator
		dc.l	APT_Version,5
		dc.l	APT_Prefs,MF_Prefsrout		;routine for Prefs
		dc.l	APT_Prefsbuffer,MF_framewide	;buffer for Prefs
		dc.l	APT_Prefsbuffersize,40		;size of buffer
		dc.l	APT_PrefsVersion,2		;Prefs Version
		dc.l	APT_Info,MF_Info
		dc.l	TAG_DONE

MF_Creator:
		dc.b	'(c) 1997 Frank Pagels /DEFECT Softworks',0
	even

MF_Info:
		dc.b	'Make Button',10,10
		dc.b	'Makes a button like 3d  frame around ',10
		dc.b	'a selected area.',0

MF_alpha	=	APG_Free2
MF_picture	=	APG_Free3
mf_width	=	APG_Free4
mf_height	=	APG_Free4+2

mf_testimage	=	APG_Free1
mf_testsize	=	APG_Free5
mf_drawhandle	=	APG_Free6
mf_x		=	APG_Free7
mf_y1		=	APG_Free8
mf_y2		=	APG_Free9
mf_bytewidth	=	APG_Free10
mf_rgb		=	APG_Free11


mf_psm:		dc.l	0

mf_framewide	dc.l	3
mf_tident	dc.l	50
mf_lident	dc.l	50
mf_rident:	dc.l	50
mf_bident:	dc.l	50
mf_pl1:		dc.l	1
mf_pl2:		dc.l	1
mf_pl3:		dc.l	0
mf_pl4:		dc.l	0
mf_gradien:	dc.l	1


MF_window:	dc.l	0
MF_Glist:	dc.l	0
MF_gadarray:	ds.l	17

mf_prefsback:	ds.l	10

mf_backcolor:	ds.l	3

mf_buttkoor:	ds.l	4

;---------------------------------------------------------------------------

mf_werror:	lea	sizetomsg(pc),a4
		move.l	APG_Status(a5),a6
		jsr	(a6)
		move.l	#APOE_Error,d3
		rts
;---------------------------------------------------------------------------
MF_Party:
		tst.w	APG_Cutflag(a5)
		bne.b	.brushok
		move.w	#0,APG_xbrush1(a5)	;Es soll das ganze 
		move.w	#0,APG_ybrush1(a5)	;Pic abgesaved werden
		move.w	APG_ImageWidth(a5),APG_xbrush2(a5)
		move.w	APG_ImageHeight(a5),APG_ybrush2(a5)	
.brushok:
		moveq	#0,d0
		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
		move.l	mf_framewide(pc),d1
		add.w	d1,d1
		cmp.w	d1,d0
		bls.b	mf_werror
		move.w	APG_ybrush2(a5),d0
		sub.w	APG_ybrush1(a5),d0
		cmp.w	d1,d0	
		bls.b	mf_werror

		moveq	#0,d0
		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
		move.l	d0,d1
		add.l	#15,d1
		lsr.l	#4,d1		;breite in worte
		move.w	d1,APG_Brushword(a5)

		jsr	APR_Openprocess(a5)
		moveq	#3,d0
		moveq	#0,d1
		lea	Makebuttonmsg(pc),a1
		jsr	APR_Initprocess(a5)

		tst.l	APG_rndr_rgb(a5)
		bne.b	.rgbok			;haben wir rgb daten ?
		
		jsr	APR_MakeRGBData(a5)
		tst.l	d0
		beq.b	.rgbok
		move.l	#APOE_NOMEM,d3
		rts

.rgbok:
		jsr	APR_DoProcess(a5)

;create alphachannel

;erstmal speicher holen

		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
		move.w	APG_ybrush2(a5),d1
		sub.w	APG_ybrush1(a5),d1
		mulu	d1,d0
		add.l	d0,d0		;2 mal weil 16 bit channel
		move.l	#MEMF_ANY!MEMF_CLEAR,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)
		tst.l	d0
		bne.b	.memok
		move.l	#APOE_NOMEM,d3
		rts

.memok:
		move.l	d0,MF_alpha(a5)

		move.w	APG_xbrush2(a5),d7
		sub.w	APG_xbrush1(a5),d7
		move.w	d7,mf_width(a5)
		move.w	APG_ybrush2(a5),d6
		sub.w	APG_ybrush1(a5),d6
		move.w	d6,mf_height(a5)

;------------------------------------------------------------------------------
		bsr.b	mf_createalpha
		jsr	APR_DoProcess(a5)

		move.w	APG_xbrush1(a5),mf_x(a5)
		move.w	APG_ybrush1(a5),mf_y1(a5)
		move.w	APG_ybrush2(a5),mf_y2(a5)
		move.l	APG_Bytewidth(a5),mf_bytewidth(a5)
		move.l	APG_rndr_rgb(a5),mf_rgb(a5)

		bsr.w	mf_mergealpha
		jsr	APR_DoProcess(a5)

mf_endmix
		move.l	MF_Alpha(a5),a1
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)

		cmp.b	#24,APG_Planes(a5)
		bhs.b	.nixrender

		jsr	APR_Render(a5)
		jsr	APR_Save2TMP(a5)
		moveq	#APOE_OK,d3	
		rts
.nixrender:

mf_ende.
		jsr	APR_Save2TMP(a5)
		jsr	APR_MakePreview(a5)

		moveq	#APOE_OK,d3	
		rts

;------------------------------------------------------------------------------
;jetzt den channel machen
		
;obere wagerechte Line
mf_createalpha:
		lea	mf_pl1(pc),a4
		sub.l	a1,a1
		lea	mf_gradien(pc),a0
		tst.l	(a0)
		beq.b	.nograd
		move.l	mf_framewide(pc),d1
		move.l	mf_tident(pc),d0
		divu	d1,d0
		ext.l	d0
		move.l	d0,a1		;Gradient faktor
.nograd:
		moveq	#0,d7
		move.w	mf_width(a5),d7
		move.l	d7,d6
		move.l	MF_framewide(pc),d5
		subq.w	#1,d5
		moveq	#0,d3
.nl:		move.l	d6,d7
		sub.l	d5,d7
		subq.w	#2,d7
		move.l	MF_Alpha(a5),a0
		move.l	d5,d4
		mulu	d6,d4
		add.l	d4,d4
		add.l	d4,a0		;auf die richtige zeile bringen
		move.l	mf_tident(pc),d2
		sub.b	d3,d2
		bcc.b	.na
		moveq	#0,d2
.na		tst.l	(a4)		;dark ?
		bne.b	.tw
		neg.w	d2
.tw		move.w	d2,(a0)+
		dbra	d7,.tw
		add.w	a1,d3
		subq.w	#1,d5
		bpl.b	.nl

;----------------------------------------------------------------------------
;linke senktrechte seite
mf_left:
		lea	mf_pl2(pc),a4
		sub.l	a1,a1
		lea	mf_gradien(pc),a0
		tst.l	(a0)
		beq.b	.nograd
		move.l	mf_framewide(pc),d1
		move.l	mf_lident(pc),d0
		divu	d1,d0
		ext.l	d0
		move.l	d0,a1		;Gradient faktor
.nograd		moveq	#0,d7
		move.w	mf_width(a5),d7
		move.w	mf_height(a5),d6
		move.l	MF_framewide(pc),d5
		subq.w	#1,d5
		moveq	#0,d3
		move.l	mf_framewide(pc),d1
		subq.l	#1,d1
		mulu	d7,d1
.nl1:		move.l	d6,d4
		sub.l	d5,d4
		subq.w	#1,d4
		move.l	MF_Alpha(a5),a0
		lea	(a0,d5.w*2),a0		;auf die richtige spalte bringen
		lea	(a0,d1.w*2),a0
		sub.l	mf_framewide(pc),d4
		move.l	mf_lident(pc),d2
		sub.b	d3,d2
		bcc.b	.na1
		moveq	#0,d2
.na1		tst.l	(a4)		;darker ?
		bne.b	.lw
		neg.w	d2
.lw		move.w	d2,(a0)
		lea	(a0,d7*2),a0
		dbra	d4,.lw
		add.w	a1,d3
		sub.l	d7,d1
		addq.w	#1,d6
		subq.w	#1,d5
		bpl.b	.nl1

;---------------------------------------------------------------------------------
;rechte senktrechte seite
mf_right:
		lea	mf_pl3(pc),a4
		sub.l	a1,a1
		lea	mf_gradien(pc),a0
		tst.l	(a0)
		beq.b	.nograd
		move.l	mf_framewide(pc),d1
		move.l	mf_rident(pc),d0
		divu	d1,d0
		ext.l	d0
		move.l	d0,a1		;Gradient faktor
.nograd		moveq	#0,d7
		move.w	mf_width(a5),d7
		move.w	mf_height(a5),d6
		move.l	MF_framewide(pc),d5
		subq.w	#1,d5
		moveq	#0,d3
		move.l	mf_framewide(pc),d1
		subq.l	#1,d1
		mulu	d7,d1
.nl1:		move.l	d6,d4
		sub.l	d5,d4
	;	subq.w	#1,d4
		move.l	MF_Alpha(a5),a0
		move.l	d7,d0
		sub.w	d5,d0
		subq.w	#1,d0
		lea	(a0,d0.w*2),a0		;auf die richtige spalte bringen
		lea	(a0,d1.w*2),a0
		sub.l	mf_framewide(pc),d4
		move.l	mf_rident(pc),d2
		sub.b	d3,d2
		bcc.b	.na1
		moveq	#0,d2
.na1		tst.l	(a4)		;darker ?
		bne.b	.lw
		neg.w	d2
.lw		move.w	d2,(a0)
		lea	(a0,d7*2),a0
		dbra	d4,.lw
		add.w	a1,d3
		sub.l	d7,d1
		addq.w	#1,d6
		subq.w	#1,d5
		bpl.b	.nl1

;---------------------------------------------------------------------------------
;untere wagerechte Line
mf_bottom:
		lea	mf_pl4(pc),a4
		sub.l	a1,a1
		lea	mf_gradien(pc),a0
		tst.l	(a0)
		beq.b	.nograd
		move.l	mf_framewide(pc),d1
		move.l	mf_bident(pc),d0
		divu	d1,d0
		ext.l	d0
		move.l	d0,a1		;Gradient faktor
.nograd		moveq	#0,d7
		move.w	mf_width(a5),d7
		move.l	d7,d6
		move.l	MF_framewide(pc),d5
		subq.w	#1,d5
		moveq	#0,d3
.nl:		move.l	d6,d7
		sub.l	d5,d7
		sub.l	d5,d7
		subq.w	#2,d7
		move.l	MF_Alpha(a5),a0
		move.w	mf_height(a5),d0
		sub.w	d5,d0
		subq.w	#1,d0
		move.l	d0,d4
		mulu	d6,d4
		add.l	d4,d4
		add.l	d4,a0		;auf die richtige zeile bringen
		lea	(a0,d5*2),a0
		move.l	mf_bident(pc),d2
		sub.b	d3,d2
		bcc.b	.na
		moveq	#0,d2
.na		tst.l	(a4)		;dark ?
		bne.b	.tw
		neg.w	d2
.tw		move.w	d2,(a0)+
		dbra	d7,.tw
		add.w	a1,d3
		subq.w	#1,d5
		bpl.b	.nl
		rts

;---------------------------------------------------------------------------------
;alphachannel mit dem Bild verkn�pfen
;
mf_mergealpha:
		move.l	mf_alpha(a5),a1
		moveq	#0,d0
		moveq	#0,d1

	;	move.w	APG_xbrush2(a5),a2
	;	sub.w	APG_xbrush1(a5),a2
		move.w	mf_width(a5),a2
		subq.w	#1,a2

	;	move.w	APG_ybrush1(a5),d7
		move.w	mf_y1(a5),d7
		
	;	move.l	APG_ByteWidth(a5),d5
		move.l	mf_bytewidth(a5),d5
		
		rol.l	#5,d5		;!!!!!

.nel		move.l	a2,d6
	;	move.l	APG_rndr_rgb(a5),a0
		move.l	mf_rgb(a5),a0
		move.l	d7,d0
		mulu	d5,d0
		add.l	d0,a0
		moveq	#0,d0
	;	move.w	APG_xbrush1(a5),d0
		move.w	mf_x(a5),d0
		lsl.l	#2,d0
		add.l	d0,a0		
.nc		move.l	(a0),d0		;rgb wert
		move.w	(a1)+,d4	;alpha wert
		moveq	#0,d1
		move.b	d0,d1

		add.w	d4,d1
		bpl.b	.ns
		moveq	#0,d1
.ns		cmp.w	#255,d1
		bls.b	.w
		move.w	#255,d1
.w
		move.b	d1,d0
		bfextu	d0{16:8},d1
		add.w	d4,d1
		bpl.b	.ns1
		moveq	#0,d1
.ns1:
		cmp.w	#255,d1
		bls.b	.w1
		move.w	#255,d1
.w1
		bfins	d1,d0{16:8}
		bfextu	d0{8:8},d1
		add.w	d4,d1
		bpl.b	.ns2
		moveq	#0,d1
.ns2:
		cmp.w	#255,d1
		bls.b	.w2
		move.w	#255,d1
.w2
		bfins	d1,d0{8:8}
		move.l	d0,(a0)+
		dbra	d6,.nc
		add.w	#1,d7
	;	cmp.w	APG_ybrush2(a5),d7
		cmp.w	mf_y2(a5),d7
		bne.b	.nel		
		rts

;------------------------------------------------------------------------------
mf_cantmsg:	dc.b	"Can't make operation, no memory?",0
Makebuttonmsg:	dc.b	"Make Button ...",0

	even

mf_drawbutton:
;alten alpha l�schen
		move.l	mf_testsize(a5),d7
		subq.w	#1,d7
		move.l	d7,d6
		move.l	mf_alpha(a5),a0
.nc:		clr.w	(a0)+
		dbra	d7,.nc

;f�lle image mit default wert

		move.l	mf_testimage(a5),a0
		move.l	mf_backcolor(pc),d0
.nf		move.l	#$a4a4a4,(a0)+
	;	move.l	d0,(a0)+
		dbra	d6,.nf

;Mache Alphachannel
		lea	mf_buttkoor(pc),a0
		move.l	8(a0),d0
		move.l	12(a0),d1
		move.w	d0,mf_width(a5)
		move.w	d1,mf_height(a5)
		move.l	mf_testimage(a5),mf_rgb(a5)

		bsr.w	mf_createalpha

;Mix Alpha
		lea	mf_buttkoor(pc),a0
		clr.w	mf_x(a5)
		clr.w	mf_y1(a5)
		move.l	12(a0),d0
		move.w	d0,mf_y2(a5)
		moveq	#0,d0
		move.l	8(a0),d0
		ror.l	#3,d0
		move.l	d0,mf_bytewidth(a5)

		bsr.w	mf_mergealpha

;Zeichne Button

		move.l	APG_Guigfxbase(a5),a6

		move.l	mf_picture(a5),d0
		beq.b	.nixpicture
		move.l	d0,a0
		jsr	_LVODeletePicture(a6)
		
.nixpicture:
;Picture machen
		move.l	mf_testimage(a5),a0	;logopuffer
		lea	mf_buttkoor(pc),a1
		move.l	8(a1),d0
		move.l	12(a1),d1
		move.l	#TAG_DONE,-(a7)
		move.l	#PIXFMT_0RGB_32,-(a7)
		move.l	#GGFX_PixelFormat,-(a7)
		move.l	a7,a1
		jsr	_LVOMakePictureA(a6)
		lea	12(a7),a7
		move.l	d0,mf_picture(a5)		;logopic

		move.l	mf_drawhandle(a5),a0
		move.l	mf_picture(a5),a1
		lea	mf_buttkoor(pc),a2
		move.l	(a2),d0
		move.l	4(a2),d1
		move.l	#TAG_DONE,-(a7)
		move.l	8(a2),-(a7)
		move.l	#GGFX_SourceWidth,-(a7)
		move.l	12(a2),-(a7)
		move.l	#GGFX_SourceHeight,-(a7)
		move.l	#DITHERMODE_EDD,-(a7)
		move.l	#GGFX_Dithermode,-(a7)
		move.l	a7,a2
		jsr	_LVODrawpictureA(a6)
		lea	28(a7),a7

		rts

;----------------------------------------------------------------------------
MF_Prefsrout:
;init Prefs
		clr.l	mf_alpha(a5)
		clr.l	mf_picture(a5)
		clr.l	mf_drawhandle(a5)
		clr.l	mf_psm

		lea	mf_framewide(pc),a0
		lea	MF_wide(pc),a1
		move.l	(a0),(a1)
		lea	MF_numwide(pc),a1
		move.l	(a0)+,(a1)
		lea	MF_bright(pc),a1
		move.l	(a0),(a1)
		lea	MF_numbright(pc),a1
		move.l	(a0)+,(a1)
		lea	MF_lbright(pc),a1
		move.l	(a0),(a1)
		lea	MF_lnumbright(pc),a1
		move.l	(a0)+,(a1)
		lea	MF_rbright(pc),a1
		move.l	(a0),(a1)
		lea	MF_rnumbright(pc),a1
		move.l	(a0)+,(a1)
		lea	MF_bobright(pc),a1
		move.l	(a0),(a1)
		lea	MF_bonumbright(pc),a1
		move.l	(a0)+,(a1)
		lea	MF_l1(pc),a1
		move.l	(a0)+,(a1)
		lea	MF_l2(pc),a1
		move.l	(a0)+,(a1)
		lea	MF_l3(pc),a1
		move.l	(a0)+,(a1)
		lea	MF_l4(pc),a1
		move.l	(a0)+,(a1)
		lea	mf_grad(pc),a1
		move.l	(a0)+,(a1)

		moveq	#20,d0
		moveq	#25,d1
		moveq	#110,d2
		moveq	#45,d3
		jsr	APR_CalcNewKoor(a5)
		lea	mf_buttkoor(pc),a0
		movem.l	d0-d3,(a0)
		
		move.l	8(a0),d0
		move.l	12(a0),d1
		mulu	d1,d0
		move.l	d0,d7
		add.l	d0,d0	;16 bit channel
		move.l	#MEMF_ANY!MEMF_CLEAR,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)
		move.l	d0,MF_alpha(a5)

		move.l	d7,d0
		lsl.l	#2,d0	;*4 f�r 0rgb
		move.l	#MEMF_ANY!MEMF_CLEAR,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)
		move.l	d0,mf_testimage(a5)

		move.l	d7,mf_testsize(a5)
		
;---------------------------------------------------------------------------

		move.l	#WindowTags,APG_WindowTagList(a5) ;Tag list for the Window
		move.l	#WinWG+4,APG_WGadgets(a5)	;addr. of WA_Gadgets Tag + 4
		move.l	#winSC+4,APG_WScreen(a5)	;addr. of WA_CustomScreen Tag + 4
		move.l	#MF_window,APG_WinWnd(a5)	;a point for the Window
		move.l	#MF_Glist,APG_Glist(a5)		;a point for the Glist
		move.l	#NTypes,APG_NGads(a5)		;the NGads list
		move.l	#Gtypes,APG_GTypes(a5)		;the GTypes list
		move.l	#Gtags,APG_Gtags(a5)		;the Gtags list
		move.w	#17,APG_CNT(a5)			;the number of Gadgets
		move.l	#MF_gadarray,APG_Gadgets(a5)	;a pointer for the Gadgetsarry, size=4*number of Gadgets
		move.l	APG_Scr(a5),APG_ScrBase(a5)	;the Screenpointer , stored in APG_Scr
	
		move.l	APG_CheckMainWinPos(a5),a6	;returns d0,d1 Pos
		jsr	(a6)

		add.w	#250,d0
		add.w	#50,d1

		move.w	d0,APG_WinLeft(a5)		;the left pos. of the Win
		move.w	d1,APG_WinTop(a5)		;the top pos. of the Win
		move.w	#400,APG_WinWidth(a5)		;the width of the Win
		move.w	#180,APG_WinHeight(a5)		;the height of the Win

		move.l	#WinL,APG_WinL(a5)		;addr. of WA_Left Tag 
		move.l	#WinT,APG_WinT(a5)		;addr. of WA_Top Tag
		move.l	#WinW,APG_WinW(a5)		;addr. of WA_Width Tag
		move.l	#WinH,APG_WinH(a5)		;addr. of WA_Height Tag

		move.l	APG_OpenWindow(a5),a6
		jsr	(a6)

		tst.l	d0
		beq.b	.wok
		rts

;Settings backuppen

.wok
		moveq	#10,d0
		moveq	#10,d1
		move.l	#130,d2
		moveq	#75,d3
		moveq	#0,d4
		move.l	mf_window(pc),a0
		jsr	APR_DrawFrame(a5)
		
		move.l	APG_guigfxbase(a5),a6
		sub.l	a0,a0
		jsr	_LVOCreatePenShareMapA(a6)
		lea	mf_psm(pc),a0
		move.l	d0,(a0)

		lea	mf_greypalette,a0
		move.l	a0,a1
		moveq	#64-1,d7
		moveq	#0,d0
.np2		move.l	d0,(a0)+
		add.l	#$0040404,d0
		dbra	d7,.np2

		move.l	mf_psm(pc),a0
		move.l	#TAG_DONE,-(a7)
		move.l	#64,-(a7)
		move.l	#GGFX_NumColors,-(a7)
		move.l	a7,a2
		jsr	_LVOAddPaletteA(a6)
		lea	12(a7),a7
		
		move.l	mf_psm(pc),a0
		move.l	APG_Scr(a5),a1
		lea	sc_viewport(a1),a2
		move.l	4(a2),a2		;Colormap
		move.l	mf_window(pc),a1
		move.l	wd_rport(a1),a1
		sub.l	a3,a3
	move.l	#TAG_DONE,-(a7)
	move.l	#50,-(a7)
	move.l	#GGFX_DitherThreshold,-(a7)
	move.l	a7,a3
		jsr	_LVOObtainDrawHandleA(a6)
		move.l	d0,mf_drawhandle(a5)
		lea	12(a7),a7

		lea	mf_prefsback(pc),a0
		lea	mf_framewide(pc),a1
		moveq	#10-1,d7
.np		move.l	(a1)+,(a0)+
		dbra	d7,.np

;Hintergrundfarbe feststellen
		move.l	APG_Scr(a5),a0
		lea	sc_viewport(a0),a0
		move.l	4(a0),a0	;Colormap
		moveq	#0,d0		;erste farbe
		moveq	#1,d1		;nur eine farbe
		lea	mf_backcolor(pc),a1
		move.l	APG_gfxbase(a5),a6
		jsr	_LVOGetRGB32(a6)

		lea	mf_backcolor(pc),a0
		moveq	#0,d0
		move.l	(a0)+,d1
		move.b	d1,d0
		lsl.l	#8,d0
		move.l	(a0)+,d1
		move.b	d1,d0
		lsl.l	#8,d0
		move.l	(a0)+,d1
		move.b	d1,d0
		lea	mf_backcolor(pc),a0
		move.l	d0,(a0)

		bsr.w	mf_drawbutton

MF_wait:
		move.l	MF_window(pc),a0
		move.l	APG_Wait(a5),a6
		jsr	(a6)				;Handle input

		cmp.l	#GADGETDOWN,d4
		beq.w	MF_slider
		cmp.l	#CLOSEWINDOW,d4
		beq.w	MF_Cancel
		cmp.l	#GADGETUP,d4
		beq.b	MF_gads
		bra.b	MF_wait

MF_gads:
		moveq	#0,d0
		move.w	38(a4),d0
		add.l	d0,d0
		move.l	d0,d1
		lea	MF_pjumptab(pc),a0
		move.w	(a0,d0.l),d0
		jmp	(a0,d0.w)

MF_pjumptab:
		dc.w	MF_ok-MF_pjumptab
		dc.w	MF_cancel-MF_pjumptab
		dc.w	MF_handlewide-MF_pjumptab	;wide
		dc.w	MF_handlebright-MF_pjumptab	;bright
		dc.w	MF_hwinter-MF_pjumptab		;winter
		dc.w	MF_hbinter-MF_pjumptab		;binter
		dc.w	MF_hgrad-MF_pjumptab		;gradient
		dc.w	MF_hlbright-MF_pjumptab 	;left bevel
		dc.w	MF_hlinter-MF_pjumptab
		dc.w	MF_hrbright-MF_pjumptab
		dc.w	MF_hrinter-MF_pjumptab
		dc.w	MF_hbobright-MF_pjumptab
		dc.w	MF_hbointer-MF_pjumptab
		dc.w	MF_hl1-MF_pjumptab
		dc.w	MF_hl2-MF_pjumptab
		dc.w	MF_hl3-MF_pjumptab
		dc.w	MF_hl4-MF_pjumptab

MF_handlewide:
		ext.l	d5
		lea	MF_wide(pc),a0
		move.l	d5,(a0)
		lea	mf_framewide(pc),a0
		move.l	d5,(a0)

		lea	MF_numtag(pc),a3
		move.l	d5,4(a3)
		move.l	#GD_winter,d0

		bra.b	mf_slidergo

MF_hbobright:
		ext.l	d5
		lea	MF_bobright(pc),a0
		move.l	d5,(a0)
		lea	mf_bident(pc),a0
		move.l	d5,(a0)

		lea	MF_numtag(pc),a3
		move.l	d5,4(a3)
		move.l	#GD_bointer,d0
		bra.b	MF_Slidergo

MF_hrbright:
		ext.l	d5
		lea	MF_rbright(pc),a0
		move.l	d5,(a0)
		lea	mf_rident(pc),a0
		move.l	d5,(a0)

		lea	MF_numtag(pc),a3
		move.l	d5,4(a3)
		move.l	#GD_rinter,d0
		bra.b	MF_Slidergo

MF_hlbright:
		ext.l	d5
		lea	MF_lbright(pc),a0
		move.l	d5,(a0)
		lea	mf_lident(pc),a0
		move.l	d5,(a0)

		lea	MF_numtag(pc),a3
		move.l	d5,4(a3)
		move.l	#GD_linter,d0
		bra.b	MF_Slidergo

MF_handlebright:
		ext.l	d5
		lea	MF_bright(pc),a0
		move.l	d5,(a0)
		lea	mf_tident(pc),a0
		move.l	d5,(a0)

		lea	MF_numtag(pc),a3
		move.l	d5,4(a3)
		move.l	#GD_binter,d0

MF_slidergo:
		lea	MF_gadarray(pc),a0
		move.l	(a0,d0.l*4),a0
		move.l	MF_window(pc),a1
		sub.l	a2,a2
		move.l	APG_Gadtoolsbase(a5),a6
		jsr	_LVOGT_SetGadgetAttrsA(a6)

MF_sliderwait:
		bsr.w	mf_drawbutton

		move.l	MF_window(pc),a0
		move.l	APG_Wait(a5),a6
		jsr	(a6)				;Handle input

		cmp.l	#GADGETUP,d4
		beq.w	MF_wait			;Slidergadet wurde losgelassen

		cmp.l	#IDCMP_MOUSEMOVE,d4
		beq.b	MF_slider
		bra.b	MF_sliderwait

MF_slider:
		move.w	38(a4),d0
		cmp.w	#GD_wide,d0
		beq.w	MF_handlewide
		cmp.w	#GD_bright,d0
		beq.b	MF_handlebright
		cmp.w	#GD_lbright,d0
		beq.w	MF_hlbright
		cmp.w	#GD_rbright,d0
		beq.w	MF_hrbright
		cmp.w	#GD_bobright,d0
		beq.w	MF_hbobright
		bra.w	MF_wait
MF_hwinter:
		move.l	#GD_winter,d0
		bsr.w	mf_holewert

		move.l	MF_number(pc),d5
		tst.l	d5
		bne.b	.n
		moveq	#1,d5
		bra.b	.nn
.n		cmp.l	#10,d5
		bls.b	.w
		move.l	#10,d5			;eingetragener wert war zu hoch
.nn
		move.l	#GD_winter,d0

		bsr.w	mf_setwert
.w
		lea	mf_wide(pc),a0
		move.l	d5,(a0)
		lea	mf_framewide(pc),a0
		move.l	d5,(a0)

		move.l	#GD_wide,d0
		bsr.w	mf_setslider
		bra.w	MF_wait

MF_hbinter:
		move.l	#GD_binter,d0
		bsr.w	mf_holewert

		move.l	MF_number(pc),d5
		cmp.l	#255,d5
		bls.b	.w1
		move.l	#255,d5			;eingetragener wert war zu hoch

		move.l	#GD_binter,d0
		bsr.w	mf_setwert
.w1
		lea	mf_bright(pc),a0
		move.l	d5,(a0)
		lea	mf_tident(pc),a0
		move.l	d5,(a0)

		move.l	#GD_bright,d0
		bsr.w	mf_setslider
		bra.w	MF_wait

MF_hlinter:
		move.l	#GD_linter,d0
		bsr.w	mf_holewert

		move.l	MF_number(pc),d5
		cmp.l	#255,d5
		bls.b	.w1
		move.l	#255,d5			;eingetragener wert war zu hoch

		move.l	#GD_linter,d0
		bsr.w	mf_setwert
.w1
		lea	mf_lbright(pc),a0
		move.l	d5,(a0)
		lea	mf_lident(pc),a0
		move.l	d5,(a0)

		move.l	#GD_lbright,d0
		bsr.w	mf_setslider
		bra.w	MF_wait

MF_hrinter:
		move.l	#GD_rinter,d0
		bsr.w	mf_holewert

		move.l	MF_number(pc),d5
		cmp.l	#255,d5
		bls.b	.w1
		move.l	#255,d5			;eingetragener wert war zu hoch

		move.l	#GD_rinter,d0
		bsr.w	mf_setwert
.w1
		lea	mf_rbright(pc),a0
		move.l	d5,(a0)
		lea	mf_rident(pc),a0
		move.l	d5,(a0)

		move.l	#GD_rbright,d0
		bsr.w	mf_setslider
		bra.w	MF_wait

MF_hbointer:
		move.l	#GD_bointer,d0
		bsr.w	mf_holewert

		move.l	MF_number(pc),d5
		cmp.l	#255,d5
		bls.b	.w1
		move.l	#255,d5			;eingetragener wert war zu hoch

		move.l	#GD_bointer,d0
		bsr.w	mf_setwert
.w1
		lea	mf_bobright(pc),a0
		move.l	d5,(a0)
		lea	mf_bident(pc),a0
		move.l	d5,(a0)

		move.l	#GD_bobright,d0
		bsr.w	mf_setslider
		bra.w	MF_wait


mf_cbutton:
		clr.l	(a1)
		clr.l	(a2)
		lea	MF_gadarray(pc),a0
		move.l	(a0,d0),a0
		move.w	gg_Flags(a0),d0
		and.w	#SELECTED,d0
		beq.b	.w		;nicht selected
		move.l	#1,(a1)
		move.l	#1,(a2)
.w:
		bsr.w	mf_drawbutton
		bra.w	mf_wait



MF_hgrad:
		lea	mf_gradien(pc),a1
		lea	MF_grad(pc),a2
		move.l	#GD_grad*4,d0
		bra.b	mf_cbutton

MF_hl1:
		lea	mf_pl1(pc),a1
		lea	MF_l1(pc),a2
		move.l	#GD_light1*4,d0
		bra.b	mf_cbutton
MF_hl2:
		lea	mf_pl2(pc),a1
		lea	MF_l2(pc),a2
		move.l	#GD_light2*4,d0
		bra.b	mf_cbutton
MF_hl3:
		lea	mf_pl3(pc),a1
		lea	MF_l3(pc),a2
		move.l	#GD_light3*4,d0
		bra.b	mf_cbutton
MF_hl4:
		lea	mf_pl4(pc),a1
		lea	MF_l4(pc),a2
		move.l	#GD_light4*4,d0
		bra.b	mf_cbutton



MF_cancel:
		lea	mf_prefsback(pc),a0
		lea	mf_framewide(pc),a1
		moveq	#10-1,d7
.np		move.l	(a0)+,(a1)+
		dbra	d7,.np

MF_ok:

;speicher freigeben
		move.l	4.w,a6
		move.l	mf_alpha(a5),d0
		beq.b	.nm
		move.l	d0,a1
		jsr	_LVOFreeVec(a6)
		clr.l	mf_alpha(a5)
.nm:
		move.l	mf_testimage(a5),d0
		beq.b	.ni
		move.l	d0,a1
		jsr	_LVOFreeVec(a6)
		clr.l	mf_testimage(a5)
.ni:
		move.l	APG_guigfxbase(a5),a6
		move.l	mf_picture(a5),d0
		beq.b	.nixpicture
		move.l	d0,a0
		jsr	_LVODeletePicture(a6)
		clr.l	mf_picture(a5)
.nixpicture:
		move.l	mf_psm(pc),d0
		beq.b	.np
		move.l	d0,a0
		jsr	_LVODeletePenShareMap(a6)
		clr.l	mf_psm
		
.np:		move.l	mf_drawhandle(a5),d0
		beq.b	.nd
		move.l	d0,a0
		jsr	_LVOReleaseDrawHandle(a6)
		clr.l	mf_drawhandle(a5)
.nd:
		move.l	MF_window(pc),a0
		move.l	APG_Intuitionbase(a5),a6
		jmp	_LVOCloseWindow(a6)

;hole wert aus integer gadget
mf_holewert:
		lea	MF_numtag1(pc),a3
		lea	MF_gadarray(pc),a0
		move.l	(a0,d0.l*4),a0
		move.l	MF_window(pc),a1
		sub.l	a2,a2
		move.l	APG_Gadtoolsbase(a5),a6
		jmp	_LVOGT_GetGadgetAttrsA(a6)

;setzt wert in integer gadget
mf_setwert:
		lea	MF_numtag(pc),a3
		move.l	d5,4(a3)
		lea	MF_gadarray(pc),a0
		move.l	(a0,d0.l*4),a0
		move.l	MF_window(pc),a1
		sub.l	a2,a2
		move.l	APG_Gadtoolsbase(a5),a6
		jmp	_LVOGT_SetGadgetAttrsA(a6)

mf_setslider:
		bsr.w	mf_drawbutton

		lea	MF_leveltag(pc),a3
		move.l	d5,4(a3)
		lea	MF_gadarray(pc),a0
		move.l	(a0,d0.l*4),a0
		move.l	MF_window(pc),a1
		sub.l	a2,a2
		move.l	APG_Gadtoolsbase(a5),a6
		jmp	_LVOGT_SetGadgetAttrsA(a6)

;--------------------------------------------------------------------------------
	even

WindowTags:
winL:	dc.l	WA_Left,212
winT:	dc.l	WA_Top,78
winW:	dc.l	WA_Width,220
winH:	dc.l	WA_Height,85
		dc.l	WA_IDCMP,SLIDERIDCMP!IDCMP_CLOSEWINDOW!GADGETUP!GADGETDOWN!VANILLAKEY!BUTTONIDCMP!IDCMP_REFRESHWINDOW!INTEGERIDCMP!IDCMP_MOUSEBUTTONS
		dc.l	WA_Flags,WFLG_CLOSEGADGET!WFLG_DEPTHGADGET!WFLG_ACTIVATE!WFLG_DRAGBAR!WFLG_SMART_REFRESH!WFLG_RMBTRAP
winWG:	dc.l	WA_Gadgets,0
		dc.l	WA_Title,winWTitle
winSC:	dc.l	WA_CustomScreen,0
		dc.l	TAG_DONE

WinWTitle:	dc.b	'Make Button Options',0

	even

MF_numtag:	dc.l	GTIN_Number,0,TAG_DONE

MF_numtag1:
		dc.l	GTIN_Number,MF_number
		dc.l	TAG_DONE

MF_number:	dc.l	0

MF_leveltag:
		dc.l	GTSL_Level,0
		dc.l	TAG_DONE


GD_Ok		=	0
GD_Cancel	=	1
GD_wide		=	2
GD_bright	=	3
GD_winter	=	4
GD_binter	=	5
GD_grad		=	6

GD_lbright	=	7
GD_linter	=	8
GD_rbright	=	9
GD_rinter	=	10
GD_bobright	=	11
GD_bointer	=	12

GD_Light1	=	13
GD_Light2	=	14
GD_Light3	=	15
GD_Light4	=	16


Gtypes:
		dc.w	BUTTON_KIND
		dc.w	BUTTON_KIND
		dc.w	SLIDER_KIND
		dc.w	SLIDER_KIND
		dc.w	INTEGER_KIND
		dc.w	INTEGER_KIND
		dc.w	CHECKBOX_KIND
		dc.w	SLIDER_KIND
		dc.w	INTEGER_KIND
		dc.w	SLIDER_KIND
		dc.w	INTEGER_KIND
		dc.w	SLIDER_KIND
		dc.w	INTEGER_KIND
		dc.w	CHECKBOX_KIND
		dc.w	CHECKBOX_KIND
		dc.w	CHECKBOX_KIND
		dc.w	CHECKBOX_KIND

Gtags:
		dc.l	TAG_DONE
		dc.l	TAG_DONE
		dc.l	GTSL_Level
MF_wide:	dc.l	3
		dc.l	GTSL_Max,20
		dc.l	GTSL_Min,1
		dc.l	GTSL_MaxLevelLen,3
		dc.l	PGA_Freedom,LORIENT_HORIZ
		dc.l	GA_RelVerify,1
		dc.l	GA_Immediate,1
		dc.l	TAG_DONE
		dc.l	GTSL_Level
MF_bright:	dc.l	50
		dc.l	GTSL_Max,255
		dc.l	GTSL_MaxLevelLen,3
		dc.l	PGA_Freedom,LORIENT_HORIZ
		dc.l	GA_RelVerify,1
		dc.l	GA_Immediate,1
		dc.l	TAG_DONE
		dc.l	GTIN_Number
MF_numwide:	dc.l	3
		dc.l	GTIN_MaxChars,3
		dc.l	TAG_DONE
		dc.l	GTIN_Number
MF_numbright:	dc.l	50
		dc.l	GTIN_MaxChars,3
		dc.l	TAG_DONE
		dc.l	GTCB_Checked
MF_grad:	dc.l	1
		dc.l	GTCB_Scaled,1
		dc.l	TAG_DONE

		dc.l	GTSL_Level
MF_lbright:	dc.l	50
		dc.l	GTSL_Max,255
		dc.l	GTSL_MaxLevelLen,3
		dc.l	PGA_Freedom,LORIENT_HORIZ
		dc.l	GA_RelVerify,1
		dc.l	GA_Immediate,1
		dc.l	TAG_DONE
		dc.l	GTIN_Number
MF_lnumbright:	dc.l	50
		dc.l	GTIN_MaxChars,3
		dc.l	TAG_DONE

		dc.l	GTSL_Level
MF_rbright:	dc.l	50
		dc.l	GTSL_Max,255
		dc.l	GTSL_MaxLevelLen,3
		dc.l	PGA_Freedom,LORIENT_HORIZ
		dc.l	GA_RelVerify,1
		dc.l	GA_Immediate,1
		dc.l	TAG_DONE
		dc.l	GTIN_Number
MF_rnumbright:	dc.l	50
		dc.l	GTIN_MaxChars,3
		dc.l	TAG_DONE

		dc.l	GTSL_Level
MF_bobright:	dc.l	50
		dc.l	GTSL_Max,255
		dc.l	GTSL_MaxLevelLen,3
		dc.l	PGA_Freedom,LORIENT_HORIZ
		dc.l	GA_RelVerify,1
		dc.l	GA_Immediate,1
		dc.l	TAG_DONE
		dc.l	GTIN_Number
MF_bonumbright:	dc.l	50
		dc.l	GTIN_MaxChars,3
		dc.l	TAG_DONE

		dc.l	GTCB_Checked
MF_l1:		dc.l	1
		dc.l	GTCB_Scaled,1
		dc.l	TAG_DONE
		dc.l	GTCB_Checked
MF_l2:		dc.l	1
		dc.l	GTCB_Scaled,1
		dc.l	TAG_DONE
		dc.l	GTCB_Checked
MF_l3:		dc.l	0
		dc.l	GTCB_Scaled,1
		dc.l	TAG_DONE
		dc.l	GTCB_Checked
MF_l4:		dc.l	0
		dc.l	GTCB_Scaled,1
		dc.l	TAG_DONE
NTypes:
		DC.W    50,160,100,14
		DC.L    oktext,0
		DC.W    GD_Ok
		DC.L    PLACETEXT_IN,0,0
		DC.W    250,160,100,14
		DC.L    canceltext,0
		DC.W    GD_Cancel
		DC.L    PLACETEXT_IN,0,0
		dc.w	10+40+150,67-50,150,12
		dc.l	wideText,0
		dc.w	GD_wide
		dc.l	PLACETEXT_ABOVE!NG_HIGHLABEL,0,0
 		dc.w	10+40+150,95-50,150,12
		dc.l	brightText,0
		dc.w	GD_bright
		dc.l	PLACETEXT_ABOVE!NG_HIGHLABEL,0,0
		dc.w	162+40+150,17,38,12
		dc.l	0,0
		dc.w	GD_winter
		dc.l	0,0,0
		dc.w	162+40+150,45,38,12
		dc.l	0,0
		dc.w	GD_binter
		dc.l	0,0,0
		dc.w	10,101,26,11
		dc.l	gradtext,0
		dc.w	GD_grad
		DC.L    PLACETEXT_RiGHT,0,0
 		dc.w	10+40+150,123-50,150,12
		dc.l	lbrightText,0
		dc.w	GD_lbright
		dc.l	PLACETEXT_ABOVE!NG_HIGHLABEL,0,0
		dc.w	162+40+150,123-50,38,12
		dc.l	0,0
		dc.w	GD_linter
		dc.l	0,0,0
 		dc.w	10+40+150,151-50,150,12
		dc.l	rbrightText,0
		dc.w	GD_rbright
		dc.l	PLACETEXT_ABOVE!NG_HIGHLABEL,0,0
		dc.w	162+40+150,151-50,38,12
		dc.l	0,0
		dc.w	GD_rinter
		dc.l	0,0,0
 		dc.w	10+40+150,179-50,150,12
		dc.l	bobrightText,0
		dc.w	GD_bobright
		dc.l	PLACETEXT_ABOVE!NG_HIGHLABEL,0,0
		dc.w	162+40+150,179-50,38,12
		dc.l	0,0
		dc.w	GD_bointer
		dc.l	0,0,0
		dc.w	10+150,95-50,26,11
		dc.l	0,0
		dc.w	GD_Light1
		DC.L    0,0,0
		dc.w	10+150,123-50,26,11
		dc.l	0,0
		dc.w	GD_Light2
		DC.L    0,0,0
		dc.w	10+150,151-50,26,11
		dc.l	0,0
		dc.w	GD_Light3
		DC.L    0,0,0
		dc.w	10+150,179-50,26,11
		dc.l	0,0
		dc.w	GD_Light4
		DC.L    0,0,0


oktext:		dc.b	'Ok',0
canceltext:	dc.b	'Cancel',0
wideText:	dc.b	'Frame Width',0
brighttext:	dc.b	'Top intensity',0
lbrighttext:	dc.b	'Left intensity',0
rbrighttext:	dc.b	'Right intensity',0
bobrighttext:	dc.b	'Bottom intensity',0
invertext:	dc.b	'Invers',0
gradtext:	dc.b	'Gradient',0

sizetomsg:	dc.b	'Selected brush to small!',0
 
		section	laber,bss

mf_greypalette:	ds.l	64
		
