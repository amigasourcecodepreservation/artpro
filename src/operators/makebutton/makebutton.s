����  ��&b�&b�&b�&b�&b�&b�&b�&b;=6=;===========================================================================
;
;		Macht einen 3D-Rahmen um ein Bild
;		(c) 1997 ,Frank (Copper) Pagels  /DFT
;
;		05.11.97

		incdir 	include:

		include	exec/exec_lib.i
		include	exec/lists.i
		include	exec/memory.i
		include intuition/intuition_lib.i
		include	libraries/wb_lib.i
		include	libraries/reqtools_lib.i
		include	graphics/graphics_lib.i
		Include utility/tagitem.i
		include	dos/dos.i		
		include	dos/dos_lib.i		

		include misc/artpro.i
		include render/render_lib.i
		include render/render.i
		include	guigfx/guigfx.i
		include	guigfx/guigfx_lib.i
		

		OPERATORHEADER MF_NODE

		dc.b	'$VER: 3D Button Operator Modul 0.3 (09.11.97)',0
	even
;---------------------------------------------------------------------------
MF_NODE:
		dc.l	0,0
		dc.b	0,0
		dc.l	MF_name
		dc.l	MF_Party
		dc.b	'MFOP'
		dc.b	'EXTR'	;for extern loader
		dc.l	0
		dc.l	MF_Tags
MF_name:
		dc.b	'Make Button',0
	even
;---------------------------------------------------------------------------

MF_Tags:
		dc.l	APT_Creator,MF_Creator
		dc.l	APT_Version,5
		dc.l	TAG_DONE

MF_Creator:
		dc.b	'(c) 1997 Frank Pagels /DEFECT Softworks',0
	even

MF_alphasize	=	APG_Free1
MF_alpha	=	APG_Free2
MF_picture	=	APG_Free3
mf_width	=	APG_Free4
mf_height	=	APG_Free4+2

;---------------------------------------------------------------------------
MF_Party:

		tst.w	APG_Cutflag(a5)
		bne.b	.brushok
		move.w	#0,APG_xbrush1(a5)	;Es soll das ganze 
		move.w	#0,APG_ybrush1(a5)	;Pic abgesaved werden
		move.w	APG_ImageWidth(a5),APG_xbrush2(a5)
		move.w	APG_ImageHeight(a5),APG_ybrush2(a5)	
.brushok:
		moveq	#0,d0
		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
		move.l	d0,d1
		add.l	#15,d1
		lsr.l	#4,d1		;breite in worte
		move.w	d1,APG_Brushword(a5)

;create alphachannel

;erstmal speicher holen

		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
		move.w	APG_ybrush2(a5),d1
		sub.w	APG_ybrush1(a5),d1
		mulu	d1,d0
		move.l	d0,MF_alphasize(a5)	;gr��e merken
		move.l	#MEMF_ANY!MEMF_CLEAR,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)
		tst.l	d0
		bne	.memok
		move.l	#APOE_NOMEM,d3
		rts

.memok:
		move.l	d0,MF_alpha(a5)

;jetzt den channel machen
;erstmal fest f�r eine breite von 4 pixel
		

;obere wagerechte Line

		moveq	#0,d7
		move.w	APG_xbrush2(a5),d7
		sub.w	APG_xbrush1(a5),d7
		move.w	d7,mf_width(a5)
		move.l	d7,d6
		moveq	#4-1,d5
.nl:		move.l	d6,d7
		sub.l	d5,d7
		subq.w	#2,d7
		move.l	MF_Alpha(a5),a0
		move.l	d5,d4
		mulu	d6,d4
		add.l	d4,a0		;auf die richtige zeile bringen
.na		move.b	#50,(a0)+
		dbra	d7,.na
		subq.w	#1,d5
		bpl	.nl

;linke sektrechte seite

		moveq	#0,d7
		move.w	APG_xbrush2(a5),d7
		sub.w	APG_xbrush1(a5),d7
		move.w	APG_ybrush2(a5),d6
		sub.w	APG_ybrush1(a5),d6
		move.w	d6,mf_height(a5)
		moveq	#4-1,d5
.nl1:		move.l	d6,d4
		sub.l	d5,d4
		subq.w	#1,d4
		move.l	MF_Alpha(a5),a0
		add.l	d5,a0		;auf die richtige spalte bringen
.na1		move.b	#50,(a0)
		add.l	d7,a0
		dbra	d4,.na1
		subq.w	#1,d5
		bpl	.nl1

;Image objekt erzeugen

		move.l	APG_rndr_rgb(a5),a0	;logopuffer
		moveq	#0,d0
		move.w	apg_imagewidth(a5),d0
		add.l	#15,d0
		lsr.l	#4,d0
		lsl.l	#4,d0
		moveq	#0,d1
		move.w	apg_imageheight(a5),d1
		move.l	#TAG_DONE,-(a7)
		move.l	#0,-(a7)
		move.l	#GGFX_Independent,-(a7)
		move.l	#PIXFMT_0RGB_32,-(a7)
		move.l	#GGFX_PixelFormat,-(a7)
		move.l	a7,a1
		move.l	APG_Guigfxbase(a5),a6
		jsr	_LVOMakePictureA(a6)
		lea	20(a7),a7
		move.l	d0,MF_picture(a5)		;logopic
		bne	.pok
		lea	mf_cantmsg(pc),a4
		move.l	APG_Status(a5),a6
		jsr	(a6)
		move.l	MF_Alpha(a5),a1
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)
		moveq	#APOE_ERROR,d3
		rts
.pok:

		move.l	mf_picture(a5),a0
		move.l	#PICMTHD_SETALPHA,d0
		move.l	#TAG_DONE,-(a7)
		moveq	#0,d1
		move.w	mf_height(a5),d1
		move.l	d1,-(a7)
		move.l	#GGFX_DestHeight,-(a7)
		move.w	mf_width(a5),d1
		move.l	d1,-(a7)
		move.l	#GGFX_DestWidth,-(a7)
		move.w	APG_ybrush1(a5),d1
		move.l	d1,-(a7)
		move.l	#GGFX_DestY,-(a7)
		move.w	APG_xbrush1(a5),d1
		move.l	d1,-(a7)
		move.l	#GGFX_DestX,-(a7)
		move.w	mf_height(a5),d1
		move.l	d1,-(a7)
		move.w	mf_width(a5),d1
		move.l	d1,-(a7)
		move.l	MF_Alpha(a5),-(a7)
		move.l	a7,a1
		jsr	_LVODoPictureMethodA(a6)		
		lea	48(a7),a7

;Jetzt TintAlpha machen

		move.l	mf_picture(a5),a0
		move.l	#PICMTHD_TINTALPHA,d0
		move.l	#TAG_DONE,-(a7)
		moveq	#0,d1
		move.w	mf_height(a5),d1
		move.l	d1,-(a7)
		move.l	#GGFX_DestHeight,-(a7)
		move.w	mf_width(a5),d1
		move.l	d1,-(a7)
		move.l	#GGFX_DestWidth,-(a7)
		move.w	APG_ybrush1(a5),d1
		move.l	d1,-(a7)
		move.l	#GGFX_DestY,-(a7)
		move.w	APG_xbrush1(a5),d1
		move.l	d1,-(a7)
		move.l	#GGFX_DestX,-(a7)
		move.l	#$00ffffff,-(a7)
		move.l	a7,a1
		jsr	_LVODoPictureMethodA(a6)		
		lea	40(a7),a7

;Alphachannel drehen
		jsr	APR_Dummy(a5)

		move.l	mf_alpha(a5),a0
		move.l	a0,a1
		move.l	mf_alphasize(a5),d7
		add.l	d7,a1
		subq.l	#1,a1
		lsr.w	#1,d7
		beq	.w
		subq.w	#1,d7
.w		move.b	(a0),d0
		move.b	(a1),d1
		move.b	d0,(a1)
		subq.l	#1,a1
		move.b	d1,(a0)+
		dbra	d7,.w

		move.l	mf_picture(a5),a0
		move.l	#PICMTHD_SETALPHA,d0
		move.l	#TAG_DONE,-(a7)
		moveq	#0,d1
		move.w	mf_height(a5),d1
		move.l	d1,-(a7)
		move.l	#GGFX_DestHeight,-(a7)
		move.w	mf_width(a5),d1
		move.l	d1,-(a7)
		move.l	#GGFX_DestWidth,-(a7)
		move.w	APG_ybrush1(a5),d1
		move.l	d1,-(a7)
		move.l	#GGFX_DestY,-(a7)
		move.w	APG_xbrush1(a5),d1
		move.l	d1,-(a7)
		move.l	#GGFX_DestX,-(a7)
		move.w	mf_height(a5),d1
		move.l	d1,-(a7)
		move.w	mf_width(a5),d1
		move.l	d1,-(a7)
		move.l	MF_Alpha(a5),-(a7)
		move.l	a7,a1
		jsr	_LVODoPictureMethodA(a6)		
		lea	48(a7),a7

;und wieder tintalpha

		move.l	mf_picture(a5),a0
		move.l	#PICMTHD_TINTALPHA,d0
		move.l	#TAG_DONE,-(a7)
		moveq	#0,d1
		move.w	mf_height(a5),d1
		move.l	d1,-(a7)
		move.l	#GGFX_DestHeight,-(a7)
		move.w	mf_width(a5),d1
		move.l	d1,-(a7)
		move.l	#GGFX_DestWidth,-(a7)
		move.w	APG_ybrush1(a5),d1
		move.l	d1,-(a7)
		move.l	#GGFX_DestY,-(a7)
		move.w	APG_xbrush1(a5),d1
		move.l	d1,-(a7)
		move.l	#GGFX_DestX,-(a7)
		clr.l	-(a7)
		move.l	a7,a1
		jsr	_LVODoPictureMethodA(a6)		
		lea	40(a7),a7


;free alles
		move.l	MF_picture(a5),a0
		move.l	APG_Guigfxbase(a5),a6
		jsr	_LVODeletePicture(a6)
mf_ende.
		move.l	MF_Alpha(a5),a1
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)

		rts

mf_cantmsg:	dc.b	"Can't make operation, no memory?",0
