����   ?   ?   ?   ?   ?   ?   ?   ?   ?   ?My_Width	=	4
My_Height	=	8
My_Size		=	12

bla
	lea	image+15,a0
	LEA	Image_info(PC),A4
	MOVE.L	a0,(A4)			;Mempointer
	move.l	#376,d4
	MOVE.L	d4,My_Width(A4)
	move.l	#512,d5
	MOVE.L	d5,My_Height(A4)
	move.l	#512*376,d6
	MOVE.L	d6,My_Size(A4)

	MOVE.L	(A4),A0
	MOVEQ	#0,D0
	MOVEQ	#0,D3
	MOVE.L	D6,D2
.LP3	MOVE.B	(A0)+,D0		;Bilde Durchschnitt �ber das Bild
	ADD.L	D0,D3
	SUBQ.L	#1,D2
	BNE.B	.LP3
	DIVU.L	D6,D3			; D3 - AVG
	DIVU.L	#8,D4
	DIVU.L	#8,D5
	LEA	PPM_Head(PC),A3
	MOVEQ	#7,D1
.LP5	MOVEQ	#7,D0
	MOVEQ	#0,D6
.LP4	BSR.B	GetBit
	ADD.W	D6,D6
	CMP.W	D2,D3
	BHI.B	.skip
	ADDQ.L	#1,D6
.skip	DBF	D0,.LP4
	MOVE.B	D6,(A3)+
	DBF	D1,.LP5
	MOVE.L	(A4),A1

	LEA	Str_End(PC),A1
	MOVEQ	#1,D2
.LP7	MOVE.L	-(A3),D1
	MOVEQ	#7,D3
.lp6	MOVE.B	D1,D0
	AND.B	#$F,D0
	CMP.B	#$A,D0
	BMI.B	.No_L
	ADDQ.B	#7,D0
;	ADD.B	#$20,D0		enable this to get lower case letters in the id
.No_L	ADD.B	#$30,D0
	MOVE.B	D0,-(A1)
	LSR.L	#4,D1
	DBF	D3,.lp6

	DBF	D2,.LP7



.End1	MOVE.L	D7,D1
	MOVE.L	A5,A6
Exit	MOVE.L	A6,A1
	MOVEM.L	(A7)+,D1-D7/A0-A6
	RTS



Short_Read

GetBit		MOVEM.L	D0-D1/D3-D7/A0,-(A7)
		MOVE.L	D4,D6
		MOVE.L	D5,D7
		MULU.L	D0,D6			;X-Start
		MULU.L	D1,D7			;Y-Start
		MOVE.L	(A4),A0
		MULU.L	My_Width(A4),D7		;Real Y
		ADD.L	D6,A0			;Add X
		ADD.L	D7,A0			;Add Y
		MOVE.L	My_Width(A4),D7		;Row-(Row/8)
		SUB.L	D4,D7
		MOVE.L	D5,-(A7)
		MOVE.L	D4,-(A7)
		MOVEQ	#0,D0
		MOVEQ	#0,D2
.LP		MOVE.B	(A0)+,D0
		ADD.L	D0,D2
		SUBQ.L	#1,D4
		BNE.B	.LP
		ADD.L	D7,A0
		MOVE.L	(A7),D4
		SUBQ.L	#1,D5
		BNE.B	.LP
		MOVE.L	(A7)+,D4
		MOVE.L	(A7)+,D5
		MULU.L	D4,D5			;Blocksize
		DIVU.L	D5,D2			;Avg
		MOVEM.L	(A7)+,D0-D1/D3-D7/A0
		RTS


	CNOP	0,2
PPM_Head:	DC.L	0,0
Image_info	DC.L	0	;Mempointer
		DC.L	0	;Width
		DC.L	0	;H
		DC.L	0	;Size
		DC.L	0
Text_Buffer	BLK.B	16
		DC.B	10
Str_End

image:
		incbin	sources:converter/operators/finddouble/1normal.pbm
