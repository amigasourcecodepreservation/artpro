����                                        Debug		=	0

	incdir	Include:
	include	exec/exec_lib.i
	include	exec/memory.i
	include	dos/dos.i
	include	dos/dos_lib.i
	include	dos/dosextens.i


My_Width	=	4
My_Height	=	8
My_Size		=	12

	IFNE	Debug
	LEA	TEST(PC),A6
	BRA	Loop
	ENDC


Start:
	MOVEM.L	D1-D7/A0-A6,-(A7)
	LEA	DosName(PC),A1
	MOVEQ	#0,D0
	CALLEXEC	OpenLibrary
	MOVE.L	D0,A6				;A6 - DosLib

	MOVE.L	#DOS_RDARGS,D1
	LEA	Image_info(PC),A0
	MOVE.L	A0,D2
	CALLLIB	AllocDosObject
	MOVE.L	D0,D6				;D6-DosObject
	LEA	Template(PC),A0
	MOVE.L	A0,D1
	LEA	Image_info(PC),A0
	MOVE.L	A0,D2
	MOVE.L	D6,D3
	CALLLIB	ReadArgs
	MOVE.L	Image_info(PC),D7
	MOVE.L	D0,D5				;RDArgs-Struktur
	MOVE.L	D5,D1
	CALLLIB	FreeArgs
	MOVE.L	#DOS_RDARGS,D1
	MOVE.L	D6,D2
	CALLLIB	FreeDosObject
	MOVE.L	D7,D1
	MOVE.L	#MODE_OLDFILE,D2
	CALLLIB	Open
	MOVE.L	D0,D7				;D7 - FileHandle
	BEQ.W	Exit
Loop	LEA	PPM_Head(PC),A5			
	MOVEQ	#3,D3
	BSR.W	Short_Read
	TST.L	D0
	BEQ.W	.End1
	CMP.W	#"P5",(A5)
	BNE.W	.End1

	MOVEQ	#0,D4				;destination
.LP1	MOVEQ	#1,D3
	BSR.W	Short_Read
	MOVE.B	(A5),D0
	SUB.B	#"0",D0
	BLO.B	.ok1
	CMP.B	#9,D0
	BGT.W	.End1
	ADD.L	D4,D4
	MOVE.L	D4,D5
	ADD.L	D4,D4
	ADD.L	D4,D4
	ADD.L	D5,D4
	ADD.L	D0,D4
	BRA.B	.LP1
;----------------------
.ok1
	MOVEQ	#0,D5
.LP2	MOVEQ	#1,D3
	BSR.W	Short_Read
	MOVE.B	(A5),D0
	SUB.B	#"0",D0
	BLO.B	.ok2
	CMP.B	#9,D0
	BGT.W	.End1
	ADD.L	D5,D5
	MOVE.L	D5,D6
	ADD.L	D5,D5
	ADD.L	D5,D5
	ADD.L	D6,D5
	ADD.L	D0,D5
	BRA.B	.LP2
;----------------------
.ok2	MOVEQ	#4,D3
	BSR.W	Short_Read
	CMP.L	#$3235350A,(A5)
	BNE.W	.End1
	MOVE.L	D5,D6			; D5 - Y
	MULU.W	D4,D6			; D4 - X
	MOVE.L	D6,D0			; D6 - Size
	MOVE.L	A6,A5			;save doslib
	MOVE.L	#MEMF_ANY,D1
	CALLEXEC	AllocVec
	MOVE.L	D6,D3
	TST.L	D0
	BEQ.W	.End1
	LEA	Image_info(PC),A4
	MOVE.L	D0,(A4)			;Mempointer
	MOVE.L	D4,My_Width(A4)
	MOVE.L	D5,My_Height(A4)
	MOVE.L	D6,My_Size(A4)
	MOVE.L	D7,D1
	MOVE.L	D0,D2
	MOVE.L	A5,A6
	CALLLIB	Read
	MOVE.L	(A4),A0
	MOVEQ	#0,D0
	MOVEQ	#0,D3
	MOVE.L	D6,D2
.LP3	MOVE.B	(A0)+,D0		;Bilde Durchschnitt �ber das Bild
	ADD.L	D0,D3
	SUBQ.L	#1,D2
	BNE.B	.LP3
	DIVU.L	D6,D3			; D3 - AVG
	DIVU.L	#8,D4
	DIVU.L	#8,D5
	LEA	PPM_Head(PC),A3
	MOVEQ	#7,D1
.LP5	MOVEQ	#7,D0
	MOVEQ	#0,D6
.LP4	BSR.B	GetBit
	ADD.W	D6,D6
	CMP.W	D2,D3
	BHI.B	.skip
	ADDQ.L	#1,D6
.skip	DBF	D0,.LP4
	MOVE.B	D6,(A3)+
	DBF	D1,.LP5
	MOVE.L	(A4),A1
	CALLEXEC	FreeVec

	LEA	Str_End(PC),A1
	MOVEQ	#1,D2
.LP7	MOVE.L	-(A3),D1
	MOVEQ	#7,D3
.lp6	MOVE.B	D1,D0
	AND.B	#$F,D0
	CMP.B	#$A,D0
	BMI.B	.No_L
	ADDQ.B	#7,D0
;	ADD.B	#$20,D0		enable this to get lower case letters in the id
.No_L	ADD.B	#$30,D0
	MOVE.B	D0,-(A1)
	LSR.L	#4,D1
	DBF	D3,.lp6

	DBF	D2,.LP7
	MOVE.L	A5,A6
	CALLLIB	Output
	MOVE.L	D0,D1
	MOVE.L	A1,D2
	MOVEQ	#17,D3
	CALLLIB	Write


.End1	MOVE.L	D7,D1
	MOVE.L	A5,A6
	CALLLIB	Close
Exit	MOVE.L	A6,A1
	CALLEXEC	CloseLibrary
	MOVEM.L	(A7)+,D1-D7/A0-A6
	RTS



Short_Read	IFNE	Debug
		MOVE.L	A5,-(A7)
		MOVE.L	D3,D0
.LP		MOVE.B	(A6)+,(A5)+
		SUBQ	#1,D3
		BNE	.LP
		MOVE.L	(A7)+,A5
		RTS

		ENDC
		MOVE.L	A5,D2
		MOVE.L	D7,D1
		JMP	_LVORead(A6)

GetBit		MOVEM.L	D0-D1/D3-D7/A0,-(A7)
		MOVE.L	D4,D6
		MOVE.L	D5,D7
		MULU.L	D0,D6			;X-Start
		MULU.L	D1,D7			;Y-Start
		MOVE.L	(A4),A0
		MULU.L	My_Width(A4),D7		;Real Y
		ADD.L	D6,A0			;Add X
		ADD.L	D7,A0			;Add Y
		MOVE.L	My_Width(A4),D7		;Row-(Row/8)
		SUB.L	D4,D7
		MOVE.L	D5,-(A7)
		MOVE.L	D4,-(A7)
		MOVEQ	#0,D0
		MOVEQ	#0,D2
.LP		MOVE.B	(A0)+,D0
		ADD.L	D0,D2
		SUBQ.L	#1,D4
		BNE.B	.LP
		ADD.L	D7,A0
		MOVE.L	(A7),D4
		SUBQ.L	#1,D5
		BNE.B	.LP
		MOVE.L	(A7)+,D4
		MOVE.L	(A7)+,D5
		MULU.L	D4,D5			;Blocksize
		DIVU.L	D5,D2			;Avg
		MOVEM.L	(A7)+,D0-D1/D3-D7/A0
		RTS


DosName			DOSNAME
Template:	DC.B	"FILE/A",0


	CNOP	0,2
PPM_Head:	DC.L	0,0
Image_info	DC.L	0	;Mempointer
		DC.L	0	;Width
		DC.L	0	;H
		DC.L	0	;Size
		DC.L	0
Text_Buffer	BLK.B	16
		DC.B	10
Str_End

	IFNE	Debug
	TEST:	DC.L	$50350A35,$37382038,$30330A32,$35350A85
	ENDC
