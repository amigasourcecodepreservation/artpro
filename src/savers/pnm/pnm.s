����  
��&b�&b�&b�&b�&b�&b�&b�&b;=@�;===========================================================================
;
;		Test f�r einen PCX saver
;		(c) 1995 ,Frank Pagels (Crazy Copper) /DFT
;
;		22.08.98

		incdir 	include:

		include	exec/exec_lib.i
		include	exec/lists.i
		include	exec/memory.i
		include intuition/intuition_lib.i
		include libraries/gadtools.i
		include	libraries/wb_lib.i
		include	graphics/graphics_lib.i
		Include utility/tagitem.i
		include	dos/dos.i		
		include	dos/dos_lib.i		
		include	libraries/reqtools_lib.i

		include misc/artpro.i
		include	render/render.i
		include	render/render_lib.i
		include	render/renderhooks.i
		
		SAVERHEADER pnm_NODE

		dc.b	'$VER: PNM saver module 1.0 (22.08.98)',0
	even
;---------------------------------------------------------------------------
pnm_NODE:
		dc.l	0,0
		dc.b	0,0
		dc.l	pnm_name
		dc.l	pnm_save
		dc.b	'PNMS'		;saverkennung f�r ArtPRO
		dc.b	'EXTR'		;for extern saver
		dc.l	0
		dc.l	pnm_Tags
pnm_name:
		dc.b	'PNM',0
	even
;---------------------------------------------------------------------------

pnm_Tags:
		dc.l	APT_Creator,pnm_Creator
		dc.l	APT_Version,5
		dc.l	APT_Info,pnm_Info
		dc.l	APT_Prefs,pnm_Prefsrout		;routine for Prefs
		dc.l	APT_Prefsbuffer,pnm_format
		dc.l	APT_Prefsbuffersize,1
		dc.l	APT_PrefsVersion,1
		dc.l	APT_OperatorUse,1
		dc.l	0

pnm_Creator:
		dc.b	'(c) 1995/98 Frank Pagels /DEFECT Softworks',0
	even
pnm_Info:
		dc.b	'PNM saver',10,10
		dc.b	'Saves an image as PNM',0
	even
;----------------------------------------------------------------------------

c24to3Byte	macro
		move.l	\1,\2
		and.l	#$0000ff00,\2
		lsr.l	#8,\2
		move.l	\1,\3
		and.l	#$000000ff,\3
		and.l	#$00ff0000,\1
		swap	\1
		endm

pnm_zeile 	=	APG_Free1
pnm_Puffermem	=	APG_Free2

pnm_window	dc.l	0
pnm_GList	dc.l	0
pnm_gadarray	dcb.l	4,0


pnm_format	ds.b	1

pnm_24Bit	ds.b	1
pnm_EGA		ds.b	1
pnm_HAM		ds.b	1
pnm_M16		ds.b	1	;mehr als 16 Farben

	even
;---------------------------------------------------------------------------
pnm_save:
		tst.l	APG_rndr_rendermem(a5)
		bne	.rgbok

		jsr	APR_MakeRGBData(a5)

.rgbok:

;berechne ben�tigten Speicher

		tst.w	APG_Cutflag(a5)
		bne.b	.brushok
		move.w	#0,APG_xbrush1(a5)	;Es soll das ganze 
		move.w	#0,APG_ybrush1(a5)	;Pic abgesaved werden
		move.w	APG_ImageWidth(a5),APG_xbrush2(a5)
		move.w	APG_ImageHeight(a5),APG_ybrush2(a5)	

.brushok:	moveq	#0,d0
		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
		lsl.l	#2,d0
		add.l	#100,d0
		move.l	#MEMF_PUBLIC+MEMF_CLEAR,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)
		move.l	d0,pnm_Puffermem(a5)
		bne.w	.memok
		moveq	#-2,d3		;no mem !
		rts
.memok:

;F�lle Header
pnm_FillHeader:
		move.l	pnm_Puffermem(a5),a4

		move.b	#'P',(a4)+
		move.b	#'6',d0
		tst.b	pnm_format
		beq	.w
		move.b	#'5',d0
.w		move.b	d0,(a4)+

		move.b	#$a,(a4)+

		moveq	#0,d0
		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
		bsr	pnm_writeascii
		move.b	#$a,(a4)+
		moveq	#0,d0
		move.w	APG_ybrush2(a5),d0
		sub.w	APG_ybrush1(a5),d0
		bsr	pnm_writeascii
		move.b	#$a,(a4)+
		move.l	#255,d0
		bsr	pnm_writeascii
		move.b	#$a,(a4)+
				
		bsr.w	pnm_Savepuffer 
		tst.l	d0
		bmi.w	pnm_Abort	


;-----------------------------------------------------------------
pnm_WriteBitmap:

		jsr	APR_OpenProcess(a5)
		lea	saveppmmsg(pc),a1		
		tst.b	pnm_format
		beq	.w
		lea	savepgmmsg(pc),a1		
.w		moveq	#0,d0
		move.w	APG_ImageHeight(a5),d0
		jsr	APR_InitProcess(a5)

		moveq	#0,d7
		move.w	APG_ybrush1(a5),d7
		move.l	d7,pnm_zeile(a5)
		move.l	APG_ByteWidth(a5),d6
		lsl.l	#3+2,d6
		moveq	#0,d7
		move.w	APG_ybrush2(a5),d7
		sub.w	APG_ybrush1(a5),d7
		subq	#1,d7
.nextline:
		move.l	pnm_zeile(a5),d0
		move.l	APG_rndr_rgb(a5),a0
		mulu	d6,d0
		add.l	d0,a0
		move.w	APG_xbrush1(a5),d3
		lea	(a0,d3*4),a0		
.nextrow:
		move.l	(a0)+,d0

		c24to3Byte d0,d1,d2

		tst.b	pnm_format
		bne	.grey
		move.b	d0,(a4)+
		move.b	d1,(a4)+
		move.b	d2,(a4)+
		bra	.we
.grey:
		add.l	d1,d0
		add.l	d2,d0
		divu.l	#3,d0
		ext.l	d0
		move.b	d0,(a4)+
.we:
		addq.l	#1,d3
		cmp.w	APG_xbrush2(a5),d3
		blo	.nextrow	
		bsr	pnm_savepuffer

		jsr	APR_DoProcess(a5)
		tst.l	d0
		beq	pnm_abort
		
		add.l	#1,pnm_zeile(a5)
		dbra	d7,.nextline

;--------------------------------------------------------------------

		moveq	#0,d3
pnm_end:
		move.l	4.w,a6
		move.l	pnm_Puffermem(a5),d0
		beq	.w
		move.l	d0,a1
		jsr	_LVOFreeVec(a6)
.w		
		rts

pnm_Abort:
		move.l	#APSE_ABORT,d3
		bra	pnm_end
;--------------------------------------------------------------------

		
pnm_Savepuffer:
		movem.l	d3/a0,-(a7)
		move.l	pnm_puffermem(a5),d2
		sub.l	d2,a4
		move.l	a4,d3
		move.l	APG_Filehandle(a5),d1
		move.l	APG_Dosbase(a5),a6
		jsr	_LVOWrite(a6)
		move.l	pnm_Puffermem(a5),a4
		movem.l	(a7)+,d3/a0
		rts
				
;------------------------------------------------------------------------------
pnm_writeascii:
		moveq	#0,d7		;flag f�r die nullen
		lea	pnm_hexdeztab(pc),a0	;Tabellendisgs
		moveq	#3,d3
.Hexdez2:	move.l	(a0)+,d1		;Tabellenzahl
		moveq	#0,d2			;Zahl
.Hexdez3:	addq.l	#1,d2
		sub.l	d1,d0
		bcc.s	.Hexdez3
		add.l	d1,d0
		add.b	#$2f,d2
		cmp.b	#$30,d2
		bne	.cp
		cmp.b	#1,d1
		beq	.cp
		tst.l	d7	;waren schon andere zahlen  ?
		beq	.np	
.cp:		move.b	d2,(a4)+
		moveq	#1,d7
.np:		dbf	d3,.Hexdez2
		rts

pnm_hexdeztab:	dc.l	1000,100,10,1

;------------------------------------------------------------------------------
pnm_Prefsrout:
		moveq	#0,d0
		move.b	pnm_format(pc),d0
		lea	pnm_modetag(pc),a0
		move.l	d0,(a0)

;----------------------------------------------------------------------------

		move.l	#WindowTags,APG_WindowTagList(a5)	;Tag list for the Window
		move.l	#WinWG+4,APG_WGadgets(a5)	;addr. of WA_Gadgets Tag + 4
		move.l	#winSC+4,APG_WScreen(a5)	;addr. of WA_CustomScreen Tag + 4
		move.l	#pnm_window,APG_WinWnd(a5)	;a point for the Window
		move.l	#pnm_Glist,APG_Glist(a5)		;a point for the Glist
		move.l	#NTypes,APG_NGads(a5)		;the NGads list
		move.l	#Gtypes,APG_GTypes(a5)		;the GTypes list
		move.l	#Gtags,APG_Gtags(a5)		;the Gtags list
		move.w	#3,APG_CNT(a5)			;the number of Gadgets
		move.l	#pnm_gadarray,APG_Gadgets(a5)	;a pointer for the Gadgetsarry, size=4*number of Gadgets
		move.l	APG_Scr(a5),APG_ScrBase(a5)	;the Screenpointer , stored in APG_Scr
	
		move.l	APG_CheckMainWinPos(a5),a6
		jsr	(a6)

		add.w	#70,d0
		add.w	#100,d1
		
		move.w	d0,APG_WinLeft(a5)		;the left pos. of the Win
		move.w	d1,APG_WinTop(a5)		;the top pos. of the Win
		move.w	#160,APG_WinWidth(a5)		;the width of the Win
		move.w	#60,APG_WinHeight(a5)		;the height of the Win

		move.l	#WinL,APG_WinL(a5)		;addr. of WA_Left Tag 
		move.l	#WinT,APG_WinT(a5)		;addr. of WA_Top Tag
		move.l	#WinW,APG_WinW(a5)		;addr. of WA_Width Tag
		move.l	#WinH,APG_WinH(a5)		;addr. of WA_Height Tag

		move.l	APG_OpenWindow(a5),a6
		jsr	(a6)

		lea	Text0(pc),a2
		move.w	#Project0_TNUM,APG_TNUM(a5)
		move.l	APG_Writetext(a5),a6
		jsr	(a6)

		move.b	pnm_format(pc),APG_Mark1(a5)	;rette alte Prefs

pnm_wait:	move.l	pnm_window(pc),a0
		move.l	APG_Wait(a5),a6
		jsr	(a6)

		cmp.l	#CLOSEWINDOW,d4
		beq	pnm_cancel
		cmp.l	#GADGETUP,d4
		beq.b	pnm_gads
		cmp.l	#GADGETDOWN,d4
		beq.b	pnm_gads
		bra.b	pnm_wait

pnm_gads:
		moveq	#0,d0
		move.w	38(a4),d0

		add.l	d0,d0
		lea	pnm_jumptab(pc),a0
		move.w	(a0,d0.l),d0
		jmp	(a0,d0.w)

pnm_cancel:
		move.b	APG_Mark1(a5),d0
		ext.w	d0
		ext.l	d0
		move.b	d0,pnm_format

pnm_pok:
		move.l	pnm_window(pc),a0
		move.l	APG_Intuitionbase(a5),a6
		jmp	_LVOCloseWindow(a6)

pnm_jumptab:
		dc.w	handleformat-pnm_jumptab
		dc.w	pnm_pok-pnm_jumptab,pnm_cancel-pnm_jumptab

handleformat:
		move.b	d5,pnm_format
		ext.w	d5
		ext.l	d5
		move.l	d5,pnm_modetag
		bra.b	pnm_wait

;------------------------------------------------------------------------------
WindowTags:
winL:	dc.l	WA_Left,212
winT:	dc.l	WA_Top,78
winW:	dc.l	WA_Width,220
winH:	dc.l	WA_Height,85
		dc.l	WA_IDCMP,MXIDCMP!GADGETUP!VANILLAKEY!BUTTONIDCMP!IDCMP_REFRESHWINDOW!IDCMP_CLOSEWINDOW
		dc.l	WA_Flags,WFLG_CLOSEGADGET!WFLG_DEPTHGADGET!WFLG_ACTIVATE!WFLG_DRAGBAR!WFLG_SMART_REFRESH!WFLG_RMBTRAP
winWG:	dc.l	WA_Gadgets,0
		dc.l	WA_Title,winWTitle
winSC:	dc.l	WA_CustomScreen,0
		dc.l	TAG_DONE

WinWTitle:	dc.b	'PNM Options',0
	even

GD_format	=	0
GD_Ok		=	1
GD_Cancel	=	2

Gtypes:
		dc.w	CYCLE_KIND
		dc.w	BUTTON_KIND
		dc.w	BUTTON_KIND

Gtags:
		dc.l	GTCY_Labels,pnm_modeLabels
		dc.l	GT_Underscore,'_'
		dc.l	GTCY_ACTIVE
pnm_modetag:	dc.l	0
		dc.l	TAG_DONE
		dc.l	GT_Underscore,'_'
		dc.l	TAG_DONE
		dc.l	GT_Underscore,'_'
		dc.l	TAG_DONE

pnm_modeLabels:
		dc.l	pnm_lab1
		dc.l	pnm_lab2
		dc.l	0

pnm_lab1:	dc.b	'PPM (Colour)',0
pnm_lab2:	dc.b	'PGM (Grey)',0

	even

NTypes:
		DC.W    14,20,130,13
		DC.L    0,0
		DC.W    GD_format
		DC.L    PLACETEXT_RIGHT,0,0
		DC.W    10,40,60,13
		DC.L    oktext,0
		DC.W    GD_Ok
		DC.L    PLACETEXT_IN,0,0
		DC.W    85,40,60,13
		DC.L    canceltext,0
		DC.W    GD_Cancel
		DC.L    PLACETEXT_IN,0,0

oktext:		dc.b	'_Ok',0
canceltext:	dc.b	'_Cancel',0

Text0:
		DC.B    2,0
		DC.B    RP_JAM1
		DC.B    0
		DC.W    80,10
		DC.L    0
		DC.L    Project0IText0
		DC.L    0

Project0_TNUM EQU 1

Project0IText0:
		DC.B    'Save format',0

saveppmmsg:	dc.b	'Save PPM Image',0
savepgmmsg:	dc.b	'Save PGM Image',0

