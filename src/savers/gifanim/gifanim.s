����  ��&b�&b�&b�&b�&b�&b�&b�&b;=�;-------------------------------------------------------------------------
;
;		GIF saver modul for ArtPRO
;		(c) 1996 ,Frank Pagels (Crazy Copper) /DFT
;
;		12.10.1996
;
;
		incdir 	include:

		include	exec/exec_lib.i
		include	exec/lists.i
		include	exec/memory.i
		include intuition/intuition_lib.i
		include	libraries/wb_lib.i
		include libraries/gadtools.i
		include libraries/gadtools_lib.i
		include	graphics/graphics_lib.i
		Include utility/tagitem.i
		include	dos/dos.i		
		include	dos/dos_lib.i		
		include	libraries/reqtools_lib.i
		include	libraries/reqtools.i
		include misc/artpro.i
		
		SAVERHEADER GFA_NODE

		dc.b	'$VER: GIF saver module 0.10 (14.10.96)',0
	even
;---------------------------------------------------------------------------
GFA_NODE:
		dc.l	0,0
		dc.b	0,0
		dc.l	GFA_name
		dc.l	GFA_save
		dc.b	'GIFA'		;saverkennung f�r ArtPRO
		dc.b	'EXTR'		;for extern saver
		dc.l	0
		dc.l	GFA_Tags
GFA_name:
		dc.b	'GIF ANIM',0
	even
;---------------------------------------------------------------------------

GFA_Tags:
		dc.l	APT_Creator,GFA_Creator
		dc.l	APT_Version,1
		dc.l	APT_Info,GFA_Info
		dc.l	APT_Prefs,GFA_Prefsrout		;routine for Prefs
		dc.l	APT_Prefsbuffer,GFA_Prefs	;buffer for Prefs
		dc.l	APT_Prefsbuffersize,3		;size of buffer
		dc.l	APT_PrefsVersion,1		;Prefs Version
		dc.l	0

GFA_Creator:
		dc.b	'(c) 1996 Frank Pagels /DEFECT Softworks',0
	even
GFA_Info:
		dc.b	'GIF ANIM saver',10,10
		dc.b	'Saves an GIF Animation.',0
	even
;----------------------------------------------------------------------------

GFA_Puffermem	=	APG_Free1
codesize	=	APG_Free2	;code gr��e mit der gerade codiert wird
clearcode	=	APG_Free3	;clear code wenn 12 bits erreicht sind
lastcode	=	APG_Free4	;letzter code befor die codesize erh�ht wird
nextcode	=	APG_Free5
EOFCode		=	APG_Free6
puffermerk	=	APG_Free7
blockcounter	=	APG_Free8	;.w
GFA_linecounter	=	APG_Free8+2
GFA_Chunkymem	=	APG_Free9
GFA_chunkylen	=	APG_Free10
GFA_aktbyte	=	APG_Free11
GFA_bytemask	=	APG_Free11+2

GFA_Window:	dc.l	0
GFA_Glist:	dc.l	0
GFA_gadarray:	ds.l	6

GFA_Prefs:
GFA_prefs_inter:	dc.b	0
GFA_prefs_trans:	dc.b	0
GFA_prefs_tcolor:dc.b	0
		dc.b	0
		
GFA_Prefsbak:	dc.l	0

GFA_Tabmem:	dc.l	0
gfa_frames:	dc.l	0
gfa_filehandle:	dc.l	0
gfa_time:	dc.l	0

;----------------------------------------------------------------------------
GFA_save:
		tst.l	APG_Picmem(a5)
		bne	.pmemok
		moveq	#APSE_NOBITMAP,d3
		rts
.pmemok:

;Berechne ben�tigten Speicher

		lea	gfa_filehandle(pc),a1
		move.l	APG_Filehandle(a5),(a1)

		move.w	#0,APG_xbrush1(a5)	;Es soll das ganze 
		move.w	#0,APG_ybrush1(a5)	;Pic abgesaved werden
		move.w	APG_ImageWidth(a5),APG_xbrush2(a5)
		move.w	APG_ImageHeight(a5),APG_ybrush2(a5)	

		moveq	#0,d0
		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
		move.l	d0,d1
		add.l	#15,d1
		lsr.l	#4,d1		;breite in worte
		move.w	d1,APG_Brushword(a5)

		add.l	#256*3+100,d0
		move.l	#MEMF_ANY,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)

		tst.l	d0
		bne	.memok

		move.l	#APSE_NOMEM,d3
		rts

.memok:
		move.l	d0,GFA_Puffermem(a5)

		move.l	d0,a0

		add.l	#256*3+100,d0
		move.l	d0,GFA_Chunkymem(a5)
		

;Ask for frame numbers

		move.l	APG_ReqToolsbase(a5),a6
		lea	gfa_frames(pc),a1
		lea	gfa_framestitel(pc),a2
		sub.l	a3,a3
		move.l	APG_RTRequestsTags(a5),a0
		jsr	_LVOrtGetLongA(a6)

;ask for Delay Time

		lea	gfa_time(pc),a1
		lea	gfa_DelayTime(pc),a2
		sub.l	a3,a3
		move.l	APG_RTRequestsTags(a5),a0
		jsr	_LVOrtGetLongA(a6)


	;*---------- Init Header --------------

		move.l	GFA_Puffermem(a5),a0
		move.l	#'GIF8',(a0)+
		move.w	#'9a',(a0)+

		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
		rol.w	#8,d0
		move.w	d0,(a0)+
		move.w	APG_ybrush2(a5),d0
		sub.w	APG_ybrush1(a5),d0
		rol.w	#8,d0
		move.w	d0,(a0)+

		moveq	#0,d0
		or.b	#%10000000,d0	;Global Colormap

		moveq	#0,d1
		move.b	APG_Planes(a5),d1
		subq.b	#1,d1

		or.b	d1,d0
		lsl.b	#4,d1
		or.b	d1,d0

		move.b	d0,(a0)+	;Packed Bits

		clr.b	(a0)+		;Background Color Index
		clr.b	(a0)+		;Pixel Aspect Ratio

	;*---- Schreibe Colormap -----*

		move.w	APG_Colorcount(a5),d7
		subq.w	#1,d7

		lea	APG_Farbtab8+4(a5),a1

.nc		move.l	(a1)+,d0
		move.l	(a1)+,d1
		move.l	(a1)+,d2

		rol.l	#8,d0
		rol.l	#8,d1
		rol.l	#8,d2

		move.b	d0,(a0)+
		move.b	d1,(a0)+
		move.b	d2,(a0)+

		dbra	d7,.nc

	;*----- Write Image Descriptor ----*



	;*---- Schreibe Graphic Control Extension ----*

		move.b	#$21,(a0)+	;Extension Introducer
		move.b	#$f9,(a0)+	;Graphic Control Label
		move.b	#4,(a0)+	;Blocksize
		moveq	#0,d0
		tst.b	GFA_prefs_trans
		beq	.nixtrans
		or.b	#%00000001,d0
.nixtrans:
		or.b	#%10000000,d0
		move.b	d0,(a0)+	;Packed Field

;		move.w	#5,d0
		move.l	gfa_time(pc),d0
		rol.w	#8,d0
		move.w	d0,(a0)+		;Delay Time
		move.b	GFA_prefs_tcolor(pc),(a0)+
		clr.b	(a0)+		;Block terminator

		move.b	#$2c,(a0)+	;Image Separator
		clr.l	(a0)+		;Image left and right position
		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
 		rol.w	#8,d0
		move.w	d0,(a0)+
		move.w	APG_ybrush2(a5),d0
		sub.w	APG_ybrush1(a5),d0
		rol.w	#8,d0
		move.w	d0,(a0)+

		moveq	#0,d0
		move.b	APG_Planes(a5),d0
		subq.b	#1,d0
		tst.b	GFA_prefs_inter
		beq	.nixinter
		or.b	#%01000000,d0		;Interlaced Flag
.nixinter:		
		move.b	d0,(a0)+		;packed Bitfield

gfa_next:
		move.b	APG_Planes(a5),d0
		cmp.b	#1,d0
		bne	.ncw
		move.b	#2,d0
.ncw		move.b	d0,(a0)+	;LZW Minimum Code Size

		move.l	GFA_Puffermem(a5),d2
		sub.l	d2,a0
		move.l	a0,d3
		move.l	gfa_Filehandle(pc),d1
		move.l	APG_Dosbase(a5),a6
		jsr	_LVOWrite(a6)

;Write ersten Frame

		bsr	gfa_writegif


;bereite f�r n�chsten frame vor

gfa_nextread:
		lea	gfa_frames(pc),a0
		sub.l	#1,(a0)
		beq	GFA_finish
		
		move.l	gfa_Filehandle(pc),d1
		lea	endstring(pc),a0
		move.l	a0,d2
		moveq	#1,d3
		jsr	_LVOWrite(a6)

		lea	APG_LoadFullname(a5),a0
		move.l	a0,d5
.w		move.b	(a0)+,d0
		bne.b	.w		

;erh�he file nummer

		subq.l	#2,a0
.nextnumber:	move.b	(a0),d0
		add.b	#1,d0
		cmp.b	#$40,d0
		bne	.nixueberlauf
		move.b	#'0',-(a0)
		bra	.nextnumber
.nixueberlauf:
		move.b	d0,(a0)
		
		move.l	APG_picmem(a5),a1	;Speicher vom alten Pic
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)

		clr.w	APG_loadflag(a5)
		clr.l	APG_picmem(a5)
		
		move.l	d5,d1
		move.l	#MODE_OLDFILE,d2
		move.l	APG_Dosbase(a5),a6
		jsr	_LVOOpen(a6)
		move.l	d0,APG_Filehandle(a5)
		bne	.fileok
		lea	gfa_filenotfoundmsg(pc),a4
		move.l	APG_Status(a5),a6
		jsr	(a6)

		move.l	gfa_filehandle(pc),APG_Filehandle(a5)

		move.l	4.w,a6
		move.l	GFA_Puffermem(a5),d0
		beq	.nm
		move.l	d0,a1
		jsr	_LVOFreeVec(a6)
		clr.l	GFA_Puffermem(a5)
.nm
		move.l	GFA_Tabmem(pc),d0
		beq	.nlm
		move.l	d0,a1
		jsr	_LVOFreeVec(a6)
		clr.l	GFA_Tabmem
.nlm		
		moveq	#APSE_ERROR,d3
		rts

.fileok

		clr.b	APG_Intern1(a5)		;usedatatype
		st	APG_Intern2(a5)		;newloaded
		
		jsr	APR_LoadUniversal(a5)

;		jsr	APR_ClearProcess(a5)

		move.l	APG_Filehandle(a5),d1
		move.l	APG_Dosbase(a5),a6
		jsr	_LVOClose(a6)

		tst.l	d3
		beq	gfa_nextframe

gfa_nextframe:
		
	;*---- Schreibe Graphic Control Extension ----*

		move.l	GFA_Puffermem(a5),a0

		move.b	#$21,(a0)+	;Extension Introducer
		move.b	#$f9,(a0)+	;Graphic Control Label
		move.b	#4,(a0)+	;Blocksize
		moveq	#0,d0
		tst.b	GFA_prefs_trans
		beq	.nixtrans
		or.b	#%00000001,d0
.nixtrans:
		or.b	#%10000000,d0
		move.b	d0,(a0)+	;Packed Field

;		move.w	#5,d0
		move.l	gfa_time(pc),d0
		rol.w	#8,d0
		move.w	d0,(a0)+		;Delay Time
		move.b	GFA_prefs_tcolor(pc),(a0)+
		clr.b	(a0)+		;Block terminator


		move.b	#$2c,(a0)+	;Image Separator
		clr.l	(a0)+		;Image left and right position
		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
 		rol.w	#8,d0
		move.w	d0,(a0)+
		move.w	APG_ybrush2(a5),d0
		sub.w	APG_ybrush1(a5),d0
		rol.w	#8,d0
		move.w	d0,(a0)+

		moveq	#0,d0
		move.b	APG_Planes(a5),d0
		subq.b	#1,d0
		tst.b	GFA_prefs_inter
		beq	.nixinter
		or.b	#%01000000,d0		;Interlaced Flag
.nixinter:		
		or.b	#%10000000,d0		;Global color table
		move.b	d0,(a0)+		;packed Bitfield


	;schreibe colormap
	
		move.w	APG_Colorcount(a5),d7
		subq.w	#1,d7

		lea	APG_Farbtab8+4(a5),a1

.nc		move.l	(a1)+,d0
		move.l	(a1)+,d1
		move.l	(a1)+,d2

		rol.l	#8,d0
		rol.l	#8,d1
		rol.l	#8,d2

		move.b	d0,(a0)+
		move.b	d1,(a0)+
		move.b	d2,(a0)+

		dbra	d7,.nc

		bra	gfa_next

;----------------------------------------------------------------------------
; Beende File
GFA_finish:
		move.l	gfa_filehandle(pc),APG_Filehandle(a5)
		
		move.l	APG_Filehandle(a5),d1
		lea	endstring(pc),a0
		move.l	a0,d2
		moveq	#2,d3
		jsr	_LVOWrite(a6)

GFA_ende:
		move.l	4.w,a6
		move.l	GFA_Puffermem(a5),d0
		beq	.nm
		move.l	d0,a1
		jsr	_LVOFreeVec(a6)
		clr.l	GFA_Puffermem(a5)
.nm
		move.l	GFA_Tabmem(pc),d0
		beq	.nlm
		move.l	d0,a1
		jsr	_LVOFreeVec(a6)
		clr.l	GFA_Tabmem
.nlm		
		moveq	#0,d3
		rts

		
GFA_stop:
		move.l	(a7)+,d0
		moveq	#APSE_ABORT,d3
		bra	GFA_ende

endstring:	dc.b	0,';'

;---------------------------------------------------------------------------
gfa_writegif:
	;--- L�sche Puffer ----

		move.l	#((256*3+100)/4)-1,d7
		move.l	GFA_Puffermem(a5),a0
.npc		clr.l	(a0)+
		dbra	d7,.npc
		
		
	;*---- Init Knotentab ----*

BITS			=	12
MAX_CODE		=	((1<<BITS)-1)
HSIZE			=	5021	/* hash table size for 80% occupancy */
UNBENUTZT		=	-1


		lea	GFA_Knotentab,a0
		move.l	#HSIZE-1,d7
.nc		move.w	#UNBENUTZT,code_wert(a0)
		lea	tablen(a0),a0
		dbra	d7,.nc


	;*---- Berechne Zeilen Tab ------

		clr.l	APG_Free12(a5)
		moveq	#0,d0
		move.w	APG_ybrush2(a5),d0
		sub.w	APG_ybrush1(a5),d0
		move.l	d0,d7
		add.w	d0,d0			;Puffer daf�r
		addq.w	#2,d0			;F�r Endkennung
		move.l	#MEMF_ANY!MEMF_CLEAR,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)
		lea	GFA_Tabmem(pc),a0
		move.l	d0,(a0)

		tst.b	GFA_prefs_inter
		beq	GFA_makenormaltab

;berechne interlace tab

		move.l	GFA_Tabmem(pc),a0

		move.w	APG_ybrush1(a5),d0	;erste 8'er schleife
.nl:		move.w	d0,(a0)+
		addq.w	#8,d0
		cmp.w	APG_ybrush2(a5),d0
		bmi	.nl

		move.w	APG_ybrush1(a5),d0	;zweite 8'er schleife
		add.w	#4,d0
		cmp.w	APG_ybrush2(a5),d0
		bhi	.m4

.nl8:		move.w	d0,(a0)+
		addq.w	#8,d0
		cmp.w	APG_ybrush2(a5),d0
		bmi	.nl8
.m4:
		move.w	APG_ybrush1(a5),d0	;4'er schleife
		add.w	#2,d0
		cmp.w	APG_ybrush2(a5),d0
		bhi	.m2

.nl4:		move.w	d0,(a0)+
		addq.w	#4,d0
		cmp.w	APG_ybrush2(a5),d0
		bmi	.nl4
.m2:
		move.w	APG_ybrush1(a5),d0	;zweite 8'er schleife
		add.w	#1,d0
		cmp.w	APG_ybrush2(a5),d0
		bhi	.end

.nl2:		move.w	d0,(a0)+
		addq.w	#2,d0
		cmp.w	APG_ybrush2(a5),d0
		bmi	.nl2
.end:
		move.w	#-1,(a0)+
		bra	GFA_Init


GFA_makenormaltab:
		subq.w	#1,d7
		moveq	#0,d0
		move.w	APG_ybrush1(a5),d0
		move.l	GFA_Tabmem(pc),a0
.ntw:		move.w	d0,(a0)+
		add.w	#1,d0
		dbra	d7,.ntw

		move.w	#-1,(a0)+

	;*---- Beginne mit Codierung ----*
GFA_Init:

;Init Code werte

		moveq	#0,d0
		move.b	APG_Planes(a5),d0
		cmp.b	#1,d0
		bne	.nc
		moveq	#2,d0
		
.nc		moveq	#1,d1
		lsl.w	d0,d1
		move.l	d1,clearcode(a5)
		
		addq	#1,d1
		move.l	d1,eofcode(a5)

		addq.l	#1,d0
		move.l	d0,codesize(a5)

		moveq	#1,d1
		lsl.w	d0,d1
		subq.l	#1,d1
		move.l	d1,lastcode(a5)

;----------------------------------------------------------------------------

		move.l	APG_Checkfirstword(a5),a6
		jsr	(a6)		;vorbereitung f�r bpl2chunkyline

		moveq	#0,d0
		move.l	GFA_Tabmem(pc),a0
		move.w	(a0)+,d0
		move.l	a0,APG_Free12(a5)

		move.l	GFA_Chunkymem(a5),a0
		move.l	APG_Bpl2ChunkyLine(a5),a6
		jsr	(a6)
		
		move.l	GFA_Chunkymem(a5),a3
		move.l	GFA_Puffermem(a5),a4
				
		moveq	#0,d0
		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
		move.l	d0,GFA_chunkylen(a5)
		
		move.b	#$fe,(a4)+	;blockgr��e
		
		move.w	#1,blockcounter(a5)

		move.l	eofcode(a5),d0
		addq.l	#1,d0
		move.l	d0,nextcode(a5)		;Erster_Code

		clr.w	GFA_aktbyte(a5)		;aktuelles Byte
		move.w	#1,GFA_bytemask(a5)	;Bytemaske

		move.l	clearcode(a5),d1
		bsr	GFA_writebits

		moveq	#0,d0
		move.b	(a3)+,d0	;neues Zeichen gelesen, stringcode
		subq.l	#1,GFA_Chunkylen(a5)
GFA_nextchar:
		moveq	#0,d4
		move.b	(a3)+,d4	;zeichen
		subq.l	#1,GFA_Chunkylen(a5)
		bpl	.nixend		

	;Hole eine neue Zeile

		move.l	d0,-(a7)

		moveq	#0,d0
		move.l	APG_Free12(a5),a0
		move.w	(a0)+,d0		;Zeile
		bpl	.noe
		move.l	(a7)+,d0
		bra	GFA_wech
.noe
		move.l	a0,APG_Free12(a5)
		move.l	GFA_Chunkymem(a5),a0
		move.l	APG_Bpl2ChunkyLine(a5),a6
		jsr	(a6)

		moveq	#0,d0
		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
		move.l	d0,GFA_chunkylen(a5)

		move.l	GFA_Chunkymem(a5),a3
		move.b	(a3)+,d4	;neues Zeichen gelesen, stringcode
		subq.l	#1,GFA_Chunkylen(a5)
;		jsr	APR_DoProcess(a5)
		tst.l	d0
		beq	GFA_stop

		move.l	(a7)+,d0
.nixend
;		bsr	suche_kind_knoten

;--------------------------------------------------------------------------
;---- Kucke ob es den String schon gibt ? ---------------------------------
;--------------------------------------------------------------------------
;> d0 = Eltern_Code d4=Kindzeichen
;< d1 = Index

suche_kind_knoten:

		movem.l	d0/d4/a3/a4,-(a7)

		move.l	d4,d1		;Index
		lsl.l	#BITS-8,d1
		eor.l	d0,d1
		bne	.nozero		;Index=0 ?
		moveq	#1,d2		;Offset
		bra	.such
.nozero		move.l	#HSIZE,d2
		sub.l	d1,d2		;Offset=HSize-index
.such
		lea	GFA_Knotentab,a0
		lea	(a0,d1*8),a0	;tab[Index]

		cmp.w	#UNBENUTZT,code_wert(a0)
		beq	such_end
		cmp.l	eltern_code(a0),d0
		bne	.nixgleich
		cmp.w	zeichen(a0),d4
		beq	such_end
.nixgleich:
		sub.l	d2,d1		;index-offset
		bpl	.such		;index<0 ?
		add.l	#HSize,d1	;index+TABELLEN_GROESSE
		bra	.such

such_end:	movem.l	(a7)+,d0/d4/a3/a4

;--------------------------------------------------------------------------

		move.w	code_wert(a0),d1
		cmp.w	#UNBENUTZT,d1
		beq	.newcode		;Code gefunden
		move.l	d1,d0		;string_code=zeichen

		bra	GFA_nextchar

;wir m�ssen einen neuen Code eintragen
.newcode:
		move.l	d0,d1

;------------------------------------------------------------------------
	;Code Speichern
	
		move.w	GFA_aktbyte(a5),d6
		move.w	GFA_bytemask(a5),d5

		move.l	codesize(a5),d2
		subq	#1,d2
		moveq	#1,d7	;maske
.nextbit:
;		add.b	d1,d1
;		roxl.b	#1,d6

		move.w	d1,d3
		and.w	d7,d3	;teste Bit
		beq	.nobit
		or.b	d5,d6	;setze bit
.nobit:

;		lsl.b	#1,d5
		add.b	d5,d5

		bne	.byteok
		move.b	d6,(a4)+

		addq.w	#1,blockcounter(a5)
		cmp.w	#$ff,blockcounter(a5)
		bne	.nixblock

;----- Block Speichern -----------

		movem.l	d0-d3,-(a7)
		move.l	gfa_Filehandle(pc),d1
		move.l	GFA_Puffermem(a5),d2
		move.l	#$ff,d3
		move.l	APG_Dosbase(a5),a6
		jsr	_LVOWrite(a6)
		movem.l	(a7)+,d0-d3

		move.w	#1,blockcounter(a5)
		move.l	GFA_Puffermem(a5),a4
		move.b	#$fe,(a4)+
.nixblock:
		moveq	#0,d6
		moveq	#1,d5
.byteok:
		lsl.w	#1,d7
		dbra	d2,.nextbit

		move.w	d6,GFA_aktbyte(a5)
		move.w	d5,GFA_bytemask(a5)

;------------------------------------------------------------------------
;		bsr	GFA_writebits

		move.l	nextcode(a5),d1
		cmp.l	lastcode(a5),d1
		bls	.nixlast

		addq.l	#1,codesize(a5)
		
		move.l	lastcode(a5),d1
		lsl.l	#1,d1
		or.l	#1,d1
		move.l	d1,lastcode(a5)
.nixlast:
		move.l	nextcode(a5),d1
		move.w	d1,code_wert(a0)
		addq.l	#1,nextcode(a5)			
		move.l	d0,eltern_code(a0)	;string_code
		move.w	d4,zeichen(a0)

		move.l	d4,d0		;string_code=zeichen

		cmp.l	#MAX_CODE,nextcode(a5)
		bne	.codeok

;--- Tabelle l�schen und clearcode ausgeben ---

		move.l	d0,-(a7)

		move.l	clearcode(a5),d1
		bsr	GFA_writebits

		lea	GFA_Knotentab,a0
		move.l	#HSIZE-1,d7
.nc		move.w	#UNBENUTZT,code_wert(a0)
		lea	tablen(a0),a0
		dbra	d7,.nc

		move.l	eofcode(a5),d0
		addq.l	#1,d0
		move.l	d0,nextcode(a5)		;ERSTER_CODE

		moveq	#0,d0
		move.b	APG_Planes(a5),d0
		cmp.b	#1,d0
		bne	.ncw1
		moveq	#2,d0
.ncw1
		addq.l	#1,d0
		move.l	d0,codesize(a5)

		moveq	#1,d1
		lsl.w	d0,d1
		subq.l	#1,d1
		move.l	d1,lastcode(a5)

		move.l	(a7)+,d0

.codeok
		bra	GFA_nextchar


GFA_wech:
		move.l	d0,d1
		bsr	GFA_Writebits
		move.l	eofcode(a5),d1
		bsr	GFA_Writebits

		cmp.b	#1,d5
		beq	.w

		move.b	d6,(a4)+
		addq.w	#1,blockcounter(a5)
		
.w		move.l	APG_Dosbase(a5),a6

		move.w	blockcounter(a5),d0
		cmp.w	#1,d0
		beq	.nixwrite

		move.l	gfa_Filehandle(pc),d1
		move.l	GFA_Puffermem(a5),d2
		move.l	d2,a0
		move.b	d0,(a0)
		sub.b	#1,(a0)
		move.l	d0,d3
		jsr	_LVOWrite(a6)
.nixwrite:
		rts

;-- Gebe Bits aus --
;d1= Code d2=Code_bits d6=aktuelles Byte a4=output 

GFA_writebits:
		move.w	GFA_aktbyte(a5),d6
		move.w	GFA_bytemask(a5),d5

		move.l	codesize(a5),d2
		subq	#1,d2
		moveq	#1,d7	;maske
.nextbit:
;		add.b	d1,d1
;		roxl.b	#1,d6

		move.w	d1,d3
		and.w	d7,d3	;teste Bit
		beq	.nobit
		or.b	d5,d6	;setze bit
.nobit:

;		lsl.b	#1,d5
		add.b	d5,d5

		bne	.byteok
		move.b	d6,(a4)+

		addq.w	#1,blockcounter(a5)
		cmp.w	#$ff,blockcounter(a5)
		bne	.nixblock

;----- Block Speichern -----------

		movem.l	d0-d3,-(a7)
		move.l	gfa_Filehandle(pc),d1
		move.l	GFA_Puffermem(a5),d2
		move.l	#$ff,d3
		move.l	APG_Dosbase(a5),a6
		jsr	_LVOWrite(a6)
		movem.l	(a7)+,d0-d3

		move.w	#1,blockcounter(a5)
		move.l	GFA_Puffermem(a5),a4
		move.b	#$fe,(a4)+
.nixblock:
		moveq	#0,d6
		moveq	#1,d5
.byteok:
		lsl.w	#1,d7
		dbra	d2,.nextbit

		move.w	d6,GFA_aktbyte(a5)
		move.w	d5,GFA_bytemask(a5)
		rts

;--------------------------------------------------------------------------
GFA_Prefsrout:
;init Prefs
		move.l	GFA_prefs(pc),GFA_Prefsbak

		lea	GFA_discolor(pc),a2
		clr.l	(a2)

		moveq	#0,d0
		move.b	GFA_prefs_tcolor(pc),d0
		lea	GFA_tcolor(pc),a0
		move.l	d0,(a0)

		lea	GFA_incheck(pc),a0
		clr.l	(a0)
		tst.b	GFA_prefs_inter
		beq	.nixin
		move.l	#1,(a0)
.nixin:
		lea	GFA_trcheck(pc),a0
		clr.l	(a0)
		tst.b	GFA_prefs_trans
		beq	.nixtr
		move.l	#1,(a0)
.nixtr:

;---------------------------------------------------------------------------

		move.l	#WindowTags,APG_WindowTagList(a5) ;Tag list for the Window
		move.l	#WinWG+4,APG_WGadgets(a5)	;addr. of WA_Gadgets Tag + 4
		move.l	#winSC+4,APG_WScreen(a5)	;addr. of WA_CustomScreen Tag + 4
		move.l	#GFA_window,APG_WinWnd(a5)	;a point for the Window
		move.l	#GFA_Glist,APG_Glist(a5)		;a point for the Glist
		move.l	#NTypes,APG_NGads(a5)		;the NGads list
		move.l	#Gtypes,APG_GTypes(a5)		;the GTypes list
		move.l	#Gtags,APG_Gtags(a5)		;the Gtags list
		move.w	#5,APG_CNT(a5)			;the number of Gadgets
		move.l	#GFA_gadarray,APG_Gadgets(a5)	;a pointer for the Gadgetsarry, size=4*number of Gadgets
		move.l	APG_Scr(a5),APG_ScrBase(a5)	;the Screenpointer , stored in APG_Scr
	
		move.l	APG_CheckMainWinPos(a5),a6	;returns d0,d1 Pos
		jsr	(a6)

		add.w	#70,d0
		add.w	#100,d1
		
		move.w	d0,APG_WinLeft(a5)		;the left pos. of the Win
		move.w	d1,APG_WinTop(a5)		;the top pos. of the Win
		move.w	#158,APG_WinWidth(a5)		;the width of the Win
		move.w	#100,APG_WinHeight(a5)		;the height of the Win

		move.l	#WinL,APG_WinL(a5)		;addr. of WA_Left Tag 
		move.l	#WinT,APG_WinT(a5)		;addr. of WA_Top Tag
		move.l	#WinW,APG_WinW(a5)		;addr. of WA_Width Tag
		move.l	#WinH,APG_WinH(a5)		;addr. of WA_Height Tag

		move.l	APG_OpenWindow(a5),a6
		jsr	(a6)

GFA_wait:	move.l	GFA_window(pc),a0
		move.l	APG_Wait(a5),a6
		jsr	(a6)				;Handle input

		cmp.l	#GADGETUP,d4
		beq.b	GFA_gads
		cmp.l	#GADGETDOWN,d4
		beq.b	GFA_gads
		bra.b	GFA_wait

GFA_gads:
		moveq	#0,d0
		move.w	38(a4),d0
		add.l	d0,d0
		move.l	d0,d1
		lea	GFA_pjumptab(pc),a0
		move.w	(a0,d0.l),d0
		jmp	(a0,d0.w)

GFA_pjumptab:
		dc.w	GFA_handleinter-GFA_pjumptab
		dc.w	GFA_handletrans-GFA_pjumptab
		dc.w	GFA_handlecolor-GFA_pjumptab
		dc.w	GFA_ok-GFA_pjumptab
		dc.w	GFA_cancel-GFA_pjumptab

		
GFA_ghost:	dc.l	GA_Disabled,0,0


GFA_handleinter:
		lea	GFA_prefs_inter(pc),a1
		lea	GFA_incheck(pc),a2
		clr.b	(a1)
		clr.l	(a2)
		add.l	d1,d1
		lea	GFA_gadarray(pc),a0
		move.l	(a0,d1.l),a0
		move.w	gg_Flags(a0),d0
		and.w	#SELECTED,d0
		beq.w	GFA_wait		;nicht selected
		st	(a1)
		move.l	#1,(a2)
		bra	GFA_wait

GFA_handletrans:
		lea	GFA_prefs_trans(pc),a1
		lea	GFA_trcheck(pc),a2
		clr.b	(a1)
		clr.l	(a2)
		add.l	d1,d1
		lea	GFA_gadarray(pc),a0
		move.l	(a0,d1.l),a0
		move.w	gg_Flags(a0),d0
		and.w	#SELECTED,d0
		beq.w	GFA_wait		;nicht selected
		st	(a1)
		move.l	#1,(a2)
		bra	GFA_wait

GFA_handlecolor:
		add.l	d1,d1
		lea	GFA_gadarray(pc),a0
		move.l	(a0,d1.l),a0
		move.l	GFA_window(pc),a1
		sub.l	a2,a2
		lea	GFA_numtag(pc),a3
		move.l	APG_Gadtoolsbase(a5),a6
		jsr	_LVOGT_GetGadgetAttrsA(a6)

		move.l	GFA_number(pc),d0
		lea	GFA_tcolor(pc),a0
		move.l	d0,(a0)
		lea	GFA_prefs_tcolor(pc),a0
		move.b	d0,(a0)
		bra	GFA_wait



GFA_numtag:	dc.l	GTIN_Number,GFA_number,TAG_DONE
GFA_number:	dc.l	0

GFA_cancel:
		move.l	GFA_prefsbak(pc),GFA_Prefs


GFA_ok:
		move.l	GFA_window(pc),a0
		move.l	APG_Intuitionbase(a5),a6
		jmp	_LVOCloseWindow(a6)


;--------------------------------------------------------------------------------
	even

WindowTags:
winL:	dc.l	WA_Left,212
winT:	dc.l	WA_Top,78
winW:	dc.l	WA_Width,220
winH:	dc.l	WA_Height,85
		dc.l	WA_IDCMP,MXIDCMP!GADGETUP!VANILLAKEY!BUTTONIDCMP!IDCMP_REFRESHWINDOW
		dc.l	WA_Flags,WFLG_DEPTHGADGET!WFLG_ACTIVATE!WFLG_DRAGBAR!WFLG_SMART_REFRESH!WFLG_RMBTRAP
winWG:	dc.l	WA_Gadgets,0
		dc.l	WA_Title,winWTitle
winSC:	dc.l	WA_CustomScreen,0
		dc.l	TAG_DONE

WinWTitle:	dc.b	'GIF Options',0

	even

GD_Inter	=	0
GD_trans	=	1
GD_transcolor	=	2
GD_Ok		=	3
GD_Cancel	=	4

Gtypes:
		dc.w	CHECKBOX_KIND
		dc.w	CHECKBOX_KIND
		dc.w	INTEGER_KIND
		dc.w	BUTTON_KIND
		dc.w	BUTTON_KIND
Gtags:
		dc.l	GTCB_Checked
GFA_incheck	dc.l	0
		dc.l	TAG_DONE
		dc.l	GTCB_Checked
GFA_trcheck	dc.l	0
		dc.l	TAG_DONE
		dc.l	GTIN_Number
GFA_tcolor:	dc.l	0
		dc.l	GTIN_MaxChars,3
		dc.l	GA_Disabled
GFA_discolor	dc.l	1
		dc.l	TAG_DONE
		dc.l	TAG_DONE
		dc.l	TAG_DONE

NTypes:
		dc.w	10,32,26,11
		dc.l	GFA_interText,0
		dc.w	GD_inter
		dc.l	PLACETEXT_RIGHT,0,0
		dc.w	10,45,26,11
		dc.l	GFA_transText,0
		dc.w	GD_trans
		dc.l	PLACETEXT_RIGHT,0,0
		dc.w	10,59,30,14
		dc.l	GFA_transcolorText,0
		dc.w	GD_transcolor
		dc.l	PLACETEXT_RIGHT,0,0
		DC.W    10,80,60,14
		DC.L    oktext,0
		DC.W    GD_Ok
		DC.L    PLACETEXT_IN,0,0
		DC.W    85,80,60,13
		DC.L    canceltext,0
		DC.W    GD_Cancel
		DC.L    PLACETEXT_IN,0,0

oktext:		dc.b	'Ok',0
canceltext:	dc.b	'Cancel',0
GFA_interText:	dc.b	'Interlaced',0
GFA_transText:	dc.b	'Transparency',0
GFA_transcolorText:dc.b 'Trans. color',0


;----------------------------------------------------------------------------

gfa_framestitel:dc.b	'How much frames?',0

gfa_DelayTime:	dc.b	'Delay Time?',0

savemsg:	dc.b	'Save a GIF Amin!',0
gfa_filenotfoundmsg:
		dc.b	'Error open file!',0
	even
		
	RSRESET
	
code_wert	rs.w	1
eltern_code	rs.l	1
zeichen		rs.w	1
tablen		rs.b	0


		section	daten,BSS

GFA_Knotentab:
		ds.b	5021*tablen
