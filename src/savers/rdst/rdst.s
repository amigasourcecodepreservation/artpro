����  !�                                    ;
;Save bitmap as 16x16 bits data
;Rafik/RDST
;Gdynia 1997
;28-29-V-97
;20-VI-97 config change!
;31-VIII-97 added raw output !, prefs is fucked up... set interleaved to 0
;		for raw



;
;written in trash'one 2.0, case sensitive ON (Mayby You have higher
; version of TSM?)
;rather won't work in asm 'cos asm have one error that thing like this
;    'ml (a0,d0.w),d0' treat as 3 not 2 argument for my macros. :(((

;ml is move.l mw .w etc... check out macra.s for details :)

;example saver for ArtPro

;it saves bitmap in form like this:

;lets say we have screen 320x256 depth 3
;[] a word
;normal interleawed raw 
;			1bpl [a][b][c][d][][][][][][][][][][][][]
;			2bpl [a][b][c][d][][][][][][][][][][][][]
;			3bpl [a][b][c][d][][][][][][][][][][][][]

;after save	its look like this

;1bpl [a], 2bpl [a], 3bpl [a],   1bpl [b], 2bpl [b], 3bpl [b],

;IN OTHER WAY....
;It will do same as: 1.cut rectangle at x0,y0 with 16x16 size
;save it as interleaved, select next at x16,y0 same size save again & again
;again again 0,16, 16,16 etc.....
;				but in one file not in 100. 

;		why ???

;ok lets say that i set copper to interleaved screen
;
;normal raw procedure
;
;	lea	Klocki,a0	;adres of my piece
;	lea	Screen,a1	;my raw screen
;	moveq	#16-1,d0
;.loop	move.w	(a0),(a1)
;	add.l	#Klocki_sizeX,a0
;	add.l	#Screen_sizeX,a1	;lea	Screen_sizeX(a1),a1
;	dbf	d0,.loop

;my procedure
;
;	lea	Klocki,a0	;adres of my piece
;	lea	Screen,a1	;my interleaved screen
;	moveq	#Screen_sizeX,d1
;	moveq	#16-1,d0
;.loop	move.w	(a0)+,(a1)
;	add.l	d1,a1
;	dbf	d0,.loop

;upps one instruction less, what the diffrent, little ? hmmm?

;Not so little if you have to repeat this loop for 20*X & 256*Y.
;this givs 320 less. Cos this instruction is used every time this givs
;20*256=5120!. Procesor tacts take about 4 on every move.l so final
;result is about 10000 tacts.
;that will help specialy on slow a500!

;Its alse usefull if you writting something for the OS all image i put in
;1 screen & save it as ready data for draw image !

;BTW in this procedure every piece is at sizey*2*depth offset
;in my program that givs 256 so to find a pice i just need to do
;lea	Klocki,a0 lsl.w	#8,d0	add.l	d0,a0
;
;email: brainiax@manta.univ.gda.pl
;im rather rarly on the net so don't expect fast answer.
;
;				Have fun....
;							Rafik


VERSION:	MACRO
		dc.b	'V1.1e (12-X-97)'
		ENDM

     ;;
SAVE=1			;1 to save

DEBUG	set	0	;1 
			;at save automaticy set to 0
			;loads test picture 320x256x8 bpl!

DOREQUEST	set	0	;givs some debug info with request library
				;after save as object, bitmap size etc.


	IFNE	SAVE
DOREQUEST	set	0	;0set to 0 when save!!!
DEBUG		set	0	;0set to 0 when save!!!

	AUTO	wo\
	ELSE
	bra	MyOwnSaver
	ENDIF

		incdir 	sources:converter/savers/rdst/
		include	lvos.i		;all *_lib.i joined together
		include	macra.s		;my own macros :)

		incdir 	sc:include/

		include libraries/gadtools.i
		include libraries/gadtools_lib.i

		include	exec/lists.i
		include	exec/memory.i
		include libraries/gadtools.i
		include utility/tagitem.i
		include	dos/dos.i		
		include	dos/dos_lib.i		
		include	libraries/reqtools_lib.i
		include misc/artpro.i
	;	include	render/render.i
	;	include	render/render_lib.i
	;	include	render/renderhooks.i
		
		SAVERHEADER rd_NODE

		dc.b	'RafikSaver'
		VERSION
		dc.b	0
	even
;---------------------------------------------------------------------------
rd_NODE:
		dc.l	0,0
		dc.b	0,0
		dc.l	rd_name
		dc.l	MyOwnSaver
		dc.b	'RDST'
		dc.b	'EXTR'	;for extern Saver
		dc.l	0
		dc.l	rd_Tags
rd_name:
		dc.b	'RDST',0
	even
;---------------------------------------------------------------------------

rd_Tags:
		dc.l	APT_Creator,rd_Creator		;autor
		dc.l	APT_Version,1			;version
		dc.l	APT_Info,rd_Info		;saver info
		dc.l	APT_Prefs,rd_Prefsrout		;routine for Prefs
		dc.l	APT_Prefsbuffer,raf_Prefs	;buffer for Prefs
		dc.l	APT_Prefsbuffersize,3		;size of buffer
		dc.l	APT_PrefsVersion,1		;Prefs Version
		dc.l	APT_OperatorUse,1
		dc.l	0

	dc.b	'$VER:'
rd_Creator:
		dc.b	'(c) Rafik/RDST '
		VERSION
		dc.b	0
		even
rd_Info:
		dc.b	'My own format saver',10,10
		dc.b	'Saves an image 16x16 data for fast writting',10
		dc.b	'read doc for more info',$a
		dc.b	'brainiax@manta.univ.gda.pl',0
		even

;---------------------------------------------------------------------------

rd_window	dc.l	0		;pointer for prefs window
rd_Glist	dc.l	0		
rd_gadarray	dcb.l	4,0

;---------------------------------------------------------------------------
MyOwnSaver:			;save!!!
;;

	IFNE	DOREQUEST
	bsr	DoRequest
	ENDIF

	bsr	AllocMem
	ml	d0,_mem
	beq.s	.nomem

	bsr	Cutter
	tst.l	d0
	bne.s	.tosmall


	IFEQ	DEBUG
	bsr	save
	ENDIF

	bsr	FreeMem

	q0 d0
.rts	rts

.nomem:
	moveq	#APSE_NOMEM,d3		;no mem !
	rts

.tosmall:
	;wrong size error screen is lower than 16x16*1 (Frank what shuold i
	;do with this ? what error numer is corect ?

;do you use your own error message with the APG_Status funktion
;move.l	apg_status(a5),a6
;lea	msg(pc),a4
;jsr	(a6)
;then use APSE_ERROR as error number  

	moveq	#-4,d3	;? main deuche is not 2 good :((
	rts		;yes really :)
			;-4=APSE_NOBITMAP mean that no valid bitmap is
			;available, we have only 24 bit datas
			  
******************************************************************************
save:
	move.l	APG_Filehandle(a5),d1
	 move.l	APG_Dosbase(a5),a6
	  ml	_mem(pc),d2
	   ml	_size(pc),d3
	    JUMP	Write

********************************************************************
AllocMem:	;alloc block size!!!

	IFNE	DEBUG
	ml	#40*256*8,d0
	bra.s	.alloc
	ENDIF

	q0 d0
	 mw APG_ImageHeight(a5),d0	;y size
	  lsr.w	#3,d0	;/8 na byty
	   and.w	#-1-%1,d0	;and 
	    beq.s	.wrong

	q0 d1
	 mw	APG_ImageWidth(a5),d1
	  q0 d2
	   mw	ysize,d2
	    beq.s	.wrong
	     divs	d2,d1	;how much
	    and.l	#$ffff,d1
	   mulu	d2,d1	;image that won't fit will be cutted!
	  mulu	d1,d0	;x*y

	q0 d1
	 mb	APG_Planes(a5),d1	;x*y*depth
	  beq.s	.wrong
	   mulu	d1,d0

	ml	d0,_size

.alloc
	 move.l	#MEMF_PUBLIC+MEMF_CLEAR,d1
	  EXEC
	   JUMP	AllocVec
.wrong
	q0 d0
	rts
_mem:	dc.l	0
_size:	dc.l	0

*****************************************************************************
FreeMem:	
;a1 adres
;=<>
	ml	_mem(pc),a1
	EXEC
	JUMP	FreeVec

*****************************************************************************
Cutter:
;zamienia bitmape w iff na kolejne klocki
			;lea	ScreenIff,a0
			;lea	KlockiIffRaw,a1


	IFNE	DEBUG
	q 8,d0
	ELSE
	q0 d0
	mb	APG_Planes(a5),d0
	ENDIF
	subq.w	#1,d0	;dbf
	mw	d0,_depth

	IFNE	DEBUG
	q 40,d0
	ELSE
	ml	APG_ByteWidth(a5),d0
	ENDIF
	lsr.w	#1,d0	;/2
	subq.w	#1,d0
	mw	d0,_sizex


	ml	_mem,a1
	IFNE	DEBUG
	lea	KlockiIff,a0
	ELSE
	ml	APG_Picmem(a5),a0
	ENDIF

	q0 d1
	mw	ysize(pc),d1
	IFNE	DEBUG
	q 40,d0
	ELSE
	ml	APG_ByteWidth(a5),d0
	ENDIF
	mulu	d1,d0
	ml	d0,_pci	;APG_ByteWidth*ysize
	subq.w	#1,d1
	mw	d1,_sizey1

;y
	q0 d0
	IFNE	DEBUG
	mw	#256,d0
	ELSE
	mw APG_ImageHeight(a5),d0	;y size
	ENDIF
	lsr.w	#4,d0	;/16
	subq.w	#1,d0	;dbf
	bmi.s	.wrong
;x
	q0 d3
	IFNE	DEBUG
	mw	#320,d3
	ELSE
	mw	APG_ImageWidth(a5),d3
	ENDIF

	lsr.w	#4,d3	;byte/2
	subq.w	#1,d3	;dbf
	bmi.s	.wrong	;to loow screen

	tst.b	interleaved
	beq.s	DoRawCut

.ylo
	mw	_sizex,d1 ;x size	q [IffSize/2]-1,d1	;ilo�� na x
	ml	a0,-(sp)
.xlo
	ml	a0,a2
	mw	_sizey1,d2 ;q [16*8]-1,d2	;x*depth
.cp
	mw	_depth,d5
	 ml	a2,-(sp)
.bpl	  mw	(a2),(a1)+
	IFNE	DEBUG
	add.l	#40*256,a2
	ELSE
	   add.l APG_Oneplanesize(a5),a2	;change raw to iff
	ENDIF
	 dbf	d5,.bpl
	ml	(sp)+,a2

	IFNE	DEBUG
	 lea	40(a2),a2
	ELSE
	 add.l	APG_ByteWidth(a5),a2	;nextline lea	IffSize(a2),a2
	ENDIF

	dbf	d2,.cp

	addq.l	#2,a0
	dbf	d1,.xlo
	ml	(sp)+,a0
	add.l	_pci,a0
			;add.l	#IffSize*8*16-IffSize,a0
	dbf	d0,.ylo

	q0 d0
	rts

.wrong
	q -1,d0
	rts


*********************************************
DoRawCut:	;cut to pices output in raw.

;zamienia bitmape w iff na kolejne klocki
IffSize:	equ	40	;wielko�� obrazkax
RawSize:	equ	2*16	;wielko�� rawa! 1klocka!

;in d0 there is how many y times to do

.ylo
;--------------------1 line of piece ok
	ml	a0,-(sp)
	q0 d1
	mw	_sizex,d1	;q [IffSize/2]-1,d1	;ilo�� na x
.xlo
	ml	a0,a2

;----------------------------1 piece ok
	mw	_depth,d2
.bplloop:
	ml	a2,-(sp)
	mw	_sizey1,d3	;y size
.cp	 mw	(a2),(a1)+
	IFNE	DEBUG
	add.l	#40,a2
	ELSE
	  add.l	APG_ByteWidth(a5),a2 ;	  lea	IffSize(a2),a2
	ENDIF
	 dbf	d3,.cp
	ml	(sp)+,a2
	 add.l APG_Oneplanesize(a5),a2
	dbf	d2,.bplloop

	;add.l	_masksize,a1	;skip mask

;----------------------------1 piece ok
	addq.l	#2,a0
	dbf	d1,.xlo

;--------------------1 line of piece ok
	ml	(sp)+,a0

	add.l	_pci,a0	;scrX*ycut

	dbf	d0,.ylo

;all ok
	q0 d0
	rts


**********************************Prefs******************************
;;
raf_Prefs:

ysize:		dc.w	16	;size of y piece
interleaved:	dc.b	0	;what caind of output do you want ?
				;0 raw 1 interleaved!
**********************************Prefs******************************

	even
_pci:	dc.l	0	;APG_ByteWidth*ysize for add
_depth:	dc.w	0	;-1
_sizex:	dc.w	0	;screen bytes/2 -1
_sizey1:	dc.w	0	;sizey-1 ysize of block


	;	move.l	APG_CheckFirstWord(a5),a6
	;	jsr	(a6)		;? what is this ?

;you must call this routine before you use APG_Bpl2ChunkyLine
;this is just a init routine

	;	move.l	APG_Bpl2ChunkyLine(a5),a6 ;?
	;	jsr	(a6)		;& this

;read a single line of chunkys from the bitmap
;from xbrush1 to xbrush2
;d0 y
;a0 chunkybuffer

	;	jsr	APR_DoProcess(a5) ;& that


Abort:
		bsr	FreeMem

		moveq	#APSE_ABORT,d3
		rts

;--------------------------------------------------------------------------
rd_Prefsrout:		;config!!!!
;;init Prefs
		moveq	#0,d0
		move.b	interleaved(pc),d0
		lea	in_MX(pc),a0
		move.l	d0,(a0)
		move.w	ysize(pc),d0
		lea	pysize(pc),a0
		move.l	d0,(a0)
		
;---------------------------------------------------------------------------

		move.l	#WindowTags,APG_WindowTagList(a5) ;Tag list for the Window
		move.l	#WinWG+4,APG_WGadgets(a5)	;addr. of WA_Gadgets Tag + 4
		move.l	#winSC+4,APG_WScreen(a5)	;addr. of WA_CustomScreen Tag + 4
		move.l	#rd_window,APG_WinWnd(a5)	;a point for the Window
		move.l	#rd_Glist,APG_Glist(a5)		;a point for the Glist
		move.l	#NTypes,APG_NGads(a5)		;the NGads list
		move.l	#Gtypes,APG_GTypes(a5)		;the GTypes list
		move.l	#Gtags,APG_Gtags(a5)		;the Gtags list
		move.w	#4,APG_CNT(a5)			;the number of Gadgets
		move.l	#rd_gadarray,APG_Gadgets(a5)	;a pointer for the Gadgetsarry, size=4*number of Gadgets
		move.l	APG_Scr(a5),APG_ScrBase(a5)	;the Screenpointer , stored in APG_Scr
	
		move.l	APG_CheckMainWinPos(a5),a6	;returns d0,d1 Pos
		jsr	(a6)

		add.w	#70,d0
		add.w	#100,d1
		
		move.w	d0,APG_WinLeft(a5)		;the left pos. of the Win
		move.w	d1,APG_WinTop(a5)		;the top pos. of the Win
		move.w	#160,APG_WinWidth(a5)		;the width of the Win
		move.w	#90,APG_WinHeight(a5)		;the height of the Win

		move.l	#winL,APG_WinL(a5)		;addr. of WA_Left Tag 
		move.l	#winT,APG_WinT(a5)		;addr. of WA_Top Tag
		move.l	#winW,APG_WinW(a5)		;addr. of WA_Width Tag
		move.l	#winH,APG_WinH(a5)		;addr. of WA_Height Tag

		move.l	APG_Openwindow(a5),a6
		jsr	(a6)

		lea	Text0(pc),a2
		move.w	#Project0_TNUM,APG_TNUM(a5)
		move.l	APG_Writetext(a5),a6
		jsr	(a6)

;		move.b	rd_Format(pc),APG_Mark1(a5)	;backup the prefs
		move.l	ysize(pc),APG_Free1(a5)
		
rd_wait:	move.l	rd_window(pc),a0
		move.l	APG_Wait(a5),a6
		jsr	(a6)				;Handle input

		cmp.l	#CLOSEWINDOW,d4
		beq	rd_cancel
		cmp.l	#GADGETUP,d4
		beq.b	rd_gads
		cmp.l	#GADGETDOWN,d4
		beq.b	rd_gads
		bra.b	rd_wait

rd_gads:
		moveq	#0,d0
		move.w	38(a4),d0

		cmp.w	#GD_format,d0
		beq.b	handleformat
		cmp.w	#GD_ysize,d0
		beq	handlesize
		cmp.w	#GD_Ok,d0
		beq.b	rd_cancel\.wech
		cmp.w	#GD_Cancel,d0
		bne.b	rd_wait
;get settings back
rd_cancel:
		move.w	APG_Free1(a5),d0
		ext.w	d0
		ext.l	d0
		move.l	d0,pysize
		move.b	APG_Free1+2(a5),d0
		ext.w	d0
		ext.l	d0
		move.b	d0,interleaved
		move.l	d0,in_MX
.wech:
		move.l	rd_window(pc),a0
		move.l	APG_Intuitionbase(a5),a6
		JUMP	CloseWindow

handleformat:
		move.b	d5,interleaved
		ext.w	d5
		ext.l	d5
		move.l	d5,in_MX
		bra.w	rd_wait

handlesize:
		move.l	#GD_ysize,d0	
		lea	rd_gadarray(pc),a0
		move.l	(a0,d0.l*4),a0
		move.l	rd_window(pc),a1
		sub.l	a2,a2
		lea	rd_taglist,a3		
		move.l	APG_Gadtoolsbase(a5),a6
		jsr	_LVOGT_GetGadgetAttrsA(a6)
		move.l	rd_number(pc),d0
		move.w	d0,ysize
		bra	rd_wait
		
rd_taglist:
		dc.l	GTIN_Number,rd_number
		dc.l	TAG_DONE

rd_number:	dc.l	0
;--------------------------------------------------------------------------------
	even
WindowTags:
winL:	dc.l	WA_Left,212
winT:	dc.l	WA_Top,78
winW:	dc.l	WA_Width,220
winH:	dc.l	WA_Height,85
	dc.l	WA_IDCMP,IDCMP_CLOSEWINDOW!MXIDCMP!GADGETUP!VANILLAKEY!BUTTONIDCMP!IDCMP_REFRESHWINDOW
	dc.l	WA_Flags,WFLG_CLOSEGADGET!WFLG_DEPTHGADGET!WFLG_ACTIVATE!WFLG_DRAGBAR!WFLG_SMART_REFRESH!WFLG_RMBTRAP
WinWG:	dc.l	WA_Gadgets,0
	dc.l	WA_Title,WinWTitle
winSC:	dc.l	WA_CustomScreen,0
	dc.l	TAG_DONE

WinWTitle:	dc.b	'Rafik Options',0
	even

GD_format	=	0
GD_ysize	=	1
GD_Ok		=	2
GD_Cancel	=	3

Gtypes:
		dc.w	MX_KIND
		dc.w	INTEGER_KIND
		dc.w	BUTTON_KIND
		dc.w	BUTTON_KIND
Gtags:
		dc.l	GTMX_Labels,Gadget00Labels
		dc.l	GTMX_Active
in_MX:		dc.l	0
		dc.l	TAG_DONE
		dc.l	GTIN_Number
pysize:		dc.l	16
		dc.l	TAG_DONE
		dc.l	TAG_DONE
		dc.l	TAG_DONE
Gadget00Labels:
		dc.l	Gadget00Lab0
		dc.l	Gadget00Lab1
;		dc.l	t_mask
		dc.l	0

Gadget00Lab0:    DC.B    'Interleawed',0
Gadget00Lab1:    DC.B    'Raw',0

;???????????????
;t_mask:	dc.b	'Add Mask ?',0

		even

NTypes:
		DC.W    29,25,17,9
		DC.L    0,0
		DC.W    GD_format
		DC.L    PLACETEXT_RIGHT,0,0
		DC.W    29,50,40,13
		DC.L    sizetext,0
		DC.W    GD_ysize
		DC.L    PLACETEXT_RIGHT,0,0
		DC.W    10,70,60,13
		DC.L    oktext,0
		DC.W    GD_Ok
		DC.L    PLACETEXT_IN,0,0
		DC.W    85,70,60,13
		DC.L    canceltext,0
		DC.W    GD_Cancel
		DC.L    PLACETEXT_IN,0,0

oktext:		dc.b	'Ok',0
canceltext:	dc.b	'Cancel',0
sizetext:	dc.b	'y size',0

	even

Text0:
		DC.B    2,0
		DC.B    RP_JAM1
		DC.B    0
		DC.W    80,10
		DC.L    0
		DC.L    Project0IText0
		DC.L    0

Project0_TNUM EQU 1

Project0IText0:
		DC.B    'Save format',0
savebmpmsg:	dc.b	'Save a rdst image!',0




	IFNE	DOREQUEST
	even
***************
Hex2Ascii:	;na hexy!
	q 8-1,d7
.l
	rol.l	#4,d0
	mw	d0,d1
	and.w	#$f,d1
	move.b	HexT(pc,d1.w),(a0)+

	dbf	d7,.l
	rts

HexT:
	dc.b	'0123456789abcdef'
	even


__request:	dc.l	0

DoRequest:
;>>>>>test
;----------example request
	pusha

	ml	a5,string

	lea	APG_PicMap(a5),a4
	ml	a4,string+4


	q0 d0
	 mw APG_ImageHeight(a5),d0	;y size
	ml	d0,string

	q0 d0
	 mw APG_ImageWidth(a5),d0	;y size
	ml	d0,string+4

	q0 d0
	 mb APG_Planes(a5),d0	;y size
	ml	d0,string+8

	q0 d0
	lea	t_qw,a0
	ml	APG_Picmem(a5),d0	;adres
	bsr.w	Hex2Ascii

	EXEC
	lea	rq_Name,a1
	CALL	OldOpenLibrary
	ml	d0,b_rq

	movel	rq
	q0 d0
	CALL	rq_AllocRequest
	ml	d0,__request

	lea	TestTxt,a0
	lea	Response,a1
	lea	Tags,a2
	lea	string,a4
	ml __request,d0 ;lea RequestStr,a5 ;TO POWINNO BY� ZAALOCOWANE!
	CALL	rq_Request
			;jsr	-42(a6)	;rq_Request

	ml __request,d0 ;lea RequestStr,a5 ;TO POWINNO BY� ZAALOCOWANE!
	CALL	rq_FreeRequest

	popa
	rts
Tags:	;dc.l	rqLOGO,rq_logo
	dc.l	rqWindowTitle,title
	dc.l	rqScreenTitle,title
	dc.l	rqStringMax,10

	dc.l	0

b_rq:	dc.l	0

string:	dc.l	Response
	dc.l	1
	dc.l	0

Response:	dc.b	'ok|ok',0
title:	dc.b	'Wersja testowa!!',0

TestTxt:	dc.b	'Request Library',$a
	dc.b	'structre = %ld',$a
	dc.b	'bitmap adr = %ld',$a
	dc.b	'%ld',$a
	dc.b	'picmem=$'
t_qw:
	dc.b	'xxxxxxxx'


	dc.b	0,0

rq_Name:	dc.b	'request.library',0

	even

	ENDIF


;		section	map,BSS
;rd_rgbmap:	ds.b	48
;rd_Farbtab8pure:ds.l	256





		IFNE	DOREQUEST

;definicja tag�w
tu set $80000000

tag:	macro
\1=tu
tu set tu+1
	endm

;r request ;*nie zrobione
;nic dla wszystkich..
;--------------------------tags definition

	tag	rqLOGO	;structure image
	tag	rqWindowB
	tag	rqWindowPtr
	tag	rqWindowTitle
	tag	rqScreenTitle
	tag	rqScreenB
	tag	rqScreenPtr	;*
	tag	rqCenterScreen
	tag	rqLockWindow	;*
	tag	rqMouseMove	;R*
	tag	rqCloseGad	;*
	tag	rqString	;s adres textu (buffor dla string gad..)
	tag	rqStringMax	;s maxymalna ilo�� tekstu


;--------------------------tags definition
;--------------------------jumps
to set -30
cal:	macro
_\1=to
to set to-6
	endm

	cal	rq_AllocRequest
	cal	rq_FreeRequest
	cal	rq_Request
	cal	rq_OpenLib

;--------------------------jumps
;--------------------------b��dy


	ENDIF

	IFNE	DEBUG

	SECTION	Public,DATA_P

KlockiIff:
	incdir	hd1:Diamonds/pic/
	incbin	Diamonds_stworki.raw
	;inciff	Diamonds_stworki.pi

	ENDIF



