����  $,�&b�&b�&b�&b�&b�&b�&b�&b;=t�;-------------------------------------------------------------------------
;
;		GIF saver modul for ArtPRO
;		(c) 1996 ,Frank Pagels (Crazy Copper) /DFT
;
;		28.08.1996
;
;
		incdir 	include:

		include	exec/exec_lib.i
		include	exec/lists.i
		include	exec/memory.i
		include intuition/intuition_lib.i
		include	libraries/wb_lib.i
		include	graphics/graphics_lib.i
		Include utility/tagitem.i
		include	dos/dos.i		
		include	dos/dos_lib.i		
		include	libraries/reqtools_lib.i

		include misc/artpro.i
		
		SAVERHEADER GF_NODE

		dc.b	'$VER: GIF saver module 0.3 (05.09.96)',0
	even
;---------------------------------------------------------------------------
GF_NODE:
		dc.l	0,0
		dc.b	0,0
		dc.l	GF_name
		dc.l	GF_save
		dc.b	'GIFS'		;saverkennung f�r ArtPRO
		dc.b	'EXTR'		;for extern saver
		dc.l	0
		dc.l	GF_Tags
GF_name:
		dc.b	'GIF',0
	even
;---------------------------------------------------------------------------

GF_Tags:
		dc.l	APT_Creator,GF_Creator
		dc.l	APT_Version,1
		dc.l	APT_Info,GF_Info
		dc.l	0

GF_Creator:
		dc.b	'(c) 1996 Frank Pagels /DEFECT Softworks',0
	even
GF_Info:
		dc.b	'GIF saver',10,10
		dc.b	'Saves an image as GIF. The formats GIF87',10
		dc.b	'and GIF89 are supportet!',0
	even
;----------------------------------------------------------------------------

GF_Puffermem	=	APG_Free1
codesize	=	APG_Free2	;code gr��e mit der gerade codiert wird
clearcode	=	APG_Free3	;clear code wenn 12 bits erreicht sind
lastcode	=	APG_Free4	;letzter code befor die codesize erh�ht wird
nextcode	=	APG_Free5
EOFCode		=	APG_Free6
puffermerk	=	APG_Free7
blockcounter	=	APG_Free8	;.w
GF_linecounter	=	APG_Free8+2
GF_Chunkymem	=	APG_Free9
GF_chunkylen	=	APG_Free10
GF_aktbyte	=	APG_Free11
GF_bytemask	=	APG_Free11+2

;----------------------------------------------------------------------------
GF_save:


;Berechne ben�tigten Speicher

		tst.w	APG_Cutflag(a5)
		bne.b	.brushok
		move.w	#0,APG_xbrush1(a5)	;Es soll das ganze 
		move.w	#0,APG_ybrush1(a5)	;Pic abgesaved werden
		move.w	APG_ImageWidth(a5),APG_xbrush2(a5)
		move.w	APG_ImageHeight(a5),APG_ybrush2(a5)	
.brushok:
		moveq	#0,d0
		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
		move.l	d0,d1
		add.l	#15,d1
		lsr.l	#4,d1		;breite in worte
		move.w	d1,APG_Brushword(a5)

		add.l	#256*3+100,d0
		move.l	#MEMF_ANY,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)

		tst.l	d0
		bne	.memok

		move.l	#APSE_NOMEM,d3
		rts

.memok:
		move.l	d0,GF_Puffermem(a5)

		move.l	d0,a0

		add.l	#256*3+100,d0
		move.l	d0,GF_Chunkymem(a5)
		
		jsr	APR_OpenProcess(a5)

		moveq	#0,d0
		move.w	APG_ybrush2(a5),d0
		sub.w	APG_ybrush1(a5),d0
		lea	savemsg(pc),a1
		jsr	APR_InitProcess(a5)

	;*---------- Init Header --------------

		move.l	GF_Puffermem(a5),a0
		move.l	#'GIF8',(a0)+
		move.w	#'7a',(a0)+

		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
		rol.w	#8,d0
		move.w	d0,(a0)+
		move.w	APG_ybrush2(a5),d0
		sub.w	APG_ybrush1(a5),d0
		rol.w	#8,d0
		move.w	d0,(a0)+

		moveq	#0,d0
		or.b	#%10000000,d0	;Global Colormap

		moveq	#0,d1
		move.b	APG_Planes(a5),d1
		subq.b	#1,d1

		or.b	d1,d0
		lsl.b	#4,d1
		or.b	d1,d0

		move.b	d0,(a0)+	;Packed Bits

		clr.b	(a0)+		;Background Color Index
		clr.b	(a0)+		;Pixel Aspect Ratio

	;*---- Schreibe Colormap -----*

		move.w	APG_Colorcount(a5),d7
		subq.w	#1,d7

		lea	APG_Farbtab8+4(a5),a1

.nc		move.l	(a1)+,d0
		move.l	(a1)+,d1
		move.l	(a1)+,d2

		rol.l	#8,d0
		rol.l	#8,d1
		rol.l	#8,d2

		move.b	d0,(a0)+
		move.b	d1,(a0)+
		move.b	d2,(a0)+

		dbra	d7,.nc

	;*----- Write Image Descriptor ----*

		move.b	#$2c,(a0)+	;Image Separator

		clr.l	(a0)+		;Image left and right position
		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
		rol.w	#8,d0
		move.w	d0,(a0)+
		move.w	APG_ybrush2(a5),d0
		sub.w	APG_ybrush1(a5),d0
		rol.w	#8,d0
		move.w	d0,(a0)+

		clr.b	(a0)+		;packed Bitfield, ersma null

		move.b	APG_Planes(a5),(a0)+	;LZW Minimum Code Size

		move.l	GF_Puffermem(a5),d2
		sub.l	d2,a0
		move.l	a0,d3
		move.l	APG_Filehandle(a5),d1
		move.l	APG_Dosbase(a5),a6
		jsr	_LVOWrite(a6)

	;--- L�sche Puffer ----

		move.l	#((256*3+100)/4)-1,d7
		move.l	GF_Puffermem(a5),a0
.npc		clr.l	(a0)+
		dbra	d7,.npc
		
		
	;*---- Init Knotentab ----*

BITS			=	12
MAX_CODE		=	((1<<BITS)-1)
HSIZE			=	5021	/* hash table size for 80% occupancy */
UNBENUTZT		=	-1


		lea	GF_Knotentab,a0
		move.l	#HSIZE-1,d7
.nc		move.w	#UNBENUTZT,code_wert(a0)
		lea	tablen(a0),a0
		dbra	d7,.nc


	;*---- Beginne mit Codierung ----*

;Init Code werte

		moveq	#0,d0
		move.b	APG_Planes(a5),d0

		moveq	#1,d1
		lsl.w	d0,d1
		move.l	d1,clearcode(a5)
		
		addq	#1,d1
		move.l	d1,eofcode(a5)

		addq.l	#1,d0
		move.l	d0,codesize(a5)

		moveq	#1,d1
		lsl.w	d0,d1
		subq.l	#1,d1
		move.l	d1,lastcode(a5)

;----------------------------------------------------------------------------

		move.l	APG_Checkfirstword(a5),a6
		jsr	(a6)		;vorbereitung f�r bpl2chunkyline

		move.w	APG_ybrush1(a5),GF_linecounter(a5)
		
		moveq	#0,d0
		move.w	GF_Linecounter(a5),d0
		move.l	GF_Chunkymem(a5),a0
		move.l	APG_Bpl2ChunkyLine(a5),a6
		jsr	(a6)
		
		move.l	GF_Chunkymem(a5),a3
		move.l	GF_Puffermem(a5),a4
				
		moveq	#0,d0
		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
	;	subq.w	#1,d0
		move.l	d0,GF_chunkylen(a5)
		
		move.b	#$fe,(a4)+	;blockgr��e
		
		move.w	#1,blockcounter(a5)

		move.l	eofcode(a5),d0
		addq.l	#1,d0
		move.l	d0,nextcode(a5)		;Erster_Code

		clr.w	GF_aktbyte(a5)		;aktuelles Byte
		move.w	#1,GF_bytemask(a5)	;Bytemaske

		move.l	clearcode(a5),d1
		bsr	GF_writebits

		moveq	#0,d0
		move.b	(a3)+,d0	;neues Zeichen gelesen, stringcode
		subq.l	#1,GF_Chunkylen(a5)
	;	bpl	.nixend

gf_nextchar:
		moveq	#0,d4
		move.b	(a3)+,d4	;zeichen
		subq.l	#1,GF_Chunkylen(a5)
		bpl	.nixend		

	;Hole eine neue Zeile

		move.l	d0,-(a7)

		moveq	#0,d0
		add.w	#1,GF_Linecounter(a5)
		move.w	GF_Linecounter(a5),d0
		cmp.w	APG_ybrush2(a5),d0
		bls	.noe
		move.l	(a7)+,d0
		bra	GF_wech
.noe		move.l	GF_Chunkymem(a5),a0
		move.l	APG_Bpl2ChunkyLine(a5),a6
		jsr	(a6)

		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
	;	subq.w	#1,d0
		move.l	d0,GF_chunkylen(a5)

		move.l	GF_Chunkymem(a5),a3
		move.b	(a3)+,d4	;neues Zeichen gelesen, stringcode
		subq.l	#1,GF_Chunkylen(a5)
		jsr	APR_DoProcess(a5)

		move.l	(a7)+,d0
.nixend
		bsr	suche_kind_knoten

		move.w	code_wert(a0),d1
		cmp.w	#UNBENUTZT,d1
		beq	.newcode		;Code gefunden
		move.l	d1,d0		;string_code=zeichen

		bra	gf_nextchar

;wir m�ssen einen neuen Code eintragen
.newcode:
		move.l	d0,d1

		bsr	GF_writebits

		move.l	nextcode(a5),d1
		cmp.l	lastcode(a5),d1
		bls	.nixlast

		addq.l	#1,codesize(a5)
		
		move.l	lastcode(a5),d1
		lsl.l	#1,d1
		or.l	#1,d1
		move.l	d1,lastcode(a5)
.nixlast:

		move.l	nextcode(a5),d1
		move.w	d1,code_wert(a0)
		addq.l	#1,nextcode(a5)			
		move.l	d0,eltern_code(a0)	;string_code
		move.w	d4,zeichen(a0)

		move.l	d4,d0		;string_code=zeichen

		cmp.l	#MAX_CODE,nextcode(a5)
		bne	.codeok

;--- Tabelle l�schen und clearcode ausgeben ---

		move.l	d0,-(a7)

		move.l	clearcode(a5),d1
		bsr	GF_writebits

		lea	GF_Knotentab,a0
		move.l	#HSIZE-1,d7
.nc		move.w	#UNBENUTZT,code_wert(a0)
		lea	tablen(a0),a0
		dbra	d7,.nc

		move.l	eofcode(a5),d0
		addq.l	#1,d0
		move.l	d0,nextcode(a5)		;ERSTER_CODE

		moveq	#0,d0
		move.b	APG_Planes(a5),d0

		addq.l	#1,d0
		move.l	d0,codesize(a5)

		moveq	#1,d1
		lsl.w	d0,d1
		subq.l	#1,d1
		move.l	d1,lastcode(a5)

		move.l	(a7)+,d0

.codeok
		bra	gf_nextchar

GF_wech:
		move.l	d0,d1
		bsr	GF_Writebits
		move.l	eofcode(a5),d1
		bsr	GF_Writebits

		move.l	APG_Dosbase(a5),a6

		move.w	blockcounter(a5),d0
		cmp.w	#1,d0
		beq	.nixwrite

		move.l	APG_Filehandle(a5),d1
		move.l	GF_Puffermem(a5),d2
		move.l	d2,a0
		move.b	d0,(a0)
		sub.b	#1,(a0)
		move.l	d0,d3
		jsr	_LVOWrite(a6)
.nixwrite:
		move.l	APG_Filehandle(a5),d1
		lea	endstring(pc),a0
		move.l	a0,d2
		moveq	#2,d3
		jsr	_LVOWrite(a6)


		move.l	GF_Puffermem(a5),a1
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)

		jsr	APR_ClearProcess(a5)
		
		rts

endstring:	dc.b	0,';'

;--------------------------------------------------------------------------
;---- Kucke ob es den String schon gibt ? ---------------------------------
;--------------------------------------------------------------------------
;> d0 = Eltern_Code d4=Kindzeichen
;< d1 = Index

suche_kind_knoten:

		movem.l	d0/d4/a3/a4,-(a7)

		move.l	d4,d1		;Index
		lsl.l	#BITS-8,d1
		eor.l	d0,d1
		bne	.nozero		;Index=0 ?
		moveq	#1,d2		;Offset
		bra	.such
.nozero		move.l	#HSIZE,d2
		sub.l	d1,d2		;Offset=HSize-index
.such
		lea	GF_Knotentab,a0
		lea	(a0,d1*8),a0	;tab[Index]

		cmp.w	#UNBENUTZT,code_wert(a0)
		beq	such_end
		cmp.l	eltern_code(a0),d0
		bne	.nixgleich
		cmp.w	zeichen(a0),d4
		beq	such_end
.nixgleich:
		sub.l	d2,d1		;index-offset
		bpl	.such		;index<0 ?
		add.l	#HSize,d1	;index+TABELLEN_GROESSE
		bra	.such

such_end:	movem.l	(a7)+,d0/d4/a3/a4
		rts


;-- Gebe Bits aus --
;d1= Code d2=Code_bits d6=aktuelles Byte a4=output 

GF_writebits:
		move.w	GF_aktbyte(a5),d6
		move.w	GF_bytemask(a5),d5

		move.l	codesize(a5),d2
		subq	#1,d2
		moveq	#1,d7	;maske
.nextbit:
		move.w	d1,d3
		and.w	d7,d3	;teste Bit
		beq	.nobit
		or.b	d5,d6	;setze bit
.nobit:
		lsl.b	#1,d5
		bne	.byteok
		move.b	d6,(a4)+

		addq.w	#1,blockcounter(a5)
		cmp.w	#$ff,blockcounter(a5)
		bne	.nixblock

;----- Block Speichern -----------

		movem.l	d0-d3,-(a7)
		move.l	APG_Filehandle(a5),d1
		move.l	GF_Puffermem(a5),d2
		move.l	#$ff,d3
		move.l	APG_Dosbase(a5),a6
		jsr	_LVOWrite(a6)
		movem.l	(a7)+,d0-d3

		move.w	#1,blockcounter(a5)
		move.l	GF_Puffermem(a5),a4
		move.b	#$fe,(a4)+

.nixblock:
		moveq	#0,d6
		moveq	#1,d5
.byteok:
		lsl.w	#1,d7
		dbra	d2,.nextbit

		move.w	d6,GF_aktbyte(a5)
		move.w	d5,GF_bytemask(a5)

		rts


;----------------------------------------------------------------------------

savemsg:	dc.b	'Save a GIF image!',0
	RSRESET
	
code_wert	rs.w	1
eltern_code	rs.l	1
zeichen		rs.w	1
tablen		rs.b	0


		section	daten,BSS

GF_Knotentab:
		ds.b	5021*tablen
