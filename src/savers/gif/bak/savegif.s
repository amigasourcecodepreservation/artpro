����  z                                    ;==========================================================================
;
;
;		Test f�r einen GIF-Saver
;
;	� 1996 Defect SoftWorks
;
;	11.02.1996
;	24.04.1996	;erster test



breite	=	160
hoehe	=	160
colors	=	256
planes	=	8

c24to3Byte	macro
		move.l	\1,\2
		and.l	#$0000ff00,\2
		lsr.l	#8,\2
		move.l	\1,\3
		and.l	#$000000ff,\3
		and.l	#$00ff0000,\1
		swap	\1
		endm

;----------------------------------------------------------------------
;		We have began! (FGTH)

;F�lle Header

		lea	puffer,a4
		move.l	#'GIF8',(a4)+
		move.w	#'7a',(a4)+

		move.w	#breite,d0
		bsr	makeword
		move.w	d0,(a4)+	;Screenbreite
		move.l	#hoehe,d0
		bsr	makeword
		move.w	d0,(a4)+	;Screenhoehe
				
		move.b	#$f7,(a4)+	;Flags
		move.b	#0,(a4)+	;Background Color
		move.b	#0,(a4)+	;Pixel Ratio		
		
;write Colormap

		lea	colortab,a0
		move.l	#colors-1,d7

.nc		move.l	(a0)+,d0

		c24to3Byte d0,d1,d2

		move.b	d0,(a4)+
		move.b	d1,(a4)+
		move.b	d2,(a4)+

		dbra	d7,.nc

		move.b	#$2c,(a4)+	;Image Separator --> anfang eines
					;		     neuen Bildes
;write Image Descriptor

		move.w	#0,(a4)+	;Image Left Position
		move.w	#0,(a4)+	;Image Top Position

		move.w	#breite,d0
		bsr	makeword
		move.w	d0,(a4)+	;Image Breite
		move.w	#hoehe,d0
		bsr	makeword
		move.w	d0,(a4)+	;Image H�he

		move.b	#7,(a4)+	;Diverse Flags

		move.b	#planes,(a4)+

;---- Init Knotentab ----

UNBENUTZT	=	-1

		lea	knotentab,a0
		move.l	#HSIZE-1,d7
.nc		move.w	#UNBENUTZT,code_wert(a0)
		lea	tablen(a0),a0
		dbra	d7,.nc

;---- Beginne mit Codierung ----

BITS			=	12
MAX_CODE		=	((1<<BITS)-1)
HSIZE			=	5021	/* hash table size for 80% occupancy */


ERSTER_CODE		=	258

;Init Code werte

		lea	datas,a5

		move.l	#planes,d0

		moveq	#1,d1
		lsl.w	d0,d1
		move.l	d1,clearcode(a5)
		
		addq	#1,d1
		move.l	d1,eofcode(a5)

		addq.l	#1,d0
		move.l	d0,codesize(a5)

		moveq	#1,d1
		lsl.w	d0,d1
		subq.l	#1,d1
		move.l	d1,lastcode(a5)

;----------------------------------------------------------------------
		move.l	a4,puffermerk(a5)

		lea	sourceimage,a3
		lea	blockpuffer,a4
				
		move.l	#sourcelen-1,filelen(a5)
		
		move.b	#$fe,(a4)+	;blockgr��e
		
		move.w	#1,blockcounter(a5)

		move.l	#ERSTER_CODE,nextcode(a5)

		clr.l	aktbyte(a5)		;aktuelles Byte
		move.l	#1,bytemask(a5)		;Bytemaske

		move.l	clearcode(a5),d1
		bsr	writebits

		moveq	#0,d0
		move.b	(a3)+,d0	;neues Zeichen gelesen, stringcode
		subq.l	#1,filelen(a5)
		bne	.nixend
		move.l	eofcode(a5),d0 ;string_code
.nixend
gf_nextchar:
		moveq	#0,d4
		move.b	(a3)+,d4	;zeichen
		subq.l	#1,filelen(a5)
		beq	gif_wech		

		bsr	suche_kind_knoten

		move.w	code_wert(a0),d1
		cmp.w	#UNBENUTZT,d1
		beq	.newcode		;Code gefunden
		move.l	d1,d0		;string_code=zeichen
		bra	gf_nextchar

;wir m�ssen einen neuen Code eintragen
.newcode:
		move.l	nextcode(a5),d1
		move.w	d1,code_wert(a0)
		addq.l	#1,nextcode(a5)			
		move.l	d0,eltern_code(a0)	;string_code
		move.w	d4,zeichen(a0)

		move.l	d0,d1
		bsr	writebits

		move.l	d4,d0		;string_code=zeichen

		cmp.l	#MAX_CODE,nextcode(a5)
		bls	.codeok

;--- Tabelle l�schen und clearcode ausgeben ---

		move.l	d0,-(a7)

		move.l	clearcode(a5),d1
		bsr	writebits

		lea	knotentab,a0
		move.l	#HSIZE-1,d7
.nc		move.w	#UNBENUTZT,code_wert(a0)
		lea	tablen(a0),a0
		dbra	d7,.nc
		move.l	(a7)+,d1

		move.l	#ERSTER_CODE,nextcode(a5)
		move.l	#planes,d0

		addq.l	#1,d0
		move.l	d0,codesize(a5)

		moveq	#1,d1
		lsl.w	d0,d1
		move.l	d1,lastcode(a5)

		move.l	(a7),d0

		bra	gf_nextchar
.codeok
		move.l	nextcode(a5),d1
		cmp.l	lastcode(a5),d1		;naechster_code > naechste_erhoehung
		bls	gf_nextchar

		addq.l	#1,codesize(a5)
		
		move.l	lastcode(a5),d1
		lsl.l	#1,d1
		or.l	#1,d1
		move.l	d1,lastcode(a5)
		bra	gf_nextchar

gif_wech:
		bsr	writebits
		move.l	eofcode(a5),d1
		bsr	writebits
		

		illegal

		rts
;--------------------------------------------------------------------------
;---- Kucke ob es den String schon gibt ? ---------------------------------
;--------------------------------------------------------------------------
;> d0 = Eltern_Code d4=Kindzeichen
;< d1 = Index

suche_kind_knoten:

		movem.l	d0/d4/a3/a4,-(a7)

		move.l	d4,d1		;Index
		lsl.l	#BITS-8,d1
		eor.l	d0,d1
		bne	.nozero		;Index=0 ?
		moveq	#1,d2		;Offset
		bra	.such
.nozero		move.l	#HSIZE,d2
		sub.l	d1,d2		;Offset=HSize-index
.such
		lea	knotentab,a0
		lea	(a0,d1*8),a0	;tab[Index]

		cmp.w	#UNBENUTZT,code_wert(a0)
		beq	such_end
		cmp.l	eltern_code(a0),d0
		bne	.nixgleich
		cmp.w	zeichen(a0),d4
		beq	such_end
.nixgleich:
		sub.l	d2,d1		;index-offset
		bpl	.such		;index<0 ?
		add.l	#HSize,d1	;index+TABELLEN_GROESSE
		bra	.such

such_end:	movem.l	(a7)+,d0/d4/a3/a4
		rts


;-- Gebe Bits aus --
;d1= Code d2=Code_bits d6=aktuelles Byte a4=output 

writebits:
		move.l	aktbyte(a5),d6
		move.l	bytemask(a5),d5

		move.l	codesize(a5),d2
		subq	#1,d2
		moveq	#1,d0	;maske
.nextbit:
		move.w	d1,d3
		and.w	d0,d3	;teste Bit
		beq	.nobit
		or.b	d5,d6	;setze bit
.nobit:
		lsl.b	#1,d5
		bne	.byteok
		move.b	d6,(a4)+

		addq.w	#1,blockcounter(a5)
		cmp.w	#$ff,blockcounter(a5)
		bne	.nixblock

;----- Block Speichern -----------

		move.l	puffermerk(a5),a0
		move.w	#$fe,d7
		lea	blockpuffer,a4
		move.l	a4,a1
.np		move.b	(a1)+,(a0)+
		dbra	d7,.np
		move.w	#1,blockcounter(a5)
		move.l	a0,puffermerk(a5)

		move.b	#$fe,(a4)+

.nixblock:
		moveq	#0,d6
		moveq	#1,d5
.byteok:
		lsl.w	#1,d0
		dbra	d2,.nextbit

		move.l	d6,aktbyte(a5)
		move.l	d5,bytemask(a5)

		rts
		
makeword:
;> d0
;< d0
;1122 --> 2211
		move.l	d0,d1	;1122
		lsl.w	#8,d0	;2200
		lsr.w	#8,d1	;0011
		move.b	d1,d0	;2211
		rts


sourceimage:
		incbin	sources:gif/save/test160x160.chunky

sourcelen	=	*-sourceimage

blockpuffer:	ds.b	300

puffer:
		ds.b	25000

	rsreset
	
code_wert	rs.w	1
eltern_code	rs.l	1
zeichen		rs.w	1
tablen		rs.b	0

knotentab:
		ds.b	5021*tablen
pufferend


	rsreset

datas:

codesize:	rs.l	1	;code gr��e mit gerade codiert wird
clearcode	rs.l	1	;clear code wenn 12 bits erreicht sind
lastcode:	rs.l	1	;letzter code befor die codesize erh�ht wird
nextcode:	rs.l	1
EOFCode		rs.l	1	;ende des data streams

blockcounter:	rs.w	1	;z�hlt eintrage einem block
puffermerk:	rs.l	1

n_bits		rs.l	1

filelen:	rs.l	1

aktbyte:	rs.l	1
bytemask:	rs.l	1

datalen		rs.w	0
	
		ds.b	datalen


colortab:
		dc.w	$0029,$2222,$0031,$2A2A,$0039,$2929,$0035,$3931
		dc.w	$003E,$3031,$0038,$3B39,$0043,$3736,$0044,$3C3A
		dc.w	$004A,$3838,$004F,$3B38,$004D,$3F3E,$005A,$3839
		dc.w	$0050,$4A38,$0051,$4243,$004F,$4A41,$0060,$3F38
		dc.w	$005A,$4043,$005A,$4A3E,$0051,$4F44,$0063,$4143
		dc.w	$0051,$4F4C,$005A,$523F,$0068,$463B,$005F,$4A48
		dc.w	$0059,$5249,$0073,$3F3A,$0064,$523F,$0059,$544F
		dc.w	$0053,$4F5A,$0069,$4947,$0073,$4143,$0068,$5247
		dc.w	$007B,$403A,$0062,$5651,$005A,$565A,$0076,$4A41
		dc.w	$0075,$5241,$007F,$4242,$0077,$4A4A,$006A,$5A4F
		dc.w	$0063,$585B,$0077,$524A,$0068,$6350,$0084,$4944
		dc.w	$0062,$625B,$0077,$5A49,$006C,$585B,$0074,$5E51
		dc.w	$006C,$615B,$0086,$5245,$008C,$4945,$0070,$6B51
		dc.w	$0069,$6B5A,$007A,$595A,$0085,$5A48,$0075,$615A
		dc.w	$008E,$4F48,$007B,$6650,$0073,$695B,$0089,$5A4D
		dc.w	$006A,$6B66,$0097,$5142,$007A,$6263,$007D,$6859
		dc.w	$008B,$6349,$0097,$514A,$0077,$6A63,$0095,$5A46
		dc.w	$0088,$6551,$0092,$595A,$0097,$5D4D,$0079,$7065
		dc.w	$0087,$6C57,$00A5,$4F43,$008D,$635C,$0084,$6A65
		dc.w	$00A6,$514B,$007A,$726D,$008C,$6E5A,$009C,$614F
		dc.w	$0098,$6C4A,$009D,$585A,$008A,$6F63,$00A6,$5A4B
		dc.w	$0083,$7B62,$0087,$726B,$00A4,$634E,$0094,$7359
		dc.w	$009F,$6459,$00A9,$575B,$0087,$7273,$008B,$7B67
		dc.w	$00AB,$644D,$0094,$7661,$00A1,$6263,$00AB,$6E49
		dc.w	$0089,$7B72,$00A0,$6B64,$0082,$7B7C,$009F,$735F
		dc.w	$00AA,$6A56,$00B5,$614A,$0094,$796D,$00A2,$7C55
		dc.w	$008B,$846E,$00B5,$6252,$00B5,$6A48,$00AD,$6164
		dc.w	$008A,$7C7B,$009E,$7B62,$009C,$796D,$00AC,$6B64
		dc.w	$00B5,$6A56,$00AB,$735F,$00B2,$7949,$008E,$807C
		dc.w	$0094,$8570,$00B6,$6063,$00B4,$7358,$00A9,$746B
		dc.w	$00B6,$5F6D,$009D,$836F,$00B5,$6B64,$0093,$857C
		dc.w	$00BD,$744C,$00B4,$7461,$00B4,$844F,$00BD,$7258
		dc.w	$009C,$837B,$00B3,$7769,$00A0,$8A72,$008D,$8691
		dc.w	$009B,$8C7C,$00BC,$844F,$00BE,$7363,$00A1,$857D
		dc.w	$00B5,$7A6D,$00BC,$7B60,$00BF,$6A6F,$00B2,$846C
		dc.w	$00B7,$717B,$00C6,$814B,$00BC,$8B53,$00A4,$8E7A
		dc.w	$00B0,$7E7B,$00BB,$7C6D,$0096,$8B8F,$00AD,$8C71
		dc.w	$00C6,$7D58,$00A4,$8C84,$00AF,$887D,$00C9,$8A4A
		dc.w	$00C6,$7C66,$00AE,$9472,$00BC,$8470,$00BE,$797C
		dc.w	$00C8,$8B53,$00A4,$9486,$00C4,$9452,$009B,$9294
		dc.w	$00BA,$857C,$00C5,$856F,$00CE,$8164,$00AD,$9682
		dc.w	$00AF,$8B8C,$00CD,$944D,$00C7,$7A7D,$00CA,$8E60
		dc.w	$00A2,$9496,$00BD,$9571,$00C0,$877D,$00C7,$8A71
		dc.w	$00CB,$955D,$00AE,$948D,$00B8,$9481,$00D6,$9250
		dc.w	$00B8,$9F79,$00D1,$9A50,$00BC,$8A8C,$00CC,$8D70
		dc.w	$00C6,$8B7D,$00B5,$9C83,$00A5,$9C98,$00D2,$9D4E
		dc.w	$00C5,$A06A,$00AD,$9B95,$00B2,$9E8C,$00D2,$8F6F
		dc.w	$00C8,$9180,$00CF,$9471,$00C6,$8A8C,$00CF,$A35C
		dc.w	$00DA,$9C4F,$00A4,$9FA6,$00CE,$937F,$00D9,$A54D
		dc.w	$00C6,$9C82,$00C1,$988E,$00B4,$A294,$00CB,$8C8D
		dc.w	$00D2,$9D6F,$00D5,$AF51,$00C0,$A18D,$00D5,$AD5B
		dc.w	$00B4,$A49C,$00DE,$AB4B,$00CE,$948D,$00D0,$9C82
		dc.w	$00D5,$AB67,$00CC,$AD75,$00BD,$A497,$00DC,$A865
		dc.w	$00B2,$A5A8,$00CF,$9C8E,$00DE,$AD5D,$00DF,$B34F
		dc.w	$00CC,$A58B,$00D6,$A976,$00BB,$AD99,$00C6,$A899
		dc.w	$00B4,$ADAA,$00E5,$B64F,$00D2,$9D98,$00D6,$A58B
		dc.w	$00DC,$A87E,$00E0,$B95A,$00DD,$BB63,$00BB,$ADAB
		dc.w	$00D4,$A595,$00E7,$BC51,$00E2,$AE78,$00DE,$A590
		dc.w	$00BE,$ACB2,$00E5,$BC64,$00D5,$AD97,$00E9,$BE59
		dc.w	$00C6,$ACAE,$00DF,$B97F,$00BC,$B5B3,$00E5,$C564
		dc.w	$00DD,$AA9A,$00D6,$AFA5,$00C6,$B5B3,$00DE,$B694
		dc.w	$00E9,$C669,$00C8,$B6B6,$00EA,$CB73,$00CE,$BCB4
		dc.w	$00C5,$BEBC,$00E5,$C58D,$00D7,$BDAE,$00D1,$C1BD
		dc.w	$00DD,$C8A9,$00DA,$CCBD,$00E0,$D3C2,$00DD,$DCD4
