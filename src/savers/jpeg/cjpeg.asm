;
; This file is part of ArtPRO.
; Copyright (C) 1996-2018 Frank Pagels
;
; ArtPRO is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; ArtPRO is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with ArtPRO.  If not, see <http://www.gnu.org/licenses/>.
;

_LVOOpenLibrary:	EQU	-$228
_LVOSetSignal:	EQU	-$132
****************************************************************************
	SECTION	cjpeg000000,CODE
ProgStart:
	MOVEM.L	D1-D6/A0-A6,-(SP)
	MOVEA.L	A0,A2
	MOVE.L	D0,D2
	LEA	lbL0007DC,A4
	MOVEA.L	4.W,A6
	LEA	lbL00080C,A3
	MOVEQ	#0,D1
	MOVE.L	#$46,D0
	BRA.S	lbC000024

lbC000022:
	MOVE.L	D1,(A3)+
lbC000024:
	DBRA	D0,lbC000022
	MOVE.L	SP,$60(A4)
	MOVE.L	A6,$58(A4)
	MOVEA.L	$114(A6),A3
	MOVE.L	$AC(A3),D0
	BEQ.S	lbC000046
	LSL.L	#2,D0
	MOVEA.L	D0,A0
	MOVE.L	$34(A0),D0
	LSL.L	#2,D0
	BRA.S	lbC00004C

lbC000046:
	MOVE.L	SP,D0
	SUB.L	$3A(A3),D0
lbC00004C:
	MOVE.L	SP,D1
	SUB.L	D0,D1
	ADDI.L	#$80,D1
	MOVE.L	D1,$34(A4)
	CMP.L	$24(A4),D0
	BCC.S	lbC0000B4
	MOVE.L	$24(A4),D0
	ADDI.L	#$80,D0
	MOVE.L	D0,$80(A4)
	MOVE.L	#$10001,D1
	JSR	-$C6(A6)
	TST.L	D0
	BEQ.L	lbC00024E
	MOVE.L	D0,$7C(A4)
	ADDI.L	#$80,D0
	MOVE.L	D0,$34(A4)
	ADD.L	$24(A4),D0
	MOVE.L	D0,D1
	CMPI.W	#$24,$14(A6)
	BLT.S	lbC0000B2
	MOVE.L	D0,$78(A4)
	MOVE.L	D1,$74(A4)
	SUB.L	$80(A4),D1
	LEA	$70(A4),A0
	MOVE.L	D1,(A0)
	JSR	-$2DC(A6)
	BRA.S	lbC0000B4

lbC0000B2:
	MOVEA.L	D0,SP
lbC0000B4:
	CLR.L	$5C(A4)
	MOVEQ	#0,D0
	MOVE.L	#$3000,D1
	JSR	_LVOSetSignal(A6)
	MOVEA.L	$114(A6),A3
	LEA	doslibrary.MSG(PC),A1
	MOVEQ	#0,D0
	JSR	_LVOOpenLibrary(A6)
	MOVE.L	D0,$13C(A4)
	BNE.S	lbC0000DE
	MOVEQ	#$64,D0
	BRA.L	lbC0001C2

lbC0000DE:
	MOVE.L	$98(A3),$54(A4)
	TST.L	$AC(A3)
	BEQ.L	lbC000168
	MOVEA.L	$AC(A3),A0
	ADDA.L	A0,A0
	ADDA.L	A0,A0
	MOVEA.L	$10(A0),A1
	ADDA.L	A1,A1
	ADDA.L	A1,A1
	MOVE.L	D2,D0
	MOVEQ	#0,D1
	MOVE.B	(A1)+,D1
	MOVE.L	A1,$64(A4)
	ADD.L	D1,D0
	ADDQ.L	#7,D0
	ANDI.W	#$FFFC,D0
	MOVE.L	D0,$6C(A4)
	MOVEM.L	D1/A1,-(SP)
	MOVE.L	#$10001,D1
	JSR	-$C6(A6)
	MOVEM.L	(SP)+,D1/A1
	TST.L	D0
	BNE.S	lbC000130
	MOVEQ	#$14,D0
	MOVE.L	D0,-(SP)
	BEQ.L	lbC000244
lbC000130:
	MOVEA.L	D0,A0
	MOVE.L	D0,$68(A4)
	MOVE.L	D2,D0
	SUBQ.L	#1,D0
	ADD.L	D1,D2
lbC00013C:
	MOVE.B	0(A2,D0.W),2(A0,D2.W)
	SUBQ.L	#1,D2
	DBRA	D0,lbC00013C
	MOVE.B	#$20,2(A0,D2.W)
	SUBQ.L	#1,D2
	MOVE.B	#$22,2(A0,D2.W)
lbC000156:
	MOVE.B	0(A1,D2.W),1(A0,D2.W)
	DBRA	D2,lbC000156
	MOVE.B	#$22,(A0)
	MOVE.L	A0,-(SP)
	BRA.S	lbC0001B2

lbC000168:
	LEA	$5C(A3),A0
	JSR	-$180(A6)
	LEA	$5C(A3),A0
	JSR	-$174(A6)
	MOVE.L	D0,$5C(A4)
	MOVE.L	D0,-(SP)
	MOVEA.L	D0,A2
	MOVE.L	$24(A2),D0
	BEQ.S	lbC00019E
	MOVEA.L	$13C(A4),A6
	MOVEA.L	D0,A0
	MOVE.L	0(A0),D1
	JSR	-$60(A6)
	MOVE.L	D0,$54(A4)
	MOVE.L	D0,D1
	JSR	-$7E(A6)
lbC00019E:
	MOVEA.L	$5C(A4),A0
	MOVE.L	A0,-(SP)
	PEA	$30(A4)
	MOVEA.L	$24(A0),A0
	MOVE.L	4(A0),$64(A4)
lbC0001B2:
	JSR	lbC000558(PC)
	JSR	lbC000588(PC)
	MOVEQ	#0,D0
	BRA.S	lbC0001C2

lbC0001BE:
	MOVE.L	4(SP),D0
lbC0001C2:
	MOVEA.L	$60(A4),A2
	MOVE.L	D0,-(A2)
	MOVEA.L	4.W,A6
	CMPI.W	#$24,$14(A6)
	BLT.S	lbC0001E6
	TST.L	$70(A4)
	BEQ.S	lbC0001E6
	LEA	$70(A4),A0
	SUBQ.L	#4,$78(A4)
	JSR	-$2DC(A6)
lbC0001E6:
	MOVEA.L	A2,SP
	MOVE.L	$4C(A4),D0
	BEQ.S	lbC0001F2
	MOVEA.L	D0,A0
	JSR	(A0)
lbC0001F2:
	JSR	lbC000570(PC)
	JSR	lbC000630(PC)
	MOVE.L	$80(A4),D0
	BEQ.S	lbC00020C
	MOVEA.L	$7C(A4),A1
	MOVEA.L	4.W,A6
	JSR	-$D2(A6)
lbC00020C:
	TST.L	$5C(A4)
	BEQ.S	lbC000232
	MOVEA.L	$13C(A4),A6
	MOVE.L	$54(A4),D1
	BEQ.S	lbC000220
	JSR	-$5A(A6)
lbC000220:
	MOVEA.L	4.W,A6
	JSR	-$84(A6)
	MOVEA.L	$5C(A4),A1
	JSR	-$17A(A6)
	BRA.S	lbC000244

lbC000232:
	MOVEA.L	4.W,A6
	MOVE.L	$6C(A4),D0
	BEQ.S	lbC000244
	MOVEA.L	$68(A4),A1
	JSR	-$D2(A6)
lbC000244:
	MOVEA.L	$13C(A4),A1
	JSR	-$19E(A6)
	MOVE.L	(SP)+,D0
lbC00024E:
	MOVEM.L	(SP)+,D1-D6/A0-A6
	RTS

doslibrary.MSG:
	dc.b	'dos.library',0

lbC000260:
	SUBQ.W	#4,SP
	MOVEM.L	D2/D3/A6,-(SP)
	MOVE.L	D0,D1
	LEA	15(SP),A0
	MOVE.L	A0,D2
	MOVEA.L	$13C(A4),A6
	MOVEQ	#1,D3
	JSR	-$2A(A6)
	SUBQ.L	#1,D0
	BNE.S	lbC000284
	MOVEQ	#0,D0
	MOVE.B	15(SP),D0
	BRA.S	lbC000286

lbC000284:
	MOVEQ	#-1,D0
lbC000286:
	MOVEM.L	(SP)+,D2/D3/A6
	ADDQ.W	#4,SP
	RTS

intuitionlibr.MSG:
	dc.b	'intuition.library',0
towerlibrary.MSG:
	dc.b	'tower.library',0
jpegcodec.MSG:
	dc.b	'jpeg.codec',0,0

lbC0002BA:
	SUBA.W	#$18,SP
	MOVEM.L	D2/D6/D7/A5/A6,-(SP)
	MOVE.L	D0,D7
	CLR.L	$18(SP)
	LEA	$1D(SP),A5
lbC0002CC:
	MOVE.L	D7,D0
	BSR.S	lbC000260
	MOVE.L	D0,D6
	MOVEQ	#$23,D0
	CMP.B	D0,D6
	BNE.S	lbC0002EA
lbC0002D8:
	MOVE.L	D7,D0
	BSR.S	lbC000260
	MOVEQ	#10,D1
	CMP.B	D1,D0
	BNE.S	lbC0002D8
	MOVE.L	D7,D0
	BSR.L	lbC000260
	MOVE.L	D0,D6
lbC0002EA:
	MOVEQ	#$20,D0
	CMP.B	D0,D6
	BEQ.S	lbC0002CC
	MOVEQ	#10,D0
	CMP.B	D0,D6
	BEQ.S	lbC0002CC
	MOVEQ	#9,D0
	CMP.B	D0,D6
	BEQ.S	lbC0002CC
	MOVEQ	#13,D0
	CMP.B	D0,D6
	BEQ.S	lbC0002CC
	MOVE.B	D6,$1C(SP)
lbC000306:
	MOVE.L	D7,D0
	BSR.L	lbC000260
	MOVE.B	D0,(A5)+
	MOVE.B	D0,$14(SP)
	MOVE.B	$14(SP),D0
	MOVEQ	#$30,D1
	CMP.B	D1,D0
	BCS.S	lbC000322
	MOVEQ	#$39,D1
	CMP.B	D1,D0
	BLS.S	lbC000306
lbC000322:
	LEA	$1C(SP),A0
	MOVE.L	A0,D1
	LEA	$18(SP),A0
	MOVE.L	A0,D2
	MOVEA.L	$13C(A4),A6
	JSR	-$330(A6)
	MOVE.L	$18(SP),D0
	MOVEM.L	(SP)+,D2/D6/D7/A5/A6
	ADDA.W	#$18,SP
	RTS

lbC000344:
	SUBQ.W	#8,SP
	MOVEM.L	D2-D5/D7/A6,-(SP)
	MOVEM.L	D0/A0,$18(SP)
	MOVEA.L	$13C(A4),A6
	JSR	-$36(A6)
	MOVE.L	D0,D7
	LEA	intuitionlibr.MSG(PC),A1
	MOVEQ	#$25,D0
	MOVEA.L	4.W,A6
	JSR	-$228(A6)
	MOVE.L	D0,$84(A4)
	BEQ.L	lbC00054C
	LEA	towerlibrary.MSG(PC),A1
	MOVEQ	#1,D0
	JSR	-$228(A6)
	MOVE.L	D0,$88(A4)
	BEQ.L	lbC000540
	MOVEA.L	$13C(A4),A6
	JSR	-$3C(A6)
	CLR.L	-(SP)
	MOVE.L	D0,-(SP)
	MOVE.L	#$80073201,-(SP)
	PEA	2
	MOVE.L	#$80073202,-(SP)
	PEA	1
	MOVE.L	#$80073203,-(SP)
	LEA	jpegcodec.MSG(PC),A0
	MOVEA.L	$88(A4),A6
	MOVEA.L	SP,A1
	JSR	-$24(A6)
	LEA	$1C(SP),SP
	MOVE.L	D0,$8C(A4)
	BEQ.L	lbC000534
	MOVE.L	D7,D1
	LEA	$9C(A4),A0
	MOVE.L	A0,D2
	MOVEA.L	$13C(A4),A6
	MOVEQ	#2,D3
	JSR	-$2A(A6)
	MOVE.W	$9C(A4),D0
	CMPI.W	#$5035,D0
	BNE.S	lbC0003E6
	MOVEQ	#1,D1
	MOVE.L	D1,$98(A4)
	BRA.S	lbC0003F8

lbC0003E6:
	CMPI.W	#$5036,D0
	BNE.S	lbC0003F2
	MOVE.L	D3,$98(A4)
	BRA.S	lbC0003F8

lbC0003F2:
	MOVEQ	#0,D0
	MOVE.L	D0,$98(A4)
lbC0003F8:
	TST.L	$98(A4)
	BEQ.L	lbC000528
	MOVE.L	D7,D0
	BSR.L	lbC0002BA
	MOVE.L	D0,$90(A4)
	MOVE.L	D7,D0
	BSR.L	lbC0002BA
	MOVE.L	D0,$94(A4)
	MOVE.L	D7,D0
	BSR.L	lbC0002BA
	MOVE.L	D0,$A2(A4)
	MOVEQ	#0,D1
	NOT.B	D1
	CMP.L	D1,D0
	BNE.L	lbC000528
	MOVE.L	$90(A4),D0
	BLE.L	lbC000528
	MOVE.L	$94(A4),D1
	BLE.L	lbC000528
	MOVE.L	$98(A4),D2
	MOVEQ	#1,D3
	CMP.L	D3,D2
	SNE	D4
	MOVEQ	#1,D5
	SUB.B	D4,D5
	SUB.B	D4,D5
	CLR.L	-(SP)
	MOVE.L	D2,-(SP)
	MOVE.L	#$80073284,-(SP)
	MOVE.L	D5,-(SP)
	MOVE.L	#$80073283,-(SP)
	MOVE.L	D1,-(SP)
	MOVE.L	#$80073282,-(SP)
	MOVE.L	D0,-(SP)
	MOVE.L	#$80073281,-(SP)
	MOVEA.L	$8C(A4),A0
	MOVEA.L	$84(A4),A6
	MOVEA.L	SP,A1
	JSR	-$288(A6)
	LEA	$24(SP),SP
	MOVE.L	#$73201,-(SP)
	MOVE.L	$8C(A4),-(SP)
	JSR	lbC00070C(PC)
	ADDQ.W	#8,SP
	TST.L	D0
	BEQ.L	lbC000528
	CMP.L	$98(A4),D3
	BNE.S	lbC00049E
	MOVE.L	$90(A4),D0
	BRA.S	lbC0004A8

lbC00049E:
	MOVE.L	$90(A4),D0
	MOVE.L	D0,D1
	ADD.L	D1,D1
	ADD.L	D1,D0
lbC0004A8:
	MOVE.L	D0,$AA(A4)
	MOVE.L	D3,D1
	MOVEA.L	4.W,A6
	JSR	-$2AC(A6)
	MOVE.L	D0,$A6(A4)
	BEQ.S	lbC000528
	CLR.L	$9E(A4)
	BRA.S	lbC0004F8

lbC0004C2:
	MOVE.L	D7,D1
	MOVE.L	$A6(A4),D2
	MOVE.L	$AA(A4),D3
	MOVEA.L	$13C(A4),A6
	JSR	-$2A(A6)
	MOVE.L	$AA(A4),-(SP)
	MOVE.L	$A6(A4),-(SP)
	MOVE.L	#$73202,-(SP)
	MOVE.L	$8C(A4),-(SP)
	JSR	lbC00070C(PC)
	LEA	$10(SP),SP
	CMP.L	$AA(A4),D0
	BNE.S	lbC000502
	ADDQ.L	#1,$9E(A4)
lbC0004F8:
	MOVE.L	$9E(A4),D0
	CMP.L	$94(A4),D0
	BLT.S	lbC0004C2
lbC000502:
	MOVE.L	$9E(A4),D0
	CMP.L	$94(A4),D0
	BNE.S	lbC00051C
	MOVE.L	#$73203,-(SP)
	MOVE.L	$8C(A4),-(SP)
	JSR	lbC00070C(PC)
	ADDQ.W	#8,SP
lbC00051C:
	MOVEA.L	$A6(A4),A1
	MOVEA.L	4.W,A6
	JSR	-$2B2(A6)
lbC000528:
	MOVEA.L	$8C(A4),A0
	MOVEA.L	$88(A4),A6
	JSR	-$2A(A6)
lbC000534:
	MOVEA.L	$88(A4),A1
	MOVEA.L	4.W,A6
	JSR	-$19E(A6)
lbC000540:
	MOVEA.L	$84(A4),A1
	MOVEA.L	4.W,A6
	JSR	-$19E(A6)
lbC00054C:
	MOVEQ	#0,D0
	MOVEM.L	(SP)+,D2-D5/D7/A6
	ADDQ.W	#8,SP
	RTS

	dc.w	0

lbC000558:
	SUBQ.W	#4,SP
	LEA	0,A0
	MOVE.L	A0,(SP)
	BEQ.S	lbC00056C
	MOVEA.L	$FFFFFFFC,A0
	JSR	(A0)
lbC00056C:
	ADDQ.W	#4,SP
	RTS

lbC000570:
	SUBQ.W	#4,SP
	LEA	0,A0
	MOVE.L	A0,(SP)
	BEQ.S	lbC000584
	MOVEA.L	$FFFFFFFC,A0
	JSR	(A0)
lbC000584:
	ADDQ.W	#4,SP
	RTS

lbC000588:
	MOVEM.L	A3/A5,-(SP)
	MOVEA.L	12(SP),A5
	BRA.S	lbC000606

lbC000592:
	ADDQ.L	#1,A5
lbC000594:
	MOVE.B	(A5),D0
	MOVEQ	#$20,D1
	CMP.B	D1,D0
	BEQ.S	lbC000592
	MOVEQ	#9,D1
	CMP.B	D1,D0
	BEQ.S	lbC000592
	MOVEQ	#10,D1
	CMP.B	D1,D0
	BEQ.S	lbC000592
	MOVE.B	(A5),D0
	BEQ.S	lbC000610
	MOVE.L	$B4(A4),D1
	ASL.L	#2,D1
	ADDQ.L	#1,$B4(A4)
	LEA	$BC(A4),A3
	ADDA.L	D1,A3
	MOVEQ	#$22,D1
	CMP.B	D1,D0
	BNE.S	lbC0005E4
	ADDQ.L	#1,A5
	MOVE.L	A5,(A3)
	BRA.S	lbC0005CA

lbC0005C8:
	ADDQ.L	#1,A5
lbC0005CA:
	MOVE.B	(A5),D0
	BEQ.S	lbC0005D4
	MOVEQ	#$22,D1
	CMP.B	D1,D0
	BNE.S	lbC0005C8
lbC0005D4:
	TST.B	(A5)
	BNE.S	lbC0005E0
	MOVEQ	#1,D0
	JSR	lbC0006B4(PC)
	BRA.S	lbC000606

lbC0005E0:
	CLR.B	(A5)+
	BRA.S	lbC000606

lbC0005E4:
	MOVE.L	A5,(A3)
	BRA.S	lbC0005EA

lbC0005E8:
	ADDQ.L	#1,A5
lbC0005EA:
	MOVE.B	(A5),D0
	BEQ.S	lbC000600
	MOVEQ	#$20,D1
	CMP.B	D1,D0
	BEQ.S	lbC000600
	MOVEQ	#9,D1
	CMP.B	D1,D0
	BEQ.S	lbC000600
	MOVEQ	#10,D1
	CMP.B	D1,D0
	BNE.S	lbC0005E8
lbC000600:
	TST.B	(A5)
	BEQ.S	lbC000610
	CLR.B	(A5)+
lbC000606:
	CMPI.L	#$20,$B4(A4)
	BLT.S	lbC000594
lbC000610:
	MOVE.L	$B4(A4),D0
	BNE.S	lbC00061C
	MOVEA.L	$5C(A4),A0
	BRA.S	lbC000620

lbC00061C:
	LEA	$BC(A4),A0
lbC000620:
	MOVE.L	A0,$B8(A4)
	JSR	lbC000344(PC)
	MOVEM.L	(SP)+,A3/A5
	JMP	lbC0006B4(PC)

lbC000630:
	MOVEM.L	D6/D7/A3/A5-A7,-(SP)
	MOVEA.L	$B0(A4),A5
	BRA.S	lbC00064E

lbC00063A:
	MOVEA.L	(A5),A3
	MOVEQ	#$14,D0
	ADD.L	4(A5),D0
	MOVEA.L	A5,A1
	MOVEA.L	4.W,A6
	JSR	-$D2(A6)
	MOVEA.L	A3,A5
lbC00064E:
	MOVE.L	A5,D0
	BNE.S	lbC00063A
	CLR.L	$B0(A4)
	BRA.S	lbC0006A4

lbC000658:
	MOVEA.L	$140(A4),A5
	MOVEQ	#-$64,D0
	ADD.L	$34(A4),D0
	MOVEA.L	D0,A3
	MOVE.L	4(A5),D0
	MOVE.L	D0,$140(A4)
	MOVE.L	8(A5),$34(A4)
	MOVE.L	$18(A5),D7
	TST.L	D0
	BNE.S	lbC000698
	MOVEA.L	$58(A4),A0
	CMPI.W	#$24,$14(A0)
	BCS.S	lbC000698
	MOVEA.L	SP,A0
	MOVE.L	A0,D6
	LEA	12(A5),A0
	MOVEA.L	4.W,A6
	JSR	-$2DC(A6)
	MOVEA.L	D6,SP
lbC000698:
	MOVEA.L	A3,A1
	MOVE.L	D7,D0
	MOVEA.L	4.W,A6
	JSR	-$D2(A6)
lbC0006A4:
	TST.L	$140(A4)
	BNE.S	lbC000658
	MOVEM.L	(SP)+,D6/D7/A3/A5-A7
	RTS

	MOVE.L	4(SP),D0
lbC0006B4:
	MOVEM.L	D6/D7/A5/A6,-(SP)
	MOVE.L	D0,D7
	MOVEQ	#1,D0
	MOVE.L	D0,$10(A4)
	MOVEA.L	$2C(A4),A5
	MOVEQ	#0,D6
	BRA.S	lbC0006FA

lbC0006C8:
	MOVE.L	4(A5),D0
	TST.B	D0
	BEQ.S	lbC0006E4
	BTST	#4,D0
	BNE.S	lbC0006E4
	MOVE.W	D6,D1
	EXT.L	D1
	MOVE.L	D1,D0
	MOVEA.L	$144(A4),A0
	JSR	(A0)
	BRA.S	lbC0006F6

lbC0006E4:
	BTST	#2,D0
	BEQ.S	lbC0006F6
	MOVE.L	8(A5),D1
	MOVEA.L	$13C(A4),A6
	JSR	-$24(A6)
lbC0006F6:
	MOVEA.L	(A5),A5
	ADDQ.W	#1,D6
lbC0006FA:
	MOVE.L	A5,D0
	BNE.S	lbC0006C8
	MOVE.L	D7,-(SP)
	JSR	lbC0001BE(PC)
	ADDQ.W	#4,SP
	MOVEM.L	(SP)+,D6/D7/A5/A6
	RTS

lbC00070C:
	MOVE.L	A2,-(SP)
	MOVEA.L	8(SP),A2
	MOVE.L	A2,D0
	BEQ.S	lbC00076E
	LEA	12(SP),A1
	MOVEA.L	-4(A2),A0
	BRA.S	lbC000764

	MOVE.L	A2,-(SP)
	MOVEM.L	8(SP),A0/A2
	MOVE.L	A2,D0
	BEQ.S	lbC00076E
	MOVE.L	A0,D0
	BEQ.S	lbC00076E
	LEA	$10(SP),A1
	MOVEA.L	$18(A0),A0
	BRA.S	lbC000764

	MOVE.L	A2,-(SP)
	MOVEM.L	8(SP),A0/A2
	MOVE.L	A2,D0
	BEQ.S	lbC00076E
	MOVE.L	A0,D0
	BEQ.S	lbC00076E
	LEA	$10(SP),A1
	BRA.S	lbC000764

	MOVE.L	A2,-(SP)
	MOVEM.L	8(SP),A0/A2
	MOVE.L	A2,D0
	BEQ.S	lbC00076E
	MOVE.L	A0,D0
	BEQ.S	lbC00076E
	MOVEA.L	$10(SP),A1
lbC000764:
	PEA	lbC000770(PC)
	MOVE.L	8(A0),-(SP)
	RTS

lbC00076E:
	MOVEQ	#0,D0
lbC000770:
	MOVEA.L	(SP)+,A2
	RTS

	MOVE.L	A2,-(SP)
	MOVEA.L	8(SP),A2
	MOVE.L	A2,D0
	BEQ.S	lbC00076E
	MOVEA.L	12(SP),A1
	MOVEA.L	-4(A2),A0
	BRA.S	lbC000764

	MOVE.L	A2,-(SP)
	MOVEM.L	8(SP),A0/A2
	MOVE.L	A2,D0
	BEQ.S	lbC00076E
	MOVE.L	A0,D0
	BEQ.S	lbC00076E
	MOVEA.L	$10(SP),A1
	MOVEA.L	$18(A0),A0
	BRA.S	lbC000764

	MOVE.L	A2,-(SP)
	MOVEM.L	8(SP),A0/A2
	MOVE.L	A2,D0
	BEQ.S	lbC00076E
	MOVE.L	A0,D0
	BEQ.S	lbC00076E
	MOVEA.L	$18(A0),A0
	MOVE.L	#0,-(SP)
	PEA	$14(SP)
	MOVE.L	#$103,-(SP)
	LEA	(SP),A1
	PEA	START+$07D2(PC)
	MOVE.L	8(A0),-(SP)
	RTS

	LEA	12(SP),SP
	MOVEA.L	(SP)+,A2
	RTS

	dc.w	0

	SECTION	cjpeg0007DC,DATA
lbL0007DC:
	dc.l	1,0,0,1,0,1,1,1,1,$FA0,0,0
lbL00080C:
	ds.l	$46

	end

