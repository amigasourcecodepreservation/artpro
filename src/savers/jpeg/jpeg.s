����  x�&b�&b�&b�&b�&b�&b�&b�&b;=��;-------------------------------------------------------------------------
;
;		JPEG saver modul for ArtPRO
;		(c) 1997 ,Frank (Copper) Pagels /Defect Softworks
;
;		12.05.1997
;
;
		incdir 	include:

		include	exec/exec_lib.i
		include	exec/lists.i
		include	exec/memory.i
		include intuition/intuition_lib.i
		include	libraries/wb_lib.i
		include libraries/gadtools.i
		include libraries/gadtools_lib.i
		include	graphics/graphics_lib.i
		Include utility/tagitem.i
		include	dos/dos.i		
		include	dos/dos_lib.i		
		include	libraries/reqtools_lib.i
		include	libraries/tower_lib.i

		include misc/artpro.i
		include	render/render.i
		include	render/render_lib.i
		include	render/renderhooks.i

		
TA_Dummy	=	TAG_USER+$00070000
TM_Dummy	=	$00070000

CDA_Dummy	=	TA_Dummy+$3200

CDA_Stream	=	CDA_Dummy+1
CDA_StreamType	=	CDA_Dummy+2
CDA_Coding	=	CDA_Dummy+3
CDA_Quality	=	CDA_Dummy+6

CDST_OBJECT	=	0
CDST_reserved	=	1	/* Do not use */
CDST_FILE	=	2

PCDA_Dummy	=	CDA_Dummy+$80

PCDA_Width	=	PCDA_Dummy+1
PCDA_Height	=	PCDA_Dummy+2
PCDA_Components	=	PCDA_Dummy+3
PCDA_ColorSpace	=	PCDA_Dummy+4

PCDCS_RGB	=	2

CDM_Dummy	=	TM_Dummy+$3200

CDM_START	=	CDM_Dummy+1
CDM_PROCESS	=	CDM_Dummy+2
CDM_FINISH	=	CDM_Dummy+3
CDM_ABORT	=	CDM_Dummy+4
CDM_READHEADER	=	CDM_Dummy+6
CDM_WRITEHEADER	=	CDM_Dummy+7


		SAVERHEADER JP_NODE

		dc.b	'$VER: JPEG saver module 0.01 (12.05.96)',0
	even
;---------------------------------------------------------------------------
JP_NODE:
		dc.l	0,0
		dc.b	0,0
		dc.l	JP_name
		dc.l	JP_save
		dc.b	'JPGS'		;saverkennung f�r ArtPRO
		dc.b	'EXTR'		;for extern saver
		dc.l	0
		dc.l	JP_Tags
JP_name:
		dc.b	'JPEG',0
	even
;---------------------------------------------------------------------------

JP_Tags:
		dc.l	APT_Creator,JP_Creator
		dc.l	APT_Version,2
		dc.l	APT_Info,JP_Info
;		dc.l	APT_Prefs,JP_Prefsrout		;routine for Prefs
;		dc.l	APT_Prefsbuffer,JP_Prefs	;buffer for Prefs
;		dc.l	APT_Prefsbuffersize,4		;size of buffer
		dc.l	APT_PrefsVersion,1		;Prefs Version
		dc.l	APT_OperatorUse,1
		dc.l	0

JP_Creator:
		dc.b	'(c) 1997 Frank Pagels /Defect Softworks',0
	even
JP_Info:
		dc.b	'JPEG saver',10,10
		dc.b	'Saves an image as JPEG using the ,',10
		dc.b	'jpeg codec class from Christoph Fleck',0
	even

towerbase:	dc.l	0
JP_myObject:	dc.l	0
JP_rgbline:	dc.l	0
JP_chunkyline:	dc.l	0

JP_Linemerk	=	APG_Free1

;----------------------------------------------------------------------------
JP_save:
		lea	towername(pc),a1
		move.l	4.w,a6
		jsr	_LVOOpenlibrary(a6)
		tst.l	d0
		bne	.libok
		lea	JP_Towermsg(pc),a4
		move.l	APG_Status(a5),a6
		jmp	(a6)
.libok:
		move.l	d0,towerbase
;------------------------------------------------------------------------------

;�ffne new Object

		move.l	towerbase(pc),a6
		lea	JP_classname(pc),a0
		lea	JP_codectags(pc),a1
		move.l	APG_Filehandle(a5),4(a1)
		jsr	_LVONewExtObjectA(a6)		
		tst.l	d0
		bne	.obok

;Konnte Objekt nicht �ffnen

		move.l	towerbase(pc),d0
		beq	.nixlib
		move.l	d0,a1
		move.l	4.w,a6
		jsr	_LVOCloseLibrary(a6)
		lea	JP_noobjectmsg(pc),a4
		move.l	APG_Status(a5),a6
		jmp	(a6)

;alles Ok
.obok:		
		move.l	d0,JP_myObject

;Setze Compressions parameter

		move.l	JP_myObject(pc),a0
		lea	JP_paratag(pc),a1
		moveq	#0,d0
	;	move.w	APG_ImageWidth(a5),d0
		move.l	APG_bytewidth(a5),d0
		lsl.l	#3,d0
		move.l	d0,4(a1)
		move.w	APG_ImageHeight(a5),d0
		move.l	d0,12(a1)
		move.l	APG_Intuitionbase(a5),a6
		jsr	_LVOSetAttrsA(a6)

;setzte Quality

		move.l	JP_myObject(pc),a0
		lea	JP_paratag1(pc),a1
		jsr	_LVOSetAttrsA(a6)

;Starte Compression

		move.l	#CDM_START,-(a7)
		move.l	JP_myObject(pc),-(a7)
		jsr	_DoMethod

		addq.l	#8,a7

;speicher zeile f�r zeile

;ersma palette einrichten

		lea	APG_Farbtab8(a5),a0
		lea	JP_Farbtab8pure,a1
		move.l	(a0)+,d7
		swap	d7
		move.l	d7,d6
		subq.w	#1,d7

.ncc		moveq	#0,d1
		move.l	(a0)+,d0
		lsr.l	#8,d0
		move.l	(a0)+,d1
		swap	d1
		or.w	d1,d0
		move.l	(a0)+,d1
		rol.l	#8,d1
		or.b	d1,d0

		move.l	d0,(a1)+

		dbra	d7,.ncc		

		move.l	APG_rndr_palette(a5),a0
		move.l	d6,d0		;Farbanzahl
		lea	JP_Farbtab8pure,a1
		lea	JP_palitag(pc),a2
		move.l	APG_Renderbase(a5),a6
		jsr	_LVOImportPaletteA(a6)		

;-------------------------------------------------------------------------------

		move.l	APG_Bytewidth(a5),d0
		lsl.l	#3,d0
		add.l	d0,d0
		move.l	d0,d5
		lsl.l	#2,d5	;speicher f�r eine rgb zeile
		add.l	d5,d0	
		move.l	#MEMF_ANY!MEMF_CLEAR,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)

		move.l	d0,JP_rgbline
		add.l	d5,d0
		move.l	d0,JP_chunkyline

;make datas
		clr.l	JP_Linemerk(a5)

		move.l	JP_Linemerk(a5),d1
		move.l	APG_Bytewidth(a5),d2
		mulu	d1,d2
		lea	JP_planetab(pc),a1	;init Planetab
		move.l	a1,a0
		moveq	#0,d7
		move.b	APG_Planes(a5),d7
		subq.l	#1,d7
		move.l	APG_Picmem(a5),d0
		add.l	d2,d0
.np		move.l	d0,(a1)+
		add.l	APG_Oneplanesize(a5),d0
		dbra	d7,.np
.weiter
		move.l	APG_Bytewidth(a5),d0
		moveq	#1,d1
		moveq	#0,d2
		move.b	APG_Planes(a5),d2
		move.l	d0,d3		
		move.l	JP_chunkyline(pc),a1
		move.l	APG_Renderbase(a5),a6
		sub.l	a2,a2
		
		jsr	_LVOPlanar2Chunky(a6)

;RGB's m�chen

		move.l	JP_chunkyline(pc),a0
		move.w	APG_ImageWidth(a5),d0
		move.w	APG_ImageHeight(a5),d1
		move.l	JP_rgbline(pc),a1
		move.l	APG_rndr_Palette(a5),a2
				


;----------------------------------------------------------------------------
;Tower lib wieder schlie�en

		move.l	JP_myObject(pc),a0
		move.l	towerbase(pc),a6
		jsr	_LVODisposeExtObject(a6)
		
		move.l	towerbase(pc),d0
		beq	.nixlib
		move.l	d0,a1
		move.l	4.w,a6
		jsr	_LVOCloseLibrary(a6)
		clr.l	towerbase
.nixlib:

		rts


JP_planetab:	ds.l	8

JP_palitag:	dc.l	RND_PaletteFormat,PALFMT_RGB8
		dc.l	TAG_DONE

JP_codectags:
		dc.l	CDA_Stream,0
		dc.l	CDA_Coding,0
		dc.l	CDA_StreamType,CDST_FILE
		dc.l	TAG_DONE

JP_paratag:
		dc.l	PCDA_Width,0
		dc.l	PCDA_Height,0
		dc.l	PCDA_Components,4
		dc.l	PCDA_ColorSpace,PCDCS_RGB
		dc.l	TAG_DONE

JP_paratag1:
		dc.l	CDA_Quality,60
		dc.l	TAG_DONE

JP_classname:	dc.b	'JPEGCODEC',0

JP_noobjectmsg:	dc.b	'Can',$27,'t create a new Object!',0
JP_Towermsg:	dc.b	'Tower.library not found!',0
towername:	dc.b	'tower.library',0

	even
;--------------------------------------------------------------------------
JP_Prefsrout:
;init Prefs

		lea	JP_form(pc),a0
		lea	JP_distrans(pc),a1
		lea	JP_discolor(pc),a2
		move.l	#1,(a0)
		clr.l	(a1)
		clr.l	(a2)

	;	tst.b	JP_prefs_format
		bne	.nix89
		clr.l	(a0)
		move.l	#1,(a1)
		move.l	#1,(a2)
.nix89:
		moveq	#0,d0
	;	move.b	JP_prefs_tcolor(pc),d0
		lea	JP_tcolor(pc),a0
		move.l	d0,(a0)

		lea	JP_incheck(pc),a0
		clr.l	(a0)
	;	tst.b	JP_prefs_inter
		beq	.nixin
		move.l	#1,(a0)
.nixin:
		lea	JP_trcheck(pc),a0
		clr.l	(a0)
	;	tst.b	JP_prefs_trans
		beq	.nixtr
		move.l	#1,(a0)
.nixtr:

;---------------------------------------------------------------------------

		move.l	#WindowTags,APG_WindowTagList(a5) ;Tag list for the Window
		move.l	#WinWG+4,APG_WGadgets(a5)	;addr. of WA_Gadgets Tag + 4
		move.l	#winSC+4,APG_WScreen(a5)	;addr. of WA_CustomScreen Tag + 4
	;	move.l	#JP_window,APG_WinWnd(a5)	;a point for the Window
	;	move.l	#JP_Glist,APG_Glist(a5)		;a point for the Glist
		move.l	#NTypes,APG_NGads(a5)		;the NGads list
		move.l	#Gtypes,APG_GTypes(a5)		;the GTypes list
		move.l	#Gtags,APG_Gtags(a5)		;the Gtags list
		move.w	#6,APG_CNT(a5)			;the number of Gadgets
	;	move.l	#JP_gadarray,APG_Gadgets(a5)	;a pointer for the Gadgetsarry, size=4*number of Gadgets
		move.l	APG_Scr(a5),APG_ScrBase(a5)	;the Screenpointer , stored in APG_Scr
	
		move.l	APG_CheckMainWinPos(a5),a6	;returns d0,d1 Pos
		jsr	(a6)

		add.w	#70,d0
		add.w	#100,d1
		
		move.w	d0,APG_WinLeft(a5)		;the left pos. of the Win
		move.w	d1,APG_WinTop(a5)		;the top pos. of the Win
		move.w	#158,APG_WinWidth(a5)		;the width of the Win
		move.w	#100,APG_WinHeight(a5)		;the height of the Win

		move.l	#WinL,APG_WinL(a5)		;addr. of WA_Left Tag 
		move.l	#WinT,APG_WinT(a5)		;addr. of WA_Top Tag
		move.l	#WinW,APG_WinW(a5)		;addr. of WA_Width Tag
		move.l	#WinH,APG_WinH(a5)		;addr. of WA_Height Tag

		move.l	APG_OpenWindow(a5),a6
		jsr	(a6)

JP_wait:;	move.l	JP_window(pc),a0
		move.l	APG_Wait(a5),a6
		jsr	(a6)				;Handle input

		cmp.l	#GADGETUP,d4
		beq.b	JP_gads
		cmp.l	#GADGETDOWN,d4
		beq.b	JP_gads
		bra.b	JP_wait

JP_gads:
		moveq	#0,d0
		move.w	38(a4),d0
		add.l	d0,d0
		move.l	d0,d1
		lea	JP_pjumptab(pc),a0
		move.w	(a0,d0.l),d0
		jmp	(a0,d0.w)

JP_pjumptab:
		dc.w	JP_handleformat-JP_pjumptab
		dc.w	JP_handleinter-JP_pjumptab
		dc.w	JP_handletrans-JP_pjumptab
		dc.w	JP_handlecolor-JP_pjumptab
		dc.w	JP_ok-JP_pjumptab
		dc.w	JP_cancel-JP_pjumptab

JP_handleformat:
		ext.l	d5
	;	lea	JP_prefs_format(pc),a0
		move.b	d5,(a0)
		lea	JP_form(pc),a0
		move.l	d5,(a0)
		moveq	#0,d5
		tst.l	(a0)
		bne	.w
		moveq	#1,d5
.w
		lea	JP_ghost(pc),a3
		move.l	d5,4(a3)
		move.l	APG_Gadtoolsbase(a5),a6

		moveq	#GD_trans,d1
	;	lea	JP_gadarray(pc),a4
		move.l	(a4,d1.l*4),a0
	;	move.l	JP_window(pc),a1
		sub.l	a2,a2
		lea	JP_ghost(pc),a3
		jsr	_LVOGT_SetGadgetAttrsA(a6)
		moveq	#GD_transcolor,d1
		move.l	(a4,d1.l*4),a0
	;	move.l	JP_window(pc),a1
		sub.l	a2,a2
		lea	JP_ghost(pc),a3
		jsr	_LVOGT_SetGadgetAttrsA(a6)

		bra	JP_wait
		
JP_ghost:	dc.l	GA_Disabled,0,0


JP_handleinter:
	;	lea	JP_prefs_inter(pc),a1
		lea	JP_incheck(pc),a2
		clr.b	(a1)
		clr.l	(a2)
		add.l	d1,d1
	;	lea	JP_gadarray(pc),a0
		move.l	(a0,d1.l),a0
		move.w	gg_Flags(a0),d0
		and.w	#SELECTED,d0
		beq.w	JP_wait		;nicht selected
		st	(a1)
		move.l	#1,(a2)
		bra	JP_wait

JP_handletrans:
	;	lea	JP_prefs_trans(pc),a1
		lea	JP_trcheck(pc),a2
		clr.b	(a1)
		clr.l	(a2)
		add.l	d1,d1
	;	lea	JP_gadarray(pc),a0
		move.l	(a0,d1.l),a0
		move.w	gg_Flags(a0),d0
		and.w	#SELECTED,d0
		beq.w	JP_wait		;nicht selected
		st	(a1)
		move.l	#1,(a2)
		bra	JP_wait

JP_handlecolor:
		add.l	d1,d1
	;	lea	JP_gadarray(pc),a0
		move.l	(a0,d1.l),a0
	;	move.l	JP_window(pc),a1
		sub.l	a2,a2
		lea	JP_numtag(pc),a3
		move.l	APG_Gadtoolsbase(a5),a6
		jsr	_LVOGT_GetGadgetAttrsA(a6)

		move.l	JP_number(pc),d0
		lea	JP_tcolor(pc),a0
		move.l	d0,(a0)
	;	lea	JP_prefs_tcolor(pc),a0
		move.b	d0,(a0)
		bra	JP_wait



JP_numtag:	dc.l	GTIN_Number,JP_number,TAG_DONE
JP_number:	dc.l	0

JP_cancel:
	;	move.l	JP_prefsbak(pc),JP_Prefs


JP_ok:
	;	move.l	JP_window(pc),a0
		move.l	APG_Intuitionbase(a5),a6
		jmp	_LVOCloseWindow(a6)


;--------------------------------------------------------------------------------
	even

WindowTags:
winL:	dc.l	WA_Left,212
winT:	dc.l	WA_Top,78
winW:	dc.l	WA_Width,220
winH:	dc.l	WA_Height,85
		dc.l	WA_IDCMP,MXIDCMP!GADGETUP!VANILLAKEY!BUTTONIDCMP!IDCMP_REFRESHWINDOW
		dc.l	WA_Flags,WFLG_DEPTHGADGET!WFLG_ACTIVATE!WFLG_DRAGBAR!WFLG_SMART_REFRESH!WFLG_RMBTRAP
winWG:	dc.l	WA_Gadgets,0
		dc.l	WA_Title,winWTitle
winSC:	dc.l	WA_CustomScreen,0
		dc.l	TAG_DONE

WinWTitle:	dc.b	'GIF Options',0

	even

GD_format	=	0
GD_Inter	=	1
GD_trans	=	2
GD_transcolor	=	3
GD_Ok		=	4
GD_Cancel	=	5

Gtypes:
		dc.w	CYCLE_KIND
		dc.w	CHECKBOX_KIND
		dc.w	CHECKBOX_KIND
		dc.w	INTEGER_KIND
		dc.w	BUTTON_KIND
		dc.w	BUTTON_KIND
Gtags:
		dc.l	GTCY_Labels,JP_formatLabels
		dc.l	GTCY_Active
JP_form		dc.l	0
		dc.l	TAG_DONE
		dc.l	GTCB_Checked
JP_incheck	dc.l	0
		dc.l	TAG_DONE
		dc.l	GTCB_Checked
JP_trcheck	dc.l	0
		dc.l	GA_Disabled
JP_distrans	dc.l	1
		dc.l	TAG_DONE
		dc.l	GTIN_Number
JP_tcolor:	dc.l	0
		dc.l	GTIN_MaxChars,3
		dc.l	GA_Disabled
JP_discolor	dc.l	1
		dc.l	TAG_DONE
		dc.l	TAG_DONE
		dc.l	TAG_DONE

JP_formatLabels:
		dc.l	JP_formatLab0
		dc.l	JP_formatLab1
		dc.l	0

JP_formatLab0:	dc.b	'GIF87a',0
JP_formatLab1:	dc.b	'GIF89a',0

	even
NTypes:
		dc.w	10,15,135,14
		dc.l	JP_formatText,0
		dc.w	GD_format
		dc.l	PLACETEXT_ABOVE!NG_HIGHLABEL,0,0
		dc.w	10,32,26,11
		dc.l	JP_interText,0
		dc.w	GD_inter
		dc.l	PLACETEXT_RIGHT,0,0
		dc.w	10,45,26,11
		dc.l	JP_transText,0
		dc.w	GD_trans
		dc.l	PLACETEXT_RIGHT,0,0
		dc.w	10,59,30,14
		dc.l	JP_transcolorText,0
		dc.w	GD_transcolor
		dc.l	PLACETEXT_RIGHT,0,0
		DC.W    10,80,60,14
		DC.L    oktext,0
		DC.W    GD_Ok
		DC.L    PLACETEXT_IN,0,0
		DC.W    85,80,60,13
		DC.L    canceltext,0
		DC.W    GD_Cancel
		DC.L    PLACETEXT_IN,0,0

oktext:		dc.b	'Ok',0
canceltext:	dc.b	'Cancel',0
JP_formatText:	dc.b	'Save Format',0
JP_interText:	dc.b	'Interlaced',0
JP_transText:	dc.b	'Transparency',0
JP_transcolorText:dc.b 	'Trans. color',0


;----------------------------------------------------------------------------

savemsg:	dc.b	'Save a JPEG image!',0


_DoMethod:
	MOVE.L	A2,-(SP)
	MOVEA.L	8(SP),A2
	MOVE.L	A2,D0
	BEQ.S	cmnullreturn
	LEA	12(SP),A1
	MOVEA.L	-4(A2),A0
	BRA.S	cminvoke

_DoSuperMethod:
	MOVE.L	A2,-(SP)
	MOVEM.L	8(SP),A0/A2
	MOVE.L	A2,D0
	BEQ.S	cmnullreturn
	MOVE.L	A0,D0
	BEQ.S	cmnullreturn
	LEA	$10(SP),A1
	MOVEA.L	$18(A0),A0
	BRA.S	cminvoke

_CoerceMethod:
	MOVE.L	A2,-(SP)
	MOVEM.L	8(SP),A0/A2
	MOVE.L	A2,D0
	BEQ.S	cmnullreturn
	MOVE.L	A0,D0
	BEQ.S	cmnullreturn
	LEA	$10(SP),A1
	BRA.S	cminvoke

_CoerceMethodA:
	MOVE.L	A2,-(SP)
	MOVEM.L	8(SP),A0/A2
	MOVE.L	A2,D0
	BEQ.S	cmnullreturn
	MOVE.L	A0,D0
	BEQ.S	cmnullreturn
	MOVEA.L	$10(SP),A1
cminvoke:
	PEA	cmreturn(PC)
	MOVE.L	8(A0),-(SP)
	RTS

cmnullreturn:
	MOVEQ	#0,D0
cmreturn:
	MOVEA.L	(SP)+,A2
	RTS

_DoMethodA:
	MOVE.L	A2,-(SP)
	MOVEA.L	8(SP),A2
	MOVE.L	A2,D0
	BEQ.S	cmnullreturn
	MOVEA.L	12(SP),A1
	MOVEA.L	-4(A2),A0
	BRA.S	cminvoke

_DoSuperMethodA:
	MOVE.L	A2,-(SP)
	MOVEM.L	8(SP),A0/A2
	MOVE.L	A2,D0
	BEQ.S	cmnullreturn
	MOVE.L	A0,D0
	BEQ.S	cmnullreturn
	MOVEA.L	$10(SP),A1
	MOVEA.L	$18(A0),A0
	BRA.S	cminvoke

_SetSuperAttrs:
	MOVE.L	A2,-(SP)
	MOVEM.L	8(SP),A0/A2
	MOVE.L	A2,D0
	BEQ.S	cmnullreturn
	MOVE.L	A0,D0
	BEQ.S	cmnullreturn
	MOVEA.L	$18(A0),A0
	MOVE.L	#0,-(SP)
	PEA	$14(SP)
	MOVE.L	#$103,-(SP)
	LEA	(SP),A1
	PEA	ssaret(PC)
	MOVE.L	8(A0),-(SP)
	RTS

ssaret:
	LEA	12(SP),SP
	MOVEA.L	(SP)+,A2
	RTS

	dc.w	0


		section	datas,bss

JP_Farbtab8pure:	ds.l	256
