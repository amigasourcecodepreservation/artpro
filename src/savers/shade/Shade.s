����  M�&b�&b�&b�&b�&b�&b�&b�&b;=.�;===========================================================================
;
;		ShadeTable Generator for Bumpmapping stuff
;		(c) 1997 ,Frank (Copper) Pagels  /DFT
;
;		03.08.97

		incdir 	include:

		include	exec/exec_lib.i
		include	exec/lists.i
		include	exec/memory.i
		include intuition/intuition_lib.i
		include	libraries/wb_lib.i
		include	libraries/reqtools_lib.i
		include	graphics/graphics_lib.i
		Include utility/tagitem.i
		include	dos/dos.i		
		include	dos/dos_lib.i		

		include misc/artpro.i
		include render/render_lib.i
		include render/render.i
		

		SAVERHEADER ST_NODE

		dc.b	'$VER: ShadeTable Generator Saver Modul 0.2 (03.08.97)',0
	even
;---------------------------------------------------------------------------
ST_NODE:
		dc.l	0,0
		dc.b	0,0
		dc.l	ST_name
		dc.l	ST_Party
		dc.b	'STSA'
		dc.b	'EXTR'	;for extern loader
		dc.l	0
		dc.l	ST_Tags
ST_name:
		dc.b	'ShadeTable',0
	even
;---------------------------------------------------------------------------

ST_Tags:
		dc.l	APT_Creator,ST_Creator
		dc.l	APT_Version,5
		dc.l	TAG_DONE

ST_Creator:
		dc.b	'(c) 1997 Frank Pagels /Defect Softworks',0
	even

st_shademem	=	APG_Free1
st_shaderender	=	APG_Free2
st_histo	=	APG_Free3
st_palette	=	APG_Free4

;---------------------------------------------------------------------------
ST_Party:
		move.l	#256*256*4+256*256,d0
		move.l	#MEMF_ANY+MEMF_Clear,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)
		move.l	d0,st_shademem(a5)
		bne	.mok
		moveq	#APSE_NOMEM,d3
		rts
.mok:		
		add.l	#256*256*4,d0
		move.l	d0,st_shaderender(a5)
		
		jsr	APR_Openprocess(a5)		

		lea	generatemsg(pc),a1
		moveq	#5,d0
		moveq	#0,d1
		jsr	APR_InitProcess(a5)


CalcShadeTones:
; this table should be editable by the user... REAL brightness should be used
; instead of just multiply shading!
	move.l	#$00000001,d0
	move.l	#$00020002,d1
	lea.l	ST_ShadeTones,a0
	move.w	#256/2-1,d7
.lop:	move.l	d0,(a0)+
	add.l	d1,d0
	dbf	d7,.lop


	lea	APG_Farbtab8+4(a5),a0
	lea	ST_ShadeTones,a1
	move.l	st_shademem(a5),a2

	move.w	#256-1,d7
.lopy:	swap	d7
	move.l	a0,a3
	move.w	#256-1,d7
	move.w	(a1)+,d2	; current tone
.lopx:
	move.l	(a3)+,d0	; current color
	rol.l	#8,d0
	move.l	d0,d1		; current color
	and.l	#$ff,d1	; red
	mulu	d2,d1
	lsr.l	#8,d1
	move.l	d1,d3

	move.l	(a3)+,d0	; current color
	rol.l	#8,d0
	move.l	d0,d1
	and.l	#$ff,d1	; green
	mulu	d2,d1
	lsr.l	#8,d1
	lsl.l	#8,d3
	move.b	d1,d3

	move.l	(a3)+,d0	; current color
	rol.l	#8,d0
	move.l	d0,d1
	and.l	#$ff,d1	; red
	mulu	d2,d1
	lsr.l	#8,d1
	lsl.l	#8,d3
	move.b	d1,d3

	move.l	d3,(a2)+

	dbf	d7,.lopx
	swap	d7
	dbf	d7,.lopy

		jsr	APR_DoProcess(A5)

;now render for 8 bit

;create Histogramm

;		move.l	#TAG_DONE,-(a7)
;		move.l	#HSTYPE_15Bit,-(a7)
;		move.l	#RND_HSTYPE,-(a7)
;		move.l	a7,a1

		sub.l	a1,a1
		move.l	APG_Renderbase(a5),a6
		jsr	_LVOCreateHistogramA(a6)
		tst.l	d0
		bne	.ok
		lea	nohistomsg(pc),a4
		move.l	APG_Status(a5),a6
		jsr	(a6)
		move.l	#APSE_ERROR,d3
		rts
.ok
		move.l	d0,st_Histo(a5)
;Make Palette:

		sub.l	a1,a1		
		move.l	APG_Renderbase(a5),a6
		jsr	_LVOCreatePaletteA(a6)
		move.l	d0,st_palette(a5)

;add RGB datas to Histogram

		move.l	st_histo(a5),a0
		move.l	st_shademem(a5),a1		;RGB Pic
		move.l	#256,d0
		move.l	#256,d1
		sub.l	a2,a2
		jsr	_LVOAddRGBImageA(a6)
;		cmp.l	#ADDH_SUCCESS,d0	;!!!!!!!!!!!!!

		jsr	APR_DoProcess(A5)


;Extract Palette

		move.l	st_histo(a5),a0
		move.l	st_palette(a5),a1
		move.l	#256,d0			;anz Colors
		sub.l	a2,a2
		jsr	_LVOExtractPaletteA(a6)

		jsr	APR_DoProcess(A5)


;Render Table
		move.l	st_shademem(a5),a0
		move.l	#256,d0
		move.l	#256,d1
		move.l	st_shaderender(a5),a1
		move.l	st_palette(a5),a2

		move.l	#TAG_DONE,-(a7)
		move.l	#COLORMODE_CLUT,-(a7)
		move.l	#RND_ColorMode,-(a7)
		move.l	#DITHERMODE_FS,-(a7)
		move.l	#RND_DitherMode,-(a7)
		move.l	a7,a3
		jsr	_LVORenderA(a6)
		lea	5*4(a7),a7

		jsr	APR_DoProcess(A5)

;write Table
		move.l	APG_Filehandle(a5),d1
		move.l	st_shaderender(a5),d2
		move.l	#256*256,d3
		move.l	APG_Dosbase(a5),a6
		jsr	_LVOWrite(a6)

		jsr	APR_DoProcess(A5)

		move.l	APG_RenderBase(a5),a6
		move.l	st_palette(a5),a0
		jsr	_LVODeletePalette(a6)

		move.l	st_histo(a5),a0
		jsr	_LVODeleteHistogram(a6)

		move.l	st_shademem(a5),a1
		move.l	4.w,a6
		jsr	_LVOFreevec(a6)

		rts


generatemsg:	dc.b	'Generate Shadetable ...',0
nohistomsg:	dc.b	'Can',39,'t create histogram!',0

		section laber,bss
ST_ShadeTones:

		ds.w	256


