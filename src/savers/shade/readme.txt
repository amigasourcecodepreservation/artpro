this archive contains some information on how to render and use
shadetables.

files:

ShadeDemo.exe
     an example of a used shadetable

ShadeDemo.readme
     a very short readme

ShadeTable.lbm
     the output of my shadetable calculator rendered in 256 colors
     ready to be used!

ShadeTable.s
     simple, slow source for calculation shadetables

texture.lbm
     the original texture, in 256 colors

readme.txt
     this readme

Fresh Prince / Rebels

Daniel Povlsen
Bodilsvej 25
DK-7000 FA
Denmark

daniel@post5.tele.dk
