����  ��&b�&b�&b�&b�&b�&b�&b�&b;=U;===========================================================================
;
;		Test f�r einen Bitmap saver
;		(c) 1995 ,Frank Pagels (Crazy Copper) /DFT
;
;		14.04.95

		incdir 	include:

		include	exec/exec_lib.i
		include	exec/lists.i
		include	exec/memory.i
		include intuition/intuition_lib.i
		include	libraries/wb_lib.i
		include libraries/gadtools.i
		include	graphics/graphics_lib.i
		Include utility/tagitem.i
		include	dos/dos.i		
		include	dos/dos_lib.i		
		include	libraries/reqtools_lib.i

		include misc/artpro.i
		
		SAVERHEADER BM_NODE

		dc.b	'$VER: Windows Bitmap saver module 1.31 (31.10.96)',0
	even
;---------------------------------------------------------------------------
BM_NODE:
		dc.l	0,0
		dc.b	0,0
		dc.l	BM_name
		dc.l	BM_Save
		dc.b	'BMPS'
		dc.b	'EXTR'	;for extern Saver
		dc.l	0
		dc.l	BM_Tags
BM_name:
		dc.b	'BMP',0
	even
;---------------------------------------------------------------------------

BM_Tags:
		dc.l	APT_Creator,BM_Creator
		dc.l	APT_Version,1
		dc.l	APT_Info,BM_Info
		dc.l	APT_Prefs,BM_Prefsrout		;routine for Prefs
		dc.l	APT_Prefsbuffer,BM_Format	;buffer for Prefs
		dc.l	APT_Prefsbuffersize,1		;size of buffer
		dc.l	APT_PrefsVersion,1		;Prefs Version
		dc.l	0

BM_Creator:
		dc.b	'(c) 1996 Frank Pagels /DEFECT Softworks',0
	even
BM_Info:
		dc.b	'Windows bitmap saver',10,10
		dc.b	'Saves an image as BMP (Windows Bitmaps) from 1',10
		dc.b	'to 24Bit.',0
	even

;---------------------------------------------------------------------------

BM_Newwidth	=	APG_Free1
BM_Puffermem	=	APG_Free2
BM_biBitCount	=	APG_Free3
BM_Linemerk	=	APG_Free3+2
BM_Chunkymem	=	APG_Free4
BM_TempLine	=	APG_Free5
BM_Spaces	=	APG_Free6

BM_window	dc.l	0		;Enth�lt pointer vom eingenen Window
BM_GList	dc.l	0		
BM_gadarray	dcb.l	3,0

BM_Ham:		dc.b	0

BM_Format:	dc.b	0		;welches Format saven
					;0=rendered, 1=24Bit
	even
;---------------------------------------------------------------------------
BM_Save
		tst.b	BM_Format
		bne	.w
		tst.l	APG_Picmem(a5)
		bne	.w
		moveq	#APSE_NOBITMAP,d3
		rts
.w
		sf	BM_Ham
		moveq	#0,d0
		move.b	APG_Ham_Flag(a5),d0
		add.b	APG_Ham8_Flag(a5),d0
		tst.b	d0
		beq.b	.rendered

.ham:		st	BM_Ham	;24-Bit

;berechne ben�tigten Speicher
.rendered:
		tst.w	APG_Cutflag(a5)
		bne.b	.brushok
		move.w	#0,APG_xbrush1(a5)	;Es soll das ganze 
		move.w	#0,APG_ybrush1(a5)	;Pic abgesaved werden
		move.w	APG_ImageWidth(a5),APG_xbrush2(a5)
		move.w	APG_ImageHeight(a5),APG_ybrush2(a5)	
.brushok:
		jsr	APR_Openprocess(a5)
		lea	savebmpmsg(pc),a1
		move.w	APG_ybrush2(a5),d0
		sub.w	APG_ybrush1(a5),d0
		jsr	APR_InitProcess(a5)

		moveq	#0,d0
		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
		move.l	d0,d6
		add.l	#3,d0
		lsr.l	#2,d0		
		lsl.l	#2,d0		;breite in Byte
		move.l	d0,BM_Newwidth(a5)
		move.l	d6,d0
		add.l	#15,d0
		lsr.l	#4,d0		;breite in worte
		move.w	d0,APG_Brushword(a5)

		tst.b	BM_Ham
		bne	.24
		tst.b	BM_Format
		beq.b	.no24
.24		move.w	#24,d1
		bra.b	.planeok
.no24		moveq	#0,d1
		move.b	APG_Planes(a5),d1
		cmp.b	#1,d1
		beq.b	.planeok
		cmp.b	#4,d1
		beq.b	.planeok
		bhi.b	.acht
		moveq	#4,d1
		bra.b	.planeok
.acht:		moveq	#8,d1
.planeok:	move.w	d1,BM_biBitCount(a5)

;berechne Puffer:
		tst.b	BM_Ham
		bne	.d24
		tst.b	BM_Format
		beq.b	.n24
.d24:		move.l	d6,d0		;Breite
		mulu	#3,d0
		addq.l	#3,d0
		lsr.l	#2,d0
		lsl.l	#2,d0
		move.l	d0,d2
		move.l	d6,d3
		mulu	#3,d3
		sub.l	d3,d2
		move.w	d2,BM_Spaces(a5)
.w1:		moveq	#0,d1
		move.w	APG_ybrush2(a5),d1
		sub.w	APG_ybrush1(a5),d1
		move.l	d1,d7
		bra.w	.wq

.n24		move.l	BM_Newwidth(a5),d0
		moveq	#0,d1
		move.w	APG_ybrush2(a5),d1
		sub.w	APG_ybrush1(a5),d1
		move.l	d1,d7
		cmp.w	#1,BM_biBitCount(a5)
		bne.w	.wq
		move.w	APG_Brushword(a5),d0
		add.w	d0,d0

.wq		mulu	d1,d0

		cmp.w	#4,BM_biBitCount(a5)
		bne.b	.nohalf
		lsr.l	#1,d0
		
.nohalf:	add.l	#54,d0		;immer f�r Header

		tst.b	BM_Ham
		bne	.nocolor
		tst.b	BM_Format
		bne.b	.nocolor
		move.w	BM_biBitCount(a5),d1

		moveq	#1,d2
		lsl.l	d1,d2
		lsl.l	#2,d2		;da ja 4 eintr�ge in Colortab
		
		add.l	d2,d0
		
.nocolor:	move.l	d0,d5
		move.l	#MEMF_PUBLIC+MEMF_CLEAR,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)
		move.l	d0,BM_puffermem(a5)
		bne.b	.ok
		moveq	#-2,d3		;no mem !
		rts
.ok:		

;f�lle BITMAPFILEHEADER 
		move.l	BM_puffermem(a5),a4

		move.w	#'BM',(a4)+

		move.l	d5,d0
		bsr.w	makeDword
		move.l	d0,(a4)+	;filesize		

		clr.l	(a4)+		;reserved

		moveq	#0,d1
		move.w	BM_biBitCount(a5),d1
		moveq	#1,d0
		lsl.l	d1,d0
		lsl.l	#2,d0
		add.l	#54,d0
		tst.b	BM_Ham
		bne	.w24
		tst.b	BM_Format
		beq.b	.nw
.w24		moveq.l	#54,d0
.nw		bsr.w	makeDword
		move.l	d0,(a4)+	;BM_bfoffsetbits

;f�lle BITMAPINFOHEADER 

		move.l	#$28000000,(a4)+ ;biSize

		move.l	d6,d0
		bsr.w	makeDword
		move.l	d0,(a4)+	;imagewidth
		
		move.l	d7,d0
		bsr.w	makeDword
		move.l	d0,(a4)+	;imageheight
				
		move.w	#$0100,(a4)+	;biplanes

		move.w	BM_biBitCount(a5),d0
		bsr.w	makeword
		move.w	d0,(a4)+	;biBitCount

		clr.l	(a4)+		;Compression --> none
		clr.l	(a4)+		;biSizeImage
		clr.l	(a4)+		;biXPelsPerMeter
		clr.l	(a4)+		;biYPelsPerMeter
		clr.l	(a4)+		;biClrUsed
		clr.l	(a4)+		;biClrImportant

		tst.b	BM_Ham
		bne	BM_Write24
		tst.b	BM_Format	;24-Bit ??
		bne.w	BM_Write24
;f�lle Colortab
		move.w	BM_biBitCount(a5),d0
		moveq	#1,d7
		lsl.l	d0,d7
		subq.l	#1,d7	;anzahl colors

		lea	APG_Farbtab8(a5),a0
		addq.l	#4,a0		;header �berlesen
bm_nextcolor:	move.l	(a0)+,d0	;r
		move.l	(a0)+,d1	;g
		move.l	(a0)+,d2	;b
		rol.l	#8,d0		
		rol.l	#8,d1
		rol.l	#8,d2
		move.b	d2,(a4)+	;b
		move.b	d1,(a4)+	;g
		move.b	d0,(a4)+	;r
		clr.b	(a4)+		;reserved
		dbra	d7,bm_nextcolor
;write Bitmap

		moveq	#0,d0
		move.w	APG_Brushword(a5),d0
		lsl.l	#4,d0

;Besorge Speicher f�r eine ChunkyLine

		move.l	#MEMF_PUBLIC+MEMF_CLEAR,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)
		move.l	d0,BM_Chunkymem(a5)
		
		move.l	APG_CheckFirstWord(a5),a6
		jsr	(a6)		

		move.w	APG_ybrush2(a5),BM_linemerk(a5)

		move.w	APG_ybrush2(a5),d7
		move.w	d7,BM_Linemerk(a5)
		tst.w	d7
		beq.b	.w
		subq.w	#1,BM_linemerk(a5)
.w		sub.w	APG_ybrush1(a5),d7
		tst.w	d7
		beq.b	BM_nextline
		subq.w	#1,d7

		cmp.w	#1,BM_biBitCount(a5)
		beq.b	BM_write1

BM_nextline:
		moveq	#0,d0
		move.w	BM_Linemerk(a5),d0
		move.l	BM_Chunkymem(a5),a0
		move.l	APG_Bpl2ChunkyLine(a5),a6
		jsr	(a6)
;copy line
		move.l	BM_Newwidth(a5),d6
		subq.l	#1,d6
		cmp.w	#4,BM_biBitCount(a5)
		bne.b	.nb
.nb4:		move.b	(a0)+,d0	;f�r 4 bit
		lsl.b	#4,d0
		move.b	(a0)+,d1
		or.b	d1,d0
		move.b	d0,(a4)+
		subq.w	#1,d6
		dbra	d6,.nb4
		bra.b	.w
.nb:		move.b	(a0)+,(a4)+
		dbra	d6,.nb
.w		subq.w	#1,BM_Linemerk(a5)

		jsr	APR_DoProcess(a5)
		tst.l	d0
		beq	BM_Abort

		dbra	d7,BM_nextline
		bra.w	BM_writeall

;-------------------------------------------------------------------------
BM_write1:

BM_newline:	move.w	BM_Linemerk(a5),d5
		move.l	APG_Bytewidth(a5),d3
		mulu	d3,d5

		move.l	APG_Firstword(a5),d0
		add.l	d0,d0		;in Byte
		move.l	APG_Picmem(a5),a0
		add.l	d0,a0
		add.l	d5,a0		;Y-Offset
		
		move.l	APG_Bitshift(a5),d6
		swap	d7
		move.w	APG_brushword(a5),d7
		subq.l	#1,d7
BM_newword:
		move.l	(a0),d0
		lsl.l	d6,d0
		swap	d0
		add.l	#16,d4
		move.w	APG_xbrush2(a5),d1
		cmp.w	d4,d1
		bhs.b	.pixok
		move.w	d4,d3
		sub.w	d1,d3
		lsr.w	d3,d0
		lsl.w	d3,d0	;zuviele Bits l�schen
.pixok:
		move.w	d0,(a4)+

		addq.l	#2,a0
		dbra	d7,BM_newword
		swap	d7
		subq.w	#1,BM_Linemerk(a5)
		move.w	APG_xbrush1(a5),d4

		jsr	APR_DoProcess(a5)
		tst.l	d0
		beq	BM_Abort
		
		dbra	d7,BM_newline
		bra.w	BM_Writeall
		
;---------------------------------------------------------------------
BM_Write24:
		moveq	#0,d0
		move.w	APG_ImageWidth(a5),d0
		add.w	#16,d0		;zus�tzlicher buffer
		move.l	d6,d7
		lsl.l	#2,d7
		add.l	d7,d0
		move.l	#MEMF_ANY!MEMF_CLEAR,d1
		jsr	_LVOAllocVec(a6)
		move.l	d0,BM_Chunkymem(a5)

		add.l	d7,d0
		move.l	d0,BM_TempLine(a5)
		
		lea	BM_RGBmap,a0	
		moveq	#0,d0
		moveq	#0,d1
		moveq	#0,d2
		move.b	APG_Planes(a5),d0
		move.w	APG_ImageWidth(a5),d1
		moveq	#0,d2
		move.w	APG_ImageHeight(a5),d2
		move.l	APG_gfxbase(a5),a6
		jsr	_LVOinitbitmap(a6)
		
		lea	BM_RGBmap+8,a0
		move.l	APG_Picmem(a5),d0
		moveq	#0,d7		
		move.b	APG_Planes(a5),d7	
		subq	#1,d7
.newplane
		move.l	d0,(a0)+	;Planes in Strucktur einbinden
		add.l	APG_Oneplanesize(a5),d0
		dbra	d7,.newplane

		lea	BM_Farbtab8pure,a0
		lea	APG_Farbtab8(a5),a1
		move.w	(a1)+,d7
		subq.w	#1,d7
		addq.l	#2,a1
.nc:		move.l	(a1)+,d0
		move.l	(a1)+,d1
		move.l	(a1)+,d2
		lsr.l	#8,d0
		swap	d1
		move.w	d1,d0
		rol.l	#8,d2
		move.b	d2,d0
		and.l	#$00ffffff,d0
		move.l	d0,(a0)+
		dbra	d7,.nc		

		move.w	APG_ybrush2(a5),d7
		move.w	d7,BM_Linemerk(a5)
		tst.w	d7
		beq.b	.w
		subq.w	#1,BM_linemerk(a5)
.w		sub.w	APG_ybrush1(a5),d7
		tst.w	d7
		beq.w	BM_nextline24
		subq.w	#1,d7

BM_nextline24:	lea	BM_rgbmap,a0
		move.l	BM_Chunkymem(a5),a1
		lea	BM_Farbtab8pure,a2
		move.l	BM_TempLine(a5),a3
		moveq	#0,d0
		moveq	#0,d1
		moveq	#0,d2
		move.w	APG_xbrush1(a5),d0
		move.w	BM_linemerk(a5),d1
		move.l	d6,d2		;Width
		moveq	#1,d3		;one Line

		moveq	#0,d4
		moveq	#0,d5
		move.b	APG_Ham_Flag(a5),d5
		add.b	APG_Ham8_Flag(a5),d5
		tst.b	d5
		beq.b	.noham
		moveq	#1,d4
.noham:		tst.b	APG_EHB_Flag(a5)
		beq.b	.noehb
		moveq	#-1,d4
.noehb:
		move.l	APG_BPL2RGB24(a5),a6
		jsr	(a6)

		move.l	d6,d5
		subq.w	#1,d5
		move.l	BM_Chunkymem(a5),a0
.nchunk:	move.l	(a0)+,d0
		move.b	d0,(a4)+
		lsr.l	#8,d0
		move.b	d0,(a4)+
		lsr.l	#8,d0
		move.b	d0,(a4)+
		dbra	d5,.nchunk
		move.w	BM_Spaces(a5),d5
		beq.b	.nospace
		subq.w	#1,d5
.nc		clr.b	(a4)+
		dbra	d5,.nc
.nospace:					
		subq.w	#1,BM_Linemerk(a5)
		jsr	APR_DoProcess(a5)
		tst.l	d0
		beq	BM_Abort
		
		dbra	d7,BM_nextline24

;---------------------------------------------------------------------
BM_writeall:	move.l	BM_puffermem(a5),d2
		sub.l	d2,a4
		move.l	a4,d3
		move.l	APG_Filehandle(a5),d1
		move.l	APG_Dosbase(a5),a6
		jsr	_LVOWrite(a6)
		
		move.l	BM_puffermem(a5),a1
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)

		move.l	BM_Chunkymem(a5),a1
		jsr	_LVOFreeVec(a6)

		rts

BM_Abort:
		move.l	BM_puffermem(a5),a1
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)

		move.l	BM_Chunkymem(a5),a1
		jsr	_LVOFreeVec(a6)

		moveq	#APSE_ABORT,d3
		rts

makeDword:
;> d0
;< d0
;11223344 --> 44332211

		move.l	d0,d1	;11223344
		lsl.w	#8,d0	;11224400
		lsr.w	#8,d1	;11220033
		move.b	d1,d0	;11224433
		swap	d0	;44331122
		swap	d1	;00331122
		lsl.w	#8,d0	;44332200
		lsr.w	#8,d1	;00330011
		move.b	d1,d0	;44332211	
		rts
makeword:
;> d0
;< d0
;1122 --> 2211
		move.l	d0,d1	;1122
		lsl.w	#8,d0	;2200
		lsr.w	#8,d1	;0011
		move.b	d1,d0	;2211
		rts

;--------------------------------------------------------------------------
BM_Prefsrout:
;init Prefs
		moveq	#0,d0
		move.b	BM_Format(pc),d0
		lea	BM_MX(pc),a0
		move.l	d0,(a0)
;---------------------------------------------------------------------------

		move.l	#WindowTags,APG_WindowTagList(a5) ;Tag list for the Window
		move.l	#WinWG+4,APG_WGadgets(a5)	;addr. of WA_Gadgets Tag + 4
		move.l	#winSC+4,APG_WScreen(a5)	;addr. of WA_CustomScreen Tag + 4
		move.l	#BM_window,APG_WinWnd(a5)	;a point for the Window
		move.l	#BM_Glist,APG_Glist(a5)		;a point for the Glist
		move.l	#NTypes,APG_NGads(a5)		;the NGads list
		move.l	#Gtypes,APG_GTypes(a5)		;the GTypes list
		move.l	#Gtags,APG_Gtags(a5)		;the Gtags list
		move.w	#3,APG_CNT(a5)			;the number of Gadgets
		move.l	#BM_gadarray,APG_Gadgets(a5)	;a pointer for the Gadgetsarry, size=4*number of Gadgets
		move.l	APG_Scr(a5),APG_ScrBase(a5)	;the Screenpointer , stored in APG_Scr
	
		move.l	APG_CheckMainWinPos(a5),a6	;returns d0,d1 Pos
		jsr	(a6)

		add.w	#70,d0
		add.w	#100,d1
		
		move.w	d0,APG_WinLeft(a5)		;the left pos. of the Win
		move.w	d1,APG_WinTop(a5)		;the top pos. of the Win
		move.w	#160,APG_WinWidth(a5)		;the width of the Win
		move.w	#70,APG_WinHeight(a5)		;the height of the Win

		move.l	#WinL,APG_WinL(a5)		;addr. of WA_Left Tag 
		move.l	#WinT,APG_WinT(a5)		;addr. of WA_Top Tag
		move.l	#WinW,APG_WinW(a5)		;addr. of WA_Width Tag
		move.l	#WinH,APG_WinH(a5)		;addr. of WA_Height Tag

		move.l	APG_OpenWindow(a5),a6
		jsr	(a6)

		lea	Text0(pc),a2
		move.w	#Project0_TNUM,APG_TNUM(a5)
		move.l	APG_Writetext(a5),a6
		jsr	(a6)

		move.b	BM_Format(pc),APG_Mark1(a5)	;backup the prefs

BM_wait:	move.l	BM_window(pc),a0
		move.l	APG_Wait(a5),a6
		jsr	(a6)				;Handle input

		cmp.l	#GADGETUP,d4
		beq.b	BM_gads
		cmp.l	#GADGETDOWN,d4
		beq.b	BM_gads
		bra.b	BM_wait

BM_gads:
		moveq	#0,d0
		move.w	38(a4),d0

		cmp.w	#GD_format,d0
		beq.b	handleformat
		cmp.w	#GD_Ok,d0
		beq.b	.wech
		cmp.w	#GD_Cancel,d0
		bne.b	BM_wait
		move.b	APG_Mark1(a5),d0
		ext.w	d0
		ext.l	d0
		move.b	d0,BM_Format
		move.l	d0,BM_MX
.wech:
		move.l	BM_window(pc),a0
		move.l	APG_Intuitionbase(a5),a6
		jmp	_LVOCloseWindow(a6)

handleformat:
		move.b	d5,BM_Format
		ext.w	d5
		ext.l	d5
		move.l	d5,BM_MX
		bra.b	BM_wait

;--------------------------------------------------------------------------------
	even

WindowTags:
winL:	dc.l	WA_Left,212
winT:	dc.l	WA_Top,78
winW:	dc.l	WA_Width,220
winH:	dc.l	WA_Height,85
		dc.l	WA_IDCMP,MXIDCMP!GADGETUP!VANILLAKEY!BUTTONIDCMP!IDCMP_REFRESHWINDOW
		dc.l	WA_Flags,WFLG_DEPTHGADGET!WFLG_ACTIVATE!WFLG_DRAGBAR!WFLG_SMART_REFRESH!WFLG_RMBTRAP
winWG:	dc.l	WA_Gadgets,0
		dc.l	WA_Title,winWTitle
winSC:	dc.l	WA_CustomScreen,0
		dc.l	TAG_DONE

WinWTitle:	dc.b	'BMP Options',0
	even

GD_format	=	0
GD_Ok		=	1
GD_Cancel	=	2

Gtypes:
		dc.w	MX_KIND
		dc.w	BUTTON_KIND
		dc.w	BUTTON_KIND
Gtags:
		dc.l	GTMX_Labels,Gadget00Labels
		dc.l	GTMX_Active
BM_MX:		dc.l	0
		dc.l	TAG_DONE
		dc.l	TAG_DONE
		dc.l	TAG_DONE
Gadget00Labels:
		dc.l	Gadget00Lab0
		dc.l	Gadget00Lab1
		dc.l	0

Gadget00Lab0:    DC.B    'Rendered',0
Gadget00Lab1:    DC.B    '24-Bit',0

NTypes:
		DC.W    29,25,17,9
		DC.L    0,0
		DC.W    GD_format
		DC.L    PLACETEXT_RIGHT,0,0
		DC.W    10,50,60,13
		DC.L    oktext,0
		DC.W    GD_Ok
		DC.L    PLACETEXT_IN,0,0
		DC.W    85,50,60,13
		DC.L    canceltext,0
		DC.W    GD_Cancel
		DC.L    PLACETEXT_IN,0,0

oktext:		dc.b	'Ok',0
canceltext:	dc.b	'Cancel',0

Text0:
		DC.B    2,0
		DC.B    RP_JAM1
		DC.B    0
		DC.W    80,10
		DC.L    0
		DC.L    Project0IText0
		DC.L    0

Project0_TNUM EQU 1

Project0IText0:
		DC.B    'Select Save format',0
savebmpmsg:	dc.b	'Save a BMP image!',0

		section	map,BSS
BM_rgbmap:	ds.b	48
BM_Farbtab8pure:ds.l	256
