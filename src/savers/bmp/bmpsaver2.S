����  
��&b�&b�&b�&b�&b�&b�&b�&b;=Cj;===========================================================================
;
;		Test f�r einen Bitmap saver
;		(c) 1995 ,Frank Pagels (Crazy Copper) /DFT
;
;		14.04.95

		incdir 	include:

		include	exec/exec_lib.i
		include	exec/lists.i
		include	exec/memory.i
		include intuition/intuition_lib.i
		include	libraries/wb_lib.i
		include	graphics/graphics_lib.i
		Include utility/tagitem.i
		include	dos/dos.i		
		include	dos/dos_lib.i		
		include	libraries/reqtools_lib.i

		include misc/artpro.i
		
		SAVERHEADER BM_NODE

		dc.b	'$VER: Windows Bitmap saver module 1.01 (18.06.95)',0
	even
;---------------------------------------------------------------------------
BM_NODE:
		dc.l	0,0
		dc.b	0,0
		dc.l	BM_name
		dc.l	BM_Save
		dc.b	'BMPS'
		dc.b	'EXTR'	;for extern Saver
		dc.l	0
		dc.l	BM_Tags
BM_name:
		dc.b	'BMP',0
	even
;---------------------------------------------------------------------------

BM_Tags:
		dc.l	APT_Creator,BM_Creator
		dc.l	APT_Version,1
		dc.l	APT_Info,BM_Info
		dc.l	0

BM_Creator:
		dc.b	'(c) 1995 Frank Pagels /DEFECT Softworks',0
	even
BM_Info:
		dc.b	'Windows bitmap saver',10,10
		dc.b	'Saves an image as BMP (Windows Bitmaps) from 1',10
		dc.b	'to 24Bit.',0
	even

;---------------------------------------------------------------------------

BM_Newwidth	=	APG_Free1
BM_Puffermem	=	APG_Free2
BM_biBitCount	=	APG_Free3
BM_Linemerk	=	APG_Free3+2
BM_Chunkymem	=	APG_Free4
BM_TempLine	=	APG_Free5
BM_Spaces	=	APG_Free6

;---------------------------------------------------------------------------
BM_Save
		clr.b	APG_Mark1(a5)
		
		moveq	#0,d0
		move.b	APG_Ham_Flag(a5),d0
		add.b	APG_Ham8_Flag(a5),d0
		tst.b	d0
		bne.b	.ham

		lea	testmsg,a1
		lea	okmsg,a2
		sub.l	a3,a3
		sub.l	a4,a4
		move.l	APG_RTRequestsTags(a5),a0
		move.l	APG_ReqToolsbase(a5),a6
		jsr	_LVOrtEZRequestA(a6)

		tst.l	d0
		bne.b	.rendered
.ham:		st	APG_Mark1(a5)

;berechne ben�tigten Speicher
.rendered:
		tst.w	APG_Cutflag(a5)
		bne.b	.brushok
		move.w	#0,APG_xbrush1(a5)	;Es soll das ganze 
		move.w	#0,APG_ybrush1(a5)	;Pic abgesaved werden
		move.w	APG_ImageWidth(a5),APG_xbrush2(a5)
		move.w	APG_ImageHeight(a5),APG_ybrush2(a5)	

.brushok:
		moveq	#0,d0
		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
		move.l	d0,d6
		add.l	#3,d0
		lsr.l	#2,d0		
		lsl.l	#2,d0		;breite in Byte
		move.l	d0,BM_Newwidth(a5)
		move.l	d6,d0
		add.l	#15,d0
		lsr.l	#4,d0		;breite in worte
		move.w	d0,APG_Brushword(a5)

		tst.b	APG_mark1(a5)
		beq.b	.no24
		move.w	#24,d1
		bra.b	.planeok
.no24		moveq	#0,d1
		move.b	APG_Planes(a5),d1
		cmp.b	#1,d1
		beq.b	.planeok
		cmp.b	#4,d1
		beq.b	.planeok
		bhi.b	.acht
		moveq	#4,d1
		bra.b	.planeok
.acht:		moveq	#8,d1
.planeok:	move.w	d1,BM_biBitCount(a5)

;berechne Puffer:
		tst.b	APG_Mark1(a5)
		beq.b	.n24
		move.l	d6,d0
		mulu	#3,d0
		addq.l	#3,d0
		lsr.l	#2,d0
		lsl.l	#2,d0
		move.l	d0,d2
		move.l	d6,d3
		mulu	#3,d3
		sub.l	d3,d2
		move.w	d2,BM_Spaces(a5)
.w1:		moveq	#0,d1
		move.w	APG_ybrush2(a5),d1
		sub.w	APG_ybrush1(a5),d1
		move.l	d1,d7
		bra.b	.w

.n24		move.l	BM_Newwidth(a5),d0
		moveq	#0,d1
		move.w	APG_ybrush2(a5),d1
		sub.w	APG_ybrush1(a5),d1
		move.l	d1,d7
		cmp.w	#1,BM_biBitCount(a5)
		bne.b	.w
		move.w	APG_Brushword(a5),d0
		add.w	d0,d0

.w		mulu	d1,d0

		cmp.w	#4,BM_biBitCount(a5)
		bne.b	.nohalf
		lsr.l	#1,d0
		
.nohalf:	add.l	#54,d0		;immer f�r Header

		tst.b	APG_Mark1(a5)
		bne.b	.nocolor
		move.w	BM_biBitCount(a5),d1

		moveq	#1,d2
		lsl.l	d1,d2
		lsl.l	#2,d2		;da ja 4 eintr�ge in Colortab
		
		add.l	d2,d0
		
.nocolor:	move.l	d0,d5
		move.l	#MEMF_PUBLIC+MEMF_CLEAR,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)
		move.l	d0,BM_puffermem(a5)
		bne.b	.ok
		moveq	#-2,d3		;no mem !
		rts
.ok:		

;f�lle BITMAPFILEHEADER 

		move.l	BM_puffermem(a5),a4

		move.w	#'BM',(a4)+

		move.l	d5,d0
		bsr.w	makeDword
		move.l	d0,(a4)+	;filesize		

		clr.l	(a4)+		;reserved

		moveq	#0,d1
		move.w	BM_biBitCount(a5),d1
		moveq	#1,d0
		lsl.l	d1,d0
		lsl.l	#2,d0
		add.l	#54,d0
		tst.b	APG_Mark1(a5)
		beq.b	.nw
		moveq.l	#54,d0
.nw		bsr.w	makeDword
		move.l	d0,(a4)+	;BM_bfoffsetbits

;f�lle BITMAPINFOHEADER 

		move.l	#$28000000,(a4)+ ;biSize

		move.l	d6,d0
		bsr.w	makeDword
		move.l	d0,(a4)+	;imagewidth
		
		move.l	d7,d0
		bsr.w	makeDword
		move.l	d0,(a4)+	;imageheight
				
		move.w	#$0100,(a4)+	;biplanes

		move.w	BM_biBitCount(a5),d0
		bsr.w	makeword
		move.w	d0,(a4)+	;biBitCount

		clr.l	(a4)+		;Compression --> none
		clr.l	(a4)+		;biSizeImage
		clr.l	(a4)+		;biXPelsPerMeter
		clr.l	(a4)+		;biYPelsPerMeter
		clr.l	(a4)+		;biClrUsed
		clr.l	(a4)+		;biClrImportant

		tst.b	APG_Mark1(a5)	;24-Bit ??
		bne.w	BM_Write24
;f�lle Colortab
		move.w	BM_biBitCount(a5),d0
		moveq	#1,d7
		lsl.l	d0,d7
		subq.l	#1,d7	;anzahl colors

		lea	APG_Farbtab8(a5),a0
		addq.l	#4,a0		;header �berlesen
bm_nextcolor:	move.l	(a0)+,d0	;r
		move.l	(a0)+,d1	;g
		move.l	(a0)+,d2	;b
		rol.l	#8,d0		
		rol.l	#8,d1
		rol.l	#8,d2
		move.b	d2,(a4)+	;b
		move.b	d1,(a4)+	;g
		move.b	d0,(a4)+	;r
		clr.b	(a4)+		;reserved
		dbra	d7,bm_nextcolor

;write Bitmap

		moveq	#0,d0
		move.w	APG_Brushword(a5),d0
		lsl.l	#4,d0

;Besorge Speicher f�r eine ChunkyLine

		move.l	#MEMF_PUBLIC+MEMF_CLEAR,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)
		move.l	d0,BM_Chunkymem(a5)
		
		move.l	APG_CheckFirstWord(a5),a6
		jsr	(a6)		

		move.w	APG_ybrush2(a5),BM_linemerk(a5)

		move.w	APG_ybrush2(a5),d7
		move.w	d7,BM_Linemerk(a5)
		tst.w	d7
		beq.b	.w
		subq.w	#1,BM_linemerk(a5)
.w		sub.w	APG_ybrush1(a5),d7
		tst.w	d7
		beq.b	BM_nextline
		subq.w	#1,d7

		cmp.w	#1,BM_biBitCount(a5)
		beq.b	BM_write1

BM_nextline:
		moveq	#0,d0
		move.w	BM_Linemerk(a5),d0
		move.l	BM_Chunkymem(a5),a0
		move.l	APG_Bpl2ChunkyLine(a5),a6
		jsr	(a6)
;copy line
		move.l	BM_Newwidth(a5),d6
		subq.l	#1,d6
		cmp.w	#4,BM_biBitCount(a5)
		bne.b	.nb
.nb4:		move.b	(a0)+,d0	;f�r 4 bit
		lsl.b	#4,d0
		move.b	(a0)+,d1
		or.b	d1,d0
		move.b	d0,(a4)+
		subq.w	#1,d6
		dbra	d6,.nb4
		bra.b	.w
.nb:		move.b	(a0)+,(a4)+
		dbra	d6,.nb
.w		subq.w	#1,BM_Linemerk(a5)
		dbra	d7,BM_nextline
		bra.w	BM_writeall

;-------------------------------------------------------------------------
BM_write1:

BM_newline:	move.w	BM_Linemerk(a5),d5
		move.l	APG_Bytewidth(a5),d3
		mulu	d3,d5

		move.l	APG_Firstword(a5),d0
		add.l	d0,d0		;in Byte
		move.l	APG_Picmem(a5),a0
		add.l	d0,a0
		add.l	d5,a0		;Y-Offset
		
		move.l	APG_Bitshift(a5),d6
		swap	d7
		move.w	APG_brushword(a5),d7
		subq.l	#1,d7
BM_newword:
		move.l	(a0),d0
		lsl.l	d6,d0
		swap	d0
		add.l	#16,d4
		move.w	APG_xbrush2(a5),d1
		cmp.w	d4,d1
		bhs.b	.pixok
		move.w	d4,d3
		sub.w	d1,d3
		lsr.w	d3,d0
		lsl.w	d3,d0	;zuviele Bits l�schen
.pixok:
		move.w	d0,(a4)+

		addq.l	#2,a0
		dbra	d7,BM_newword
		swap	d7
		subq.w	#1,BM_Linemerk(a5)
		move.w	APG_xbrush1(a5),d4
		dbra	d7,BM_newline
		bra.w	BM_Writeall
		
;---------------------------------------------------------------------
BM_Write24:
		moveq	#0,d0
		move.w	APG_ImageWidth(a5),d0
		add.w	#16,d0		;zus�tzlicher buffer
		move.l	d6,d7
		lsl.l	#2,d7
		add.l	d7,d0
		move.l	#$10001,d1
		jsr	_LVOAllocVec(a6)
		move.l	d0,BM_Chunkymem(a5)

		add.l	d7,d0
		move.l	d0,BM_TempLine(a5)
		
		lea	BM_RGBmap,a0	
		moveq	#0,d0
		moveq	#0,d1
		moveq	#0,d2
		move.b	APG_Planes(a5),d0
		move.w	APG_ImageWidth(a5),d1
		moveq	#0,d2
		move.w	APG_ImageHeight(a5),d2
		move.l	APG_gfxbase(a5),a6
		jsr	_LVOinitbitmap(a6)
		
		lea	BM_RGBmap+8,a0
		move.l	APG_Picmem(a5),d0
		moveq	#0,d7		
		move.b	APG_Planes(a5),d7	
		subq	#1,d7
.newplane
		move.l	d0,(a0)+	;Planes in Strucktur einbinden
		add.l	APG_Oneplanesize(a5),d0
		dbra	d7,.newplane

		lea	BM_Farbtab8pure,a0
		lea	APG_Farbtab8(a5),a1
		move.w	(a1)+,d7
		subq.w	#1,d7
		addq.l	#2,a1
.nc:		move.l	(a1)+,d0
		move.l	(a1)+,d1
		move.l	(a1)+,d2
		lsr.l	#8,d0
		swap	d1
		move.w	d1,d0
		rol.l	#8,d2
		move.b	d2,d0
		and.l	#$00ffffff,d0
		move.l	d0,(a0)+
		dbra	d7,.nc		

		move.w	APG_ybrush2(a5),d7
		move.w	d7,BM_Linemerk(a5)
		tst.w	d7
		beq.b	.w
		subq.w	#1,BM_linemerk(a5)
.w		sub.w	APG_ybrush1(a5),d7
		tst.w	d7
		beq.w	BM_nextline
		subq.w	#1,d7

BM_nextline24:	lea	BM_rgbmap,a0
		move.l	BM_Chunkymem(a5),a1
		lea	BM_Farbtab8pure,a2
		move.l	BM_TempLine(a5),a3
		moveq	#0,d0
		moveq	#0,d1
		moveq	#0,d2
		move.w	APG_xbrush1(a5),d0
		move.w	BM_linemerk(a5),d1
		move.l	d6,d2		;Width
		moveq	#1,d3		;one Line

		moveq	#0,d4
		moveq	#0,d5
		move.b	APG_Ham_Flag(a5),d5
		add.b	APG_Ham8_Flag(a5),d5
		tst.b	d5
		beq.b	.noham
		moveq	#1,d4
.noham:		tst.b	APG_EHB_Flag(a5)
		beq.b	.noehb
		moveq	#-1,d4
.noehb:
		move.l	APG_BPL2RGB24(a5),a6
		jsr	(a6)

		move.l	d6,d5
		subq.w	#1,d5
		move.l	BM_Chunkymem(a5),a0
.nchunk:	move.l	(a0)+,d0
		move.b	d0,(a4)+
		lsr.l	#8,d0
		move.b	d0,(a4)+
		lsr.l	#8,d0
		move.b	d0,(a4)+
		dbra	d5,.nchunk
		move.w	BM_Spaces(a5),d5
		beq	.nospace
		subq.w	#1,d5
.nc		clr.b	(a4)+
		dbra	d5,.nc
.nospace:					
		subq.w	#1,BM_Linemerk(a5)
		dbra	d7,BM_nextline24

;---------------------------------------------------------------------
BM_writeall:	move.l	BM_puffermem(a5),d2
		sub.l	d2,a4
		move.l	a4,d3
		move.l	APG_Filehandle(a5),d1
		move.l	APG_Dosbase(a5),a6
		jsr	_LVOWrite(a6)
		
		move.l	BM_puffermem(a5),a1
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)

		move.l	BM_Chunkymem(a5),a1
		jsr	_LVOFreeVec(a6)

		rts

makeDword:
;> d0
;< d0
;11223344 --> 44332211

		move.l	d0,d1	;11223344
		lsl.w	#8,d0	;11224400
		lsr.w	#8,d1	;11220033
		move.b	d1,d0	;11224433
		swap	d0	;44331122
		swap	d1	;00331122
		lsl.w	#8,d0	;44332200
		lsr.w	#8,d1	;00330011
		move.b	d1,d0	;44332211	
		rts
makeword:
;> d0
;< d0
;1122 --> 2211
		move.l	d0,d1	;1122
		lsl.w	#8,d0	;2200
		lsr.w	#8,d1	;0011
		move.b	d1,d0	;2211
		rts

testmsg:	dc.b	'Select save file format:',0
okmsg:		dc.b	'Rendered Image|24-Bit',0

		section	map,BSS
BM_rgbmap:	ds.b	48
BM_Farbtab8pure:ds.l	256
