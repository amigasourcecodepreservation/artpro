����  ,��&b�&b�&b�&b�&b�&b�&b�&b;=V;===========================================================================
;
;		TIFF saver
;		(c) 1997 ,Frank Pagels (Crazy Copper) /DFT
;
;		14.01.97

		incdir 	include:

		include	exec/exec_lib.i
		include	exec/lists.i
		include	exec/memory.i
		include intuition/intuition_lib.i
		include	libraries/wb_lib.i
		include	graphics/graphics_lib.i
		Include utility/tagitem.i
		include	dos/dos.i		
		include	dos/dos_lib.i		
		include	libraries/reqtools_lib.i

		include misc/artpro.i
		include	render/render.i
		include	render/render_lib.i
		include	render/renderhooks.i
		
		SAVERHEADER TF_NODE

		dc.b	'$VER: TIFF saver module 0.71 (24.01.97)',0
	even
;---------------------------------------------------------------------------
TF_NODE:
		dc.l	0,0
		dc.b	0,0
		dc.l	TF_name
		dc.l	TF_save
		dc.b	'TIFS'		;saverkennung f�r ArtPRO
		dc.b	'EXTR'		;for extern saver
		dc.l	0
		dc.l	TF_Tags
TF_name:
		dc.b	'TIFF',0
	even
;---------------------------------------------------------------------------

TF_Tags:
		dc.l	APT_Creator,TF_Creator
		dc.l	APT_Version,1
		dc.l	APT_Info,TF_Info
		dc.l	0

TF_Creator:
		dc.b	'(c) 1997 Frank Pagels /DEFECT Softworks',0
	even
TF_Info:
		dc.b	'TIFF saver',10,10
		dc.b	'Saves an image as TIFF from 1 to 24Bit.',0
	even
;----------------------------------------------------------------------------

;define Tags
NewSubfileType		= $fe
SubFile			= $ff
ImageWidth		= $100
ImageLength		= $101
BitsPerSample		= $102
Compression		= $103
XResolution		= $11a
YResolution		= $11b
ResolutionUnit		= $128
Orientation		= $112
PlanarConfiguration	= $11c
StripOffset		= $111
StripByteCounts		= $117
RowsPerStrip		= $116
SamplesPerPixel		= $115
Thersholding		= $107
CellWidth		= $108
CellLength		= $109
MinSampleValue		= $118
MaxSampleValue		= $119
PhotometricInterpretation	= $106
GrayResponseUnit	= $122
GrayResponseCurve	= $123
ColorResponseUnit	= $12c
ColorResponseCurve	= $12d
FillOrder		= $10a
Group3Options		= $124
Group4Options		= $125
DocumentName		= $10d
PageName		= $11d
XPosition		= $11e
YPosition		= $11f
ImageDescriptor 	= $10e
Make			= $10f
Model			= $110
PageNumber		= $129
FreeOffsets		= $120
FreeByteCount		= $121
ColorMap		= $140
Software		= $131
HostComputer		= $13c

TF_24Bit	=	APG_Mark1
TF_Puffermem	=	APG_Free1
TF_ByteCount	=	APG_Free2
TF_linemerk	=	APG_Free3
TF_Brushbyte	=	APG_Free4
TF_Chunkymem	=	APG_Free5
TF_RGBMem	=	APG_Free6


TF_Bitspersamplemerk:	dc.l	0
TF_Bitspersample:	dc.l	0
TF_Stripmerk:		dc.l	0
TF_PalMerk:		dc.l	0
TF_SoftMerk:		dc.l	0
TF_HostMerk:		dc.l	0
;---------------------------------------------------------------------------
TF_save
		clr.l	TF_Chunkymem(a5)

		moveq	#0,d0
		move.b	APG_Ham_Flag(a5),d0
		add.b	APG_Ham8_Flag(a5),d0
		move.b	d0,TF_24Bit(a5)

		tst.l	APG_Picmem(a5)
		bne	.pmemok
		tst.l	APG_rndr_rgb(a5)
		bne	.w
		moveq	#APSE_NOBITMAP,d3
		rts
.w		st	TF_24Bit(a5)

.pmemok:
		tst.w	APG_Cutflag(a5)
		bne.b	.brushok
		move.w	#0,APG_xbrush1(a5)	;Es soll das ganze 
		move.w	#0,APG_ybrush1(a5)	;Pic abgesaved werden
		move.w	APG_ImageWidth(a5),APG_xbrush2(a5)
		move.w	APG_ImageHeight(a5),APG_ybrush2(a5)	
.brushok:
		move.l	#50000,d0
		move.l	#MEMF_PUBLIC+MEMF_CLEAR,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)
		move.l	d0,TF_Puffermem(a5)
		bne.w	.memok
		moveq	#-2,d3		;no mem !
		rts

.memok

		jsr	APR_Openprocess(a5)

;----- F�lle Header ---------------------

		move.l	TF_Puffermem(a5),a0
		move.l	#$4D4D002A,(a0)+
		move.l	#8,(a0)+
		moveq	#10,d0		;Anzahl der Tags
		tst.b	TF_24Bit(a5)
		beq	.n24
		moveq	#9,d0
.n24		
		move.w	d0,(a0)+
		moveq	#1,d5
		move.w	#ImageWidth,(a0)+
		move.w	#3,(a0)+
		move.l	d5,(a0)+
		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
		move.w	d0,(a0)+
		clr.w	(a0)+

;-------------------------------------------------------------------		
		move.w	#ImageLength,(a0)+
		move.w	#3,(a0)+
		move.l	d5,(a0)+
		move.w	APG_ybrush2(a5),d0
		sub.w	APG_ybrush1(a5),d0
		move.w	d0,(a0)+
		clr.w	(a0)+

;-------------------------------------------------------------------		
		move.w	#BitsPerSample,(a0)+
		move.w	#3,(a0)+
		moveq	#1,d0
		tst.b	TF_24Bit(a5)
		beq	.w24
		moveq	#3,d0
.w24		move.l	d0,(a0)+

		move.l	a0,d0
		move.l	TF_Puffermem(a5),d1
		sub.l	d1,d0
		lea	TF_Bitspersamplemerk(pc),a1
		move.l	d0,(a1)+
		moveq	#0,d1
		tst.b	TF_24Bit(a5)
		bne	.24
		move.b	APG_Planes(a5),d1
		cmp.b	#1,d1
		beq	.write
		cmp.b	#4,d1
		beq	.write
		bhi.s	.w8
		moveq	#4,d1
		bra	.write
.w8:		moveq	#8,d1

.write:		swap	d1
.24:		move.l	d1,(a0)+
		lea	TF_Bitspersample(pc),a1
		swap	d1
		move.l	d1,(a1)

;-------------------------------------------------------------------		
		move.w	#Compression,(a0)+
		move.w	#3,(a0)+
		move.l	d5,d0
		move.l	d0,(a0)+
		swap	d0
		move.l	d0,(a0)+
		
;-------------------------------------------------------------------		
		move.w	#PhotometricInterpretation,(a0)+
		move.w	#3,(a0)+
		move.l	d5,(a0)+
		moveq	#3,d0
		tst.b	TF_24Bit(a5)
		beq	.nrgb
		moveq	#2,d0
.nrgb		swap	d0
		move.l	d0,(a0)+

;-------------------------------------------------------------------		
		move.w	#StripOffset,(a0)+
		move.w	#4,(a0)+
		move.l	d5,(a0)+
		move.l	a0,d0
		move.l	TF_Puffermem(a5),d1
		sub.l	d1,d0
		lea	TF_Stripmerk(pc),a1
		move.l	d0,(a1)
		clr.l	(a0)+
		
;-------------------------------------------------------------------		
		move.w	#SamplesPerPixel,(a0)+
		move.w	#3,(a0)+
		move.l	d5,(a0)+
		moveq	#1,d0
		tst.b	TF_24Bit(a5)
		beq	.wn
		moveq	#3,d0
.wn:		swap	d0
		move.l	d0,(a0)+

;-------------------------------------------------------------------		
		move.w	#Software,(a0)+
		move.w	#2,(a0)+
		move.l	#TF_softmsglen,(a0)+
		move.l	a0,d0
		move.l	TF_Puffermem(a5),d1
		lea	TF_SoftMerk(pc),a1
		move.l	d0,(a1)
		clr.l	(a0)+

		move.w	#HostComputer,(a0)+
		move.w	#2,(a0)+
		move.l	#TF_Hostmsglen,(a0)+
		move.l	a0,d0
		move.l	TF_Puffermem(a5),d1
		lea	TF_HostMerk(pc),a1
		move.l	d0,(a1)
		clr.l	(a0)+

;-------------------------------------------------------------------		
		tst.b	TF_24Bit(a5)
		bne	TF_NixPal

		move.w	#ColorMap,(a0)+
		move.w	#3,(a0)+
		moveq	#0,d0
		move.b	APG_Planes(a5),d0
	move.l	TF_Bitspersample(pc),d0
		moveq	#1,d1
		lsl.l	d0,d1
		mulu	#3,d1
		move.l	d1,(a0)+
		move.l	a0,d0
		move.l	TF_Puffermem(a5),d1
		sub.l	d1,d0
		lea	TF_PalMerk(pc),a1
		move.l	d0,(a1)
		clr.l	(a0)+
TF_NixPal:
		clr.l	(a0)+		;es kommen keine Tags mehr

;Schreibe HostComputer

		move.l	a0,d0
		move.l	TF_Puffermem(a5),a1
		sub.l	a1,d0
		move.l	TF_HostMerk(pc),d1
		move.l	d0,(a1,d1.l)
		lea	TF_HostMsg(pc),a1
		moveq	#TF_HostMsglen-1,d7
.nh:		move.b	(a1)+,(a0)+
		dbra	d7,.nh

;Schreibe Software version

		move.l	a0,d0
		move.l	TF_Puffermem(a5),a1
		sub.l	a1,d0
		move.l	TF_SoftMerk(pc),d1
		move.l	d0,(a1,d1.l)
		lea	TF_SoftMsg(pc),a1
		moveq	#TF_SoftMsglen-1,d7
.nc:		move.b	(a1)+,(a0)+
		dbra	d7,.nc


;Schreibe Palette

		tst.b	TF_24Bit(a5)
		bne	.npal		;Keine Palette
		move.l	a0,d0
		move.l	TF_Puffermem(a5),a1
		sub.l	a1,d0
		move.l	TF_Palmerk(pc),d1
		move.l	d0,(a1,d1.l)

		lea	APG_Farbtab8(a5),a1
		move.l	(a1)+,d7
		swap	d7

	move.l	TF_Bitspersample(pc),d0
	moveq	#1,d7
	lsl.w	d0,d7

		move.l	d7,d5
		move.l	d7,d6
		add.l	d5,d5
		add.l	d6,d6
		add.l	d6,d6
		subq.l	#1,d7
.nextcolor:
		move.l	(a1)+,d0
		swap	d0
		move.w	d0,(a0)
		move.l	(a1)+,d0
		swap	d0
		move.w	d0,(a0,d5.l)
		move.l	(a1)+,d0
		swap	d0
		move.w	d0,(a0,d6.l)
		addq.l	#2,a0
		dbra	d7,.nextcolor
		add.l	d6,a0
.npal:
		tst.b	TF_24Bit(a5)
		beq	.n24
		move.l	a0,d0
		move.l	TF_Bitspersamplemerk(pc),d1
		sub.l	a1,d0
		move.l	TF_Palmerk(pc),d1
		move.l	d0,(a1,d1.l)
		move.w	#$0008,(a0)+
		move.w	#$0008,(a0)+
		move.w	#$0008,(a0)+

.n24:

;Write Body
		move.l	a0,d0
		move.l	TF_Puffermem(a5),a1
		sub.l	a1,d0
		move.l	TF_Stripmerk(pc),d1
		move.l	d0,(a1,d1.l)
		move.l	a1,d2
		sub.l	d2,a0
		move.l	a0,d3
		move.l	APG_dosbase(a5),a6
		move.l	APG_Filehandle(a5),d1
		jsr	_LVOWrite(a6)
		
		move.w	APG_ybrush1(a5),TF_linemerk(a5)

		move.w	APG_ybrush2(a5),d7
		sub.w	APG_ybrush1(a5),d7
		tst.w	d7
		beq	.nz
		subq.w	#1,d7
.nz
		moveq	#0,d0
		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
		move.l	d0,d1
		add.l	#7,d0
		lsr.l	#3,d0		;breite in Byte
		move.w	d0,TF_Brushbyte(a5)
		add.l	#15,d1
		lsr.l	#4,d1
		move.w	d1,APG_BrushWord(a5)

		move.l	APG_CheckFirstWord(a5),a6
		jsr	(a6)		

		move.w	APG_ybrush2(a5),d7
		sub.w	APG_ybrush1(a5),d7
		tst.w	d7
		beq	.nz
		subq.w	#1,d7

		move.l	TF_Puffermem(a5),a4
		move.l	#50000,TF_ByteCount(a5)

		move.l	TF_Bitspersample(pc),d0
		cmp.b	#1,d0
		beq	TF_Write1
		cmp.b	#4,d0
		beq	TF_Write4
		cmp.b	#8,d0
		beq	TF_Write256
		tst.b	TF_24Bit(a5)
		bne	TF_write24
		bra	TF_Ende

;-------- Schreibe OnePlane Bitmap ----------------------------------
TF_Write1:
		lea	TF_savetiff1msg(pc),a1
		move.w	APG_ybrush2(a5),d0
		sub.w	APG_ybrush1(a5),d0
		jsr	APR_InitProcess(a5)


TF_newline:	move.w	TF_Linemerk(a5),d5
		move.l	APG_Bytewidth(a5),d3
		mulu	d3,d5

		move.l	APG_Firstword(a5),d0
		add.l	d0,d0		;in Byte
		move.l	APG_Picmem(a5),a0
		add.l	d0,a0
		add.l	d5,a0		;Y-Offset
		
		move.l	APG_Bitshift(a5),d6
		swap	d7
		move.w	TF_brushbyte(a5),d7
TF_newword:
		move.l	(a0),d0
		lsl.l	d6,d0
		swap	d0
		add.l	#16,d4
		move.w	APG_xbrush2(a5),d1
		cmp.w	d4,d1
		bhs.b	.pixok
		move.w	d4,d3
		sub.w	d1,d3
		lsr.w	d3,d0
		lsl.w	d3,d0	;zuviele Bits l�schen
.pixok:
		move.w	d0,d1
		lsr.w	#8,d0
		move.b	d0,(a4)+
		subq.l	#1,TF_ByteCount(a5)
		bne	.w		
		bsr	TF_WritePuffer
.w		subq.w	#1,d7
		beq	.endline
		move.b	d1,(a4)+
		subq.l	#1,TF_ByteCount(a5)
		bne	.w1
		bsr	TF_WritePuffer
.w1		subq.w	#1,d7
		beq	.endline
		addq.l	#2,a0
		bra	TF_newword		
.endline
		swap	d7
		add.w	#1,TF_Linemerk(a5)
		move.w	APG_xbrush1(a5),d4

		jsr	APR_DoProcess(a5)
		dbra	d7,TF_newline
		bsr	TF_WritePuffer
		bra.w	TF_Ende
		
;---- Save 16 Farben Image ---------------------
TF_Write4:
		lea	TF_savetiff4msg(pc),a1
		move.w	APG_ybrush2(a5),d0
		sub.w	APG_ybrush1(a5),d0
		jsr	APR_InitProcess(a5)

		moveq	#0,d0
		move.w	TF_Brushbyte(a5),d0
		lsl.l	#3,d0
		move.l	#MEMF_PUBLIC+MEMF_CLEAR,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)
		move.l	d0,TF_Chunkymem(a5)

		move.w	APG_xbrush2(a5),d6
		sub.w	APG_xbrush1(a5),d6
TF_newline4:
		swap	d7
		move.w	d6,d7
				
		move.l	TF_Chunkymem(a5),a0
		moveq	#0,d0
		move.w	TF_Linemerk(a5),d0
		move.l	APG_Bpl2ChunkyLine(a5),a6
		jsr	(a6)

		move.l	TF_Chunkymem(a5),a0
.nc:		moveq	#0,d1
		move.b	(a0)+,d0
		subq.w	#1,d7
		beq	.le
		move.b	(a0)+,d1
		subq.w	#1,d7
.le		lsl.b	#4,d0
		or.b	d1,d0
		move.b	d0,(a4)+
		subq.l	#1,TF_ByteCount(a5)
		bne	.w		
		bsr	TF_WritePuffer
.w		tst.w	d7
		bne	.nc

		swap	d7
		add.w	#1,TF_Linemerk(a5)

		jsr	APR_DoProcess(a5)
		dbra	d7,TF_newline4
		bsr	TF_WritePuffer
		bra.w	TF_Ende

;---- Save 256 Farben Image ---------------------
TF_Write256:

		lea	TF_savetiff256msg(pc),a1
		move.w	APG_ybrush2(a5),d0
		sub.w	APG_ybrush1(a5),d0
		jsr	APR_InitProcess(a5)

		moveq	#0,d0
		move.w	TF_Brushbyte(a5),d0
		lsl.l	#3,d0
		move.l	#MEMF_PUBLIC+MEMF_CLEAR,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)
		move.l	d0,TF_Chunkymem(a5)

		move.w	APG_xbrush2(a5),d6
		sub.w	APG_xbrush1(a5),d6
TF_newline256:
		swap	d7
		move.w	d6,d7
				
		move.l	TF_Chunkymem(a5),a0
		moveq	#0,d0
		move.w	TF_Linemerk(a5),d0
		move.l	APG_Bpl2ChunkyLine(a5),a6
		jsr	(a6)

		move.l	TF_Chunkymem(a5),a0
.nc:		moveq	#0,d1
		move.b	(a0)+,(a4)+
		subq.l	#1,TF_ByteCount(a5)
		bne	.w		
		bsr	TF_WritePuffer
.w		subq.w	#1,d7
		bne	.nc

		swap	d7
		add.w	#1,TF_Linemerk(a5)

		jsr	APR_DoProcess(a5)
		dbra	d7,TF_newline256
		bsr	TF_WritePuffer
		bra.w	TF_Ende

;------ save a 24 Bit Image ----------
TF_write24:
		lea	TF_savetiff24msg(pc),a1
		move.w	APG_ybrush2(a5),d0
		sub.w	APG_ybrush1(a5),d0
		jsr	APR_InitProcess(a5)

		moveq	#0,d0
		move.w	TF_Brushbyte(a5),d5
		lsl.l	#3,d5
		move.l	d5,d0
		lsl.l	#2,d0
		add.l	d5,d0
		move.l	#MEMF_PUBLIC+MEMF_CLEAR,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)
		move.l	d0,TF_Chunkymem(a5)
		add.l	d5,d0
		move.l	d0,TF_RGBMem(a5)


		lea	TF_Farbtab8pure,a0
		move.l	a0,a3
		lea	APG_Farbtab8(a5),a1
		move.w	(a1)+,d7
		moveq	#0,d5
		move.w	d7,d5		;f�r ImportPalette merken
		subq.w	#1,d7
		addq.l	#2,a1
.nc:		move.l	(a1)+,d0
		move.l	(a1)+,d1
		move.l	(a1)+,d2
		lsr.l	#8,d0
		swap	d1
		move.w	d1,d0
		rol.l	#8,d2
		move.b	d2,d0
		and.l	#$00ffffff,d0
		move.l	d0,(a0)+
		dbra	d7,.nc

		move.l	APG_rndr_palette(a5),a0
		move.l	a3,a1			;Farbtab
		move.l	d5,d0			;Anzahl Farben
		lea	TF_PalTag(pc),a2
		move.l	APG_RenderBase(a5),a6
		jsr	_LVOImportPaletteA(a6)

		move.w	APG_ybrush2(a5),d7
		sub.w	APG_ybrush1(a5),d7
		tst.w	d7
		beq	.nz
		subq.w	#1,d7
.nz
		move.w	APG_xbrush2(a5),d6
		sub.w	APG_xbrush1(a5),d6

		moveq	#COLORMODE_CLUT,d5
		tst.b	APG_HAM_Flag(a5)
		beq.w	.wc
		moveq	#COLORMODE_HAM6,d5
		bra.b	.ren
.wc		tst.b	APG_HAM8_Flag(a5)
		beq.b	.ren
		moveq	#COLORMODE_HAM8,d5
.ren
		lea	TF_c2rtag(pc),a3
		move.l	d5,4(a3)		;Colormode eintragen

TF_newline24:
		swap	d7
		move.w	d6,d7
				
		move.l	TF_Chunkymem(a5),a0
		moveq	#0,d0
		move.w	TF_Linemerk(a5),d0
		move.l	APG_Bpl2ChunkyLine(a5),a6
		jsr	(a6)

		move.l	TF_Chunkymem(a5),a0	;Chunkypuffer
		move.l	d6,d0
		moveq	#1,d1
		move.l	TF_RGBmem(a5),a1	;RGB puffer
		move.l	APG_rndr_Palette(a5),a2		;Palette
		lea	TF_c2rtag(pc),a3
		move.l	APG_RenderBase(a5),a6
		jsr	_LVOChunky2RGBA(a6)

		move.l	TF_RGBmem(a5),a0
.nc:
		move.l	(a0)+,d0
		move.l	d0,d1
		swap	d1
		move.b	d1,(a4)+
		subq.l	#1,TF_ByteCount(a5)
		bne	.w		
		bsr	TF_WritePuffer
.w
		swap	d1
		and.w	#$ff00,d1
		lsr.w	#8,d1
		move.b	d1,(a4)+
		subq.l	#1,TF_ByteCount(a5)
		bne	.w1		
		bsr	TF_WritePuffer
.w1
		move.b	d0,(a4)+
		subq.l	#1,TF_ByteCount(a5)
		bne	.w2
		bsr	TF_WritePuffer
.w2
		subq.w	#1,d7
		bne	.nc

		swap	d7
		add.w	#1,TF_Linemerk(a5)

		jsr	APR_DoProcess(a5)
		dbra	d7,TF_newline24
		bsr	TF_WritePuffer
		bra.w	TF_Ende

TF_Ende:
		move.l	TF_Puffermem(a5),a1
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)

		rts

TF_WritePuffer:
		move.l	a0,-(a7)
		move.l	TF_Puffermem(a5),d2
		sub.l	d2,a4
		move.l	a4,d3
		move.l	APG_Filehandle(a5),d1
		move.l	APG_Dosbase(a5),a6
		jsr	_LVOWrite(a6)
		move.l	#50000,TF_ByteCount(a5)
		move.l	TF_Puffermem(a5),a4
		move.l	(a7)+,a0
		rts


TF_c2rtag:
		dc.l	RND_ColorMode,0
		dc.l	TAG_DONE

TF_PalTag:	dc.l	RND_NewPalette,1
		dc.l	RND_PaletteFormat,PALFMT_RGB8
		dc.l	TAG_DONE


TF_SoftMsg:
	dc.b	"Written with Defect SoftWork's ArtPRO by Frank Pagels",0
TF_SoftMsglen	=	*-TF_SoftMSg

TF_Hostmsg:	dc.b	'Amiga!',0
TF_Hostmsglen	=	*-TF_Hostmsg


TF_savetiff1msg:	dc.b	'Save a one plane TIFF image',0
TF_savetiff4msg:	dc.b	'Save a 16 color TIFF image',0
TF_savetiff256msg:	dc.b	'Save a 256 color TIFF image',0
TF_savetiff24msg:	dc.b	'Save a 24 bit image',0

		section	laber,bss

TF_Farbtab8pure:	ds.l	256
