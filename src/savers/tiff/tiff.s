����  ��&b�&b�&b�&b�&b�&b�&b�&b;=?~;===========================================================================
;
;		TIFF saver
;		(c) 1997 ,Frank Pagels (Crazy Copper) /DFT
;
;		14.01.97

		incdir 	include:

		include	exec/exec_lib.i
		include	exec/lists.i
		include	exec/memory.i
		include intuition/intuition_lib.i
		include	libraries/wb_lib.i
		include	graphics/graphics_lib.i
		Include utility/tagitem.i
		include	dos/dos.i		
		include	dos/dos_lib.i		
		include	libraries/reqtools_lib.i

		include misc/artpro.i
		
		SAVERHEADER TF_NODE

		dc.b	'$VER: TF saver module 0.2 (15.01.97)',0
	even
;---------------------------------------------------------------------------
TF_NODE:
		dc.l	0,0
		dc.b	0,0
		dc.l	TF_name
		dc.l	TF_save
		dc.b	'TIFS'		;saverkennung f�r ArtPRO
		dc.b	'EXTR'		;for extern saver
		dc.l	0
		dc.l	TF_Tags
TF_name:
		dc.b	'TIFF',0
	even
;---------------------------------------------------------------------------

TF_Tags:
		dc.l	APT_Creator,TF_Creator
		dc.l	APT_Version,1
		dc.l	APT_Info,TF_Info
		dc.l	0

TF_Creator:
		dc.b	'(c) 1997 Frank Pagels /DEFECT Softworks',0
	even
TF_Info:
		dc.b	'TIFF saver',10,10
		dc.b	'Saves an image as TIFF from 1 to 24Bit.',0
	even
;----------------------------------------------------------------------------

;define Tags
NewSubfileType		= $fe
SubFile			= $ff
ImageWidth		= $100
ImageLength		= $101
BitsPerSample		= $102
Compression		= $103
XResolution		= $11a
YResolution		= $11b
ResolutionUnit		= $128
Orientation		= $112
PlanarConfiguration	= $11c
StripOffset		= $111
StripByteCounts		= $117
RowsPerStrip		= $116
SamplesPerPixel		= $115
Thersholding		= $107
CellWidth		= $108
CellLength		= $109
MinSampleValue		= $118
MaxSampleValue		= $119
PhotometricInterpretation	= $106
GrayResponseUnit	= $122
GrayResponseCurve	= $123
ColorResponseUnit	= $12c
ColorResponseCurve	= $12d
FillOrder		= $10a
Group3Options		= $124
Group4Options		= $125
DocumentName		= $10d
PageName		= $11d
XPosition		= $11e
YPosition		= $11f
ImageDescriptor 	= $10e
Make			= $10f
Model			= $110
PageNumber		= $129
FreeOffsets		= $120
FreeByteCount		= $121
ColorMap		= $140
Software		= $131

TF_24Bit	=	APG_Mark1
TF_Puffermem	=	APG_Free1
TF_ByteCount	=	APG_Free2
TF_linemerk	=	APG_Free3
TF_Brushbyte	=	APG_Free4

TF_Bitspersamplemerk:	dc.l	0
TF_Bitspersample:	dc.l	0
TF_Stripmerk:		dc.l	0
TF_PalMerk:		dc.l	0
TF_SoftMerk:		dc.l	0

;---------------------------------------------------------------------------
TF_save

		moveq	#0,d0
		move.b	APG_Ham_Flag(a5),d0
		add.b	APG_Ham8_Flag(a5),d0
		move.b	d0,TF_24Bit(a5)

		tst.l	APG_Picmem(a5)
		bne	.pmemok
		tst.l	APG_rndr_rgb(a5)
		bne	.w
		moveq	#APSE_NOBITMAP,d3
		rts
.w		st	TF_24Bit(a5)

.pmemok:
		tst.w	APG_Cutflag(a5)
		bne.b	.brushok
		move.w	#0,APG_xbrush1(a5)	;Es soll das ganze 
		move.w	#0,APG_ybrush1(a5)	;Pic abgesaved werden
		move.w	APG_ImageWidth(a5),APG_xbrush2(a5)
		move.w	APG_ImageHeight(a5),APG_ybrush2(a5)	
.brushok:
		move.l	#50000,d0
		move.l	#MEMF_PUBLIC+MEMF_CLEAR,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)
		move.l	d0,TF_Puffermem(a5)
		bne.w	.memok
		moveq	#-2,d3		;no mem !
		rts

.memok

;----- F�lle Header ---------------------

		move.l	TF_Puffermem(a5),a0
		move.l	#$4D4D002A,(a0)+
		move.l	#8,(a0)+
		moveq	#9,d0		;Anzahl der Tags
		tst.b	TF_24Bit(a5)
		beq	.n24
		moveq	#8,d0
.n24		
		move.w	d0,(a0)+
		moveq	#1,d5
		move.w	#ImageWidth,(a0)+
		move.w	#3,(a0)+
		move.l	d5,(a0)+
		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
		move.w	d0,(a0)+
		clr.w	(a0)+

;-------------------------------------------------------------------		
		move.w	#ImageLength,(a0)+
		move.w	#3,(a0)+
		move.l	d5,(a0)+
		move.w	APG_ybrush2(a5),d0
		sub.w	APG_ybrush1(a5),d0
		move.w	d0,(a0)+
		clr.w	(a0)+

;-------------------------------------------------------------------		
		move.w	#BitsPerSample,(a0)+
		move.w	#3,(a0)+
		moveq	#1,d0
		tst.b	TF_24Bit(a5)
		beq	.w24
		moveq	#3,d0
.w24		move.l	d0,(a0)+

		move.l	a0,d0
		move.l	TF_Puffermem(a5),d1
		sub.l	d1,d0
		lea	TF_Bitspersamplemerk(pc),a1
		move.l	d0,(a1)+
		moveq	#0,d1
		tst.b	TF_24Bit(a5)
		bne	.24
		move.b	APG_Planes(a5),d1
		cmp.b	#1,d1
		beq	.write
		cmp.b	#4,d1
		beq	.write
		bhi.s	.w8
		moveq	#4,d1
		bra	.write
.w8:		moveq	#8,d1

.write:		swap	d1
.24:		move.l	d1,(a0)+
		lea	TF_Bitspersample(pc),a1
		swap	d1
		move.l	d1,(a1)

;-------------------------------------------------------------------		
		move.w	#Compression,(a0)+
		move.w	#3,(a0)+
		move.l	d5,d0
		move.l	d0,(a0)+
		swap	d0
		move.l	d0,(a0)+
		
;-------------------------------------------------------------------		
		move.w	#PhotometricInterpretation,(a0)+
		move.w	#3,(a0)+
		move.l	d5,(a0)+
		moveq	#3,d0
		tst.b	TF_24Bit(a5)
		beq	.nrgb
		moveq	#2,d0
.nrgb		swap	d0
		move.l	d0,(a0)+

;-------------------------------------------------------------------		
		move.w	#StripOffset,(a0)+
		move.w	#4,(a0)+
		move.l	d5,(a0)+
		move.l	a0,d0
		move.l	TF_Puffermem(a5),d1
		sub.l	d1,d0
		lea	TF_Stripmerk(pc),a1
		move.l	d0,(a1)
		clr.l	(a0)+
		
;-------------------------------------------------------------------		
		move.w	#SamplesPerPixel,(a0)+
		move.w	#3,(a0)+
		move.l	d5,(a0)+
		moveq	#1,d0
		tst.b	TF_24Bit(a5)
		beq	.wn
		moveq	#3,d0
.wn:		swap	d0
		move.l	d0,(a0)+

;-------------------------------------------------------------------		
		move.w	#Software,(a0)+
		move.w	#2,(a0)+
		move.l	#TF_softmsglen,(a0)+
		move.l	a0,d0
		move.l	TF_Puffermem(a5),d1
		lea	TF_SoftMerk(pc),a1
		move.l	d0,(a1)
		clr.l	(a0)+

;-------------------------------------------------------------------		
		tst.b	TF_24Bit(a5)
		bne	TF_NixPal

		move.w	#ColorMap,(a0)+
		move.w	#3,(a0)+
		moveq	#0,d0
		move.b	APG_Planes(a5),d0
		moveq	#1,d1
		lsl.l	d0,d1
		mulu	#3,d1
		move.l	d1,(a0)+
		move.l	a0,d0
		move.l	TF_Puffermem(a5),d1
		sub.l	d1,d0
		lea	TF_PalMerk(pc),a1
		move.l	d0,(a1)
		clr.l	(a0)+
TF_NixPal:
		clr.l	(a0)+		;es kommen keine Tags mehr

;Schreibe Software version

		move.l	a0,d0
		move.l	TF_Puffermem(a5),a1
		sub.l	a1,d0
		move.l	TF_SoftMerk(pc),d1
		move.l	d0,(a1,d1.l)
		lea	TF_SoftMsg(pc),a1
		moveq	#TF_SoftMsglen-1,d7
.nc:		move.b	(a1)+,(a0)+
		dbra	d7,.nc

;Schreibe Palette

		tst.b	TF_24Bit(a5)
		bne	.npal		;Keine Palette
		move.l	a0,d0
		move.l	TF_Puffermem(a5),a1
		sub.l	a1,d0
		move.l	TF_Palmerk(pc),d1
		move.l	d0,(a1,d1.l)

		lea	APG_Farbtab8(a5),a1
		move.l	(a1)+,d7
		swap	d7
		move.l	d7,d5
		move.l	d7,d6
		add.l	d5,d5
		add.l	d6,d6
		add.l	d6,d6
		subq.l	#1,d7
.nextcolor:
		move.l	(a1)+,d0
		swap	d0
		move.w	d0,(a0)
		move.l	(a1)+,d0
		swap	d0
		move.w	d0,(a0,d5.l)
		move.l	(a1)+,d0
		swap	d0
		move.w	d0,(a0,d6.l)
		addq.l	#2,a0
		dbra	d7,.nextcolor
		add.l	d6,a0
.npal:
		tst.b	TF_24Bit(a5)
		beq	.n24
		move.l	a0,d0
		move.l	TF_Bitspersamplemerk(pc),d1
		sub.l	a1,d0
		move.l	TF_Palmerk(pc),d1
		move.l	d0,(a1,d1.l)
		move.w	#$0008,(a0)+
		move.w	#$0008,(a0)+
		move.w	#$0008,(a0)+

.n24:

;Write Body
		move.l	a0,d0
		move.l	TF_Puffermem(a5),a1
		sub.l	a1,d0
		move.l	TF_Stripmerk(pc),d1
		move.l	d0,(a1,d1.l)
		move.l	a1,d2
		sub.l	d2,a0
		move.l	a0,d3
		move.l	APG_dosbase(a5),a6
		move.l	APG_Filehandle(a5),d1
		jsr	_LVOWrite(a6)
		

		move.l	APG_CheckFirstWord(a5),a6
		jsr	(a6)		

		move.w	APG_ybrush1(a5),TF_linemerk(a5)

		move.w	APG_ybrush2(a5),d7
		sub.w	APG_ybrush1(a5),d7
		tst.w	d7
		beq	.nz
		subq.w	#1,d7
.nz
		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
		add.l	#7,d0
		lsr.l	#3,d0		;breite in Byte
		move.w	d0,TF_Brushbyte(a5)


		move.l	TF_Puffermem(a5),a4
		move.l	#50000,TF_ByteCount(a5)

		move.l	TF_Bitspersample(pc),d0
		cmp.b	#1,d0
		beq	TF_Write1

		bra	TF_Ende

;-------- Schreibe OnePlane Bitmap ----------------------------------
TF_Write1:


TF_newline:	move.w	TF_Linemerk(a5),d5
		move.l	APG_Bytewidth(a5),d3
		mulu	d3,d5

		move.l	APG_Firstword(a5),d0
		add.l	d0,d0		;in Byte
		move.l	APG_Picmem(a5),a0
		add.l	d0,a0
		add.l	d5,a0		;Y-Offset
		
		move.l	APG_Bitshift(a5),d6
		swap	d7
		move.w	TF_brushbyte(a5),d7
		subq.w	#1,d7
TF_newword:
		move.l	(a0),d0
		lsl.l	d6,d0
		swap	d0
		add.l	#16,d4
		move.w	APG_xbrush2(a5),d1
		cmp.w	d4,d1
		bhs.b	.pixok
		move.w	d4,d3
		sub.w	d1,d3
		lsr.w	d3,d0
		lsl.w	d3,d0	;zuviele Bits l�schen
.pixok:
		move.w	d0,(a4)+
		subq.l	#2,TF_ByteCount(a5)
		bne	.w		
		bsr	TF_WritePuffer

.w		addq.l	#2,a0
		dbra	d7,TF_newword
		swap	d7
		add.w	#1,TF_Linemerk(a5)
		move.w	APG_xbrush1(a5),d4

		dbra	d7,TF_newline
		bsr	TF_WritePuffer
		bra.w	TF_Ende
		

TF_Ende:

		move.l	TF_Puffermem(a5),a1
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)

		rts

TF_WritePuffer:
		move.l	TF_Puffermem(a5),d2
		sub.l	d2,a4
		move.l	a4,d3
		move.l	APG_Filehandle(a5),d1
		move.l	APG_Dosbase(a5),a6
		jsr	_LVOWrite(a6)
		move.l	#50000,TF_ByteCount(a5)
		move.l	TF_Puffermem(a5),a4
		rts




TF_SoftMsg:
	dc.b	"Written with Defect SoftWork's ArtPRO by Frank Pagels",0
TF_SoftMsglen	=	*-TF_SoftMSg

