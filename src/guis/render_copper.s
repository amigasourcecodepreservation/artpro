*
*  Source machine generated by GadToolsBox V2.0b
*  which is (c) Copyright 1991-1993 Jaba Development
*
*  GUI Designed by : Captain Bifat
*

    include 'exec/types.i'
    include 'intuition/intuition.i'
    include 'intuition/classes.i'
    include 'intuition/classusr.i'
    include 'intuition/imageclass.i'
    include 'intuition/gadgetclass.i'
    include 'libraries/gadtools.i'
    include 'graphics/displayinfo.i'
    include 'graphics/gfxbase.i'

    XREF    _GadToolsBase
    XREF    _IntuitionBase
    XREF    _GfxBase
    XREF    _SysBase

OpenScreenTagList    EQU    -612
OpenWindowTagList    EQU    -606
CloseScreen          EQU    -66
CloseWindow          EQU    -72
PrintIText           EQU    -216
LockPubScreen        EQU    -510
UnlockPubScreen      EQU    -516
SetMenuStrip         EQU    -264
ClearMenuStrip       EQU    -54
GetVisualInfoA       EQU    -126
FreeVisualInfo       EQU    -132
CreateContext        EQU    -114
CreateGadgetA        EQU    -30
GT_RefreshWindow     EQU    -84
FreeGadgets          EQU    -36
CreateMenusA         EQU    -48
LayoutMenusA         EQU    -66
FreeMenus            EQU    -54
OpenDiskFont         EQU    -30
CloseFont            EQU    -78
DrawBevelBoxA        EQU    -120
FreeClass            EQU    -714
NewObjectA           EQU    -636
DisposeObject        EQU    -642
TextLength           EQU    -54
CopyMem              EQU    -624
FindTagItem          EQU    -30
IntuiTextLength      EQU    -330
Forbid               EQU    -132
Permit               EQU    -138

GD_ren_palmode                         EQU    0
GD_ren_colorsslider                    EQU    1
GD_ten_textcolors                      EQU    2
GD_ren_usedcolorslide                  EQU    3
GD_ren_firstcolorslide                 EQU    4
GD_ren_sortmode                        EQU    5
GD_ren_sortorder                       EQU    6
GD_ren_custom                          EQU    7
GD_ren_dither                          EQU    8
GD_ren_amountslide                     EQU    9
GD_ren_amount                          EQU    10
GD_ren_use                             EQU    11
GD_ren_cancel                          EQU    12
GD_ren_render                          EQU    13
GD_ren_usedcolors                      EQU    14
GD_ren_firstcolor                      EQU    15

Project0_CNT    EQU    16

    XDEF    Scr
    XDEF    VisualInfo
    XDEF    Project0Wnd
    XDEF    Project0GList
    XDEF    Project0Gadgets
    XDEF    Project0Left
    XDEF    Project0Top
    XDEF    Project0Width
    XDEF    Project0Height

Scr:
    DC.L    0
VisualInfo:
    DC.L    0
Project0Wnd:
    DC.L    0
Project0GList:
    DC.L    0
Project0Gadgets:
    DCB.L    16,0
BufNewGad:
    DC.W    0,0,0,0
    DC.L    0,0
    DC.W    0
    DC.L    0,0,0
TD:
    DC.L    TAG_DONE
Project0Left:
    DC.W    128
Project0Top:
    DC.W    14
Project0Width:
    DC.W    330
Project0Height:
    DC.W    199

Project0GTypes:
    DC.W    CYCLE_KIND
    DC.W    SLIDER_KIND
    DC.W    TEXT_KIND
    DC.W    SLIDER_KIND
    DC.W    SLIDER_KIND
    DC.W    CYCLE_KIND
    DC.W    CYCLE_KIND
    DC.W    CHECKBOX_KIND
    DC.W    CYCLE_KIND
    DC.W    SLIDER_KIND
    DC.W    STRING_KIND
    DC.W    BUTTON_KIND
    DC.W    BUTTON_KIND
    DC.W    BUTTON_KIND
    DC.W    INTEGER_KIND
    DC.W    INTEGER_KIND

Project0NGads:
    DC.W    102,7,201,13
    DC.L    ren_palmodeText,0
    DC.W    GD_ren_palmode
    DC.L    PLACETEXT_LEFT,0,0
    DC.W    102,25,139,12
    DC.L    ren_colorssliderText,0
    DC.W    GD_ren_colorsslider
    DC.L    PLACETEXT_LEFT,0,0
    DC.W    244,25,55,11
    DC.L    0,0
    DC.W    GD_ten_textcolors
    DC.L    0,0,0
    DC.W    154,58,147,13
    DC.L    0,0
    DC.W    GD_ren_usedcolorslide
    DC.L    0,0,0
    DC.W    154,73,147,13
    DC.L    0,0
    DC.W    GD_ren_firstcolorslide
    DC.L    0,0,0
    DC.W    104,88,197,13
    DC.L    ren_sortmodeText,0
    DC.W    GD_ren_sortmode
    DC.L    PLACETEXT_LEFT,0,0
    DC.W    104,103,197,13
    DC.L    ren_sortorderText,0
    DC.W    GD_ren_sortorder
    DC.L    PLACETEXT_LEFT,0,0
    DC.W    25,43,26,11
    DC.L    ren_customText,0
    DC.W    GD_ren_custom
    DC.L    PLACETEXT_RIGHT,0,0
    DC.W    107,129,197,13
    DC.L    ren_ditherText,0
    DC.W    GD_ren_dither
    DC.L    PLACETEXT_LEFT,0,0
    DC.W    156,145,147,13
    DC.L    0,0
    DC.W    GD_ren_amountslide
    DC.L    0,0,0
    DC.W    106,145,46,13
    DC.L    ren_amountText,0
    DC.W    GD_ren_amount
    DC.L    PLACETEXT_LEFT,0,0
    DC.W    15,177,87,13
    DC.L    ren_useText,0
    DC.W    GD_ren_use
    DC.L    PLACETEXT_IN,0,0
    DC.W    213,177,91,13
    DC.L    ren_cancelText,0
    DC.W    GD_ren_cancel
    DC.L    PLACETEXT_IN,0,0
    DC.W    114,177,87,13
    DC.L    ren_renderText,0
    DC.W    GD_ren_render
    DC.L    PLACETEXT_IN,0,0
    DC.W    104,59,47,12
    DC.L    ren_usedcolorsText,0
    DC.W    GD_ren_usedcolors
    DC.L    PLACETEXT_LEFT,0,0
    DC.W    104,74,48,12
    DC.L    ren_firstcolorText,0
    DC.W    GD_ren_firstcolor
    DC.L    PLACETEXT_LEFT,0,0

Project0GTags:
    DC.L    GTCY_Labels,ren_palmodeLabels
    DC.L    GT_Underscore,'_'
    DC.L    TAG_DONE
    DC.L    GTSL_Min,2
    DC.L    GTSL_Max,256
    DC.L    GTSL_Level,256
    DC.L    GTSL_LevelPlace,PLACETEXT_RIGHT
    DC.L    PGA_Freedom,LORIENT_HORIZ
    DC.L    GA_RelVerify,1
    DC.L    TAG_DONE
    DC.L    GTTX_Text,ten_textcolorsString
    DC.L    GTTX_Border,1
    DC.L    TAG_DONE
    DC.L    GTSL_Max,255
    DC.L    GTSL_Level,255
    DC.L    GTSL_LevelPlace,PLACETEXT_RIGHT
    DC.L    PGA_Freedom,LORIENT_HORIZ
    DC.L    GA_RelVerify,1
    DC.L    TAG_DONE
    DC.L    GTSL_Max,255
    DC.L    GTSL_Level,255
    DC.L    GTSL_LevelPlace,PLACETEXT_RIGHT
    DC.L    PGA_Freedom,LORIENT_HORIZ
    DC.L    GA_RelVerify,1
    DC.L    TAG_DONE
    DC.L    GTCY_Labels,ren_sortmodeLabels
    DC.L    GT_Underscore,'_'
    DC.L    TAG_DONE
    DC.L    GTCY_Labels,ren_sortorderLabels
    DC.L    GT_Underscore,'_'
    DC.L    TAG_DONE
    DC.L    GT_Underscore,'_'
    DC.L    TAG_DONE
    DC.L    GTCY_Labels,ren_ditherLabels
    DC.L    GT_Underscore,'_'
    DC.L    TAG_DONE
    DC.L    GTSL_Max,255
    DC.L    GTSL_Level,255
    DC.L    GTSL_LevelPlace,PLACETEXT_RIGHT
    DC.L    PGA_Freedom,LORIENT_HORIZ
    DC.L    GA_RelVerify,1
    DC.L    GA_Disabled,1
    DC.L    TAG_DONE
    DC.L    GTST_String,ren_amountString
    DC.L    GTST_MaxChars,3
    DC.L    GT_Underscore,'_'
    DC.L    GA_Disabled,1
    DC.L    TAG_DONE
    DC.L    GT_Underscore,'_'
    DC.L    TAG_DONE
    DC.L    GT_Underscore,'_'
    DC.L    TAG_DONE
    DC.L    GT_Underscore,'_'
    DC.L    TAG_DONE
    DC.L    GTIN_Number,0
    DC.L    GTIN_MaxChars,10
    DC.L    GT_Underscore,'_'
    DC.L    TAG_DONE
    DC.L    GTIN_Number,0
    DC.L    GTIN_MaxChars,10
    DC.L    GT_Underscore,'_'
    DC.L    TAG_DONE


ren_colorssliderFormat:
    DC.B    '%ld',0
    CNOP     0,2

ten_textcolorsString:
    DC.B    '16,7M',0
    CNOP    0,2

ren_amountString:
    DC.B    '100',0
    CNOP    0,2

ren_palmodeText:
    DC.B    '_Mode',0

ren_colorssliderText:
    DC.B    'Colors',0

ren_sortmodeText:
    DC.B    '_Sort Mode',0

ren_sortorderText:
    DC.B    'Sort _Order',0

ren_customText:
    DC.B    'C_ustom Palette',0

ren_ditherText:
    DC.B    '_Dither',0

ren_amountText:
    DC.B    '_Amount',0

ren_useText:
    DC.B    '_Use',0

ren_cancelText:
    DC.B    '_Cancel',0

ren_renderText:
    DC.B    '_Render',0

ren_usedcolorsText:
    DC.B    'Colors _Used',0

ren_firstcolorText:
    DC.B    '_First Color',0

    CNOP    0,2

    XDEF    ren_palmodeLabels

ren_palmodeLabels:
    DC.L    ren_palmodeLab0
    DC.L    ren_palmodeLab1
    DC.L    ren_palmodeLab2
    DC.L    ren_palmodeLab3
    DC.L    0

    XDEF    ren_sortmodeLabels

ren_sortmodeLabels:
    DC.L    ren_sortmodeLab0
    DC.L    ren_sortmodeLab1
    DC.L    ren_sortmodeLab2
    DC.L    ren_sortmodeLab3
    DC.L    ren_sortmodeLab4
    DC.L    0

    XDEF    ren_sortorderLabels

ren_sortorderLabels:
    DC.L    ren_sortorderLab0
    DC.L    ren_sortorderLab1
    DC.L    0

    XDEF    ren_ditherLabels

ren_ditherLabels:
    DC.L    ren_ditherLab0
    DC.L    ren_ditherLab1
    DC.L    0

ren_palmodeLab0:    DC.B    'Palette',0
ren_palmodeLab1:    DC.B    'HAM8',0
ren_palmodeLab2:    DC.B    'HAM6',0
ren_palmodeLab3:    DC.B    'EHB Palette',0

    CNOP    0,2

ren_sortmodeLab0:    DC.B    'None',0
ren_sortmodeLab1:    DC.B    'Brightness',0
ren_sortmodeLab2:    DC.B    'Saturation',0
ren_sortmodeLab3:    DC.B    'Popularity',0
ren_sortmodeLab4:    DC.B    'Significance',0

    CNOP    0,2

ren_sortorderLab0:    DC.B    'High to Low',0
ren_sortorderLab1:    DC.B    'Low to High',0

    CNOP    0,2

ren_ditherLab0:    DC.B    'None',0
ren_ditherLab1:    DC.B    'Floyd-Steinberg',0

    CNOP    0,2

    XDEF    topaz8

topaz8:
    DC.L    topazFName8
    DC.W    8
    DC.B    $00,$00

topazFName8:
    DC.B    'topaz.font',0
    CNOP    0,2

    XDEF    Project0WindowTags

Project0WindowTags:
Project0L:
    DC.L    WA_Left,0
Project0T:
    DC.L    WA_Top,0
Project0W:
    DC.L    WA_Width,0
Project0H:
    DC.L    WA_Height,0
    DC.L    WA_IDCMP,CYCLEIDCMP!SLIDERIDCMP!TEXTIDCMP!CHECKBOXIDCMP!STRINGIDCMP!BUTTONIDCMP!INTEGERIDCMP!IDCMP_INTUITICKS!IDCMP_MOUSEBUTTONS!IDCMP_CLOSEWINDOW!IDCMP_REFRESHWINDOW
    DC.L    WA_Flags,WFLG_DRAGBAR!WFLG_DEPTHGADGET!WFLG_CLOSEGADGET!WFLG_SIZEBBOTTOM!WFLG_SMART_REFRESH
Project0WG:
    DC.L    WA_Gadgets,0
    DC.L    WA_Title,Project0WTitle
    DC.L    WA_ScreenTitle,Project0STitle
Project0SC:
    DC.L    WA_CustomScreen,0
    DC.L    TAG_DONE

Project0WTitle:
    DC.B    'Render',0
    CNOP    0,2

Project0STitle:
    DC.B    'GadToolsBox V2.0b � 1991-1993',0
    CNOP    0,2

DriPen:
    DC.W    $FFFF

    XDEF    ScreenTags

ScreenTags:
    DC.L    SA_Left,28
    DC.L    SA_Top,0
    DC.L    SA_Width,640
    DC.L    SA_Height,256
    DC.L    SA_Depth,8
    DC.L    SA_Font,topaz8
    DC.L    SA_Type,CUSTOMSCREEN
    DC.L    SA_DisplayID,DEFAULT_MONITOR_ID!HIRES_KEY
    DC.L    SA_Pens,DriPen
    DC.L    SA_Title,SCT
    DC.L    TAG_DONE

SCT:
    DC.B    'GadToolsBox V2.0b � 1991-1993',0
    CNOP    0,2

    XDEF    SetupScreen

SetupScreen
    movem.l d1-d3/a0-a2/a6,-(sp)
    move.l  _IntuitionBase,a6
    suba.l  a0,a0
    lea.l   ScreenTags,a1
    jsr     OpenScreenTagList(a6)
    move.l  d0,Scr
    tst.l   d0
    beq     SError
    move.l  Scr,a0
    move.l  _GadToolsBase,a6
    lea.l   TD,a1
    jsr     GetVisualInfoA(a6)
    move.l  d0,VisualInfo
    tst.l   d0
    beq     VError
    moveq   #0,d0
SDone:
    movem.l (sp)+,d1-d3/a0-a2/a6
    rts
SError:
    moveq   #1,d0
    bra.s   SDone
VError:
    moveq   #2,d0
    bra.s   SDone

    XDEF    CloseDownScreen

CloseDownScreen:
    movem.l d0-d1/a0-a1/a6,-(sp)
    move.l  _GadToolsBase,a6
    move.l  VisualInfo,a0
    cmpa.l  #0,a0
    beq.s   NoVis
    jsr     FreeVisualInfo(a6)
    move.l  #0,VisualInfo
NoVis:
    move.l  _IntuitionBase,a6
    move.l  Scr,a0
    cmpa.l  #0,a0
    beq.s   NoScr
    jsr     CloseScreen(a6)
    move.l  #0,Scr
NoScr:
    movem.l (sp)+,d0-d1/a0-a1/a6
    rts

    XDEF    OpenProject0Window

OpenProject0Window:
    movem.l d1-d4/a0-a4/a6,-(sp)
    move.l  Scr,a0
    moveq   #0,d3
    moveq   #0,d2
    move.b  sc_WBorLeft(a0),d2
    move.l  sc_Font(a0),a1
    move.w  ta_YSize(a1),d3
    addq.w  #1,d3
    move.b  sc_WBorTop(a0),d0
    ext.w   d0
    add.w   d0,d3
    move.l  _GadToolsBase,a6
    lea.l   Project0GList,a0
    jsr     CreateContext(a6)
    move.l  d0,a3
    tst.l   d0
    beq     Project0CError
    movem.w d2-d3,-(sp)
    moveq   #0,d3
    lea.l   Project0GTags,a4
Project0GL:
    move.l  _SysBase,a6
    lea.l   Project0NGads,a0
    move.l  d3,d0
    mulu    #gng_SIZEOF,d0
    add.l   d0,a0
    lea.l   BufNewGad,a1
    moveq   #gng_SIZEOF,d0
    jsr     CopyMem(a6)
    lea.l   BufNewGad,a0
    move.l  VisualInfo,gng_VisualInfo(a0)
    move.l  #topaz8,gng_TextAttr(a0)
    move.w  gng_LeftEdge(a0),d0
    add.w   (sp),d0
    move.w  d0,gng_LeftEdge(a0)
    move.w  gng_TopEdge(a0),d0
    add.w   2(sp),d0
    move.w  d0,gng_TopEdge(a0)
    move.l  _GadToolsBase,a6
    lea.l   Project0GTypes,a0
    moveq   #0,d0
    move.l  d3,d1
    asl.l   #1,d1
    add.l   d1,a0
    move.w  (a0),d0
    move.l  a3,a0
    lea.l   BufNewGad,a1
    move.l  a4,a2
    jsr     CreateGadgetA(a6)
    tst.l   d0
    bne.s    Project0COK
    movem.w (sp)+,d2-d3
    bra     Project0GError
Project0COK:
    move.l  d0,a3
    move.l  d3,d0
    asl.l   #2,d0
    lea.l   Project0Gadgets,a0
    add.l   d0,a0
    move.l  a3,(a0)
Project0TL:
    tst.l   (a4)
    beq.s   Project0DN
    addq.w  #8,a4
    bra.s   Project0TL
Project0DN:
    addq.w  #4,a4
    addq.w  #1,d3
    cmp.w   #Project0_CNT,d3
    bmi     Project0GL
    movem.w (sp)+,d2-d3
    move.l  Project0GList,Project0WG+4
    move.l  Scr,Project0SC+4
    moveq   #0,d0
    move.w  Project0Left,d0
    move.l  d0,Project0L+4
    move.w  Project0Top,d0
    move.l  d0,Project0T+4
    move.w  Project0Width,d0
    move.l  d0,Project0W+4
    move.w  Project0Height,d0
    add.w   d3,d0
    move.l  d0,Project0H+4
    move.l  _IntuitionBase,a6
    suba.l  a0,a0
    lea.l   Project0WindowTags,a1
    jsr     OpenWindowTagList(a6)
    move.l  d0,Project0Wnd
    tst.l   d0
    beq     Project0WError
    move.l  _GadToolsBase,a6
    move.l  Project0Wnd,a0
    suba.l  a1,a1
    jsr     GT_RefreshWindow(a6)
    moveq   #0,d0
Project0Done:
    movem.l (sp)+,d1-d4/a0-a4/a6
    rts
Project0CError:
    moveq   #1,d0
    bra.s   Project0Done
Project0GError:
    moveq   #2,d0
    bra.s   Project0Done
Project0WError:
    moveq   #4,d0
    bra.s   Project0Done

    XDEF    CloseProject0Window

CloseProject0Window:
    movem.l d0-d1/a0-a2/a6,-(sp)
    move.l  _IntuitionBase,a6
    move.l  Project0Wnd,a0
    cmpa.l  #0,a0
    beq     Project0NWnd
    jsr     CloseWindow(a6)
    move.l  #0,Project0Wnd
Project0NWnd:
    move.l  _GadToolsBase,a6
    move.l  Project0GList,a0
    cmpa.l  #0,a0
    beq     Project0NGad
    jsr     FreeGadgets(a6)
    move.l  #0,Project0GList
Project0NGad:
    movem.l (sp)+,d0-d1/a0-a2/a6
    rts


    end
