Hi Frank...


zum Prefs-Fenster:

ich habe noch etwas Platz gelassen f�r den Fall, da� uns noch
irgendwelche Optionen einfallen sollten. "Save & Use" und "Load" sind
meiner Meinung nach nicht mehr n�tig, weil die Prefs-Sektion sowieso
nur noch allgemeine Einstellungen beinhaltet. "Save", "Use" und
"Cancel" sollten da gen�gen.


Die Texte der Saver-Prefs gefallen mir noch nicht besonders.
Hier meine Vorschl�ge.

IFF-Options
-----------
"Save Format" 


RAW-Options
-----------
"Save Format"     "Source Format"


Palette Options
---------------
"Save Format"     "Source Format"

statt "Format": "Type"


Chunky Options
--------------
Fenstertitel: "Chunky options"!

"Save Format"     "Source Format"

statt "Chunky Type": "Type". Eintr�ge: "Byte" und "RGB"

statt "Normal Type": "Byte Type"


GIF Options
-----------
Statt "Format": "Save Format", wei� statt schwarz


BMP Options
-----------
"Save Format"


PCX Options
-----------
"Save Format"


GIF Anim Options
----------------
�berschrift! "Save Format"


Sprite Options
--------------
Fenstertitel: "Sprite options"!

"Save Format"     "Source Format"

"Labels" geh�rt in die Source-Sektion!
