����  Z��&b�&b�&b�&b�&b�&b�&b�&b;=��;===========================================================================
;
;		Test f�r einen TIFF loader
;		(c) 1995 ,Frank Pagels (Crazy Copper) /DFT
;

		incdir 	include:

		include	exec/exec_lib.i
		include	exec/lists.i
		include	exec/memory.i
		include intuition/intuition_lib.i
		include	libraries/wb_lib.i
		include	graphics/graphics_lib.i
		include utility/tagitem.i
		include	dos/dos.i		
		include	dos/dos_lib.i		

		include misc/artpro.i
		include render/render_lib.i
		

		LOADERHEADER TF_NODE

		dc.b	'$VER: TIFF loader module 1.10 (26.04.98)',0
	even
;---------------------------------------------------------------------------
TF_NODE:
		dc.l	0,0
		dc.b	0,0
		dc.l	TF_name
		dc.l	TF_load
		dc.b	'TIFL'
		dc.b	'EXTR'	;for extern loader
		dc.l	0
		dc.l	TF_Tags
TF_name:
		dc.b	'TIFF',0
	even
;---------------------------------------------------------------------------

TF_Tags:
		dc.l	APT_Creator,TF_Creator
		dc.l	APT_Version,5
		dc.l	APT_Info,TF_Info
		dc.l	APT_Autoload,1
		dc.l	0

TF_Creator:
		dc.b	'Testversion by Frank Pagels /DEFECT Softworks',0
	even
TF_Info:
		dc.b	'TIFF loader',10,10
		dc.b	'Loads TIFF pictures from 1 to 24 bit.',10,10
		dc.b	'Only Packbit compression is supportet!',0
	even
;---------------------------------------------------------------------------
;define Tags
NewSubfileType		= $fe
SubFile			= $ff
ImageWidth		= $100
ImgaeLength		= $101
BitsPerSample		= $102
Compression		= $103
XResolution		= $11a
YResolution		= $11b
ResolutionUnit		= $128
Orientation		= $112
PlanarConfiguration	= $11c
StripOffset		= $111
StripByteCounts		= $117
RowsPerStrip		= $116
SamplesPerPixel		= $115
Thersholding		= $107
CellWidth		= $108
CellLength		= $109
MinSampleValue		= $118
MaxSampleValue		= $119
PhotometricInterpretation	= $106
GrayResponseUnit	= $122
GrayResponseCurve	= $123
ColorResponseUnit	= $12c
ColorResponseCurve	= $12d
FillOrder		= $10a
Group3Options		= $124
Group4Options		= $125
DocumentName		= $10d
PageName		= $11d
XPosition		= $11e
YPosition		= $11f
ImageDescriptor 	= $10e
Make			= $10f
Model			= $110
PageNumber		= $129
FreeOffsets		= $120
FreeByteCount		= $121
ColorMap		= $140

;----------------------------------------------------------------------------
TF_puffermem:	dc.l	0
TF_filesize:	dc.l	0
TF_anztags:	dc.w	0
TF_stripoffset:	dc.l	0
TF_stripbyte:	dc.l	0	;anzahl byte pro Strip
TF_anzcolormap:	dc.l	0
TF_coloroffset: dc.l	0
TF_RowsPerStrip:dc.l	0
TF_BytesSoFar:	dc.w	0
TF_minsampleV:	dc.w	0
TF_maxsampleV:	dc.w	0
TF_Photometric:	dc.w	0
TF_Linemem:	dc.l	0
TF_Compression:	dc.w	0
TF_Striplenght:	dc.l	0
TF_Strippuffer:	dc.l	0
TF_StripPointer:dc.l	0
TF_LenghtPointer:dc.l	0
TF_len		=	*-TF_puffermem

tf_epsoffset	dc.l	0


TF_Prozessor	=	APG_Free1
TF_Alpha	=	APG_Free1+1

;---------------------------------------------------------------------------
TF_load
		lea	TF_puffermem(pc),a0
		moveq	#TF_len-1,d7
.c		clr.b	(a0)+
		dbra	d7,.c
		
		
		clr.l	TF_Linemem
		sf	TF_Alpha(a5)


		move.l	#8120,d0
		move.l	#MEMF_ANY+MEMF_CLEAR,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)
		lea	TF_puffermem(pc),a0
		move.l	d0,(a0)
		bne.b	.ok
		moveq	#-4,d3		;no mem !
		rts
.ok:		
		clr.l	tf_epsoffset
tf_readfirst
		move.l	APG_Dosbase(a5),a6
		move.l	APG_Filehandle(a5),d1
		move.l	TF_puffermem(pc),d2
		move.l	#8,d3
		jsr	_LVORead(a6)

;werte TIFF Header aus

		sf	TF_Prozessor(a5)
		
		move.l	TF_puffermem(pc),a0
		move.l	(a0)+,d0
		cmp.l	#$c5d0d3c6,d0
		beq	TF_epsf			;eine epsf datei
		cmp.l	#$4d4d002a,d0
		beq.w	TF_ok
		cmp.l	#$49492a00,d0
		bne.b	TF_raus1
		st	TF_Prozessor(a5)	;Intel Format
		bra.w	TF_ok
TF_raus1:
		move.l	#'ASDF',d7	;kennung das file nicht erkannt wurde
TF_raus:
		move.l	TF_puffermem(pc),d0
		beq	.nmf
		move.l	d0,a1
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)
		clr.l	TF_puffermem
.nmf
		moveq	#APLE_Error,d3
		cmp.l	#'ASDF',d7
		bne.b	.openok
		moveq	#-3,d3		;File nicht erkannt
.openok:
		rts
;		jmp	APR_Clearprocess(a5)

;eine epsf datei
TF_epsf:
		move.l	APG_Filehandle(a5),d1
		moveq	#20,d2
		move.l	#OFFSET_BEGINNING,d3
		move.l	APG_Dosbase(a5),a6
		jsr	_LVOSeek(a6)		;auf anfang des headers

		move.l	APG_Dosbase(a5),a6
		move.l	APG_Filehandle(a5),d1
		move.l	TF_puffermem(pc),d2
		move.l	#4,d3
		jsr	_LVORead(a6)

		move.l	TF_puffermem(pc),a0
		move.l	(a0),d0
		bsr	TF_MakeLong
		move.l	d0,d2		
		move.l	d2,tf_epsoffset
		move.l	APG_Filehandle(a5),d1
		move.l	#OFFSET_BEGINNING,d3
		move.l	APG_Dosbase(a5),a6
		jsr	_LVOSeek(a6)		;auf anfang des headers
		bra	tf_readfirst


TF_ok:
		jsr	APR_OpenProcess(a5)

		move.l	(a0)+,d2	;offset f�r Header
		tst.b	TF_Prozessor(a5)
		beq.b	.mok		;Motorola
		move.l	d2,d0
		bsr.w	TF_MakeLong	;wandle in Motorola Format
		move.l	d0,d2
		
.mok:		move.l	APG_Filehandle(a5),d1
		move.l	#OFFSET_BEGINNING,d3

	add.l	tf_epsoffset(pc),d2

		move.l	APG_Dosbase(a5),a6
		jsr	_LVOSeek(a6)

		move.l	APG_Filehandle(a5),d1
		move.l	TF_puffermem(pc),d2
		move.l	#2,d3
		jsr	_LVORead(a6)

		move.l	TF_puffermem(pc),a0
		moveq	#0,d3
		move.w	(a0),d3

		tst.b	TF_Prozessor(a5)
		beq.b	.iok1
		move.l	d3,d0
		bsr.w	TF_MakeWord
		move.l	d0,d3
		
.iok1:		lea	TF_anztags(pc),a1
		move.w	d3,(a1)
		mulu	#12,d3
		addq.l	#8,d3

		move.l	APG_Filehandle(a5),d1
		move.l	TF_puffermem(pc),d2
		jsr	_LVORead(a6)		;lese IFD

		lea	TF_minsampleV(pc),a4
		clr.w	(a4)
		lea	TF_maxsampleV(pc),a4
		move.w	#$ffff,(a4)		

		move.b	#1,APG_Planes(a5)	;Default 1 Plane

		move.l	TF_puffermem(pc),a0
		move.w	TF_anztags(pc),d7	;anzahl tags im IFD
		subq.w	#1,d7
TF_nexttag:
		move.w	(a0)+,d0
		tst.b	TF_Prozessor(a5)
		beq.b	.mok
		bsr.w	TF_MakeWord
.mok:
		cmp.w	#NewSubfileType,d0
		beq.w	TF_checksubfile
		cmp.w	#ImageWidth,d0
		beq.w	TF_checkimagewidth
		cmp.w	#ImgaeLength,d0
		beq.w	TF_checkimageheight
		cmp.w	#BitsPerSample,d0
		beq.w	TF_checkbitspersample
		cmp.w	#ColorMap,d0
		beq.w	TF_checkcolormap
		cmp.w	#Compression,d0
		beq.w	TF_checkcompression
		cmp.w	#PhotometricInterpretation,d0
		beq.w	TF_checkphoto
		cmp.w	#StripOffset,d0
		beq.w	TF_checkstripoffset
		cmp.w	#SamplesPerPixel,d0
		beq.w	TF_checksampelsperpixel
		cmp.w	#RowsPerStrip,d0
		beq.w	TF_checkRowsPerStrip
		cmp.w	#MinSampleValue,d0
		beq.b	TF_checkminsample
		cmp.w	#MaxSampleValue,d0
		beq.b	TF_checkmaxsample
		cmp.w	#StripByteCounts,d0
		beq.b	TF_checkbytecounts
		bsr.b	TF_readtag

TF_next:	dbra	d7,TF_nexttag

		moveq	#0,d0
		move.w	APG_ImageHeight(a5),d0
		lea	loadtiffmsg(pc),a1

		cmp.w	#2,TF_Photometric
		bne.b	.w
		lea	loadtiffmsg1(pc),a1
.w		jsr	APR_InitProcess(a5)

		bra.w	TF_readcoltab

TF_readtag:
		tst.b	TF_Prozessor(a5)
		bne.b	.readi
		move.w	(a0)+,d1
		move.l	(a0)+,d2	;l�nge �berlesen
		move.l	(a0)+,d0
		cmp.w	#4,d1
		beq.b	.w
		swap	d0
.w:		rts

.readi:
		move.w	(a0)+,d0
		bsr.w	TF_MakeWord
		move.w	d0,d1
		move.l	(a0)+,d0	;l�nge �berlesen
		bsr.w	TF_MakeLong
		move.l	d0,d2
		move.l	(a0)+,d0
		bsr.w	TF_MakeLong
	;	cmp.w	#4,d1
	;	beq.b	.w1
	;	swap	d0
.w1:		rts


TF_checkbytecounts:
		bsr.b	TF_readtag
		lea	TF_stripbyte(pc),a4
		move.l	d0,(a4)
		bra.b	TF_next		

TF_checkminsample:
		bsr.b	TF_readtag
		lea	TF_minsampleV(pc),a4
		move.w	d0,(a4)
		bra.b	TF_next

TF_checkmaxsample:
		bsr.b	TF_readtag
		lea	TF_maxsampleV(pc),a4
		move.w	d0,(a4)
		bra.b	TF_next

TF_checkRowsPerStrip:
		bsr.b	TF_readtag
		lea	TF_RowsPerStrip(pc),a1
		move.l	d0,(a1)
		bra.b	TF_next
		
TF_checksampelsperpixel:
		bsr.b	TF_readtag

		cmp.w	#3,d0
		bne.w	TF_next
		move.b	#24,APG_Planes(a5)
		bra	TF_next
		
TF_checkstripoffset:
		tst.b	TF_Prozessor(a5)
		bne.b	.intel
		move.w	(a0)+,d1
		move.l	(a0)+,TF_Striplenght
		move.l	(a0)+,d0
		cmp.w	#4,d1
		beq.b	.w
		swap	d0
.w:
		lea	TF_stripoffset(pc),a1	;Begin Imagedata
		move.l	d0,(a1)
		bra.w	TF_next
		
.intel:
		move.w	(a0)+,d0
		bsr.w	TF_Makeword
		move.l	d0,d1
		move.l	(a0)+,d0
		bsr.w	TF_MakeLong
		move.l	d0,TF_Striplenght
		move.l	(a0)+,d0
		bsr.w	TF_MakeLong
		cmp.w	#4,d1
		beq.b	.w
		swap	d0
		bra.b	.w

TF_checksubfile:
		bsr.w	TF_readtag
		tst.l	d0
		beq.w	TF_next
		lea	nomultifilemsg(pc),a4
		move.l	APG_Status(a5),a6
		jsr	(a6)
		bra.w	TF_raus

TF_checkimagewidth:
		bsr.w	TF_readtag
		move.w	d0,APG_ImageWidth(a5)
		bra.w	TF_next

TF_checkimageheight:
		bsr.w	TF_readtag
		move.w	d0,APG_ImageHeight(a5)
		bra.w	TF_next

TF_checkbitspersample:
		bsr.w	TF_readtag
	sf	TF_Alpha(a4)
		move.b	d0,APG_Planes(a5)
		cmp.w	#3,d2			;L�nge gleich 3
		bne.w	.w
		move.b	#24,APG_Planes(a5)	;RGB Image
		bra.w	TF_next
.w		cmp.w	#4,d2			;RGB + Alpha
		bne	TF_next
		move.b	#24,APG_Planes(a5)	;RGB Image
		st	TF_Alpha(a5)
		bra	TF_next

TF_checkcolormap:
		addq.l	#2,a0
		lea	TF_anzcolormap(pc),a1
		move.l	(a0)+,d0
		tst.b	TF_Prozessor(a5)
		beq.b	.mok
		bsr.w	TF_MakeLong
.mok:		move.l	d0,(a1)
		lea	TF_coloroffset(pc),a1
		move.l	(a0)+,d0
		tst.b	TF_Prozessor(a5)
		beq.b	.mok1
		bsr.w	TF_MakeLong
.mok1:		move.l	d0,(a1)
		bra.w	TF_next

TF_checkcompression:
		bsr.w	TF_readtag
		lea	TF_Compression(pc),a4
		move.w	d0,(a4)
		cmp.w	#1,d0
		beq.w	TF_next
		cmp.w	#$8005,d0
		beq.w	TF_next
		lea	nocompmsg(pc),a4
		move.l	APG_Status(a5),a6
		jsr	(a6)
		bra.w	TF_raus

TF_checkphoto:
		bsr.w	TF_readtag
		lea	TF_Photometric(pc),a4
		move.w	d0,(a4)
		cmp.w	#0,d0
		beq.w	TF_next
		cmp.w	#1,d0
		beq.w	TF_next
		cmp.w	#2,d0		;RGB Image
		beq.w	TF_next
		cmp.w	#3,d0
		beq.w	TF_next
		lea	formatmsg(pc),a4
		move.l	APG_Status(a5),a6
		jsr	(a6)
		bra.w	TF_raus

	;*---------- Lese Colortable ein  -------------*

TF_readcoltab:
r		lea	APG_Farbtab8(a5),a1
		lea	APG_Farbtab4(a5),a2

		lea	TF_Photometric(pc),a0

		cmp.w	#2,(a0)
		beq.w	TF_InitStriptab	;Ein RGB Image ohne Palette

		cmp.w	#3,(a0)
		beq.w	TF_readpalette

		moveq	#0,d0
		move.b	APG_Planes(a5),d0
		cmp.b	#1,d0
		beq.b	TF_BW		;ein B/W Pic

		cmp.w	#24,d0
		beq.w	TF_InitStriptab	;Ein RGB Image ohne Palette

;ein Graustufenpic

;erzeuge eine Graustufen Palette

		move.l	d0,d7
		moveq	#1,d7
		lsl.l	d0,d7
		move.w	d7,APG_ColorCount(a5)
		move.w	d7,(a1)+
		clr.w	(a1)+
		
		subq.w	#1,d7

		moveq	#0,d2
		move.w	TF_maxsampleV(pc),d2
		lsr.w	#8,d2
		divu	d7,d2
		ext.w	d2
		
		moveq	#0,d0
.nc
		move.l	d0,d1
		lsl.l	#8,d1
		swap	d1
		move.l	d1,(a1)+
		move.l	d1,(a1)+
		move.l	d1,(a1)+
		move.l	d0,d1
		lsr.w	#4,d1
		mulu	#$111,d1
		move.w	d1,(a2)+
		add.w	d2,d0
		dbra	d7,.nc
		clr.l	(a1)+

		bra.w	TF_MakeBitmap		

;ein black and white Pic --- es mu� bestimmt werden was schwarz und wei� ist 
TF_BW:
		cmp.w	#0,(a0)
		bne.b	.makepalette1
		move.w	#2,APG_ColorCount(a5)		
		move.w	#2,(a1)+
		clr.w	(a1)+
		moveq	#0,d0
		move.w	TF_maxsampleV(pc),d0
		bsr.b	.writevalues
		move.w	TF_minsampleV(pc),d0
		bsr.b	.writevalues
		clr.l	(a1)
		bra.w	TF_MakeBitmap		

.makepalette1:
		move.w	#2,APG_ColorCount(a5)		
		move.w	#2,(a1)+
		clr.w	(a1)+
		moveq	#0,d0
		move.w	TF_minsampleV(pc),d0
		bsr.b	.writevalues
		move.w	TF_maxsampleV(pc),d0
		bsr.b	.writevalues
		clr.l	(a1)
		bra.w	TF_MakeBitmap		

.writevalues:
		move.w	d0,d1
		and.w	#$ff00,d0
		move.w	d0,d1
		swap	d0
		move.l	d0,(a1)+
		move.l	d0,(a1)+
		move.l	d0,(a1)+
		moveq	#0,d2
		lsr.w	#4,d1
		and.w	#$f00,d1
		or.w	d1,d2
		lsr.w	#4,d1
		or.w	d1,d2
		lsr.w	#4,d1
		or.w	d1,d2
		move.w	d2,(a2)+
		rts

	;*---- Lese Palette aus Pic ein ----*
TF_readpalette:
		move.l	TF_coloroffset(pc),d2
		move.l	APG_Filehandle(a5),d1

	add.l	tf_epsoffset(pc),d2

		move.l	#OFFSET_BEGINNING,d3
		move.l	APG_Dosbase(a5),a6
		jsr	_LVOSeek(a6)

		moveq	#0,d0
		moveq	#1,d3
		move.b	APG_Planes(a5),d0
		lsl.l	d0,d3
		move.w	d3,APG_ColorCount(a5)

		mulu	#3*2,d3
		move.l	APG_Filehandle(a5),d1
		move.l	TF_puffermem(pc),d2
		move.l	d2,a4
		jsr	_LVORead(a6)

		moveq	#0,d7
		move.w	APG_ColorCount(a5),d7
		move.l	d7,d5
		move.l	d7,d6
		add.l	d5,d5
		add.l	d6,d6
		add.l	d6,d6
		
		lea	APG_Farbtab8(a5),a1
		lea	APG_Farbtab4(a5),a2
		move.w	d7,(a1)+
		clr.w	(a1)+
		subq.w	#1,d7
.nc:		moveq	#0,d0
		moveq	#0,d1
		moveq	#0,d2
		tst.b	TF_Prozessor(a5)
		beq.b	.mok
		move.w	(a4),d0
		bsr.w	TF_MakeWord
		lsr.w	#8,d0
		move.w	d0,d3
		move.w	(a4,d5),d0
		bsr.w	TF_MakeWord
		lsr.w	#8,d0
		move.w	d0,d1
		move.w	(a4,d6),d0
		bsr.w	TF_MakeWord
		lsr.w	#8,d0
		move.w	d0,d2
		move.w	d3,d0
		bra.b	.weiter

.mok:		
		move.w	(a4),d0		;R
		lsr.w	#8,d0
		move.w	(a4,d5),d1	;G
		lsr.w	#8,d1
		move.w	(a4,d6),d2	;B
		lsr.w	#8,d2
.weiter
		lea	2(a4),a4

		move.l	d0,d3
		lsl.l	#4,d3
		lsl.l	#8,d0
		swap	d0
		move.l	d0,(a1)+
		move.b	d1,d3
		lsl.l	#8,d1
		swap	d1
		move.l	d1,(a1)+
		move.l	d2,d1
		lsr.b	#4,d1
		and.b	#$f,d1
		or.b	d1,d3
		move.w	d3,(a2)+
		lsl.l	#8,d2
		swap	d2
		move.l	d2,(a1)+
		dbra	d7,.nc
		clr.l	(a1)+


	;*------------ Erstelle bitmap ---------------*

TF_MakeBitmap:
		move.l	APG_FindMonitor(a5),a6
		jsr	(a6)
		
		lea	APG_PicMap(a5),a0	
		moveq	#0,d0
		moveq	#0,d1
		moveq	#0,d2
		move.b	APG_Planes(a5),d0
		move.w	APG_ImageWidth(a5),d1
		move.w	APG_ImageHeight(a5),d2	;h�he
		move.l	APG_Gfxbase(a5),a6
		jsr	_LVOinitbitmap(a6)

		moveq	#0,d0
		move.w	APG_ImageWidth(a5),d0
		add.w	#15,d0
		lsr.w	#4,d0
		lsl.w	#1,d0
		move.l	d0,APG_ByteWidth(a5)
		move.w	APG_ImageHeight(a5),d1
		mulu	d1,d0
		move.l	d0,APG_Oneplanesize(a5)

		moveq	#0,d1
		moveq	#0,d7
		move.b	APG_Planes(a5),d7	
		subq.w	#1,d7
.next:
		add.l	d0,d1		;Simuliert mulu.l Befehl
		dbra	d7,.next

		move.l	d1,d0
		move.l	#MEMF_ANY+MEMF_CLEAR,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)
		move.l	d0,APG_Picmem(a5)
		bne.b	.memok
		move.l	TF_puffermem(pc),a1
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)
		moveq	#-4,d3
		rts
.memok:
		moveq	#0,d0		;Speicher f�r eine Zeile besorgen
		move.w	APG_ImageWidth(a5),d0
		add.w	#15,d0
		lsr.w	#4,d0
		lsl.w	#4,d0
		move.l	#MEMF_ANY+MEMF_CLEAR,d1
		jsr	_LVOAllocVec(a6)
		lea	TF_linemem(pc),a0
		move.l	d0,(a0)

		move.l	APG_Picmem(a5),d0
		lea	APG_Picmap+8(a5),a0
		moveq	#0,d7
		move.b	APG_Planes(a5),d7
		subq.w	#1,d7
.np		move.l	d0,(a0)+
		add.l	APG_Oneplanesize(a5),d0
		dbra	d7,.np


;-------------------------------------------------------------------

	;*------ Init Striplenghtab wenn es mehrere Strips gibt

TF_InitStriptab:
	
		lea	TF_Strippuffer(pc),a4
		clr.l	(a4)
		sf	APG_Mark1(a5)

		move.l	TF_Striplenght(pc),d0
		cmp.l	#1,d0
		beq.w	TF_nomorestrips	;nur ein Strip

		st	APG_Mark1(a5)	;multi Strip

		lsl.l	#3,d0	;	f�r die StripLenghtTab gleich auch noch
		move.l	#MEMF_ANY+MEMF_CLEAR,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)
		move.l	d0,(a4)

;Lese Stripoffset Tab

		move.l	APG_Filehandle(a5),d1
		move.l	TF_stripoffset(pc),d2

	add.l	tf_epsoffset(pc),d2

		move.l	#OFFSET_BEGINNING,d3
		move.l	APG_Dosbase(a5),a6
		jsr	_LVOSeek(a6)

		move.l	APG_Filehandle(a5),d1
		move.l	TF_Strippuffer(pc),d2
		move.l	TF_Striplenght(pc),d3
		lsl.l	#2,d3
		move.l	d3,d4
		jsr	_LVORead(a6)

		tst.b	TF_Prozessor(a5)
		beq.b	.mok
		move.l	TF_Strippuffer(pc),a0
		move.l	TF_Striplenght(pc),d7
		sub.w	#1,d7
.ni:		move.l	(a0),d0
		bsr.w	TF_MakeLong
		move.l	d0,(a0)+
		dbra	d7,.ni

;Lese StripLenght Tab
.mok:
		move.l	APG_Filehandle(a5),d1
		move.l	TF_stripbyte(pc),d2

	add.l	tf_epsoffset(pc),d2

		move.l	#OFFSET_BEGINNING,d3
		jsr	_LVOSeek(a6)

		move.l	APG_Filehandle(a5),d1
		move.l	TF_Strippuffer(pc),d2
		add.l	d4,d2
		move.l	d2,a3
		move.l	d4,d3
		jsr	_LVORead(a6)

		tst.b	TF_Prozessor(a5)
		beq.b	.mok1
		move.l	TF_Striplenght(pc),d4
		subq.w	#1,d4
.nst:		move.l	(a3),d0
		bsr.w	TF_MakeLong
		move.l	d0,(a3)+
		dbra	d4,.nst

.mok1:
		lea	TF_StripPointer(pc),a0
		move.l	(a4),a4
		move.l	a4,(a0)		;StripPointer
		add.l	d4,a4
		move.l	a4,4(a0)	;LenghtPointer
		
	;*------------ lese Bitmap daten ein --------------*
TF_nomorestrips:
		move.l	APG_Filehandle(a5),d1
		move.l	TF_stripoffset(pc),d2

		tst.b	APG_Mark1(a5)	;Multistrip ?
		beq.b	.ok	;nein
		move.l	TF_StripPointer(pc),a0
		move.l	(a0),d2
		add.l	#4,TF_StripPointer

.ok:		move.l	#OFFSET_BEGINNING,d3

	add.l	tf_epsoffset(pc),d2

		move.l	APG_Dosbase(a5),a6
		jsr	_LVOSeek(a6)

		moveq	#0,d0
		clr.w	APG_Free2(a5) 		;yStart

		cmp.b	#1,APG_Planes(a5)
		beq.b	TF_readoneplane
		cmp.b	#4,APG_Planes(a5)
		beq.w	TF_read4
		cmp.b	#8,APG_Planes(a5)
		beq.w	TF_read8
		cmp.b	#24,APG_Planes(a5)
		beq.w	TF_read24

		lea	formatmsg(pc),a4
		move.l	APG_Status(a5),a6
		jsr	(a6)
		move.l	TF_linemem(pc),a1
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)
		clr.l	TF_linemem
		bra.w	TF_raus

	;*-------- Lese 1 Bit Bitmap ein --------*
TF_readoneplane:
		moveq	#-1,d4

		tst.b	APG_Mark1(a5)
		beq.b	.onestrip
		
		move.l	TF_LenghtPointer(pc),a0
		move.l	(a0),d4
		add.l	#4,TF_LenghtPointer
.onestrip:
		lea	TF_BytesSoFar(pc),a4

		move.w	APG_ImageHeight(a5),d6
		subq.l	#1,d6

		bsr.w	TF_readbytes

		move.l	APG_Picmem(a5),a1

		move.w	TF_Compression(pc),d0
		cmp.w	#$8005,d0
		beq.b	TF_Read1PackBit

.nl:		move.w	APG_ImageWidth(a5),d7
		add.w	#7,d7
		lsr.w	#3,d7
		subq.w	#1,d7

.nextbyte:	move.b	(a0)+,d0
		sub.w	#1,(a4)		;BytesSoFar
		bne.b	.bytesok
		bsr.w	TF_readbytes
.bytesok:
		subq.l	#1,d4		;StripbyteCounter
		bne.b	.nonewstrip
		bsr.w	TF_setstrip	;setze Zeiger auf Stripanfang
.nonewstrip:
		move.b	d0,(a1)+
		dbra	d7,.nextbyte

		move.l	a1,d0		;teste ob wir auf einer Wordgrenze
		lsr.l	#1,d0		;liegen
		lsl.l	#1,d0
		cmp.l	a1,d0
		beq.b	.wordok
		clr.b	(a1)+
.wordok:
		dbra	d6,.nl
		bra.w	TF_schluss1

	;*------- Lese ein 1 Bit Packbit gepacktes Pic ein ------*
TF_Read1PackBit:

.nl:		move.w	APG_ImageWidth(a5),d7
		add.w	#7,d7
		lsr.w	#3,d7
	;	subq.w	#1,d7

.nextbyte:	move.b	(a0)+,d0
		sub.w	#1,(a4)		;BytesSoFar
		bne.b	.bytesok
		bsr.w	TF_readbytes
.bytesok:
		subq.l	#1,d4		;StripbyteCounter
		bne.b	.nonewstrip
		bsr.w	TF_setstrip	;setze Zeiger auf Stripanfang
.nonewstrip:
		cmp.b	#128,d0
		beq.b	.nextbyte

		tst.b	d0
		bmi.b	.packed	 ;kleiner als Null steuerbyte f�r gepackte Byte

		ext.w	d0

.nextbyte1:	move.b	(a0)+,d1	;databyte
		sub.w	#1,(a4)		;BytesSoFar
		bne.b	.bytesok1
		bsr.w	TF_readbytes
.bytesok1:
		subq.l	#1,d4	;StripbyteCounter
		bne.b	.nonewstrip1
		bsr.w	TF_setstrip	;setze Zeiger auf Stripanfang
.nonewstrip1:
		move.b	d1,(a1)+
		subq.w	#1,d7
		dbra	d0,.nextbyte1

.endline	tst.w	d7
		bne.b	.nextbyte

		move.l	a1,d0		;teste ob wir auf einer Wordgrenze
		lsr.l	#1,d0		;liegen
		lsl.l	#1,d0
		cmp.l	a1,d0
		beq.b	.wordok
		clr.b	(a1)+
.wordok:
		dbra	d6,.nl
		bra.w	TF_schluss1

	;lese gepackte daten ein

.packed:
		ext.w	d0
		neg.w	d0

		move.b	(a0)+,d1	;databyte
		sub.w	#1,(a4)		;BytesSoFar
		bne.b	.bytesok2
		bsr.w	TF_readbytes
.bytesok2:
		subq.l	#1,d4	;StripbyteCounter
		bne.b	.nonewstrip2
		bsr.w	TF_setstrip	;setze Zeiger auf Stripanfang
.nonewstrip2:
		move.b	d1,(a1)+
		subq.w	#1,d7
		dbra	d0,.nonewstrip2

		bra.b	.endline

	;*-------- Lese 4 Bit Chunkys ein --------*
TF_read4:
		moveq	#-1,d4

		tst.b	APG_Mark1(a5)
		beq.b	.onestrip
		
		move.l	TF_LenghtPointer(pc),a0
		move.l	(a0),d4
		add.l	#4,TF_LenghtPointer

.onestrip:
		lea	TF_BytesSoFar(pc),a4

		move.b	APG_Planes(a5),d5
		
		move.w	APG_ImageHeight(a5),d6
		subq.l	#1,d6

		bsr.w	TF_readbytes

		move.w	TF_Compression(pc),d0
		cmp.w	#$8005,d0
		beq.b	TF_Read4PackBit

.nl:		move.w	APG_ImageWidth(a5),d7

		move.l	TF_Linemem(pc),a1

.nextbyte:	move.b	(a0)+,d0
		sub.w	#1,(a4)		;BytesSoFar
		bne.b	.bytesok
		bsr.w	TF_readbytes
.bytesok:
		subq.l	#1,d4	;StripbyteCounter
		bne.b	.nonewstrip

		bsr.w	TF_setstrip	;setze Zeiger auf Stripanfang

.nonewstrip:	move.b	d0,d1
		lsr.b	#4,d0
	;	ror.b	d5,d0		;f�r Chunky2Bpl Vorschiften
		move.b	d0,(a1)+
		subq.w	#1,d7
		bne.b	.nolineend
		bsr.w	TF_copyline
		dbra	d6,.nl
		bra.w	TF_schluss1
.nolineend:
		and.b	#$0f,d1
	;	ror.b	d5,d1
		move.b	d1,(a1)+
		subq.w	#1,d7
		bne.b	.nextbyte
		bsr.w	TF_copyline
		dbra	d6,.nl
		bra.w	TF_schluss1		

	;*------- Lese ein 4 Bit Packbit gepacktes Pic ein ------*
TF_Read4PackBit:
.nl:		move.w	APG_ImageWidth(a5),d7

		move.l	TF_Linemem(pc),a1

.nextbyte:	move.b	(a0)+,d0
		sub.w	#1,(a4)		;BytesSoFar
		bne.b	.bytesok
		bsr.w	TF_readbytes

.bytesok:
		subq.l	#1,d4		;StripbyteCounter
		bne.b	.nonewstrip
		bsr.w	TF_setstrip	;setze Zeiger auf Stripanfang
.nonewstrip:
		cmp.b	#128,d0
		beq.b	.nextbyte

		tst.b	d0
		bmi.b	.packed	 ;kleiner als Null steuerbyte f�r gepackte Byte

		ext.w	d0

.nextbyte1:	move.b	(a0)+,d1
		sub.w	#1,(a4)		;BytesSoFar
		bne.b	.bytesok1
		bsr.w	TF_readbytes
.bytesok1:
		subq.l	#1,d4		;StripbyteCounter
		bne.b	.nonewstrip1
		bsr.w	TF_setstrip	;setze Zeiger auf Stripanfang
.nonewstrip1:
		move.b	d1,d2
		lsr.b	#4,d2
	;	ror.b	d5,d2		;f�r Chunky2Bpl Vorschiften
		move.b	d2,(a1)+
		subq.w	#1,d7
		bne.b	.nolineend

		bsr.w	TF_copyline
		dbra	d6,.nl
		bra.w	TF_schluss1
.nolineend:
		and.b	#$0f,d1
	;	ror.b	d5,d1
		move.b	d1,(a1)+
		subq.w	#1,d7
		dbra	d0,.nextbyte1

		tst.w	d7
		bne.b	.nextbyte

		bsr.w	TF_copyline
		dbra	d6,.nl

		bra.w	TF_schluss1
.packed:
		ext.w	d0
		neg.w	d0

.nextbyte2:	move.b	(a0)+,d1	;DatenByte lesen
		sub.w	#1,(a4)		;BytesSoFar
		bne.b	.bytesok2
		bsr.w	TF_readbytes
.bytesok2:
		subq.l	#1,d4		;StripbyteCounter
		bne.b	.nonewstrip2
		bsr.w	TF_setstrip	;setze Zeiger auf Stripanfang
.nonewstrip2:
		move.b	d1,d2
		lsr.b	#4,d1
	;	ror.b	d5,d1		;f�r Chunky2Bpl Vorschiften

		and.b	#$0f,d2
	;	ror.b	d5,d2

.nb:
		move.b	d1,(a1)+
		sub.w	#1,d7
		move.b	d2,(a1)+
		sub.w	#1,d7
		dbra	d0,.nb

		tst.w	d7
	;	bmi	.wech1
		bne.w	.nextbyte
.wech1		bsr.w	TF_copyline
		dbra	d6,.nl
		bra.w	TF_schluss1

	;*-------- Lese 8 Bit Chunkys ein --------*
TF_read8:
		moveq	#-1,d4

		tst.b	APG_Mark1(a5)
		beq.b	.onestrip
		
		move.l	TF_LenghtPointer(pc),a0
		move.l	(a0),d4
		add.l	#4,TF_LenghtPointer
.onestrip:
		lea	TF_BytesSoFar(pc),a4

		move.b	APG_Planes(a5),d5
		
		move.w	APG_ImageHeight(a5),d6
		subq.l	#1,d6

		bsr.w	TF_readbytes

		move.w	TF_Compression(pc),d0
		cmp.w	#$8005,d0
		beq.b	TF_Read8PackBit

.nl:		move.w	APG_ImageWidth(a5),d7
		subq.l	#1,d7

		move.l	TF_Linemem(pc),a1

.nextbyte:	move.b	(a0)+,d0
		sub.w	#1,(a4)		;BytesSoFar
		bne.b	.bytesok
		bsr.w	TF_readbytes

.bytesok:	subq.l	#1,d4		;StripbyteCounter
		bne.b	.nonewstrip
		bsr.w	TF_setstrip	;setze Zeiger auf Stripanfang
.nonewstrip:
	;	ror.b	d5,d0		;f�r Chunky2Bpl Vorschiften
		move.b	d0,(a1)+
		dbra	d7,.nextbyte
		bsr.w	TF_copyline
		dbra	d6,.nl
		bra.w	TF_schluss1


	;*------- Lese ein 8 Bit Packbit gepacktes Pic ein ------*
TF_Read8PackBit:


.nl:		move.w	APG_ImageWidth(a5),d7
	;	subq.l	#1,d7

		move.l	TF_Linemem(pc),a1

.nextbyte:	move.b	(a0)+,d0
		sub.w	#1,(a4)		;BytesSoFar
		bne.b	.bytesok
		bsr.w	TF_readbytes

.bytesok:
		subq.l	#1,d4		;StripbyteCounter
		bne.b	.nonewstrip
		bsr.w	TF_setstrip	;setze Zeiger auf Stripanfang
.nonewstrip:
		cmp.b	#128,d0
		beq.b	.nextbyte

		tst.b	d0
		bmi.b	.packed	 ;kleiner als Null steuerbyte f�r gepackte Byte

		ext.w	d0

.nextbyte1:	move.b	(a0)+,d1
		sub.w	#1,(a4)		;BytesSoFar
		bne.b	.bytesok1
		bsr.w	TF_readbytes
.bytesok1:
		subq.l	#1,d4		;StripbyteCounter
		bne.b	.nonewstrip1
		bsr.w	TF_setstrip	;setze Zeiger auf Stripanfang
.nonewstrip1:
	;	ror.b	d5,d1
		move.b	d1,(a1)+
		subq.w	#1,d7
		bmi.b	.wech
		dbra	d0,.nextbyte1

		tst.w	d7
		;bmi	.wech
		bne.b	.nextbyte
		bsr.w	TF_copyline
.wech:		dbra	d6,.nl
		bra.w	TF_schluss1

.packed:
		ext.w	d0
		neg.w	d0

.nextbyte2:	move.b	(a0)+,d1	;DatenByte lesen
		sub.w	#1,(a4)		;BytesSoFar
		bne.b	.bytesok2
		bsr.w	TF_readbytes
.bytesok2:
		subq.l	#1,d4		;StripbyteCounter
		bne.b	.nonewstrip2
		bsr.w	TF_setstrip	;setze Zeiger auf Stripanfang
.nonewstrip2:
	;	ror.b	d5,d1

.nb:		move.b	d1,(a1)+
		subq.w	#1,d7
		bmi.b	.wech1
		dbra	d0,.nb

		tst.w	d7
	;	bmi	.wech1
		bne.b	.nextbyte
.wech1		bsr.w	TF_copyline
		dbra	d6,.nl
		bra.w	TF_schluss1


	;*-------- Lese 24 Bit Image ein --------*
TF_read24:

;init some Stuff

		moveq	#0,d0
		move.w	APG_ImageWidth(a5),d0
		add.w	#15,d0
		lsr.w	#4,d0
		lsl.w	#1,d0
		move.l	d0,APG_ByteWidth(a5)
		move.w	APG_ImageHeight(a5),d1
		mulu	d1,d0
		move.l	d0,APG_Oneplanesize(a5)

		move.b	#24,APG_Planes(a5)
		move.l	APG_FindMonitor(a5),a6
		jsr	(a6)
		move.b	#24,APG_Planes(a5)

;Init Render Mem = RGB Puffer

		move.l	APG_ByteWidth(a5),d0
		move.w	APG_ImageHeight(a5),d1
		jsr	APR_InitRenderMem(a5)
		tst.l	d0
		bne.b	.memok

		move.l	TF_Strippuffer(pc),d0
		beq.b	.nofree
		move.l	d0,a1
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)
.nofree:
		move.l	TF_puffermem(pc),d0
		beq	.nmf
		move.l	d0,a1
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)
		clr.l	TF_puffermem
.nmf
		moveq	#APLE_NOMEM,d3
		rts

;-------------------------------------------------------------------------
.memok:		
		moveq	#-1,d4

		tst.b	APG_Mark1(a5)
		beq.b	.onestrip
		
		move.l	TF_LenghtPointer(pc),a0
		move.l	(a0),d4
		add.l	#4,TF_LenghtPointer
.onestrip:
		lea	TF_BytesSoFar(pc),a4

		move.w	APG_ImageHeight(a5),d6
		subq.l	#1,d6

		bsr.w	TF_readbytes

		move.l	APG_rndr_rgb(a5),a1	;RGB Puffer

		move.l	APG_Bytewidth(a5),d0
		lsl.l	#3+2,d0
		moveq	#0,d1
		move.w	APG_ImageWidth(a5),d1
		lsl.l	#2,d1
		sub.l	d1,d0			;Modulo
		move.l	d0,a3		

		move.w	TF_Compression(pc),d0
		cmp.w	#$8005,d0
		beq.b	TF_Read24PackBit

.nl:		move.w	APG_ImageWidth(a5),d7
		subq.l	#1,d7

.nextbyte1:
		moveq	#0,d0
		moveq	#3-1,d5		;3 Farben

	tst.b	TF_Alpha(a5)
	beq	.nextbyte
	moveq	#4-1,d5

.nextbyte:
		lsl.l	#8,d0
		move.b	(a0)+,d0
		sub.w	#1,(a4)		;BytesSoFar
		bne.b	.bytesok
		bsr.w	TF_readbytes

.bytesok:	subq.l	#1,d4		;StripbyteCounter
		bne.b	.nonewstrip
		bsr.w	TF_setstrip	;setze Zeiger auf Stripanfang
.nonewstrip:
		dbra	d5,.nextbyte

		tst.b	TF_Alpha(a5)
		beq	.w1
		lsr.l	#8,d0		;eventuellen alpha wegmaskieren

.w1		move.l	d0,(a1)+


		dbra	d7,.nextbyte1

		add.l	a3,a1		;Zeilen Modulo addieren

		jsr	APR_DoProcess(a5)

		tst.l	d0
		beq	TF_Stop

		dbra	d6,.nl

		st	APG_RenderFlag(a5)	;Wir m�ssen erst rendern

		bra.w	TF_schluss1


	;*------- Lese ein 8 Bit Packbit gepacktes Pic ein ------*
TF_Read24PackBit:


.nl:		move.w	APG_ImageWidth(a5),d7

		moveq	#0,d1
		moveq	#3,d5		;3 Farben
	tst.b	TF_Alpha(a5)
	beq	.nextbyte
	moveq	#4,d5

.nextbyte:
		move.b	(a0)+,d0
		sub.w	#1,(a4)		;BytesSoFar
		bne.b	.bytesok
		bsr.w	TF_readbytes
.bytesok:
		subq.l	#1,d4		;StripbyteCounter
		bne.b	.nonewstrip
		bsr.w	TF_setstrip	;setze Zeiger auf Stripanfang
.nonewstrip:
		cmp.b	#128,d0
		beq.b	.nextbyte

		tst.b	d0
		bmi.b	.packed	 ;kleiner als Null steuerbyte f�r gepackte Byte

		ext.w	d0

.nextbyte1:	lsl.l	#8,d1
		move.b	(a0)+,d1
		sub.w	#1,(a4)		;BytesSoFar
		bne.b	.bytesok1
		bsr.w	TF_readbytes
.bytesok1:
		subq.l	#1,d4		;StripbyteCounter
		bne.b	.nonewstrip1
		bsr.w	TF_setstrip	;setze Zeiger auf Stripanfang
.nonewstrip1:
		subq.w	#1,d5
		bne	.nc		;RGB Wert noch nicht voll
	tst.b	TF_Alpha(a5)
	beq	.wr
	lsr.l	#8,d1
.wr		move.l	d1,(a1)+

		moveq	#0,d1
		moveq	#3,d5		;3 Farben
	tst.b	TF_Alpha(a5)
	beq	.w
	moveq	#4,d5
	
.w		subq.w	#1,d7
		bmi.b	.wech
.nc:		dbra	d0,.nextbyte1

		tst.w	d7
		;bmi	.wech
		bne.b	.nextbyte

		jsr	APR_DoProcess(a5)

		tst.l	d0
		beq	TF_Stop


.wech:		dbra	d6,.nl
		bra.w	TF_schluss1

.packed:
		ext.w	d0
		neg.w	d0

.nextbyte2:
		move.b	(a0)+,d2	;DatenByte lesen
		sub.w	#1,(a4)		;BytesSoFar
		bne.b	.bytesok2
		bsr.b	TF_readbytes
.bytesok2:
		subq.l	#1,d4		;StripbyteCounter
		bne.b	.nonewstrip2
		bsr.b	TF_setstrip	;setze Zeiger auf Stripanfang
.nonewstrip2:

.nb:		lsl.l	#8,d1
		move.b	d2,d1
		subq.w	#1,d5
		bne	.ww

	tst.b	TF_Alpha(a5)
	beq	.wr1
	lsr.l	#8,d1
.wr1		move.l	d1,(a1)+
		subq.w	#1,d7
		bmi.b	.wech1
		moveq	#0,d1
		moveq	#3,d5
	tst.b	TF_Alpha(a5)
	beq	.ww
	moveq	#4,d5
.ww
		dbra	d0,.nb

		tst.w	d7
	;	bmi	.wech1
		bne.w	.nextbyte
.wech1		jsr	APR_DoProcess(a5)
		dbra	d6,.nl

		st	APG_RenderFlag(a5)	;Wir m�ssen erst rendern

		bra.b	TF_schluss1


	;*-------- Lese 8k in Puffer ----------*

TF_readbytes:
		movem.l	d0-a6,-(a7)	
		move.l	APG_Dosbase(a5),a6
		move.l	APG_Filehandle(a5),d1
		move.l	TF_puffermem(pc),d2
		move.l	#8000,d3
		jsr	_LVORead(a6)
		tst.l	d0
		bpl.b	.readok
		moveq	#-5,d3		;readerror
		movem.l	(a7)+,d0-a6
		addq.l	#4,a7
		bra.b	TF_Schluss			
.readok:
		move.w	d0,(a4)		;BytesSoFar
		movem.l	(a7)+,d0-a6
		move.l	TF_Puffermem(pc),a0
		rts

	;*------ Setze Zeiger auf neuen Stripanfang --------*
TF_setstrip:
		movem.l	d0-a6,-(a7)	
		move.l	APG_Filehandle(a5),d1

		move.l	TF_StripPointer(pc),a0
		move.l	(a0),d2
		add.l	#4,TF_StripPointer

.ok:		move.l	#OFFSET_BEGINNING,d3

	add.l	tf_epsoffset(pc),d2

		move.l	APG_Dosbase(a5),a6
		jsr	_LVOSeek(a6)

		movem.l	(a7)+,d0-a6

		bsr.b	TF_readbytes

		move.l	TF_LenghtPointer(pc),a6
		move.l	(a6),d4
		add.l	#4,TF_LenghtPointer
		rts
		
;---------------------------------------------------------------------------
TF_schluss1:
		move.w	#1,APG_LoadFlag(a5)
		moveq	#0,d3
TF_schluss:
		
		move.l	4.w,a6
		move.l	TF_linemem(pc),d0
		beq.b	.w
		move.l	d0,a1
		jsr	_LVOFreeVec(a6)
		clr.l	TF_linemem
.w		
		move.l	TF_Strippuffer(pc),d0
		beq.b	.w1
		move.l	d0,a1
		jsr	_LVOFreeVec(a6)
		clr.l	TF_Strippuffer

.w1:		move.l	TF_puffermem(pc),d0
		beq.b	TF_wech
		move.l	d0,a1
		jsr	_LVOFreeVec(a6)		
		clr.l	TF_puffermem

TF_wech:
;		jsr	APR_ClearProcess(a5)
		cmp.l	#'STOP',d7		;wurde laden abgebrochen
		bne	.w
		moveq	#APLE_ABORT,d3
.w		rts

;--------------------------------------------------------------------------
TF_Stop:
		move.l	4.w,a6
		move.l	APG_rndr_rendermem(a5),d0
		beq	.nrf
		move.l	d0,a1
		jsr	_LVOFreeVec(a6)
		clr.l	APG_rndr_rendermem(a5)
.nrf:		move.l	APG_Picmem(a5),d0
		beq	.w
		move.l	d0,a1
		jsr	_LVOFreeVec(a6)
.w		move.l	#'STOP',d7
		bra	TF_schluss


TF_copyline:
		movem.l	d0-a6,-(a7)

		move.l	TF_Linemem(pc),a0
		lea	APG_PicMap(a5),a1
		sub.l	a2,a2
		moveq	#0,d0
		moveq	#0,d1
		move.l	APG_ByteWidth(a5),d2
		lsl.l	#3,d2
		moveq	#1,d3		;eine Zeile
		moveq	#0,d4
		move.w	APG_Free2(a5),d5	;yStart
		move.l	APG_Renderbase(a5),a6
		jsr	_LVOChunky2BitmapA(a6)

		addq.w	#1,APG_Free2(a5)	;yStart

		jsr	APR_DoProcess(a5)
		tst.l	d0
		beq	.TF_Stop

		movem.l	(a7)+,d0-a6
		rts

.TF_Stop:
		movem.l	(a7)+,d0-a6
		addq.l	#4,a7
		bra	TF_Stop

TF_MakeLong:
		move.l	d1,-(a7)
		move.l	d0,d1	;11223344
		lsl.w	#8,d0	;11224400
		lsr.w	#8,d1	;11220033
		move.b	d1,d0	;11224433
		swap	d0	;44331122
		swap	d1	;00331122
		lsl.w	#8,d0	;44332200
		lsr.w	#8,d1	;00330011
		move.b	d1,d0	;44332211	
		move.l	(a7)+,d1
		rts

TF_MakeWord:
		move.l	d1,-(a7)
		move.l	d0,d1	;1122
		lsl.w	#8,d0	;2200
		lsr.w	#8,d1	;0011
		move.b	d1,d0	;2211
		move.l	(a7)+,d1
		rts

nomultifilemsg:	dc.b	'TIFF: Multifile not supported!',0		
nocompmsg:	dc.b	'TIFF: Compression not supported!',0
formatmsg:	dc.b	'TIFF: Format not supported!',0
loadtiffmsg:	dc.b	'Loading TIFF image!',0
loadtiffmsg1:	dc.b	'Loading 24-Bit TIFF image!',0
