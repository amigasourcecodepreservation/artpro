����  #3�&b�&b�&b�&b�&b�&b�&b�&b;=�(;-------------------------------------------------------------------------
;
;		JPEG saver modul for ArtPRO
;		(c) 1997 ,Frank (Copper) Pagels /Defect Softworks
;
;		12.05.1997
;
;
		incdir 	include:

		include	exec/exec_lib.i
		include	exec/lists.i
		include	exec/memory.i
		include intuition/intuition_lib.i
		include	libraries/wb_lib.i
		include libraries/gadtools.i
		include libraries/gadtools_lib.i
		include	graphics/graphics_lib.i
		Include utility/tagitem.i
		include	dos/dos.i		
		include	dos/dos_lib.i		
		include	libraries/reqtools_lib.i
		include	libraries/tower_lib.i

		include misc/artpro.i
		include	render/render.i
		include	render/render_lib.i
		include	render/renderhooks.i

		
TA_Dummy	=	TAG_USER+$00070000
TM_Dummy	=	$00070000

CDA_Dummy	=	TA_Dummy+$3200

CDA_Stream	=	CDA_Dummy+1
CDA_StreamType	=	CDA_Dummy+2
CDA_Coding	=	CDA_Dummy+3
CDA_Quality	=	CDA_Dummy+6

CDST_OBJECT	=	0
CDST_reserved	=	1	/* Do not use */
CDST_FILE	=	2

PCDA_Dummy	=	CDA_Dummy+$80

PCDA_Width	=	PCDA_Dummy+1
PCDA_Height	=	PCDA_Dummy+2
PCDA_Components	=	PCDA_Dummy+3
PCDA_ColorSpace	=	PCDA_Dummy+4

PCDCS_RGB	=	2

CDM_Dummy	=	TM_Dummy+$3200

CDM_START	=	CDM_Dummy+1
CDM_PROCESS	=	CDM_Dummy+2
CDM_FINISH	=	CDM_Dummy+3
CDM_ABORT	=	CDM_Dummy+4
CDM_READHEADER	=	CDM_Dummy+6
CDM_WRITEHEADER	=	CDM_Dummy+7


		SAVERHEADER JP_NODE

		dc.b	'$VER: JPEG saver module 0.90 (14.05.96)',0
	even
;---------------------------------------------------------------------------
JP_NODE:
		dc.l	0,0
		dc.b	0,0
		dc.l	JP_name
		dc.l	JP_save
		dc.b	'JPGS'		;saverkennung f�r ArtPRO
		dc.b	'EXTR'		;for extern saver
		dc.l	0
		dc.l	JP_Tags
JP_name:
		dc.b	'JPEG',0
	even
;---------------------------------------------------------------------------

JP_Tags:
		dc.l	APT_Creator,JP_Creator
		dc.l	APT_Version,2
		dc.l	APT_Info,JP_Info
		dc.l	APT_Prefs,JP_Prefsrout		;routine for Prefs
		dc.l	APT_Prefsbuffer,JP_Prefs	;buffer for Prefs
		dc.l	APT_Prefsbuffersize,4		;size of buffer
		dc.l	APT_PrefsVersion,1		;Prefs Version
		dc.l	APT_OperatorUse,1
		dc.l	0

JP_Creator:
		dc.b	'(c) 1997 Frank Pagels /Defect Softworks',0
	even
JP_Info:
		dc.b	'JPEG saver',10,10
		dc.b	'Saves an image as JPEG using the ,',10
		dc.b	'jpeg codec class from Christoph Feck',0
	even

towerbase:	dc.l	0
JP_myObject:	dc.l	0
JP_rgbline:	dc.l	0
JP_chunkyline:	dc.l	0

JP_Linemerk	=	APG_Free1

JP_window:	dc.l	0
JP_Glist:	dc.l	0
JP_gadarray:	ds.l	4

JP_Prefs
JP_Quali:	dc.l	75

;----------------------------------------------------------------------------
JP_save:
		lea	towername(pc),a1
		moveq	#1,d0
		move.l	4.w,a6
		jsr	_LVOOpenlibrary(a6)
		tst.l	d0
		bne	.libok
		lea	JP_Towermsg(pc),a4
		move.l	APG_Status(a5),a6
		jmp	(a6)
.libok:
		move.l	d0,towerbase
;------------------------------------------------------------------------------

		tst.w	APG_Cutflag(a5)
		bne.b	.brushok
		move.w	#0,APG_xbrush1(a5)	;Es soll das ganze 
		move.w	#0,APG_ybrush1(a5)	;Pic abgesaved werden
		move.w	APG_ImageWidth(a5),APG_xbrush2(a5)
		move.w	APG_ImageHeight(a5),APG_ybrush2(a5)	
.brushok:
		moveq	#0,d0
		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
		move.l	d0,d1
		add.l	#15,d1
		lsr.l	#4,d1		;breite in worte
		move.w	d1,APG_Brushword(a5)
;------------------------------------------------------------------------------

;�ffne new Object

		move.l	towerbase(pc),a6
		lea	JP_classname(pc),a0
		lea	JP_codectags(pc),a1
		move.l	APG_Filehandle(a5),4(a1)
		jsr	_LVONewExtObjectA(a6)		
		tst.l	d0
		bne	.obok

;Konnte Objekt nicht �ffnen

		move.l	towerbase(pc),a1
		move.l	4.w,a6
		jsr	_LVOCloseLibrary(a6)
		lea	JP_noobjectmsg(pc),a4
		move.l	APG_Status(a5),a6
		jsr	(a6)
		moveq	#APSE_ERROR,d3
		rts

;alles Ok
.obok:		
		move.l	d0,JP_myObject

		jsr	APR_Openprocess(a5)

		lea	JP_savemsg(pc),a1
		move.w	APG_ybrush2(a5),d0
		sub.w	APG_ybrush1(a5),d0
		subq.w	#1,d0
		jsr	APR_InitProcess(a5)

;Setze Compressions parameter

		move.l	JP_myObject(pc),a0
		lea	JP_paratag(pc),a1
		moveq	#0,d0
		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
		move.l	d0,4(a1)
		move.w	APG_ybrush2(a5),d0
		sub.w	APG_ybrush1(a5),d0
		move.l	d0,12(a1)
		move.l	APG_Intuitionbase(a5),a6
		jsr	_LVOSetAttrsA(a6)

;setzte Quality

		move.l	JP_myObject(pc),a0
		lea	JP_paratag1(pc),a1
		move.l	JP_Quali(pc),4(a1)
		jsr	_LVOSetAttrsA(a6)

;Starte Compression

		move.l	#CDM_START,-(a7)
		move.l	JP_myObject(pc),-(a7)
		jsr	_DoMethod

		addq.l	#8,a7

;speicher zeile f�r zeile

;ersma palette einrichten

		lea	APG_Farbtab8(a5),a0
		lea	JP_Farbtab8pure,a1
		move.l	(a0)+,d7
		swap	d7
		move.l	d7,d6
		subq.w	#1,d7

.ncc		moveq	#0,d1
		move.l	(a0)+,d0
		lsr.l	#8,d0
		move.l	(a0)+,d1
		swap	d1
		or.w	d1,d0
		move.l	(a0)+,d1
		rol.l	#8,d1
		or.b	d1,d0

		move.l	d0,(a1)+

		dbra	d7,.ncc		

		move.l	APG_rndr_palette(a5),a0
		move.l	d6,d0		;Farbanzahl
		lea	JP_Farbtab8pure,a1
		lea	JP_palitag(pc),a2
		move.l	APG_Renderbase(a5),a6
		jsr	_LVOImportPaletteA(a6)		

;-------------------------------------------------------------------------------

		move.l	APG_Bytewidth(a5),d0
		lsl.l	#3,d0
		add.l	d0,d0
		move.l	d0,d5
		lsl.l	#2,d5	;speicher f�r eine rgb zeile
		add.l	d5,d0	
		move.l	#MEMF_ANY!MEMF_CLEAR,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)

		move.l	d0,JP_rgbline
		add.l	d5,d0
		move.l	d0,JP_chunkyline

;init tag f�r c2rgb

		lea	JP_c2rtag(pc),a3				
		move.l	APG_Bytewidth(a5),d3
		lsl.l	#3,d3
		move.l	d3,4(a3)		;Sourcewidth
		moveq	#COLORMODE_CLUT,d5
		tst.b	APG_HAM_FLAG(a5)
		beq.b	.w
		moveq	#COLORMODE_HAM6,d5
		bra.b	.ren
.w		tst.b	APG_HAM8_FLAG(a5)
		beq.b	.ren
		moveq	#COLORMODE_HAM8,d5
.ren		move.l	d5,12(a3)
		moveq	#0,d0
		move.w	APG_xbrush1(a5),d0
		move.l	d0,20(a3)			;Left  Edge

;make datas
		moveq	#0,d0
		move.w	APG_ybrush1(a5),d0
		move.l	d0,JP_Linemerk(a5)
.nextline:
		tst.l	APG_rndr_rgb(a5)
		beq	.nixrgb			;wir haben schon rgb daten

;wir haben schon rgb daten

		move.l	APG_rndr_rgb(a5),a0
		move.l	JP_rgbline(pc),a1
		move.l	JP_Linemerk(a5),d1
		move.l	APG_bytewidth(a5),d0
		lsl.l	#3+2,d0
		mulu	d1,d0
		add.l	d0,a0
		moveq	#0,d0
		move.w	APG_xbrush1(a5),d0
		move.w	d0,d1
		lsl.l	#2,d0
		add.l	d0,a0
		move.w	APG_xbrush2(a5),d2
		sub.w	d1,d2
		subq.w	#1,d2
.nr		move.l	(a0)+,(a1)+
		dbra	d2,.nr

		bra	.JP_convert

.nixrgb
		move.l	JP_Linemerk(a5),d1
		move.l	APG_Bytewidth(a5),d2
		mulu	d1,d2
		lea	JP_planetab(pc),a1	;init Planetab
		move.l	a1,a0
		moveq	#0,d7
		move.b	APG_Planes(a5),d7
		subq.l	#1,d7
		move.l	APG_Picmem(a5),d0
		add.l	d2,d0
.np		move.l	d0,(a1)+
		add.l	APG_Oneplanesize(a5),d0
		dbra	d7,.np
.weiter
		move.l	APG_Bytewidth(a5),d0
		moveq	#1,d1
		moveq	#0,d2
		move.b	APG_Planes(a5),d2
		move.l	d0,d3		
		move.l	JP_chunkyline(pc),a1
		move.l	APG_Renderbase(a5),a6
		sub.l	a2,a2
		
		jsr	_LVOPlanar2Chunky(a6)

;RGB's m�chen

		move.l	JP_chunkyline(pc),a0
		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
		moveq	#1,d1
		move.l	JP_rgbline(pc),a1
		move.l	APG_rndr_Palette(a5),a2
		lea	JP_c2rtag(pc),a3				
		jsr	_LVOChunky2RGBA(a6)

;convert ARGB nach RGB 
.JP_convert
		move.l	JP_rgbline(pc),a0
		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
		tst.w	d0
		beq	.ns
		subq.w	#1,d0		
.ns:
		moveq	#1,d1
.nrgb:
		move.b	(a0,d1),(a0)+
		move.b	(a0,d1),(a0)+
		move.b	(a0,d1),(a0)+

		addq.l	#1,d1
		dbra	d0,.nrgb

		move.w	APG_xbrush2(a5),d0
		sub.w	APG_xbrush1(a5),d0
		mulu	#3,d0			;Buffersize
		move.l	d0,-(a7)
		move.l	JP_rgbline(pc),-(a7)
		move.l	#CDM_PROCESS,-(a7)
		move.l	JP_myObject(pc),-(a7)
		jsr	_DoMethod

		lea	16(a7),a7
		
		jsr	APR_DoProcess(a5)
		tst.l	d0
		beq	JP_Stop
		
		move.l	JP_Linemerk(a5),d1
		addq.l	#1,d1
		move.l	d1,JP_Linemerk(a5)
		cmp.w	APG_ybrush2(a5),d1
		bls	.nextline


;finish compression

		move.l	#CDM_FINISH,-(a7)
		move.l	JP_myObject(pc),-(a7)
		jsr	_DoMethod

		addq.l	#8,a7

;----------------------------------------------------------------------------
;Tower lib wieder schlie�en

JP_Ende:

		moveq	#0,d3
JP_Ende1:
		move.l	JP_rgbline(pc),d0
		beq	.nixfree
		move.l	d0,a1
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)
		clr.l	JP_rgbline
.nixfree:
		move.l	JP_myObject(pc),a0
		move.l	towerbase(pc),a6
		jsr	_LVODisposeExtObject(a6)
		
		move.l	towerbase(pc),d0
		beq	.nixlib
		move.l	d0,a1
		move.l	4.w,a6
		jsr	_LVOCloseLibrary(a6)
		clr.l	towerbase
.nixlib:
		rts

JP_Stop:
		moveq	#APSE_ABORT,d3
		bra	JP_ende1


JP_planetab:	ds.l	8


JP_c2rtag:
		dc.l	RND_SourceWidth,0
		dc.l	RND_ColorMode,0
		dc.l	RND_LeftEdge,0
		dc.l	TAG_DONE


JP_palitag:	dc.l	RND_PaletteFormat,PALFMT_RGB8
		dc.l	TAG_DONE

JP_codectags:
		dc.l	CDA_Stream,0
		dc.l	CDA_Coding,1
		dc.l	CDA_StreamType,CDST_FILE
		dc.l	TAG_DONE

JP_paratag:
		dc.l	PCDA_Width,0
		dc.l	PCDA_Height,0
		dc.l	PCDA_Components,3
		dc.l	PCDA_ColorSpace,PCDCS_RGB
		dc.l	TAG_DONE

JP_paratag1:
		dc.l	CDA_Quality,75
		dc.l	TAG_DONE

JP_classname:	dc.b	'jpeg.codec',0
			 
JP_noobjectmsg:	dc.b	'Can',$27,'t create a new Object!',0
JP_Towermsg:	dc.b	'Tower.library not found!',0
towername:	dc.b	'tower.library',0
JP_savemsg:	dc.b	'Save JPEG image!',0

	even
;--------------------------------------------------------------------------
JP_Prefsrout:
;init Prefs
		move.l	JP_quali(pc),d0
		lea	JP_Level(pc),a0
		move.l	d0,(a0)
		lea	JP_numlevel(pc),a0
		move.l	d0,(a0)

;---------------------------------------------------------------------------

		move.l	#WindowTags,APG_WindowTagList(a5) ;Tag list for the Window
		move.l	#WinWG+4,APG_WGadgets(a5)	;addr. of WA_Gadgets Tag + 4
		move.l	#winSC+4,APG_WScreen(a5)	;addr. of WA_CustomScreen Tag + 4
		move.l	#JP_window,APG_WinWnd(a5)	;a point for the Window
		move.l	#JP_Glist,APG_Glist(a5)		;a point for the Glist
		move.l	#NTypes,APG_NGads(a5)		;the NGads list
		move.l	#Gtypes,APG_GTypes(a5)		;the GTypes list
		move.l	#Gtags,APG_Gtags(a5)		;the Gtags list
		move.w	#4,APG_CNT(a5)			;the number of Gadgets
		move.l	#JP_gadarray,APG_Gadgets(a5)	;a pointer for the Gadgetsarry, size=4*number of Gadgets
		move.l	APG_Scr(a5),APG_ScrBase(a5)	;the Screenpointer , stored in APG_Scr
	
		move.l	APG_CheckMainWinPos(a5),a6	;returns d0,d1 Pos
		jsr	(a6)

		add.w	#70,d0
		add.w	#100,d1
		
		move.w	d0,APG_WinLeft(a5)		;the left pos. of the Win
		move.w	d1,APG_WinTop(a5)		;the top pos. of the Win
		move.w	#210,APG_WinWidth(a5)		;the width of the Win
		move.w	#55,APG_WinHeight(a5)		;the height of the Win

		move.l	#WinL,APG_WinL(a5)		;addr. of WA_Left Tag 
		move.l	#WinT,APG_WinT(a5)		;addr. of WA_Top Tag
		move.l	#WinW,APG_WinW(a5)		;addr. of WA_Width Tag
		move.l	#WinH,APG_WinH(a5)		;addr. of WA_Height Tag

		move.l	APG_OpenWindow(a5),a6
		jsr	(a6)

		move.l	JP_Quali(pc),APG_Free2(a5)	;Prefs backuppen

JP_wait:
		move.l	JP_window(pc),a0
		move.l	APG_Wait(a5),a6
		jsr	(a6)				;Handle input

		cmp.l	#GADGETDOWN,d4
		beq.b	JP_slider
		cmp.l	#CLOSEWINDOW,d4
		beq	JP_Cancel
		cmp.l	#GADGETUP,d4
		beq.b	JP_gads
		bra.b	JP_wait

JP_gads:
		moveq	#0,d0
		move.w	38(a4),d0
		add.l	d0,d0
		move.l	d0,d1
		lea	JP_pjumptab(pc),a0
		move.w	(a0,d0.l),d0
		jmp	(a0,d0.w)

JP_pjumptab:
		dc.w	JP_handlequali-JP_pjumptab
		dc.w	JP_ok-JP_pjumptab
		dc.w	JP_cancel-JP_pjumptab
		dc.w	JP_handelnum-JP_pjumptab

JP_handlequali:
		ext.l	d5
		move.l	d5,JP_Quali

		lea	JP_numtag(pc),a3
		move.l	d5,4(a3)
		move.l	#GD_qinter,d0
		lea	JP_gadarray(pc),a0
		move.l	(a0,d0.l*4),a0
		move.l	JP_window(pc),a1
		sub.l	a2,a2
		move.l	APG_Gadtoolsbase(a5),a6
		jsr	_LVOGT_SetGadgetAttrsA(a6)

JP_sliderwait:
		move.l	JP_window(pc),a0
		move.l	APG_Wait(a5),a6
		jsr	(a6)				;Handle input

		cmp.l	#GADGETUP,d4
		beq.w	JP_wait			;Slidergadet wurde losgelassen

		cmp.l	#IDCMP_MOUSEMOVE,d4
		beq.b	JP_slider
		bra.b	JP_sliderwait

JP_slider:
		move.w	38(a4),d0
		cmp.w	#GD_quali,d0
		beq.w	JP_handlequali

		bra	JP_wait
JP_handelnum:
		lea	JP_numtag1(pc),a3
		move.l	#GD_qinter,d0
		lea	JP_gadarray(pc),a0
		move.l	(a0,d0.l*4),a0
		move.l	JP_window(pc),a1
		sub.l	a2,a2
		move.l	APG_Gadtoolsbase(a5),a6
		jsr	_LVOGT_GetGadgetAttrsA(a6)

		move.l	JP_number(pc),d5
		cmp.l	#100,d5
		bls	.w
		move.l	#100,d5			;eingetragener wert war zu hoch

		lea	JP_numtag(pc),a3
		move.l	d5,4(a3)
		move.l	#GD_qinter,d0
		lea	JP_gadarray(pc),a0
		move.l	(a0,d0.l*4),a0
		move.l	JP_window(pc),a1
		sub.l	a2,a2
		jsr	_LVOGT_SetGadgetAttrsA(a6)
.w
		move.l	d5,JP_Quali

		lea	JP_leveltag(pc),a3
		move.l	d5,4(a3)
		move.l	#GD_quali,d0
		lea	JP_gadarray(pc),a0
		move.l	(a0,d0.l*4),a0
		move.l	JP_window(pc),a1
		sub.l	a2,a2
		jsr	_LVOGT_SetGadgetAttrsA(a6)

		bra	JP_wait


JP_cancel:
		move.l	APG_Free2(a5),JP_Quali

JP_ok:
		move.l	JP_window(pc),a0
		move.l	APG_Intuitionbase(a5),a6
		jmp	_LVOCloseWindow(a6)


;--------------------------------------------------------------------------------
	even

WindowTags:
winL:	dc.l	WA_Left,212
winT:	dc.l	WA_Top,78
winW:	dc.l	WA_Width,220
winH:	dc.l	WA_Height,85
		dc.l	WA_IDCMP,SLIDERIDCMP!IDCMP_CLOSEWINDOW!GADGETUP!GADGETDOWN!VANILLAKEY!BUTTONIDCMP!IDCMP_REFRESHWINDOW!INTEGERIDCMP!IDCMP_MOUSEBUTTONS
		dc.l	WA_Flags,WFLG_CLOSEGADGET!WFLG_DEPTHGADGET!WFLG_ACTIVATE!WFLG_DRAGBAR!WFLG_SMART_REFRESH!WFLG_RMBTRAP
winWG:	dc.l	WA_Gadgets,0
		dc.l	WA_Title,winWTitle
winSC:	dc.l	WA_CustomScreen,0
		dc.l	TAG_DONE

WinWTitle:	dc.b	'JPEG Options',0

	even

JP_numtag:	dc.l	GTIN_Number,0,TAG_DONE

JP_numtag1:
		dc.l	GTIN_Number,JP_number
		dc.l	TAG_DONE

JP_number:	dc.l	0

JP_leveltag:
		dc.l	GTSL_Level,0
		dc.l	TAG_DONE


GD_quali	=	0
GD_Ok		=	1
GD_Cancel	=	2
GD_qinter	=	3

Gtypes:
		dc.w	SLIDER_KIND
		dc.w	BUTTON_KIND
		dc.w	BUTTON_KIND
		dc.w	INTEGER_KIND

Gtags:
		dc.l	GTSL_Level
JP_Level:	dc.l	75
		dc.l	GTSL_Max,100
		dc.l	GTSL_MaxLevelLen,3
		dc.l	PGA_Freedom,LORIENT_HORIZ
		dc.l	GA_RelVerify,1
		dc.l	GA_Immediate,1
		dc.l	TAG_DONE
		dc.l	TAG_DONE
		dc.l	TAG_DONE
		dc.l	GTIN_Number
JP_numlevel:	dc.l	75
		dc.l	GTIN_MaxChars,3
		dc.l	TAG_DONE

	even
NTypes:
 		dc.w	10,15,150,14
		dc.l	qualiText,0
		dc.w	GD_quali
		dc.l	PLACETEXT_ABOVE!NG_HIGHLABEL,0,0
		DC.W    10,37,70,14
		DC.L    oktext,0
		DC.W    GD_Ok
		DC.L    PLACETEXT_IN,0,0
		DC.W    130,37,70,13
		DC.L    canceltext,0
		DC.W    GD_Cancel
		DC.L    PLACETEXT_IN,0,0
		dc.w	162,15,38,14
		dc.l	0,0
		dc.w	GD_qinter
		dc.l	0,0,0

oktext:		dc.b	'Ok',0
canceltext:	dc.b	'Cancel',0
qualiText:	dc.b	'Quality',0

;----------------------------------------------------------------------------

savemsg:	dc.b	'Save a JPEG image!',0

	even

_DoMethod:
	MOVE.L	A2,-(SP)
	MOVEA.L	8(SP),A2
	MOVE.L	A2,D0
	BEQ.S	cmnullreturn
	LEA	12(SP),A1
	MOVEA.L	-4(A2),A0
	BRA.W	cminvoke

;_DoSuperMethod:
;	MOVE.L	A2,-(SP)
;	MOVEM.L	8(SP),A0/A2
;	MOVE.L	A2,D0
;	BEQ.S	cmnullreturn
;	MOVE.L	A0,D0
;	BEQ.S	cmnullreturn
;	LEA	$10(SP),A1
;	MOVEA.L	$18(A0),A0
;	BRA.S	cminvoke
;
;_CoerceMethod:
;	MOVE.L	A2,-(SP)
;	MOVEM.L	8(SP),A0/A2
;	MOVE.L	A2,D0
;	BEQ.S	cmnullreturn
;	MOVE.L	A0,D0
;	BEQ.S	cmnullreturn
;	LEA	$10(SP),A1
;	BRA.S	cminvoke
;
;_CoerceMethodA:
;	MOVE.L	A2,-(SP)
;	MOVEM.L	8(SP),A0/A2
;	MOVE.L	A2,D0
;	BEQ.S	cmnullreturn
;	MOVE.L	A0,D0
;	BEQ.S	cmnullreturn
;	MOVEA.L	$10(SP),A1
cminvoke:
	PEA	cmreturn(PC)
	MOVE.L	8(A0),-(SP)
	RTS

cmnullreturn:
	MOVEQ	#0,D0
cmreturn:
	MOVEA.L	(SP)+,A2
	RTS

;_DoMethodA:
;	MOVE.L	A2,-(SP)
;	MOVEA.L	8(SP),A2
;	MOVE.L	A2,D0
;	BEQ.S	cmnullreturn
;	MOVEA.L	12(SP),A1
;	MOVEA.L	-4(A2),A0
;	BRA.S	cminvoke
;
;_DoSuperMethodA:
;	MOVE.L	A2,-(SP)
;	MOVEM.L	8(SP),A0/A2
;	MOVE.L	A2,D0
;	BEQ.S	cmnullreturn
;	MOVE.L	A0,D0
;	BEQ.S	cmnullreturn
;	MOVEA.L	$10(SP),A1
;	MOVEA.L	$18(A0),A0
;	BRA.S	cminvoke
;
;_SetSuperAttrs:
;	MOVE.L	A2,-(SP)
;	MOVEM.L	8(SP),A0/A2
;	MOVE.L	A2,D0
;	BEQ.S	cmnullreturn
;	MOVE.L	A0,D0
;	BEQ.S	cmnullreturn
;	MOVEA.L	$18(A0),A0
;	MOVE.L	#0,-(SP)
;	PEA	$14(SP)
;	MOVE.L	#$103,-(SP)
;	LEA	(SP),A1
;	PEA	ssaret(PC)
;	MOVE.L	8(A0),-(SP)
;	RTS
;
;ssaret:
;	LEA	12(SP),SP
;	MOVEA.L	(SP)+,A2
;	RTS
;
;	dc.w	0


		section	datas,bss

JP_Farbtab8pure:	ds.l	256
