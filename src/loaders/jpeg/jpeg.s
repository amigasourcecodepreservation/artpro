����  �&b�&b�&b�&b�&b�&b�&b�&b;=o�;-------------------------------------------------------------------------
;
;		JPEG loader modul for ArtPRO
;		(c) 1997 ,Frank (Copper) Pagels /Defect Softworks
;
;		15.05.1997
;
;
		incdir 	include:

		include	exec/exec_lib.i
		include	exec/lists.i
		include	exec/memory.i
		include intuition/intuition_lib.i
		include	libraries/wb_lib.i
		include libraries/gadtools.i
		include libraries/gadtools_lib.i
		include	graphics/graphics_lib.i
		Include utility/tagitem.i
		include	dos/dos.i		
		include	dos/dos_lib.i		
		include	libraries/reqtools_lib.i
		include	libraries/tower_lib.i

		include misc/artpro.i
		include	render/render.i
		include	render/render_lib.i
		include	render/renderhooks.i

		
TA_Dummy	=	TAG_USER+$00070000
TM_Dummy	=	$00070000

CDA_Dummy	=	TA_Dummy+$3200

CDA_Stream	=	CDA_Dummy+1
CDA_StreamType	=	CDA_Dummy+2
CDA_Coding	=	CDA_Dummy+3
CDA_Quality	=	CDA_Dummy+6

CDST_OBJECT	=	0
CDST_reserved	=	1	/* Do not use */
CDST_FILE	=	2

PCDA_Dummy	=	CDA_Dummy+$80

PCDA_Width	=	PCDA_Dummy+1
PCDA_Height	=	PCDA_Dummy+2
PCDA_Components	=	PCDA_Dummy+3
PCDA_ColorSpace	=	PCDA_Dummy+4

CDM_Dummy	=	TM_Dummy+$3200

CDM_START	=	CDM_Dummy+1
CDM_PROCESS	=	CDM_Dummy+2
CDM_FINISH	=	CDM_Dummy+3
CDM_ABORT	=	CDM_Dummy+4
CDM_READHEADER	=	CDM_Dummy+6
CDM_WRITEHEADER	=	CDM_Dummy+7

HEADER_ERROR	=	0
HEADER_READY	=	1
HEADER_EMPTY	=	2
HEADER_SUSPENDED=	3
HEADER_PROTECTED=	4

PCDCS_UNKNOWN	=	0
PCDCS_GRAYSCALE	=	1
PCDCS_RGB	=	2
PCDCS_YCC	=	3
PCDCS_CMYK	=	4
PCDCS_YCCK	=	5

		LOADERHEADER JP_NODE

		dc.b	'$VER: JPEG loader module 0.5 (16.05.96)',0
	even
;---------------------------------------------------------------------------
JP_NODE:
		dc.l	0,0
		dc.b	0,0
		dc.l	JP_name
		dc.l	JP_save
		dc.b	'JPGL'		;saverkennung f�r ArtPRO
		dc.b	'EXTR'		;for extern saver
		dc.l	0
		dc.l	JP_Tags
JP_name:
		dc.b	'JPEG',0
	even
;---------------------------------------------------------------------------

JP_Tags:
		dc.l	APT_Creator,JP_Creator
		dc.l	APT_Version,2
		dc.l	APT_Info,JP_Info
;		dc.l	APT_Prefs,JP_Prefsrout		;routine for Prefs
;		dc.l	APT_Prefsbuffer,JP_Prefs	;buffer for Prefs
;		dc.l	APT_Prefsbuffersize,4		;size of buffer
;		dc.l	APT_PrefsVersion,1		;Prefs Version
		dc.l	APT_OperatorUse,1
		dc.l	APT_Autoload,1
		dc.l	0

JP_Creator:
		dc.b	'(c) 1997 Frank Pagels /Defect Softworks',0
	even
JP_Info:
		dc.b	'JPEG loader',10,10
		dc.b	'Loads an JPEG image using the ,',10
		dc.b	'jpeg codec class from Christoph Feck',0
	even

towerbase:	dc.l	0
JP_myObject:	dc.l	0
JP_rgbline:	dc.l	0
JP_chunkyline:	dc.l	0
JP_Colorspace:	dc.l	0

JP_Linemerk	=	APG_Free1
JP_Modulo	=	APG_Free2
JP_Buffermerk	=	APG_Free3

JP_window:	dc.l	0
JP_Glist:	dc.l	0
JP_gadarray:	ds.l	4

JP_Prefs
JP_Quali:	dc.l	75

;----------------------------------------------------------------------------
JP_save:
;ersma checken ob wir ein Jpeg file haben

		move.l	APG_Dosbase(a5),a6
		move.l	APG_Filehandle(a5),d1
		lea	JP_planetab(pc),a4
		move.l	a4,d2
		move.l	#2,d3
		jsr	_LVORead(a6)

		move.w	(a4),d0
		cmp.w	#$ffd8,d0
		beq	.jpegok		;fileerkannt
		
		moveq	#APLE_UNKNOWN,d3
		rts

;------------------------------------------------------------------------------
.jpegok
.w1		move.l	APG_Filehandle(a5),d1
		moveq	#0,d2
		move.l	#OFFSET_BEGINNING,d3
		move.l	APG_Dosbase(a5),a6
		jsr	_LVOSeek(a6)

		lea	towername(pc),a1
		moveq	#1,d0
		move.l	4.w,a6
		jsr	_LVOOpenlibrary(a6)
		tst.l	d0
		bne	.libok
		lea	JP_Towermsg(pc),a4
		move.l	APG_Status(a5),a6
		jmp	(a6)
.libok:
		move.l	d0,towerbase
;------------------------------------------------------------------------------


;�ffne new Object

		move.l	towerbase(pc),a6
		lea	JP_classname(pc),a0
		lea	JP_codectags(pc),a1
		move.l	APG_Filehandle(a5),4(a1)
		jsr	_LVONewExtObjectA(a6)		
		tst.l	d0
		bne	.obok

;Konnte Objekt nicht �ffnen

		move.l	towerbase(pc),a1
		move.l	4.w,a6
		jsr	_LVOCloseLibrary(a6)
		lea	JP_noobjectmsg(pc),a4
		move.l	APG_Status(a5),a6
		jsr	(a6)
		moveq	#APSE_ERROR,d3
		rts

;alles Ok
.obok:		
		move.l	d0,JP_myObject
;-----------------------------------------------------------------------
;lese stream header

		move.l	#CDM_READHEADER,-(a7)
		move.l	JP_myObject(pc),-(a7)
		jsr	_DoMethod
		lea	8(a7),a7

		cmp.l	#HEADER_READY,d0
		beq	.noprobs

		bsr	JP_Clearstuff

		lea	JP_headererrmsg(pc),a4
		move.l	APG_Status(a5),a6
		jsr	(a6)
		moveq	#APLE_ERROR,d3

		rts
;-------------------------------------------------------------------------------
;read attribute
.noprobs:
		move.l	APG_IntuitionBase(a5),a6
		lea	JP_planetab(pc),a4

		move.l	#PCDA_Width,d0
		move.l	JP_myObject(pc),a0
		move.l	a4,a1
		jsr	_LVOGetAttr(a6)

		move.l	(a4),d0
		move.w	d0,APG_ImageWidth(a5)

		move.l	#PCDA_Height,d0
		move.l	JP_myObject(pc),a0
		move.l	a4,a1
		jsr	_LVOGetAttr(a6)

		move.l	(a4),d0
		move.w	d0,APG_ImageHeight(a5)

		move.l	#PCDA_ColorSpace,d0
		move.l	JP_myObject(pc),a0
		move.l	a4,a1
		jsr	_LVOGetAttr(a6)

		move.l	(a4),JP_Colorspace


		clr.l	-(a7)
		move.l	#PCDCS_RGB,-(a7)
		move.l	#PCDA_ColorSpace,-(a7)
		move.l	JP_myObject(pc),a0
		move.l	a7,a1
		move.l	APG_Intuitionbase(a5),a6
		jsr	_LVOSetAttrsA(a6)
		lea	12(a7),a7

;-------------------------------------------------------------------------------

		move.w	APG_ImageWidth(a5),d0
		add.w	#15,d0
		lsr.w	#4,d0
		add.w	d0,d0
		move.l	d0,APG_ByteWidth(a5)
		lsl.l	#3,d0
		mulu	#4,d0
		move.l	d0,JP_Modulo(a5)

		move.w	APG_ImageWidth(a5),d0
		mulu	#3,d0			;Buffersize

		move.l	APG_ByteWidth(a5),d0
		moveq	#0,d1
		move.w	APG_ImageHeight(a5),d1
		jsr	APR_InitRenderMem(a5)	;Init Speicher f�r RGB Puffer
		tst.l	d0
		bne	.memok

		bsr	JP_Clearstuff
		moveq	#APLE_NOMEM,d0
		rts

.memok:
		jsr	APR_Openprocess(a5)

		lea	JP_loadmsg(pc),a1
		move.w	APG_ImageHeight(a5),d0
		jsr	APR_InitProcess(a5)


;Starte Decompression

		move.l	#CDM_START,-(a7)
		move.l	JP_myObject(pc),-(a7)
		jsr	_DoMethod

		addq.l	#8,a7


		move.l	APG_Bytewidth(a5),d0
		lsl.l	#3,d0
		add.l	d0,d0
		lsl.l	#2,d0	;speicher f�r eine rgb zeile
		move.l	#MEMF_ANY!MEMF_CLEAR,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)

		move.l	d0,JP_rgbline


		clr.l	JP_Linemerk(a5)
		move.l	APG_rndr_rgb(a5),JP_Buffermerk(a5)
		move.w	APG_ImageHeight(a5),d7
		subq.w	#1,d7

.newline:
		move.w	APG_ImageWidth(a5),d0
		mulu	#3,d0			;Buffersize
		move.l	d0,-(a7)
		move.l	JP_rgbline(pc),-(a7)
		move.l	#CDM_PROCESS,-(a7)
		move.l	JP_myObject(pc),-(a7)
		jsr	_DoMethod

		lea	16(a7),a7
		
		jsr	APR_DoProcess(a5)
		tst.l	d0
		beq	JP_Stop


;convert RGB nach ARGB 
.JP_convert
		move.l	JP_Buffermerk(a5),a0
		move.l	JP_rgbline(pc),a1
		move.w	APG_ImageWidth(a5),d0

		tst.w	d0
		beq	.ns
		subq.w	#1,d0		
.ns:
		moveq	#1,d1
.nrgb:
		clr.b	(a0)+
		move.b	(a1)+,(a0)+
		move.b	(a1)+,(a0)+
		move.b	(a1)+,(a0)+
				
		dbra	d0,.nrgb

		move.l	JP_Modulo(a5),d0
		add.l	d0,JP_Buffermerk(a5)

		
		dbra	d7,.newline

;finish compression

		move.l	#CDM_FINISH,-(a7)
		move.l	JP_myObject(pc),-(a7)
		jsr	_DoMethod

		addq.l	#8,a7

		move.b	#24,APG_Planes(a5)
		st	APG_RenderFlag(a5)
		move.w	#1,APG_LoadFlag(a5)
		moveq	#APLE_OK,d3


;----------------------------------------------------------------------------
;Tower lib wieder schlie�en

JP_Ende:

		moveq	#0,d3
JP_Ende1:
		move.l	JP_rgbline(pc),d0
		beq	.nixfree
		move.l	d0,a1
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)
		clr.l	JP_rgbline
.nixfree:
		bsr	JP_Clearstuff

		rts

JP_Stop:
		moveq	#APSE_ABORT,d3
		bra	JP_ende1

JP_Clearstuff:
		move.l	JP_myObject(pc),a0
		move.l	towerbase(pc),a6
		jsr	_LVODisposeExtObject(a6)
		clr.l	JP_myObject
		
		move.l	towerbase(pc),a1
		move.l	4.w,a6
		jsr	_LVOCloseLibrary(a6)
		clr.l	towerbase
		rts


JP_planetab:	ds.l	8


JP_c2rtag:
		dc.l	RND_SourceWidth,0
		dc.l	RND_ColorMode,0
		dc.l	RND_LeftEdge,0
		dc.l	TAG_DONE


JP_palitag:	dc.l	RND_PaletteFormat,PALFMT_RGB8
		dc.l	TAG_DONE

JP_codectags:
		dc.l	CDA_Stream,0
		dc.l	CDA_Coding,0
		dc.l	CDA_StreamType,CDST_FILE
		dc.l	TAG_DONE

JP_paratag:
		dc.l	PCDA_Width,0
		dc.l	PCDA_Height,0
		dc.l	PCDA_Components,3
		dc.l	PCDA_ColorSpace,PCDCS_RGB
		dc.l	TAG_DONE

JP_paratag1:
		dc.l	CDA_Quality,75
		dc.l	TAG_DONE

JP_classname:	dc.b	'jpeg.codec',0
			 
JP_noobjectmsg:	dc.b	'Can',$27,'t create a new Object!',0
JP_Towermsg:	dc.b	'Tower.library not found!',0
towername:	dc.b	'tower.library',0
JP_loadmsg:	dc.b	'Load JPEG image!',0
JP_headererrmsg:dc.b	'Error while reading header!',0
	even
;--------------------------------------------------------------------------


savemsg:	dc.b	'Save a JPEG image!',0

	even

_DoMethod:
	MOVE.L	A2,-(SP)
	MOVEA.L	8(SP),A2
	MOVE.L	A2,D0
	BEQ.S	cmnullreturn
	LEA	12(SP),A1
	MOVEA.L	-4(A2),A0
	BRA.W	cminvoke

cminvoke:
	PEA	cmreturn(PC)
	MOVE.L	8(A0),-(SP)
	RTS

cmnullreturn:
	MOVEQ	#0,D0
cmreturn:
	MOVEA.L	(SP)+,A2
	RTS


		section	datas,bss

JP_Farbtab8pure:	ds.l	256
