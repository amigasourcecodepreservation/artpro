����                                        ;=======================================================
;=                                                     =
;=      SNES loader (�) 1995, Maik Solf (Infect)       =
;=                                                     =
;=======================================================
	incdir 	include:
	include	exec/lists.i
	include	exec/memory.i
	Include	utility/tagitem.i
	include	dos/dos.i
	include	exec/exec_lib.i
	include	intuition/intuition_lib.i
	include	libraries/wb_lib.i
	include	graphics/graphics_lib.i
	include	dos/dos_lib.i
;	include	Own/MyMakros.i
	include	misc/ArtPro.i
		
	LOADERHEADER	SNES_NODE

	dc.b	'$VER: SNES loader module 1.00 (27.11.95)',0
	EVEN
;-------------------------------------------------------
TilesAdr:	dc.l	0
;-------------------------------------------------------
SNES_NODE:	dc.l	0,0
	dc.b	0,0
	dc.l	SNES_name
	dc.l	SNES_load
	dc.b	'SNSL'		;Loaderkennung f�r ArtPRO
	dc.b	'EXTR'		;for extern loader
	dc.l	0
	dc.l	SNES_Tags

SNES_name:	dc.b	'SNES',0
	EVEN

SNES_Tags:	dc.l	APT_Creator,SNES_Creator
	dc.l	APT_Version,1
	dc.l	APT_Info,SNES_Info
	dc.l	0

SNES_Creator:	dc.b	'(c) 1995 Maik Solf (Infect)',0
	EVEN

SNES_Info:	dc.b	'SNES loader',10,10
	dc.b	'Loads SNES tiles with screen.',10
	dc.b	'Only 4 Bit supported.',0
	EVEN
;-------------------------------------------------------
SNES_load:	move.l	APG_FileSize(a5),d0	;Speicher f�r File holen
	move.l	#MEMF_PUBLIC+MEMF_CLEAR,d1
	CallExec	AllocVec
	lea	TilesAdr(pc),a0
	move.l	d0,(a0)
	beq	ErrorMem
;.......................................................
	move.l	APG_Filehandle(a5),d1	;File laden
	move.l	TilesAdr(pc),d2
	move.l	APG_FileSize(a5),d3
	move.l	APG_Dosbase(a5),a6

	;call	Read
	jsr	_LVORead(a6)
	
	tst.l	d0
	bpl.s	.geladen
	move.l	TilesAdr(pc),a1	;Fehler beim Laden
	CallExec	FreeVec
	bra	ErrorMem
;.......................................................
.geladen:	move.w	#320,d0		;Bsp-Gr��en
	move.w	#256,d1
	move.b	#7,d2
	move.w	#2^7,d3
	move.w	d0,APG_ImageWidth(a5)
	move.w	d1,APG_ImageHeight(a5)
	move.b	d2,APG_Planes(a5)	;????? nimmt nur 2 Farben
;Antwort:

;dein Beispiel Pic hat doch auch nur 2 Farben. Oder sollte ich mich da t�uschen.
;rein rechnerisch kommt das doch hin : 320*200/8 = 8000
;ausserdem Solltest du eine Palette angeben. irgendein grauverlauf reicht doch.



	move.w	d3,APG_ColorCount(a5)
	

	move.l	APG_FindMonitor(a5),a6
	jsr	(a6)

	lea	APG_PicMap(a5),a0	;Pointer auf BitMap-Struktur ????? aus FindMonitor
;Antwort:

;das ist die BitmapStuc f�r das Pic innerhalb von ArtPRO. Das hat nichts mit
;FindMonitor zu tun.
	

	move.b	APG_Planes(a5),d0
	move.w	APG_ImageWidth(a5),d1
	move.w	APG_ImageHeight(a5),d2
	move.l	APG_Gfxbase(a5),a6

	;Call	InitBitMap
	jsr	_LVOInitBitmap(a6)
;.......................................................
	moveq	#0,d0		;ByteWidth und OnePlaneSize berechnen
	move.w	APG_ImageWidth(a5),d0
	move.w	APG_ImageHeight(a5),d1
	moveq	#0,d7
	move.b	APG_Planes(a5),d7
	add.w	#15,d0
	lsr.w	#4,d0
	lsl.w	#1,d0
	move.l	d0,APG_ByteWidth(a5)	;Byte=int((Pixel+15):16)*2
	mulu.w	d1,d0
	move.l	d0,APG_Oneplanesize(a5)
	mulu.l	d7,d0
	move.l	#MEMF_CHIP+MEMF_CLEAR,d1
	CallExec	AllocVec
	move.l	d0,APG_Picmem(a5)
	bne.s	.picmemda
	move.l	TilesAdr(pc),a1
	;Call	FreeVec
	jsr	_LVOFreeVec(a6)

	bra.s	ErrorMem
;.......................................................
.picmemda:	move.l	TilesAdr(pc),a0	;Raw-Bild reinkopieren (zum Test)
	move.l	APG_Picmem(a5),a1
	move.w	#200*40,d7
.copy:	move.b	(a0)+,(a1)+
	dbf	d7,.copy
;.......................................................
;	moveq	#0,d0		;Farben setzen
;	moveq	#1,d1
;	move.b	APG_Planes(a5),d0
;	lsl.w	d0,d1	
;	move.w	d1,APG_ColorCount(a5)
;	move.w	APG_ColorCount(a5),d7
;	subq.w	#1,d7
;	lea	APG_Farbtab8(a5),a1
;	lea	APG_Farbtab4(a5),a2
;	move.w	APG_ColorCount(a5),(a1)+
;	clr.w	(a1)+
;	move.w	#2,d7
;.nextcolor:	moveq	#-1,d0
;	moveq	#0,d1
;	moveq	#0,d2
;	move.l	d0,(a1)+
;	move.l	d1,(a1)+
;	move.l	d2,(a1)+
;	move.l	d3,(a2)+
;	add.l	#100,d0
;	dbf	d7,.nextcolor
;.......................................................
SNES_ready:	move.w	#1,APG_LoadFlag(a5)
	move.l	TilesAdr(pc),a1
	CallExec	FreeVec
	moveq	#0,d0
	rts

;.......................................................
ErrorMem:	moveq	#-4,d3		;autom. Meldung: "Nicht genug Speicher"
	rts
;.......................................................
;?????? Welche Fehlernummer
;!!!!!! evtl. noch in den Incluse die einzelnen Fehlermeldumgen als Konstante 

;Schon gemacht

ErrorRead:	moveq	#-3,d3		;autom. Meldung: "Fehler beim Laden"
	rts
