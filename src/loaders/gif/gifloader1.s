����  �&b�&b�&b�&b�&b�&b�&b�&b;=W4;===========================================================================
;
;		GIF loader
;		(c) 1996 ,Frank Pagels (Crazy Copper) /DFT
;
;		16.02.96

		incdir 	include:

		include	exec/exec_lib.i
		include	exec/lists.i
		include	exec/memory.i
		include	graphics/graphics_lib.i
		include	dos/dos.i		
		include	dos/dos_lib.i		

		include misc/artpro.i
		include misc/render_lib.i
		
		LOADERHEADER GIF_NODE

		dc.b	'$VER: GIF loader module 1.03 (09.06.96)',0
	even
;---------------------------------------------------------------------------
GIF_NODE:
		dc.l	0,0
		dc.b	0,0
		dc.l	GIF_name
		dc.l	GIF_load
		dc.b	'GIFL'		;Loaderkennung f�r ArtPRO
		dc.b	'EXTR'		;for extern loader
		dc.l	0
		dc.l	GIF_Tags
GIF_name:
		dc.b	'GIF',0
	even
;---------------------------------------------------------------------------

GIF_Tags:
		dc.l	APT_Creator,GIF_Creator
		dc.l	APT_Version,1
		dc.l	APT_Info,GIF_Info
		dc.l	APT_Autoload,1
		dc.l	0

GIF_Creator:
		dc.b	'(c) 1996 Frank Pagels /DEFECT SoftWorks',0
	even
GIF_Info:
		dc.b	'GIF loader',10,10
		dc.b	'Loads GIF 87a,89a pictures from 1 to 8 bit.',10
		dc.b	'No multipic support!',0
	even
;----------------------------------------------------------------------------

GIF_puffermem	=	APG_Free1

GIF_colortest	=	APG_Free2
GIF_background	=	APG_Free2+1

GIF_interlaced	=	APG_Free3+2
GIF_Group	=	APG_Free3+3	;gruppe bei Interlace

GIF_ystart	=	APG_Free4
GIF_firstcodesize =	APG_Free4+2

GIF_codesize	=	APG_Free5
GIF_Clearcode	=	APG_Free5+2
GIF_limitcode	=	APG_Free6
GIF_Endcode	=	APG_Free6+2

GIF_Linemem	=	APG_Free7

GIF_blocksize	=	APG_Free8
GIF_Line	=	APG_Free8+2

GIF_merklastcode =	APG_Free3
GIF_firstmaxcode =	APG_Free2+2


GIF_localbitperpixel dc.b	0
GIF_transcolor	dc.b	0
GIF_localcolor	dc.b	0
GIF_BitsPerPixel dc.b	0
GIF_transparent	 dc.b	0
		dc.b	0

;---------------------------------------------------------------------------
GIF_load
		sf	GIF_interlaced(a5)
		sf	GIF_Group(a5)

		clr.l	APG_Free1(a5)
		clr.l	APG_Free2(a5)
		clr.l	APG_Free3(a5)
		clr.l	APG_Free4(a5)
		clr.l	APG_Free5(a5)
		clr.l	APG_Free6(a5)
		clr.l	APG_Free7(a5)
		clr.l	APG_Free8(a5)

		lea	GIF_localbitperpixel(pc),a0
		clr.l	(a0)+
		clr.w	(a0)

		lea	knotentab,a0
		move.l	#bsslen/4-1,d7
.nbss:		clr.l	(a0)+
		dbra	d7,.nbss

		move.l	APG_FileSize(a5),d0
		move.l	#MEMF_ANY+MEMF_CLEAR,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)
		move.l	d0,GIF_puffermem(a5)
		bne	.ok
		moveq	#-4,d3		;no mem !
		rts
.ok:		
		move.l	APG_Dosbase(a5),a6
		move.l	APG_Filehandle(a5),d1
		move.l	GIF_puffermem(a5),d2
		moveq	#6,d3			;Testbyte
		jsr	_LVORead(a6)

;werte GIF Header aus

		move.l	GIF_Puffermem(a5),a0
		cmp.l	#'GIF8',(a0)+
		bne	GIF_raus		;Kein Gif
		move.w	(a0)+,d0
		cmp.w	#'7a',d0
		beq	.weit
		cmp.w	#'9a',d0
		bne	GIF_raus

.weit:
		bra	GIF_ok

GIF_raus:
		move.l	GIF_puffermem(a5),a1
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)
		moveq	#-3,d3		;File nicht erkannt
		rts
GIF_ok:
		jsr	APR_OpenProcess(a5)

		move.l	APG_Dosbase(a5),a6
		move.l	APG_Filehandle(a5),d1
		move.l	GIF_puffermem(a5),d2
		move.l	APG_FileSize(a5),d3
		subq.l	#6,d3
		jsr	_LVORead(a6)

		move.l	GIF_Puffermem(a5),a0

		moveq	#0,d0
		move.l	(a0)+,d0	;Screenbreite und h�he �berlesen 
		
		moveq	#0,d0
		moveq	#0,d1
		move.b	(a0)+,d0
		move.b	d0,d1
		clr.b	GIF_colortest(a5)
		and.b	#$80,d0		;Test ob Colormap Descriptor folgt
		beq	nocol
		move.b	#1,GIF_colortest(a5) ;sie tut es		
nocol:
;		move.l	d1,d0
;		and.b	#$70,d0		;# bits of color resolution
;		lsr.b	#4,d0
;		addq.b	#1,d0
;		move.b	d0,colorresolution(a5)
		move.b	d1,d0
		and.b	#7,d0
		addq.b	#1,d0
		move.b	d0,GIF_BitsPerPixel

		move.b	(a0)+,d0
		move.b	d0,GIF_background(a5) ;Color index of screen background

		move.b	(a0)+,d0	;ein Byte erstmal �berspringen

;--- Werte die Global Color Map aus ---

		moveq	#0,d0
		move.b	GIF_bitsperpixel,d0
		move.b	d0,APG_Planes(a5)

		bsr	readcolortab

;--- werte den  IMAGE DESCRIPTOR aus ---

		move.b	(a0)+,d0
		cmp.b	#$2c,d0
		beq	.ok		;$2c m��te vorhanden sein
		cmp.b	#$21,d0
		bne	GIF_wech

;werte Graphic Control Extension aus

		move.b	(a0)+,d0
		cmp.b	#$f9,d0
		bne	GIF_wech

		move.b	(a0)+,d0	;block l�nge sollte 4 sein

		move.b	(a0)+,d0	;flags

		sf	GIF_transparent
		or.b	#1,d0
		beq	.nixtrans
		st	GIF_transparent
.nixtrans:		
		move.w	(a0)+,d0	;delay Time

		move.b	(a0)+,GIF_transcolor ;Transparent color index
		move.b	(a0)+,d0

		move.b	(a0)+,d0
		cmp.b	#$2c,d0
		bne	GIF_wech

.ok:
		move.w	(a0)+,d0	;Abstand vom linken Rand �berlesen

		move.w	(a0)+,d0	;Abstand vom oberen Rand �berlesen

		moveq	#0,d0
		move.b	(a0)+,d0	;Tats�chliche Imagebreite
		move.b	(a0)+,d1	;higher Byte
		lsl.l	#8,d1
		or.w	d1,d0
		move.w	d0,APG_imageWidth(a5)

		moveq	#0,d0
		move.b	(a0)+,d0	;Imagebreite
		move.b	(a0)+,d1	;higher Byte
		lsl.l	#8,d1
		or.w	d1,d0
		move.w	d0,APG_imageHeight(a5)
		
		lea	GIF_loadmsg(pc),a1
		jsr	APR_InitProcess(a5)

		sf	GIF_localcolor
		moveq	#0,d0
		move.b	(a0)+,d0
		move.l	d0,d1
		and.b	#$80,d0		;Teste ob Globale Colormap
		beq	.global		;es wird eine Locale Colormap benutzt
		st	GIF_localcolor
.global:		
		sf	GIF_interlaced(a5)
		move.l	d1,d0
		and.b	#$70,d0	    ;Test ob Bitmap Sequential oder Interlace
		beq	.sequential
		st	GIF_interlaced(a5)
		sf	GIF_group(a5)
.sequential:
		sf	GIF_localbitperpixel
		move.l	d1,d0
		and.b	#7,d0
		addq.b	#1,d0
		st	GIF_localbitperpixel

		tst.b	GIF_localcolor		;locale colortab ?
		beq	.nolocalcolor

		moveq	#0,d0
		move.b	GIF_bitsperpixel(pc),d0

		bsr	readcolortab

.nolocalcolor:
		moveq	#0,d0
		move.b	(a0)+,d0
		move.w	d0,GIF_firstcodesize(a5)
		move.w	d0,GIF_codesize(a5)
		moveq	#1,d1
		lsl.w	d0,d1
		move.w	d1,GIF_clearcode(a5)

		move.w	GIF_codesize(a5),d0
		addq.w	#1,d0
		moveq	#1,d2
		lsl.w	d0,d2
		subq.w	#1,d2
		move.w	d2,GIF_limitcode(a5)

		addq.w	#1,d1
		move.w	d1,GIF_endcode(a5)
		
		moveq	#0,d0
		move.b	(a0)+,d0
		move.w	d0,GIF_blocksize(a5)

;erstelle bitmap

		movem.l	d0-a6,-(a7)

		move.l	APG_FindMonitor(a5),a6
		jsr	(a6)
		
		lea	APG_PicMap(a5),a0	
		moveq	#0,d0
		move.b	APG_planes(a5),d0
		moveq	#0,d1
		moveq	#0,d2
		move.w	APG_ImageWidth(a5),d1
		move.w	APG_ImageHeight(a5),d2	;h�he
		move.l	APG_Gfxbase(a5),a6
		jsr	_LVOinitbitmap(a6)

		moveq	#0,d0
		move.w	APG_ImageWidth(a5),d0
		add.w	#15,d0
		lsr.w	#4,d0
		lsl.w	#1,d0
		move.l	d0,APG_ByteWidth(a5)
		move.w	APG_ImageHeight(a5),d1
		mulu	d1,d0
		move.l	d0,APG_Oneplanesize(a5)

		moveq	#0,d1
		moveq	#0,d7
		move.b	APG_Planes(a5),d7	
		subq.w	#1,d7
.next:
		add.l	d0,d1		;Simuliert mulu.l Befehl
		dbra	d7,.next

		move.l	d1,d0
		move.l	#MEMF_ANY+MEMF_CLEAR,d1
		move.l	4.w,a6
		jsr	_LVOAllocVec(a6)
		move.l	d0,APG_Picmem(a5)
		bne	.memok
		move.l	GIF_puffermem(pc),a1
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)
		moveq	#-4,d3
		rts
.memok:
		move.l	APG_Picmem(a5),d0
		lea	APG_Picmap+8(a5),a0
		moveq	#0,d7
		move.b	APG_Planes(a5),d7
		subq.w	#1,d7
.np		move.l	d0,(a0)+
		add.l	APG_Oneplanesize(a5),d0
		dbra	d7,.np

		moveq	#0,d0		;Speicher f�r eine Zeile besorgen
		move.w	APG_ImageWidth(a5),d0
		add.w	#15,d0
		lsr.l	#4,d0
		lsl.l	#4,d0
		move.l	#MEMF_ANY+MEMF_CLEAR,d1
		jsr	_LVOAllocVec(a6)
		move.l	d0,GIF_Linemem(a5)


;Init Kotentabelle

		lea	knotentab,a0		;die ersten 256 Knoten
		move.l	#255,d7			;haben auch die Werte
		moveq	#0,d0			;0-255
newknoten:
		addq.l	#4,a0
		move.l	d0,(a0)+
		addq.l	#1,d0
		dbra	d7,newknoten


		movem.l	(a7)+,d0-a6

;------------------- Beginn decodierung ------------------------

zeichen		=	4
oldcode		equr	d7
code		equr	d3
lastcode	equr	d4

		move.l	GIF_linemem(a5),a6	;dort wird bild hineinentpackt
		lea	knotentab,a4

		move.w	APG_imagewidth(a5),GIF_line(a5)
		clr.w	GIF_ystart(a5)
		
		moveq	#0,d0
		move.w	GIF_codesize(a5),d0
		moveq	#1,lastcode
		lsl.w	d0,lastcode
		addq.w	#1,lastcode

		move.w	lastcode,GIF_merklastcode(a5)
		move.w	lastcode,d0
		subq.w	#2,d0
		move.w	d0,GIF_firstmaxcode(a5)
		
;		move.l	#257,lastcode

		moveq	#1,d5		;Bytemaske init
		move.b	(a0)+,d6
		sub.w	#1,GIF_blocksize(a5)

		bsr	readbits	;�berlese clearcode

outerloop:
		bsr	readbits
		move.b	code,(a6)+
		sub.w	#1,GIF_line(a5)
		bne	.ok
		bsr	GIF_copyline
.ok:

		move.w	code,oldcode	;altercode
loop:
		bsr	readbits	;begin der schleife

		cmp.w	GIF_endcode(a5),code
		beq	wech
		
		cmp.w	GIF_clearcode(a5),code
		beq	clear

		cmp.w	code,lastcode	;Teste ob Code schon existiert
		bhs	codeexits

		lea	stack,a3	;nein
		moveq	#0,d0
		move.w	oldcode,d0
		add.l	d0,d0
		add.l	d0,d0
		add.l	d0,d0
		
		move.l	zeichen(a4,d0.l),d2
		move.b	d2,(a3)+
		move.l	(a4,d0.l),a4
		cmp.l	#0,a4
		beq	lastchar
next1:		
		move.l	zeichen(a4),d2
		move.b	d2,(a3)+
		move.l	(a4),a4
		cmp.l	#0,a4
		bne	next1
lastchar:
		lea	stack,a2
		move.l	a3,d1
		sub.l	a2,d1
		subq.l	#1,d1
copy1:
		move.b	-(a3),(a6)+	;zeichen werden in richtiger
					;reihenfolge kopiert
		sub.w	#1,GIF_line(a5)
		bne	.ok
		bsr	GIF_copyline
.ok:
		dbra	d1,copy1
		move.b	d2,(a6)+	;erstes zeichen wieder anh�ngen
		sub.w	#1,GIF_line(a5)
		bne	.ok1
		bsr	GIF_copyline
.ok1:
;------------ Trage neuen Code in Tabelle ein
		
		moveq	#0,d0
		move.w	oldcode,d0
		add.l	d0,d0
		add.l	d0,d0
		add.l	d0,d0
		lea	knotentab,a4
		lea	(a4,d0.l),a1
		addq.w	#1,lastcode
		moveq	#0,d0
		move.w	lastcode,d0
		add.l	d0,d0
		add.l	d0,d0
		add.l	d0,d0
		move.l	a1,(a4,d0.l)
		move.l	d2,zeichen(a4,d0.l)	
		move.w	code,oldcode

		cmp.w	GIF_limitcode(a5),lastcode
		bne	.w
		cmp.w	#$fff,lastcode
		beq	loop
		addq.w	#1,GIF_codesize(a5)
		move.w	GIF_codesize(a5),d2
		addq.w	#1,d2
		moveq	#1,d1
		lsl.w	d2,d1
		subq.w	#1,d1
		move.w	d1,GIF_limitcode(a5)
		bra	loop
.w
		bra	loop

codeexits:
		cmp.w	GIF_firstmaxcode(a5),code
		bhi	groesser
		move.b	code,(a6)+		;nur ein single code
		sub.w	#1,GIF_line(a5)
		bne	.ok
		bsr	GIF_copyline
.ok:
		moveq	#0,d2
		move.w	code,d2
		bra	addstring
		
groesser:
		lea	stack,a3
		moveq	#0,d1
		move.w	code,d1
		add.l	d1,d1
		add.l	d1,d1
		add.l	d1,d1
		move.l	zeichen(a4,d1.l),d2
		move.b	d2,(a3)+
		move.l	(a4,d1.l),a4
		cmp.l	#0,a4
		beq	endchar
next:
		move.l	zeichen(a4),d2
		move.b	d2,(a3)+
		move.l	(a4),a4
		cmp.l	#0,a4
		bne	next
endchar:
		lea	stack,a2
		move.l	a3,d1
		sub.l	a2,d1
		subq.l	#1,d1
copy:
		move.b	-(a3),(a6)+
		sub.w	#1,GIF_line(a5)
		bne	.ok
		bsr	GIF_copyline
.ok:
		dbra	d1,copy		

;------- Addiere Zeichen zur Stringtab -------------
addstring:
		moveq	#0,d0
		move.w	oldcode,d0
		add.l	d0,d0
		add.l	d0,d0
		add.l	d0,d0
		lea	knotentab,a4
		lea	(a4,d0.l),a1
		addq.w	#1,lastcode
		moveq	#0,d0
		move.w	lastcode,d0
		add.l	d0,d0
		add.l	d0,d0
		add.l	d0,d0
		move.l	a1,(a4,d0.l)
		move.l	d2,zeichen(a4,d0.l)	
		move.w	code,oldcode

		cmp.w	GIF_limitcode(a5),lastcode
		bne	.w
		cmp.w	#$fff,lastcode
		beq	loop
		addq.w	#1,GIF_codesize(a5)
		move.w	GIF_codesize(a5),d2
		addq.w	#1,d2
		moveq	#1,d1
		lsl.w	d2,d1
		subq.w	#1,d1
		move.w	d1,GIF_limitcode(a5)
		bra	loop
.w
		bra	loop
weiter:
		bra	loop

clear:
		move.w	GIF_merklastcode(a5),lastcode
		move.w	GIF_firstcodesize(a5),GIF_codesize(a5)
		move.w	GIF_codesize(a5),d2
		addq.w	#1,d2
		moveq	#1,d1
		lsl.w	d2,d1
		subq.w	#1,d1
		move.w	d1,GIF_limitcode(a5)

		bra	outerloop

;--------------------------------------------------------------------

wech:
		move.w	#1,APG_LoadFlag(a5)
		
		move.l	4,a6
		move.l	GIF_linemem(a5),a1
		cmp.l	#0,a1
		beq	.w
		jsr	_LVOFreeVec(a6)
.w		
		move.l	GIF_puffermem(a5),a1
		cmp.l	#0,a1
		beq	raushier
		jsr	_LVOFreeVec(a6)		

raushier:
		jsr	APR_ClearProcess(a5)
		moveq	#0,d3
		rts
GIF_wech:
		jsr	APR_ClearProcess(a5)

		move.l	GIF_puffermem(a5),a1
		move.l	4.w,a6
		jsr	_LVOFreeVec(a6)

		lea	GIF_errormsg1(pc),a4
		jsr	APG_Status(a5)
		
		moveq	#APLE_ERROR,d3
		rts		

;---------------- Kopiere Line in Bitmap ------------------------
GIF_copyline:
		movem.l	d0-a6,-(a7)

		jsr	APR_DoProcess(a5)

		move.w	APG_imageheight(a5),d0
		move.w	GIF_ystart(a5),d1
		cmp.w	d0,d1
		bhi	.noline

		lea	GIF_pentab(pc),a0
		move.l	a0,a2
		moveq	#0,d0
		move.l	#256-1,d7
.pen		move.b	d0,(a0)+
		addq.l	#1,d0
		dbra	d7,.pen

		move.l	a2,a0
		moveq	#0,d0
		move.b	APG_Planes(a5),d0
		move.l	APG_Renderbase(a5),a6
		jsr	_LVOInitPentab(a6)

		move.l	GIF_Linemem(a5),a0
		lea	APG_PicMap(a5),a1
		move.l	APG_ByteWidth(a5),d0
		lsl.l	#3,d0
		moveq	#0,d1
		moveq	#0,d2
		move.l	d0,d3
		moveq	#1,d4		;eine Zeile
		moveq	#0,d5
		move.w	GIF_ystart(a5),d6	;yStart
		jsr	_LVOChunky2Bitmap(a6)
.noline

		movem.l	(a7)+,d0-a6

		tst.b	GIF_interlaced(a5)
		beq	nointer

		move.b	GIF_group(a5),d0
		tst.b	d0
		beq	makefirstgroup		

		cmp.b	#1,d0
		beq	makesecondgroup

		cmp.b	#2,d0
		beq	makethirdgroup

makefourthgroup:
		addq.w	#2,GIF_ystart(a5)
		move.w	GIF_ystart(a5),d0
		cmp.w	APG_imageHeight(a5),d0
		blo	go
		bra	go

makefirstgroup
		addq.w	#8,GIF_ystart(a5)
		move.w	GIF_ystart(a5),d0
		cmp.w	APG_imageHeight(a5),d0
		blo	go
		add.b	#1,GIF_group(a5)
		move.w	#4,GIF_ystart(a5)
		bra	go

makesecondgroup
		addq.w	#8,GIF_ystart(a5)
		move.w	GIF_ystart(a5),d0
		cmp.w	APG_imageHeight(a5),d0
		blo	go
		add.b	#1,GIF_group(a5)
		move.w	#2,GIF_ystart(a5)
		bra	go
makethirdgroup:
		addq.w	#4,GIF_ystart(a5)
		move.w	GIF_ystart(a5),d0
		cmp.w	APG_imageHeight(a5),d0
		blo	go
		add.b	#1,GIF_group(a5)
		move.w	#1,GIF_ystart(a5)
		bra	go

nointer:	addq.w	#1,GIF_ystart(a5)

go
		move.w	APG_imagewidth(a5),GIF_line(a5)
		move.l	GIF_Linemem(a5),a6	;dort wird bild hineinentpackt
		rts

planetab:	ds.l	8


;d2=code_bits a0=File d3=returnwert
readbits:
		move.w	GIF_codesize(a5),d2
		moveq	#1,d1

		moveq	#0,code	;returnwert
.nextbit:
		move.w	d5,d0
		and.w	d6,d0	;teste Bit
		beq	.nobit
		or.w	d1,code	;setze bit
.nobit:
		add.b	D5,d5
		bne	.byteok

		moveq	#1,d5
		move.b	(a0)+,d6
		sub.w	#1,GIF_blocksize(a5)
		bpl	.byteok
		and.w	#$00ff,d6
		move.w	d6,GIF_blocksize(a5)
		move.b	(a0)+,d6
		sub.w	#1,GIF_blocksize(a5)
.byteok:
		add.w	d1,d1

		dbra	d2,.nextbit

		rts
		
;--------------------------------------------------------------------
;d0=depth

readcolortab:

		moveq	#1,d7
		lsl.l	d0,d7		
		move.w	d7,APG_ColorCount(a5)	;anzahl der Farben

		move.w	APG_ColorCount(a5),d7
		subq.w	#1,d7
		lea	APG_Farbtab8(a5),a1
		lea	APG_Farbtab4(a5),a2
		move.w	APG_ColorCount(a5),(a1)+
		clr.w	(a1)+
.nc:		moveq	#0,d0
		moveq	#0,d1
		moveq	#0,d2
		move.b	(a0)+,d0	;R
		move.b	(a0)+,d1	;G
		move.b	(a0)+,d2	;B
	
		move.l	d0,d3
		lsl.l	#4,d3
		lsl.l	#8,d0
		swap	d0
		move.l	d0,(a1)+
		move.b	d1,d3
		lsl.l	#8,d1
		swap	d1
		move.l	d1,(a1)+
		move.l	d2,d1
		lsr.b	#4,d1
		and.w	#$ff0,d3
		or.b	d1,d3
		move.w	d3,(a2)+
		lsl.l	#8,d2
		swap	d2
		move.l	d2,(a1)+
		dbra	d7,.nc
		clr.l	(a1)+

		rts

;---------------------------------------------------------------------

GIF_errormsg1:
		dc.b	'Error in file or unknown GIF-format',0

GIF_loadmsg:	dc.b	'Reading GIF Image!',0		

GIF_pentab:	ds.b	256

		section	datas,BSS
knotentab:
		ds.b	4095*8

stack:		ds.b	4096
bsslen		=	*-knotentab

