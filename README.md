# ArtPro

**Description**: An easy to use gfx-converter program

  - **Technology stack**: Assembler 68k
  - **Status**:  [CHANGELOG](https://gitlab.com/amigasourcecodepreservation/artpro/blob/master/CHANGELOG.md).

**Screenshot**:

![TO-DO](TO-DO)


## Dependencies

* Amiga OS Classic
* [GuiGfx lib](http://aminet.net/package/dev/misc/guigfxlib)
* [Render lib](http://aminet.net/package/dev/misc/renderlib)
* [Cybergraphics Dev Kit](http://aminet.net/package/dev/misc/CGraphX-DevKit)

## Installation

To-Do


## Configuration

Orignally built with Thrash'em one. 

## Usage

TO-DO

## How to test the software

TO-DO

## Known issues

The artpro.i include is missing. If you manage to rebuild it, please add it to this project. 

## Getting help

If you have questions, concerns, bug reports, etc, please file an issue in this repository's Issue Tracker.

## Getting involved

Contact your old amiga friends and tell them about our project, and ask them to dig out their source code or floppies and send them to us for preservation.

Clean up our hosted archives, and make the source code buildable with standard compilers like devpac, asmone, gcc 2.9x/Beppo 6.x, sas/c ,vbcc and friends.


Cheers!

Twitter
https://twitter.com/AmigaSourcePres

Gitlab
https://gitlab.com/amigasourcecodepreservation/

WWW
https://amigasourcepres.gitlab.io/

     _____ ___   _   __  __     _   __  __ ___ ___   _   
    |_   _| __| /_\ |  \/  |   /_\ |  \/  |_ _/ __| /_\  
      | | | _| / _ \| |\/| |  / _ \| |\/| || | (_ |/ _ \ 
     _|_| |___/_/ \_\_|_ |_|_/_/_\_\_|__|_|___\___/_/_\_\
    / __|/ _ \| | | | _ \/ __| __|  / __/ _ \|   \| __|  
    \__ \ (_) | |_| |   / (__| _|  | (_| (_) | |) | _|   
    |___/\___/_\___/|_|_\\___|___|__\___\___/|___/|___|_ 
    | _ \ _ \ __/ __| __| _ \ \ / /_\_   _|_ _/ _ \| \| |
    |  _/   / _|\__ \ _||   /\ V / _ \| |  | | (_) | .` |
    |_| |_|_\___|___/___|_|_\ \_/_/ \_\_| |___\___/|_|\_|

----

## Open source licensing info

ArtPro is distributed under the terms of the GNU General Public License, version 2 or later. See the [LICENSE](https://gitlab.com/amigasourcecodepreservation/artpro/LICENSE.md) file for details.

----

## Credits and references

Many thanks to Frank Pagels for releasing the source code under GPL.

